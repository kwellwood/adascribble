--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Scribble.Token_Types is

    type Token_Type is
    (
        TK_CONSTANT,        -- "constant"
        TK_FUNCTION,        -- "function"
        TK_DEFAULT,         -- "default" (reserved)
        TK_INCLUDE,         -- "include" (reserved)
        TK_REQUIRE,         -- "require" (reserved)
        TK_MEMBER,          -- "member"
        TK_RETURN,          -- "return"
        TK_BLOCK,           -- "block"
        TK_ELSIF,           -- "elsif"
        TK_EVERY,           -- "every" (reserved)
        TK_LOCAL,           -- "local"
        TK_TIMER,           -- "timer" (reserved)
        TK_WHILE,           -- "while"
        TK_YIELD,           -- "yield"
        TK_ELSE,            -- "else"
        TK_EXIT,            -- "exit"
        TK_LOOP,            -- "loop"
        TK_SELF,            -- "self"
        TK_STEP,            -- "step"
        TK_THEN,            -- "then"
        TK_THIS,            -- "this"
        TK_WHEN,            -- "when"
        TK_AND,             -- "and"
        TK_END,             -- "end"
        TK_FOR,             -- "for"
        TK_XOR,             -- "xor"
        TK_IF,              -- "if"
        TK_IN,              -- "in"
        TK_IS,              -- "is"
        TK_OF,              -- "of"
        TK_OR,              -- "or"
        TK_TO,              -- "to"
        TK_COLON_EQUALS,    -- ":="
        TK_PLUS_EQUALS,     -- "+="
        TK_MINUS_EQUALS,    -- "-="
        TK_STAR_EQUALS,     -- "*="
        TK_SLASH_EQUALS,    -- "/="
        TK_AMP_EQUALS,      -- "&="
        TK_GREATER_EQUALS,  -- ">="
        TK_LESS_EQUALS,     -- "<="
        TK_BANG_EQUALS,     -- "!="
        TK_DOUBLE_LESS,     -- "<<"
        TK_DOUBLE_GREATER,  -- ">>"
        TK_DOUBLE_COLON,    -- "::"
        TK_GREATER,         -- ">"
        TK_LESS,            -- "<"
        TK_EQUALS,          -- "="
        TK_MINUS,           -- "-"
        TK_BANG,            -- "!"
        TK_STAR,            -- "*"
        TK_SLASH,           -- "/"
        TK_PERCENT,         -- "%"
        TK_PLUS,            -- "+"
        TK_AMP,             -- "&"
        TK_AT,              -- "@"
        TK_CARAT,           -- "^"
        TK_LPAREN,          -- "("
        TK_RPAREN,          -- ")"
        TK_LBRACE,          -- "{"
        TK_RBRACE,          -- "}"
        TK_LBRACKET,        -- "["
        TK_RBRACKET,        -- "]"

        TK_QUESTION,        -- "?"
        TK_COLON,           -- ":"
        TK_SEMICOLON,       -- ";"
        TK_COMMA,           -- ","
        TK_DOT,             -- "."

        TK_VALUE,           -- any literal value
        TK_SYMBOL,          -- any symbol

        TK_EOL,             -- ASCII line feed

        TK_EOF              -- end of file
    );

    -- Returns the syntactic representation of the token type. For token types
    -- that don't have a syntactic representation (e.g. TK_VALUE, TK_SYMBOL,
    -- TK_EOF, TK_COUNT, etc.), an empty string will be returned.
    function Image( t : Token_Type ) return String;

    -- Returns the name of the Token_Type value in readable form. Used for error
    -- messages.
    function Name( t : Token_Type ) return String;

    -- Returns the syntactic representation of the token type. For token types
    -- that don't have a syntactic representation (e.g. TK_VALUE, TK_SYMBOL, etc.)
    -- an empty string will be returned.
    procedure To_Token_Type( img : String; t : out Token_Type; valid : out Boolean );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns true if 'c' is a delimiter character, otherwise false.
    function Is_Delimiter( c : Character ) return Boolean;

end Scribble.Token_Types;
