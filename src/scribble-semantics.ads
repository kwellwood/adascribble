--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Runtimes;                 use Scribble.Runtimes;

--
-- Semantic rules enforced by Semantic_Analyzer:
--
-- 1.  The left side of an Assign must be a Symbol, but there may be 'n' number
--     of Index operators between the Symbol and the Assign.
-- 2.  No duplicate parameter names allowed within a function definition.
-- 3.  No duplicate variables or variables matching parameters within a scope.
--     - Variables may obscure a matching name in a parent scope.
-- 4.  All 'var' statements must occur at the top of their code block.
-- 5.  No more than 256 combined parameters and local variables are allowed on
--     the stack within a single function definition (sibling scopes share
--     variable space, but not nested scopes).
-- 6.  Exit statements can only exist inside loop statements.
-- 7.  All function definition parameters following a parameter with a default
--     value must also have default values specified.
-- 8.  Default values for function definition parameters must be literal values.
-- 9.  Every loop must contain a breaking statement (exit or return).
-- 10. Literal values for map keys must be strings.
-- 11. Parameters/variables may not be referenced before (or without) their
--     definition.
-- 12. Literals or the result of unary or binary operators can't be invoked as
--     a function.
-- 13. An undefined symbol, without a namespace, invoked as a function, must
--     reference a function exported by the runtime.
-- 14. Assignment to a constant is not allowed.
-- 15. Symbols referencing undefined namespaces are not allowed.
-- 16. Parameter names can't match exported function names in the runtime.
-- 17. Variable names can't match exported function names in the runtime.
-- 18. Membership operator cannot operate on any literal other than a map.
--     Operating on expressions is fine.
-- 19. Cannot assign to names in the anonymous namespace matching exported functions.
-- 20. Cannot read names from the anonymous namespace matching exported functions.
-- 21. Literal values for namespace name expressions must be strings.
-- 22. The Reference operator may only be used at the top level of an argument
--     expression in a function call.
-- 23. The right side of a Reference operator must a Symbol, but there may be
--     'n' number of Index operators between the Reference and the Symbol. (The
--     right side of a Reference operator has the same rules as the left side of
--     an Assign.)
-- 24. References to 'this' may only be used directly within member function
--     bodies (not in default argument value expressions or in nested function
--     definitions).
--
--     Add numbered rules to the bottom of this list as they are implemented.
--
private package Scribble.Semantics is

    type Semantic_Analyzer is limited new Ast_Processor with private;
    type A_Semantic_Analyzer is access all Semantic_Analyzer'Class;

    -- Creates a new instance of the Scribble semantic analyzer. The Scribble
    -- runtime provides information about the existence of directly exported
    -- Ada functions.
    function Create_Semantic_Analyzer( runtime : not null A_Scribble_Runtime ) return A_Semantic_Analyzer;
    pragma Postcondition( Create_Semantic_Analyzer'Result /= null );

    -- Analyzes AST 'value' for conformance with Scribble semantic rules and
    -- builds variable definition tables in the given AST. 'val' is expected to
    -- be a parsed Value term, meaning the root object can be an Ast_Literal,
    -- Ast_List, Ast_Map, or Ast_Function_Definition. If the AST does not
    -- conform to all semantic rules of the language, a Parse_Error will be
    -- raised.
    procedure Analyze( this  : not null access Semantic_Analyzer'Class;
                       value : not null A_Ast_Expression );
    pragma Precondition( value.Get_Type in Ast_Value_Type );

    procedure Delete( this : in out A_Semantic_Analyzer );
    pragma Postcondition( this = null );

private

    package Boolean_Vectors is new Ada.Containers.Vectors( Positive, Boolean, "=" );

    type Semantic_Analyzer is limited new Ast_Processor with
        record
            runtime    : A_Scribble_Runtime := null;
            child      : A_Semantic_Analyzer := null;       -- for analyzing nested function definitions

            exitFound  : Boolean_Vectors.Vector;
            varAllowed : Boolean := False;                  -- is a var statement allowed here?
            func       : A_Ast_Function_Definition := null; -- containing function definition
            scope      : A_Ast_Block := null;               -- current var scope (not owned)
        end record;

    -- Returns True if 'fid' is an exported (automatically visible) function
    -- defined in the runtime.
    function Is_Exported_Function( this : not null access Semantic_Analyzer'Class;
                                   fid  : Function_Id ) return Boolean;

    -- expressions: operands
    procedure Process_Literal( this : access Semantic_Analyzer; node : A_Ast_Literal ) is null;
    procedure Process_Map( this : access Semantic_Analyzer; node : A_Ast_Map );
    procedure Process_List( this : access Semantic_Analyzer; node : A_Ast_List );
    procedure Process_Function_Call( this : access Semantic_Analyzer; node : A_Ast_Function_Call );
    procedure Process_Member_Call( this : access Semantic_Analyzer; node : A_Ast_Member_Call );
    procedure Process_Self( this : access Semantic_Analyzer; node : A_Ast_Self ) is null;
    procedure Process_This( this : access Semantic_Analyzer; node : A_Ast_This );
    procedure Process_Builtin( this : access Semantic_Analyzer; node : A_Ast_Builtin );
    procedure Process_Identifier( this : access Semantic_Analyzer; node : A_Ast_Identifier );
    procedure Process_Membership( this : access Semantic_Analyzer; node : A_Ast_Membership );
    procedure Process_Name_Expr( this : access Semantic_Analyzer; node : A_Ast_Name_Expr );

    -- expressions: operators
    procedure Process_Unary_Op( this : access Semantic_Analyzer; node : A_Ast_Unary_Op );
    procedure Process_Reference( this : access Semantic_Analyzer; node : A_Ast_Reference );
    procedure Process_Binary_Op( this : access Semantic_Analyzer; node : A_Ast_Binary_Op );
    procedure Process_Assign( this : access Semantic_Analyzer; node : A_Ast_Assign );
    procedure Process_Index( this : access Semantic_Analyzer; node : A_Ast_Index );
    procedure Process_Conditional( this : access Semantic_Analyzer; node : A_Ast_Conditional );

    -- expressions: function definition
    procedure Process_Function_Definition( this : access Semantic_Analyzer; node : A_Ast_Function_Definition );

    -- statements
    procedure Process_Block( this : access Semantic_Analyzer; node : A_Ast_Block );
    procedure Process_Exit( this : access Semantic_Analyzer; node : A_Ast_Exit );
    procedure Process_Expression_Statement( this : access Semantic_Analyzer; node : A_Ast_Expression_Statement );
    procedure Process_If( this : access Semantic_Analyzer; node : A_Ast_If );
    procedure Process_Loop( this : access Semantic_Analyzer; node : A_Ast_Loop );
    procedure Process_Return( this : access Semantic_Analyzer; node : A_Ast_Return );
    procedure Process_Var( this : access Semantic_Analyzer; node : A_Ast_Var );
    procedure Process_Yield( this : access Semantic_Analyzer; node : A_Ast_Yield ) is null;

end Scribble.Semantics;
