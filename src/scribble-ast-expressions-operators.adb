--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Values;                            use Values;
with Values.Construction;               use Values.Construction;
with Values.Operations.Generics;        use Values.Operations.Generics;

package body Scribble.Ast.Expressions.Operators is

    function Create_Unary_Op( op    : Unary_Type;
                              loc   : Token_Location;
                              right : not null A_Ast_Expression ) return A_Ast_Expression is
        this : A_Ast_Unary_Op;
    begin
        case op is
            when UNARY_NOT  =>
                this := new Ast_Unary_Op;
                this.Construct( loc, right, Values.Operations.Generics.Unary_Not.Operate'Access,    "!",    inst => BNOT );

            when UNARY_NEG =>
                this := new Ast_Unary_Op;
                this.Construct( loc, right, Values.Operations.Generics.Unary_Negate.Operate'Access, "-",    inst => NEG );

            when UNARY_SIGN =>
                this := new Ast_Unary_Op;
                this.Construct( loc, right, Values.Operations.Generics.Unary_Sign.Operate'Access,   "sign", inst => SIGN );

            when UNARY_REF =>
                this := Create_Reference( loc, right );

        end case;

        return A_Ast_Expression(this);
    end Create_Unary_Op;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Ast_Unary_Op;
                         loc      : Token_Location;
                         right    : not null A_Ast_Expression;
                         pruner   : A_Unary_Function;
                         funcName : String;
                         inst     : Instruction := NOOP;
                         fid      : Function_Id := Invalid_FID ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.right := right;
        this.right.Set_Parent( A_Ast_Expression(this) );
        this.pruner := pruner;
        this.funcName := To_Unbounded_String( funcName );
        this.inst := inst;
        this.fid := fid;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Unary_Op ) is
    begin
        Delete( A_Ast_Node(this.right) );
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Function_Id( this : not null access Ast_Unary_Op'Class ) return Function_Id is (this.fid);

    ----------------------------------------------------------------------------

    function Get_Function_Name( this : not null access Ast_Unary_Op'Class ) return String is (To_String( this.funcName ));

    ----------------------------------------------------------------------------

    function Get_Instruction( this : not null access Ast_Unary_Op'Class ) return Instruction is (this.inst);

    ----------------------------------------------------------------------------

    overriding
    function Get_Right( this : access Ast_Unary_Op ) return A_Ast_Expression is (this.right);

    ----------------------------------------------------------------------------

    procedure Process( this      : access Ast_Unary_Op;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Unary_Op( A_Ast_Unary_Op(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Unary_Op ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        -- prune the child first
        pruned := this.right.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.right) );
            this.right := pruned;
        end if;

        -- then prune self
        if this.right.Get_Type = LITERAL and then this.pruner /= null then
            pruned := Create_Literal( this.Location, this.pruner.all( A_Ast_Literal(this.right).Get_Value ) );
            return pruned;
        end if;
        return null;
    end Prune;

    --==========================================================================

    function Create_Reference( loc        : Token_Location;
                               right      : not null A_Ast_Expression;
                               knownValid : Boolean := False ) return A_Ast_Unary_Op is
        this : constant A_Ast_Reference := new Ast_Reference;
    begin
        this.Construct( loc, right, null, "@" );
        if knownValid then
            this.Mark_Valid;
        end if;
        return A_Ast_Unary_Op(this);
    end Create_Reference;

    ----------------------------------------------------------------------------

    function Is_Valid( this : not null access Ast_Reference'Class ) return Boolean is (this.valid);

    ----------------------------------------------------------------------------

    procedure Mark_Valid( this : not null access Ast_Reference'Class ) is
    begin
        this.valid := True;
    end Mark_Valid;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Reference;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Reference( A_Ast_Reference(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Reference ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        pruned := this.right.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.right) );
            this.right := pruned;
        end if;
        return null;
    end Prune;

    --==========================================================================

    function Prune_Eq( left, right : Value'Class ) return Value
    is (Create( Value(left) = Value(right) ));

    function Prune_Neq( left, right : Value'Class ) return Value
    is (Create( Value(left) /= Value(right) ));

    function Prune_Gt( left, right : Value'Class ) return Value
    is (Create( Value(left) > Value(right) ));

    function Prune_Gte( left, right : Value'Class ) return Value
    is (Create( Value(left) >= Value(right) ));

    function Prune_Lt( left, right : Value'Class ) return Value
    is (Create( Value(left) < Value(right) ));

    function Prune_Lte( left, right : Value'Class ) return Value
    is (Create( Value(left) <= Value(right) ));

    ----------------------------------------------------------------------------

    function Get_Precedence( op : Binary_Type ) return Positive is
    (
        case op is
            when BINARY_AND
               | BINARY_OR
               | BINARY_XOR
               | BINARY_IN
               => 1,

            when BINARY_EQ
               | BINARY_GT
               | BINARY_GTE
               | BINARY_LT
               | BINARY_LTE
               | BINARY_NEQ
               => 2,

            when BINARY_ADD
               | BINARY_SUB
               | BINARY_CAT
               => 3,

            when BINARY_MUL
               | BINARY_DIV
               | BINARY_MOD
               => 4,

            when BINARY_POW
               => 5
    );

    ----------------------------------------------------------------------------

    function Create_Binary_Op( op    : Binary_Type;
                               loc   : Token_Location;
                               left  : not null A_Ast_Expression;
                               right : not null A_Ast_Expression ) return A_Ast_Expression is
        this : constant A_Ast_Binary_Op := new Ast_Binary_Op;
    begin
        case op is
            when BINARY_AND => this.Construct( loc, left, right, Values.Operations.Generics.Binary_And.Operate'Access,      "and", inst => BAND );
            when BINARY_OR  => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Or.Operate'Access,       "or",  inst => BOR);
            when BINARY_XOR => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Xor.Operate'Access,      "xor", inst => BXOR );
            when BINARY_IN  => this.Construct( loc, left, right, Values.Operations.Generics.Binary_In.Operate'Access,       "in",  fid => Fid_Hash( "$in") );
            when BINARY_EQ  => this.Construct( loc, left, right, Prune_Eq'Access,                                           "=",   inst => EQ );
            when BINARY_GT  => this.Construct( loc, left, right, Prune_Gt'Access,                                           ">",   inst => GT );
            when BINARY_GTE => this.Construct( loc, left, right, Prune_Gte'Access,                                          ">=",  inst => GTE );
            when BINARY_LT  => this.Construct( loc, left, right, Prune_Lt'Access,                                           "<",   inst => LT );
            when BINARY_LTE => this.Construct( loc, left, right, Prune_Lte'Access,                                          "<=",  inst => LTE );
            when BINARY_NEQ => this.Construct( loc, left, right, Prune_Neq'Access,                                          "!=",  inst => NEQ );
            when BINARY_ADD => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Add.Operate'Access,      "+",   inst => ADD );
            when BINARY_SUB => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Subtract.Operate'Access, "-",   inst => SUB );
            when BINARY_CAT => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Concat.Operate'Access,   "&",   inst => CONCAT );
            when BINARY_MUL => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Multiply.Operate'Access, "*",   inst => MUL );
            when BINARY_DIV => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Divide.Operate'Access,   "/",   inst => DIV );
            when BINARY_MOD => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Modulo.Operate'Access,   "%",   inst => MODULO );
            when BINARY_POW => this.Construct( loc, left, right, Values.Operations.Generics.Binary_Power.Operate'Access,    "^",   inst => POW );
        end case;

        return A_Ast_Expression(this);
    end Create_Binary_Op;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Ast_Binary_Op;
                         loc      : Token_Location;
                         left     : not null A_Ast_Expression;
                         right    : not null A_Ast_Expression;
                         pruner   : A_Binary_Function;
                         funcName : String;
                         inst     : Instruction := NOOP;
                         fid      : Function_Id := Invalid_FID ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.left := left;
        this.left.Set_Parent( A_Ast_Expression(this) );
        this.right := right;
        this.right.Set_Parent( A_Ast_Expression(this) );
        this.pruner := pruner;
        this.funcName := To_Unbounded_String( funcName );
        this.inst := inst;
        this.fid := fid;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Binary_Op ) is
    begin
        Delete( A_Ast_Node(this.left) );
        Delete( A_Ast_Node(this.right) );
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Function_Id( this : not null access Ast_Binary_Op'Class ) return Function_Id is (this.fid);

    ----------------------------------------------------------------------------

    function Get_Function_Name( this : not null access Ast_Binary_Op'Class ) return String is (To_String( this.funcName ));

    ----------------------------------------------------------------------------

    function Get_Instruction( this : not null access Ast_Binary_Op'Class ) return Instruction is (this.inst);

    ----------------------------------------------------------------------------

    overriding
    function Get_Left( this : access Ast_Binary_Op ) return A_Ast_Expression is (this.left);

    ----------------------------------------------------------------------------

    overriding
    function Get_Right( this : access Ast_Binary_Op ) return A_Ast_Expression is (this.right);

    ----------------------------------------------------------------------------

    procedure Process( this      : access Ast_Binary_Op;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Binary_Op( A_Ast_Binary_Op(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Binary_Op ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        -- prune the children first
        pruned := this.left.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.left) );
            this.left := pruned;
        end if;
        pruned := this.right.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.right) );
            this.right := pruned;
        end if;

        -- then prune self
        if this.left.Get_Type = LITERAL and then
           this.right.Get_Type = LITERAL and then
           this.pruner /= null
        then
            pruned := new Ast_Literal;
            A_Ast_Literal(pruned).Construct( this.Location,
                                             this.pruner.all( A_Ast_Literal(this.left).Get_Value,
                                                              A_Ast_Literal(this.right).Get_Value ) );
            return pruned;
        end if;
        return null;
    end Prune;

    --==========================================================================

    function Create_Assign( op    : Assign_Type;
                            loc   : Token_Location;
                            left  : not null access Ast_Expression'Class;
                            right : not null access Ast_Expression'Class ) return A_Ast_Expression is
        this : constant A_Ast_Assign := new Ast_Assign;
    begin
        case op is
            when ASSIGN_DIRECT => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), ":=", DIRECT );
            when ASSIGN_ADD    => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), "+=", ADD );
            when ASSIGN_SUB    => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), "-=", SUBTRACT );
            when ASSIGN_MUL    => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), "*=", MULTIPLY );
            when ASSIGN_DIV    => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), "/=", DIVIDE );
            when ASSIGN_CAT    => this.Construct( loc, A_Ast_Expression(left), A_Ast_Expression(right), "&=", CONCAT );
        end case;
        return A_Ast_Expression(this);
    end Create_Assign;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Ast_Assign;
                         loc      : Token_Location;
                         left     : not null A_Ast_Expression;
                         right    : not null A_Ast_Expression;
                         funcName : String;
                         op       : Scribble.Assign_Op ) is
    begin
        Ast_Binary_Op(this.all).Construct( loc,
                                           A_Ast_Expression(Create_Reference( left.loc, left, knownValid => True )),
                                           right,
                                           null,
                                           funcName );
        this.op := op;
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Operation( this : not null access Ast_Assign'Class ) return Scribble.Assign_Op is (this.op);

    ----------------------------------------------------------------------------

    function Get_Target( this : not null access Ast_Assign'Class ) return A_Ast_Symbol is (this.target);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Assign;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Assign( A_Ast_Assign(this) );
    end Process;

    ----------------------------------------------------------------------------

    procedure Set_Target( this   : not null access Ast_Assign'Class;
                          target : not null A_Ast_Symbol ) is
    begin
        this.target := target;
    end Set_Target;

    --==========================================================================

    function Create_Index( loc   : Token_Location;
                           left  : not null A_Ast_Expression;
                           right : not null A_Ast_Expression ) return A_Ast_Expression is
        this : constant A_Ast_Index := new Ast_Index;
    begin
        this.Construct( loc, left, right, null, "[]" );
        return A_Ast_Expression(this);
    end Create_Index;

    ----------------------------------------------------------------------------

    function Is_Reference_Expression( this : not null access Ast_Index'Class ) return Boolean
    is (this.parent /= null and then this.parent.Get_Type = REFERENCE_OP);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Index;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Index( A_Ast_Index(this) );
    end Process;

    --==========================================================================

    function Create_Conditional( loc       : Token_Location;
                                 condition,
                                 trueCase,
                                 falseCase : not null A_Ast_Expression ) return A_Ast_Expression is
        this : constant A_Ast_Conditional := new Ast_Conditional;
    begin
        this.Construct( loc, condition, trueCase, falseCase );
        return A_Ast_Expression(this);
    end Create_Conditional;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Ast_Conditional;
                         loc       : Token_Location;
                         condition : not null A_Ast_Expression;
                         trueCase  : not null A_Ast_Expression;
                         falseCase : not null A_Ast_Expression ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.condition := condition;
        this.condition.Set_Parent( A_Ast_Expression(this) );
        this.trueCase := trueCase;
        this.trueCase.Set_Parent( A_Ast_Expression(this) );
        this.falseCase := falseCase;
        this.falseCase.Set_Parent( A_Ast_Expression(this) );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Conditional ) is
    begin
        Delete( A_Ast_Node(this.condition) );
        Delete( A_Ast_Node(this.trueCase) );
        Delete( A_Ast_Node(this.falseCase) );
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Condition( this : not null access Ast_Conditional'Class ) return A_Ast_Expression is (this.condition);

    ----------------------------------------------------------------------------

    function Get_False_Case( this : not null access Ast_Conditional'Class ) return A_Ast_Expression is (this.falseCase);

    ----------------------------------------------------------------------------

    function Get_True_Case( this : not null access Ast_Conditional'Class ) return A_Ast_Expression is (this.trueCase);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Conditional;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Conditional( A_Ast_Conditional(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Conditional ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
        cond   : Value;
    begin
        pruned := this.condition.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.condition) );
            this.condition := pruned;
        end if;

        -- check if the conditional can be pruned to just one case
        if this.condition.Get_Type = LITERAL then
            -- determine whether the condition is true or false
            cond := A_Ast_Literal(this.condition).Get_Value;
            if (cond.Is_Boolean and then not cond.To_Boolean) or else
               (cond.Is_Number and then cond.To_Int = 0)
            then
                -- the condition statically evaluates to false
                -- prune the false case and return it
                pruned := this.falseCase.Prune;
                if pruned = null then
                    -- the false case can't be pruned down; release ownership
                    -- because it's going to be returned as the pruned expression
                    -- to replace the conditional.
                    pruned := this.falseCase;
                    this.falseCase := null;
                end if;
            else
                -- the condition statically evaluates to true
                -- prune the true case and return it
                pruned := this.trueCase.Prune;
                if pruned = null then
                    pruned := this.trueCase;
                    this.trueCase := null;
                end if;
            end if;

            -- pruned the conditional down to the true or false case
            return pruned;
        end if;

        pruned := this.trueCase.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.trueCase) );
            this.trueCase := pruned;
        end if;

        pruned := this.falseCase.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.falseCase) );
            this.falseCase := pruned;
        end if;

        return null;
    end Prune;

end Scribble.Ast.Expressions.Operators;
