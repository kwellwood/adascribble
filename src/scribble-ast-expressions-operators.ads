--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Opcodes;                  use Scribble.Opcodes;
with Values.Operations;                 use Values.Operations;

limited with Scribble.Ast.Processors;

package Scribble.Ast.Expressions.Operators is

    type Operator_Type is
    (
        -- unary operators
        UNARY_NOT,
        UNARY_NEG,
        UNARY_REF,
        UNARY_SIGN,

        -- binary operators
        BINARY_AND,
        BINARY_OR,
        BINARY_XOR,
        BINARY_IN,
        BINARY_EQ,
        BINARY_GT,
        BINARY_GTE,
        BINARY_LT,
        BINARY_LTE,
        BINARY_NEQ,
        BINARY_ADD,
        BINARY_SUB,
        BINARY_CAT,
        BINARY_MUL,
        BINARY_DIV,
        BINARY_MOD,
        BINARY_POW,

        -- assignment operators
        ASSIGN_DIRECT,
        ASSIGN_ADD,
        ASSIGN_SUB,
        ASSIGN_MUL,
        ASSIGN_DIV,
        ASSIGN_CAT
    );

    ----------------------------------------------------------------------------

    subtype Unary_Type is Operator_Type range UNARY_NOT..UNARY_SIGN;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Ast_Unary_Op is an expression node that represents a unary operation.
    type Ast_Unary_Op is new Ast_Expression with private;
    type A_Ast_Unary_Op is access all Ast_Unary_Op'Class;

    -- Returns a new instance of a concrete Ast_Unary_Op class, based on token
    -- type 't'. 'right' will be consumed on success. If the token type does not
    -- represent a unary operator, null will be returned.
    function Create_Unary_Op( op    : Unary_Type;
                              loc   : Token_Location;
                              right : not null A_Ast_Expression ) return A_Ast_Expression;

    -- Returns a reference to the expression on the right side of the operator.
    -- The Ast_Unary_Op maintains ownership.
    function Get_Right( this : access Ast_Unary_Op ) return A_Ast_Expression;

    -- Returns the runtime function that implements the operator, or Invalid_FID
    -- if the operator is implemented with a Scribble machine instruction.
    function Get_Function_Id( this : not null access Ast_Unary_Op'Class ) return Function_Id;

    -- Returns the name of the instruction or runtime function that implements
    -- the operator, for debugging info purposes.
    function Get_Function_Name( this : not null access Ast_Unary_Op'Class ) return String;

    -- Returns the Scribble machine instruction that implements the operator, or
    -- NOOP if the operator is implemented with a runtime function.
    function Get_Instruction( this : not null access Ast_Unary_Op'Class ) return Instruction;

    overriding
    function Get_Type( this : access Ast_Unary_Op ) return Ast_Type is (UNARY_OP);

    procedure Process( this      : access Ast_Unary_Op;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Unary_Op ) return A_Ast_Expression;

    ----------------------------------------------------------------------------

    -- Ast_Reference is a unary operation that represents the intent to pass a
    -- function argument by reference, instead of by value. It may only be used
    -- in the level of a function call argument expression and its right-side
    -- expression must produce a reference. The semantic analyzer verifies the
    -- correct use of the Reference operator, while Scribble's syntax allows it
    -- to be used much more generally.
    --
    -- Example: (Both the first and the third arguments are passed by reference)
    --
    --     foo( @myVar, x + 1, @myarray[3] );
    --
    type Ast_Reference is new Ast_Unary_Op with private;
    type A_Ast_Reference is access all Ast_Reference'Class;

    function Create_Reference( loc        : Token_Location;
                               right      : not null A_Ast_Expression;
                               knownValid : Boolean := False ) return A_Ast_Unary_Op;

    overriding
    function Get_Type( this : access Ast_Reference ) return Ast_Type is (REFERENCE_OP);

    -- Returns True if this instance has been marked as valid with Mark_Valid.
    function Is_Valid( this : not null access Ast_Reference'Class ) return Boolean;

    -- Marks this instance of the reference operator as valid, within the
    -- language rules.
    procedure Mark_Valid( this : not null access Ast_Reference'Class );

    overriding
    procedure Process( this      : access Ast_Reference;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    overriding
    function Prune( this : access Ast_Reference ) return A_Ast_Expression;

    ----------------------------------------------------------------------------

    subtype Binary_Type is Operator_Type range BINARY_AND..BINARY_POW;

    -- Returns the operator precedence of binary operator 'op'.
    function Get_Precedence( op : Binary_Type ) return Positive;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Ast_Binary_Op is an expression node that represents a binary operation.
    type Ast_Binary_Op is new Ast_Expression with private;
    type A_Ast_Binary_Op is access all Ast_Binary_Op'Class;

    -- Returns a new instance of a concrete Ast_Binary_Op class to perform the
    -- binary operation 'op'. 'left' and 'right' will be consumed on success.
    function Create_Binary_Op( op    : Binary_Type;
                               loc   : Token_Location;
                               left  : not null A_Ast_Expression;
                               right : not null A_Ast_Expression ) return A_Ast_Expression;

    -- Returns the runtime function that implements the operator, or Invalid_FID
    -- if the operator is implemented with a Scribble machine instruction.
    function Get_Function_Id( this : not null access Ast_Binary_Op'Class ) return Function_Id;

    -- Returns the name of the instruction or runtime function that implements
    -- the operator, for debugging info purposes.
    function Get_Function_Name( this : not null access Ast_Binary_Op'Class ) return String;

    -- Returns the Scribble machine instruction that implements the operator, or
    -- NOOP if the operator is implemented with a runtime function.
    function Get_Instruction( this : not null access Ast_Binary_Op'Class ) return Instruction;

    -- Returns a reference to the expression on the left side of the operator.
    -- The Ast_Binary_Op maintains ownership.
    function Get_Left( this : access Ast_Binary_Op ) return A_Ast_Expression;

    -- Returns a reference to the expression on the right side of the operator.
    -- The Ast_Binary_Op maintains ownership.
    function Get_Right( this : access Ast_Binary_Op ) return A_Ast_Expression;

    overriding
    function Get_Type( this : access Ast_Binary_Op ) return Ast_Type is (BINARY_OP);

    procedure Process( this      : access Ast_Binary_Op;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Binary_Op ) return A_Ast_Expression;

    ----------------------------------------------------------------------------

    subType Assign_Type is Operator_Type range ASSIGN_DIRECT..ASSIGN_CAT;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Ast_Assign is binary operation that represents an assignment into a
    -- symbol or a nested structure identified by a symbol.
    type Ast_Assign is new Ast_Binary_Op with private;
    type A_Ast_Assign is access all Ast_Assign'Class;

    -- Creates a new assignment operation that will assign 'right' into 'left',
    -- performing the assignment operation specified by 'op'. Both 'left' and
    -- 'right' will be consumed on success.
    --
    -- The semantic analyzer will verify that left is either an Ast_Symbol, or
    -- an Ast_Symbol enclosed within an arbitrary number of Ast_Index operations.
    -- The AST actually created prefix 'left' with a reference operator '@'
    -- because the actual target for assignment is what 'left' is referencing.
    --
    -- The valid values for 'op' are those for which Is_Assign_Op returns True.
    -- See Is_Assign_Op for further details.
    function Create_Assign( op    : Assign_Type;
                            loc   : Token_Location;
                            left  : not null access Ast_Expression'Class;
                            right : not null access Ast_Expression'Class ) return A_Ast_Expression;

    -- Returns the operation to be performed on assignment.
    function Get_Operation( this : not null access Ast_Assign'Class ) return Scribble.Assign_Op;

    -- Returns a reference to the target symbol receiving the assignment, or
    -- null if it has not yet been determined.
    function Get_Target( this : not null access Ast_Assign'Class ) return A_Ast_Symbol;

    procedure Process( this      : access Ast_Assign;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    -- Sets the target symbol receiving the assignment. This is determined
    -- during the semantic analysis stage.
    procedure Set_Target( this   : not null access Ast_Assign'Class;
                          target : not null A_Ast_Symbol );

    ----------------------------------------------------------------------------

    -- Ast_Index is a reference operation, referencing the indexed of a
    -- container value (e.g. maps or lists), for the purpose of either
    -- retrieving an element's value or updating it via assignment.
    --
    -- The purpose of the Ast_Index node is determined by its location in the
    -- expression tree relative to an Ast_Assign node. If the Ast_Index node is
    -- on the right hand side of the assignment or there is no assignment, the
    -- index operation is part of a value retrieval operation.
    --
    -- If the Ast_Index is on the left hand side of an Ast_Assign node, the
    -- index operation is part of an assignment to the indexed element of a
    -- container.
    --
    -- The left hand side of the index node is the container on which to operate,
    -- and the right hand side is the number or key of the element to reference.
    type Ast_Index is new Ast_Binary_Op with private;
    type A_Ast_Index is access all Ast_Index'Class;

    -- Creates a new Ast_Index instance to reference the 'right' element of the
    -- 'left' container value. 'left' and 'right' will be consumed on success.
    function Create_Index( loc   : Token_Location;
                           left  : not null A_Ast_Expression;
                           right : not null A_Ast_Expression ) return A_Ast_Expression;

    overriding
    function Can_Produce_Reference( this : access Ast_Index ) return Boolean is (True);

    overriding
    function Get_Type( this : access Ast_Index ) return Ast_Type is (INDEX_OP);

    -- Returns True if the Index is at the root of a reference expression.
    function Is_Reference_Expression( this : not null access Ast_Index'Class ) return Boolean;

    procedure Process( this      : access Ast_Index;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_Conditional represents a conditional/ternary operation. If the
    -- conditional clause evaluates True at runtime, the True case will be
    -- evaluated to produce the result of the operator. Otherwise, the False
    -- case will be evaluated.
    --
    -- Example: (foo ? True : False)
    type Ast_Conditional is new Ast_Expression with private;
    type A_Ast_Conditional is access all Ast_Conditional'Class;

    -- Creates a new Ast_Conditional instance. Consumes 'condition', 'trueCase'
    -- and 'falseCase'.
    function Create_Conditional( loc       : Token_Location;
                                 condition,
                                 trueCase,
                                 falseCase : not null A_Ast_Expression ) return A_Ast_Expression;
    pragma Postcondition( Create_Conditional'Result /= null );

    -- Returns a reference to the expression evaluated as the conditional test.
    -- Ownership is maintained by the Ast_Conditional.
    function Get_Condition( this : not null access Ast_Conditional'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_Condition'Result /= null );

    -- Returns a reference to the expression that will be evaluated when the
    -- condition is False. Ownership is maintained by the Ast_Conditional.
    function Get_False_Case( this : not null access Ast_Conditional'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_False_Case'Result /= null );

    -- Returns a reference to the expression that will be evaluated when the
    -- condition is True. Ownership is maintained by the Ast_Conditional.
    function Get_True_Case( this : not null access Ast_Conditional'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_True_Case'Result /= null );

    overriding
    function Get_Left( this : access Ast_Conditional ) return A_Ast_Expression is (this.Get_True_Case);

    overriding
    function Get_Right( this : access Ast_Conditional ) return A_Ast_Expression is (this.Get_False_Case);

    overriding
    function Get_Type( this : access Ast_Conditional ) return Ast_Type is (CONDITIONAL_OP);

    procedure Process( this      : access Ast_Conditional;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Conditional ) return A_Ast_Expression;

private

    type Ast_Unary_Op is new Ast_Expression with
        record
            right    : A_Ast_Expression := null;
            pruner   : A_Unary_Function := null;
            funcName : Unbounded_String;
            inst     : Instruction;
            fid      : Function_Id;
        end record;

    procedure Construct( this     : access Ast_Unary_Op;
                         loc      : Token_Location;
                         right    : not null A_Ast_Expression;
                         pruner   : A_Unary_Function;
                         funcName : String;
                         inst     : Instruction := NOOP;
                         fid      : Function_Id := Invalid_FID );

    procedure Delete( this : in out Ast_Unary_Op );

    ----------------------------------------------------------------------------

    type Ast_Reference is new Ast_Unary_Op with
        record
            valid : Boolean := False;
        end record;

    ----------------------------------------------------------------------------

    type A_Binary_Prune_Func is access function( left, right : Value'Class ) return Value;

    type Ast_Binary_Op is new Ast_Expression with
        record
            left,
            right    : A_Ast_Expression := null;
            pruner   : A_Binary_Function := null;
            funcName : Unbounded_String;
            inst     : Instruction;
            fid      : Function_Id;
        end record;

    procedure Construct( this     : access Ast_Binary_Op;
                         loc      : Token_Location;
                         left     : not null A_Ast_Expression;
                         right    : not null A_Ast_Expression;
                         pruner   : A_Binary_Function;
                         funcName : String;
                         inst     : Instruction := NOOP;
                         fid      : Function_Id := Invalid_FID );

    procedure Delete( this : in out Ast_Binary_Op );

    ----------------------------------------------------------------------------

    type Ast_Assign is new Ast_Binary_Op with
        record
            op     : Scribble.Assign_Op := DIRECT;
            target : A_Ast_Symbol := null;      -- the target symbol (not owned)
        end record;

    procedure Construct( this      : access Ast_Assign;
                         loc       : Token_Location;
                         left      : not null A_Ast_Expression;
                         right     : not null A_Ast_Expression;
                         funcName  : String;
                         op        : Scribble.Assign_Op );

    ----------------------------------------------------------------------------

    type Ast_Index is new Ast_Binary_Op with null record;

    ----------------------------------------------------------------------------

    type Ast_Conditional is new Ast_Expression with
        record
            condition : A_Ast_Expression := null;
            trueCase  : A_Ast_Expression := null;
            falseCase : A_Ast_Expression := null;
        end record;

    procedure Construct( this      : access Ast_Conditional;
                         loc       : Token_Location;
                         condition : not null A_Ast_Expression;
                         trueCase  : not null A_Ast_Expression;
                         falseCase : not null A_Ast_Expression );

    procedure Delete( this : in out Ast_Conditional );

end Scribble.Ast.Expressions.Operators;
