--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values;                            use Values;

package Scribble.Namespaces is

    -- A Namespace is an abstract name/value store, implemented by the
    -- application. When registered with a Scribble_Runtime, its data becomes
    -- accessible directly to Scribble code. A Scribble runtime can support
    -- multiple named Namespaces. See the Scribble syntax documentation for
    -- details on how to reference names in a Namespace.
    type Scribble_Namespace is interface;
    type A_Scribble_Namespace is access all Scribble_Namespace'Class;

    -- Returns the value of a name within the Scribble namespace (without copying).
    --
    -- The implementation must return a Null pointer if 'name' is undefined. The
    -- Scribble VM will generate a suitable error.
    function Get_Namespace_Name( this : access Scribble_Namespace;
                                 name : String ) return Value is abstract;

    -- Returns a reference to the value of 'name' within the Scribble namespace.
    --
    -- If 'name' is undefined but may be defined, then the namespace should
    -- define it equal to Null and then return a reference to it. Otherwise, the
    -- implementation should return Null (not a reference) and the Scribble VM
    -- will generate a suitable error regarding referencing an undefined name.
    function Get_Namespace_Ref( this : access Scribble_Namespace;
                                name : String ) return Value is abstract;

    -- the name of the anonymous namespace
    ANONYMOUS_NAMESPACE : constant String := "";

end Scribble.Namespaces;
