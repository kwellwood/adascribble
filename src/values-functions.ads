--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Opcodes;                  use Scribble.Opcodes;
with Scribble.Debugging;                use Scribble.Debugging;

package Values.Functions is

    -- Creates a new Function value, consuming all the given arguments.
    --
    -- 'argCount' is the number of arguments accepted by the function.
    -- 'defaultArgs' is an array of values to be used as default for optional
    --     arguments not supplied by the caller a runtime.
    -- 'program' is an array of program instructions.
    -- 'staticData' is an array of constant program data values.
    -- 'isMember' indicates if the function is a member function, containing a
    --     implicit required first parameter 'this'.
    -- 'name' is the name of the function at compile time. It should follow the
    --     rules for Scribble identifiers or it may be empty.
    -- 'debugInfo' is an optional record containing debugging information. It
    --     will be consumed even though it's not an in-out parameter.
    function Create_Function( argCount    : Natural;
                              defaultArgs : Value_Array;
                              program     : Opcode_Array;
                              staticData  : Value_Array;
                              isMember    : Boolean;
                              name        : String;
                              debugInfo   : A_Debug_Info ) return Value;
    pragma Precondition( argCount <= 255 );
    pragma Precondition( defaultArgs'Length <= argCount );
    pragma Precondition( program'Length > 0 );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Function_Value is new Value with private;

    -- Returns a Function_Value pointing to the data of 'this'. The data is
    -- never copied.
    function As_Function( this : Value'Class ) return Function_Value;

    -- Returns the number of arguments accepted by the function.
    function Arg_Count( this : Function_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Returns a copy of the default value for an optional argument. 'arg' must
    -- be the argument number to which the default value belongs. The first
    -- argument is number one. If a default does not exist for the argument,
    -- a null pointer will be returned.
    function Arg_Default( this : Function_Value'Class; arg : Positive ) return Value;
    pragma Precondition( this.Valid );

    -- Returns the length of the function's program data. The return value is
    -- the largest addressable index.
    function Data_Length( this : Function_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Returns a value from the function's static program data. Returns Null if
    -- 'index' is not valid. Read only.
    function Get_Data( this : Function_Value'Class; index : Integer ) return Value;
    pragma Precondition( this.Valid );

    -- Returns a pointer to the Function's debug info record, if it's available.
    -- Read only.
    function Get_Debug_Info( this : Function_Value'Class ) return A_Debug_Info with Inline;
    pragma Precondition( this.Valid );

    -- Returns the original name of the function, as defined at compile time.
    -- This may be the local variable name to which it was assigned, a namespace
    -- name, or a map value's key string. An empty string will be returned if
    -- the function does not have a name.
    function Get_Name( this : Function_Value'Class ) return String;

    -- Returns a single opcode at index 'index' in the program. The first opcode
    -- is at index 1.
    function Get_Opcode( this : Function_Value'Class; index : Positive ) return Opcode;

    -- Returns the path of the source file from which this function was compiled,
    -- or an empty string if not available. The function must be compiled with
    -- debugging info for the path to be available.
    function Get_Path( this : Function_Value'Class ) return String;

    -- Returns True if this is a member function, containing an implicit
    -- required first parameter 'this'.
    function Is_Member( this : Function_Value'Class ) return Boolean;

    -- Returns the minimum number of arguments required for evaluation (total
    -- number of arguments minus the number of argument defaults).
    function Min_Args( this : Function_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Returns the address of the function's first program instruction. The
    -- number of instructions can be found by calling Program_Length.
    function Program( this : Function_Value'Class ) return Address with Inline;
    pragma Precondition( this.Valid );

    -- Returns the length of the function's instruction array. The return value
    -- is the largest instruction index.
    function Program_Length( this : Function_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Returns a string representation of the function's prototype, using the
    -- format: "function(<param1>, <param2>, ..., <paramN>:=<typeOfDefault>)"
    -- If debugging information is not available, parameter names will be listed
    -- alphabetically (e.g.: a, b, c, etc.).
    function Prototype_Image( this : Function_Value'Class ) return String;
    pragma Precondition( this.Valid );

    -- Returns the function's original source code, if available.
    function Source_Code( this : Function_Value'Class ) return String;
    pragma Precondition( this.Valid );

    -- Returns the 'line' numbered line of the function's source code, if
    -- available.
    function Source_Line( this : Function_Value'Class; line : Natural ) return String;
    pragma Precondition( this.Valid );

    function Valid( this : Function_Value'Class ) return Boolean is (this.Is_Function);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Delete_Function( obj : Address );

    function Image_Function( this : Value'Class ) return String;
    pragma Precondition( this.Is_Function );

    procedure Read_Function( stream : access Root_Stream_Type'Class;
                             this   : in out Value'Class );
    pragma Precondition( this.Is_Function );

    procedure Write_Function( stream : access Root_Stream_Type'Class;
                              this   : Value'Class );
    pragma Precondition( this.Is_Function );

private

    type Function_Data;
    type A_Function_Data is access all Function_Data;
    pragma No_Strict_Aliasing( A_Function_Data );

    type Function_Value is new Value with
        record
            data : A_Function_Data := null;
        end record;

end Values.Functions;
