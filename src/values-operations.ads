--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Operations is

    pragma Elaborate_Body;

    type A_Unary_Function is access
        function( right : Value'Class ) return Value;

    -- Unary_Operation is the base class for all unary operator classes. It
    -- encapsulates a one-dimensional array 'table' for dispatching operations.
    -- The constructor of a subclass must override the default unary function
    -- references for each value type it operates on.
    type Unary_Operation is abstract tagged limited private;

    -- Executes the unary operation on operand 'right', sensitive to whichever
    -- value type it may contain.
    function Dispatch( this  : not null access Unary_Operation'Class;
                       right : Value'Class ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    generic
        type Operation is new Unary_Operation with private;
    package Generic_Unary is
        function Operate( right : Value'Class ) return Value;
    private
        type A_Operation is access all Operation'Class;
        instance : A_Operation := new Operation;
    end Generic_Unary;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Negate_Op is new Unary_Operation with private;
    type Not_Op    is new Unary_Operation with private;
    type Sign_Op   is new Unary_Operation with private;

    ----------------------------------------------------------------------------

    type A_Binary_Function is access
        function( left, right : Value'Class ) return Value;

    -- Binary_Operation is the base class for all binary operator classes. It
    -- encapsulates a two-dimensional array 'table' for dispatching operations.
    -- The constructor of a subclass must override the default binary function
    -- references for each pair of value types it operates on.
    type Binary_Operation is abstract tagged limited private;

    -- Executes the binary operation on operands 'left' and 'right', sensitive
    -- to whichever value types they may contain.
    function Dispatch( this  : not null access Binary_Operation'Class;
                       left  : Value'Class;
                       right : Value'Class ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    generic
        type Operation is new Binary_Operation with private;
    package Generic_Binary is
        function Operate( left, right : Value'Class ) return Value;
    private
        type A_Operation is access all Operation'Class;
        instance : A_Operation := new Operation;
    end Generic_Binary;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Add_Op       is new Binary_Operation with private;
    type And_Op       is new Binary_Operation with private;
    type Concat_Op    is new Binary_Operation with private;
    type Divide_Op    is new Binary_Operation with private;
    type In_Op        is new Binary_Operation with private;
    type Index_Op     is new Binary_Operation with private;
    type Index_Ref_Op is new Index_Op         with private;
    type Modulo_Op    is new Binary_Operation with private;
    type Multiply_Op  is new Binary_Operation with private;
    type Or_Op        is new Binary_Operation with private;
    type Power_Op     is new Binary_Operation with private;
    type Subtract_Op  is new Binary_Operation with private;
    type Xor_Op       is new Binary_Operation with private;

private

    type Unary_Func_Table is array (Value_Type) of A_Unary_Function;

    type Unary_Operation is abstract tagged limited
        record
            initialized : Boolean := False;
            table       : Unary_Func_Table;
        end record;

    procedure Construct( this : access Unary_Operation );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- This function is called when the operand is an Error, simply propagating it.
    function Unary_Error( right : Value'Class ) return Value;

    -- This function is called when the operand is an unexpected Null value
    -- type, producing an error.
    function Unary_Null( right : Value'Class ) return Value;

    -- This function is called when the operation is undefined for the given
    -- operand's type, producing an Error.
    function Unary_Undefined( right : Value'Class ) return Value;

    ----------------------------------------------------------------------------

    type Negate_Op is new Unary_Operation with null record;
    procedure Construct( this : access Negate_Op );

    type Not_Op is new Unary_Operation with null record;
    procedure Construct( this : access Not_Op );

    type Sign_Op is new Unary_Operation with null record;
    procedure Construct( this : access Sign_Op );

    ----------------------------------------------------------------------------

    type Binary_Func_Table is array (Value_Type, Value_Type) of A_Binary_Function;

    type Binary_Operation is abstract tagged limited
        record
            initialized : Boolean := False;
            table       : Binary_Func_Table;
        end record;

    procedure Construct( this : access Binary_Operation );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Add_Op is new Binary_Operation with null record;
    procedure Construct( this : access Add_Op );

    type And_Op is new Binary_Operation with null record;
    procedure Construct( this : access And_Op );

    type Concat_Op is new Binary_Operation with null record;
    procedure Construct( this : access Concat_Op );

    type Divide_Op is new Binary_Operation with null record;
    procedure Construct( this : access Divide_Op );

    type In_Op is new Binary_Operation with null record;
    procedure Construct( this : access In_Op );

    type Index_Op is new Binary_Operation with null record;
    procedure Construct( this : access Index_Op );

    type Index_Ref_Op is new Index_Op with null record;
    procedure Construct( this : access Index_Ref_Op );

    type Modulo_Op is new Binary_Operation with null record;
    procedure Construct( this : access Modulo_Op );

    type Multiply_Op is new Binary_Operation with null record;
    procedure Construct( this : access Multiply_Op );

    type Or_Op is new Binary_Operation with null record;
    procedure Construct( this : access Or_Op );

    type Power_Op is new Binary_Operation with null record;
    procedure Construct( this : access Power_Op );

    type Subtract_Op is new Binary_Operation with null record;
    procedure Construct( this : access Subtract_Op );

    type Xor_Op is new Binary_Operation with null record;
    procedure Construct( this : access Xor_Op );

end Values.Operations;
