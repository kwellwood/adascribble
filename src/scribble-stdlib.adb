--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Numerics.Long_Elementary_Functions;
 use Ada.Numerics.Long_Elementary_Functions;
with Scribble.VMs;                      use Scribble.VMs;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Errors;                     use Values.Errors;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;
with Values.Strings.Operations;         use Values.Strings.Operations;

package body Scribble.Stdlib is

    -- Returns True if the type of every element in 'args' matches the
    -- corresponding element in 'types'. There can be fewer 'args' than types.
    function Check_Types( args : Value_Array; types : Type_Array ) return Boolean is
    begin
        for i in args'Range loop
            if Get_Type( args(i) ) /= types(types'First + (i - args'First)) then
                return False;
            end if;
        end loop;
        return True;
    end Check_Types;
    pragma Inline_Always( Check_Types );

    --==========================================================================
    -- Math Functions

    -- number abs(x : number)
    procedure Abs_Val( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( abs state.args(1).To_Long_Float );
        else
            state.result := Create( Type_Mismatch, "abs()" );
        end if;
    end Abs_Val;

    ----------------------------------------------------------------------------

    -- number cos(x : number)
    procedure Cos( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Cos( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "cos()" );
        end if;
    end Cos;

    ----------------------------------------------------------------------------

    -- number sin(x : number)
    procedure Sin( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Sin( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "sin()" );
        end if;
    end Sin;

    ----------------------------------------------------------------------------

    -- number tan(x : number)
    procedure Tan( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Tan( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "tan()" );
        end if;
    end Tan;

    ----------------------------------------------------------------------------

    -- number acos(x : number)
    procedure Acos( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Arccos( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "acos()" );
        end if;
    end Acos;

    ----------------------------------------------------------------------------

    -- number asin(x : number)
    procedure Asin( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Arcsin( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "asin()" );
        end if;
    end Asin;

    ----------------------------------------------------------------------------

    -- number atan(x : number)
    procedure Atan( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Arctan( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "atan()" );
        end if;
    end Atan;

    ----------------------------------------------------------------------------

    -- number log2(x : number)
    procedure Log2( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Log( state.args(1).To_Long_Float, 2.0 ) );
        else
            state.result := Create( Type_Mismatch, "log2()" );
        end if;
    end Log2;

    ----------------------------------------------------------------------------

    -- number log10(x : number)
    procedure Log10( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Log( state.args(1).To_Long_Float, 10.0 ) );
        else
            state.result := Create( Type_Mismatch, "log10()" );
        end if;
    end Log10;

    ----------------------------------------------------------------------------

    -- number ln(x : number)
    procedure Ln( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Log( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "ln()" );
        end if;
    end Ln;

    ----------------------------------------------------------------------------

    -- any max(a : number, ...)
    procedure Max( state : in out Scribble_State ) is
    begin
        state.result := state.args(1);
        for i in 2..state.argCount loop
            if state.args(i) > state.result then
                state.result := state.args(i);
            end if;
        end loop;
    end Max;

    ----------------------------------------------------------------------------

    -- any min(a : number, ...)
    procedure Min( state : in out Scribble_State ) is
    begin
        state.result := state.args(1);
        for i in 2..state.argCount loop
            if state.args(i) < state.result then
                state.result := state.args(i);
            end if;
        end loop;
    end Min;

    ----------------------------------------------------------------------------

    -- number round(x : number)
    procedure Round( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Long_Float'Rounding( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "round()" );
        end if;
    end Round;

    ----------------------------------------------------------------------------

    -- number sqrt(x : number)
    procedure Sqrt( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Number then
            state.result := Create( Sqrt( state.args(1).To_Long_Float ) );
        else
            state.result := Create( Type_Mismatch, "sqrt()" );
        end if;
    end Sqrt;

    --==========================================================================
    -- String/List Functions

    -- list   append(container : list,   val : any   )
    -- string append(container : string, str : string)
    procedure Append( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_List then
            state.result := Clone( state.args(1) );
            state.result.Lst.Append( state.args(2), consume => False );

        elsif Check_Types( state.args, (V_STRING, V_STRING) ) then
            state.result := state.args(1).Str.Append( state.args(2).Str );

        else
            state.result := Create( Type_Mismatch, "append()" );
        end if;
    end Append;

    ----------------------------------------------------------------------------

    -- list appendUnique(container : list, val : any)
    procedure AppendUnique( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_List then
            state.result := Clone( state.args(1) );
            if not state.result.Lst.Contains( state.args(2) ) then
                state.result.Lst.Append( state.args(2), consume => False );
            end if;
        else
            state.result := Create( Type_Mismatch, "appendUnique()" );
        end if;
    end AppendUnique;

    ----------------------------------------------------------------------------

    -- boolean beginsWith(str : string, pattern : string)
    procedure BeginsWith( state : in out Scribble_State ) is
    begin
        if Check_Types( state.args, (V_STRING, V_STRING) ) then
            state.result := Create( state.args(1).Str.Begins_With( state.args(2).Str ) );
        else
            state.result := Create( Type_Mismatch, "beginsWith()" );
        end if;
    end BeginsWith;

    ----------------------------------------------------------------------------

    -- boolean endsWith(str : string, pattern : string)
    procedure EndsWith( state : in out Scribble_State ) is
    begin
        if Check_Types( state.args, (V_STRING, V_STRING) ) then
            state.result := Create( state.args(1).Str.Ends_With( state.args(2).Str ) );
        else
            state.result := Create( Type_Mismatch, "endsWith()" );
        end if;
    end EndsWith;

    ----------------------------------------------------------------------------

    -- string head(str : string, count : number)
    procedure Head( state : in out Scribble_State ) is
        count : Integer;
    begin
        if Check_Types( state.args, (V_STRING, V_NUMBER) ) then
            count := Integer'Max( state.args(2).To_Int, 0 );
            state.result := state.args(1).Str.Head( count );

        elsif Check_Types( state.args, (V_LIST, V_NUMBER) ) then
            count := Integer'Max( state.args(2).To_Int, 0 );
            state.result := state.args(1).Lst.Head( count );

        else
            state.result := Create( Type_Mismatch, "head()" );
        end if;
    end Head;

    ----------------------------------------------------------------------------

    -- number indexOf(container : string, pattern : string, from : number := 1)
    -- number indexOf(container : list,   val     : any,    from : number := 1)
    procedure IndexOf( state : in out Scribble_State ) is
        from : constant Integer := (if state.args'Length > 2 then Integer'Max( Cast_Int( state.args(3), 1 ), 1 ) else 1);
    begin
        if state.args(1).Is_List then
            state.result := Create( state.args(1).Lst.Find( state.args(2), from ) );

        elsif Check_Types( state.args, (V_STRING, V_STRING, V_NUMBER) ) then
            state.result := Create( state.args(1).Str.IndexOf( state.args(2).Str.To_String, from ) );

        else
            state.result := Create( Type_Mismatch, "indexOf()" );
        end if;
    end IndexOf;

    ----------------------------------------------------------------------------

    -- list   insert(container : list,   val : any,    before : number)
    -- string insert(container : string, str : string, before : number)
    procedure Insert( state : in out Scribble_State ) is
        before : constant Integer := Integer'Max( Cast_Int( state.args(3), 1 ), 1 );
    begin
        if state.args(1).Is_List and state.args(3).Is_Number then
            state.result := Clone( state.args(1) );
            state.result.Lst.Insert( state.args(2), before, consume => False );

        elsif Check_Types( state.args, (V_STRING, V_STRING, V_NUMBER) ) then
            state.result := state.args(1).Str.Insert( state.args(2).Str, before );

        else
            state.result := Create( Type_Mismatch, "insert()" );
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    -- number length(str       : string)
    -- number length(container : list  )
    -- number length(container : map   )
    procedure Length( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            state.result := Create( state.args(1).Str.Length );
        elsif state.args(1).Is_List then
            state.result := Create( state.args(1).Lst.Length );
        elsif state.args(1).Is_Map then
            state.result := Create( state.args(1).Map.Size );
        else
            state.result := Create( Type_Mismatch, "length()" );
        end if;
    end Length;

    ----------------------------------------------------------------------------

    -- number ltrim(str : string)
    procedure Ltrim( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            state.result := state.args(1).Str.Ltrim;
        else
            state.result := Create( Type_Mismatch, "ltrim()" );
        end if;
    end Ltrim;

    ----------------------------------------------------------------------------

    -- list   prepend(container : list,   val : any   )
    -- string prepend(container : string, str : string)
    procedure Prepend( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_List then
            state.result := Clone( state.args(1) );
            state.result.Lst.Prepend( state.args(2), consume => False );

        elsif Check_Types( state.args, (V_STRING, V_STRING) ) then
            state.result := state.args(1).Str.Prepend( state.args(2).Str );

        else
            state.result := Create( Type_Mismatch, "prepend()" );
        end if;
    end Prepend;

    ----------------------------------------------------------------------------

    -- list prependUnique(container : list, val : any)
    procedure PrependUnique( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_List then
            state.result := Clone( state.args(1) );
            if not state.result.Lst.Contains( state.args(2) ) then
                state.result.Lst.Prepend( state.args(2), consume => False );
            end if;

        else
            state.result := Create( Type_Mismatch, "prependUnique()" );
        end if;
    end PrependUnique;

    ----------------------------------------------------------------------------

    -- list   remove(container : list,   start : number, count : number := 1)
    -- string remove(container : string, start : number, count : number := 1)
    procedure Remove( state : in out Scribble_State ) is
        start : Integer;
        count : Integer;
    begin
        count := (if state.argCount > 2 then Cast_Int( state.args(3), 1 ) else 1);
        if count < 0 then
            count := 0;
        end if;

        if Check_Types( state.args, (V_LIST, V_NUMBER, V_NUMBER) ) then
            start := Integer'Max( state.args(2).To_Int, 1 );
            state.result := Clone( state.args(1) );
            state.result.Lst.Remove( start, count );

        elsif Check_Types( state.args, (V_STRING, V_NUMBER, V_NUMBER) ) then
            start := Integer'Max( state.args(2).To_Int, 1 );
            state.result := state.args(1).Str.Remove( start, count );

        else
            state.result := Create( Type_Mismatch, "remove()" );
        end if;
    end Remove;

    ----------------------------------------------------------------------------

    -- string reverse(str : string)
    -- list   reverse(lst : list)
    procedure Reversed( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            declare
                str    : constant String := state.args(1).Str.To_String;
                result : String(1..str'Length);
            begin
                for i in 1..str'Length loop
                    result(i) := str(str'Last-i+1);
                end loop;
                state.result := Create( result );
            end;

        elsif state.args(1).Is_List then
            declare
                len  : constant Integer := state.args(1).Lst.Length;
                vals : Value_Array(1..len);
            begin
                for i in reverse 1..len loop
                    vals(1 + (len - i)) := state.args(1).Lst.Get( i );
                end loop;
                state.result := Create_List( vals, consume => False );
            end;

        else
            state.result := Create( Invalid_Arguments, "reverse()" );
        end if;
    end Reversed;

    ----------------------------------------------------------------------------

    -- number rtrim(str : string)
    procedure Rtrim( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            state.result := state.args(1).Str.Rtrim;
        else
            state.result := Create( Type_Mismatch, "rtrim()" );
        end if;
    end Rtrim;

    ----------------------------------------------------------------------------

    -- number slice(str : string, start : number, count : number)
    procedure Slice( state : in out Scribble_State ) is
        start : Integer;
        count : Integer;
    begin
        if Check_Types( state.args, (V_STRING, V_NUMBER, V_NUMBER) ) then
            start := Integer'Max( state.args(2).To_Int, 1 );
            count := Integer'Max( state.args(3).To_Int, 0 );
            state.result := state.args(1).Str.Slice( start, count );

        elsif Check_Types( state.args, (V_LIST, V_NUMBER, V_NUMBER) ) then
            start := Integer'Max( state.args(2).To_Int, 1 );
            count := Integer'Max( state.args(3).To_Int, 0 );
            state.result := state.args(1).Lst.Slice( start, count );

        else
            state.result := Create( Type_Mismatch, "slice()" );
        end if;
    end Slice;

    ----------------------------------------------------------------------------

    -- list sort(container : list)
    procedure Sort( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_List then
            state.result := Clone( state.args(1) );
            state.result.Lst.Sort;
        else
            state.result := Create( Type_Mismatch, "sort()" );
        end if;
    end Sort;

    ----------------------------------------------------------------------------

    -- list split(str : string, delim : string, keepEmpty : boolean := true)
    procedure Split( state : in out Scribble_State ) is
        keepEmpty : Boolean;
    begin
        if Check_Types( state.args, (V_STRING, V_STRING, V_BOOLEAN) ) then
            keepEmpty := (if state.args'Length > 2 then state.args(3).To_Boolean else True);
            state.result := Split( state.args(1).Str.To_Unbounded_String, state.args(2).Str.To_String, keepEmpty );
        else
            state.result := Create( Type_Mismatch, "split()" );
        end if;
    end Split;

    ----------------------------------------------------------------------------

    -- string sprintf(format : string, ...)
    procedure Sprintf( state : in out Scribble_State ) is null;

    ----------------------------------------------------------------------------

    -- string tail(str : string, count : number)
    procedure Tail( state : in out Scribble_State ) is
        count : Integer;
    begin
        if Check_Types( state.args, (V_STRING, V_NUMBER) ) then
            count := Integer'Max( state.args(2).To_Int, 0 );
            state.result := state.args(1).Str.Tail( count );

        elsif Check_Types( state.args, (V_LIST, V_NUMBER) ) then
            count := Integer'Max( state.args(2).To_Int, 0 );
            state.result := state.args(1).Lst.Tail( count );

        else
            state.result := Errors.Create( Type_Mismatch, "tail()" );
        end if;
    end Tail;

    ----------------------------------------------------------------------------

    -- string trim(str : string)
    procedure Trim( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            state.result := state.args(1).Str.Trim;
        else
            state.result := Create( Type_Mismatch, "trim()" );
        end if;
    end Trim;

    --==========================================================================
    -- Map Functions

    -- list keys(container : map)
    procedure Keys( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_Map then
            state.result := state.args(1).Map.Get_Keys;
        else
            state.result := Create( Type_Mismatch, "keys()" );
        end if;
    end Keys;

    ----------------------------------------------------------------------------

    -- map merge(base : map, overriding : map)
    procedure Merge( state : in out Scribble_State ) is
    begin
        if Check_Types( state.args, (V_MAP, V_MAP) ) then
            state.result := Merge( state.args(1).Map, state.args(2).Map );
        else
            state.result := Create( Type_Mismatch, "merge()" );
        end if;
    end Merge;

    ----------------------------------------------------------------------------

    -- map rmerge(base : map, overriding : map)
    procedure Rmerge( state : in out Scribble_State ) is
    begin
        if Check_Types( state.args, (V_MAP, V_MAP) ) then
            state.result := Recursive_Merge( state.args(1).Map, state.args(2).Map );
        else
            state.result := Create( Type_Mismatch, "rmerge()" );
        end if;
    end Rmerge;

    --==========================================================================
    -- Color Functions

    -- rgbaColor(r, g, b, a : number)
    procedure RgbaColor( state : in out Scribble_State ) is
    begin
        if Check_Types( state.args, (V_NUMBER, V_NUMBER, V_NUMBER, V_NUMBER) ) then
            state.result := Create_Color( r => state.args(1).To_Float,
                                          g => state.args(2).To_Float,
                                          b => state.args(3).To_Float,
                                          a => state.args(4).To_Float );
        else
            state.result := Create( Type_Mismatch, "rgbaColor()" );
        end if;
    end RgbaColor;

    --==========================================================================
    -- Miscellaneous Functions

    -- string image(what : any)
    procedure Image( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Image );
    end Image;

    ----------------------------------------------------------------------------

    -- boolean isBoolean(val : any)
    procedure IsBoolean( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Boolean );
    end IsBoolean;

    ----------------------------------------------------------------------------

    procedure IsColor( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Color );
    end IsColor;

    ----------------------------------------------------------------------------

    -- boolean isError(val : any)
    procedure IsError( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Error );
    end IsError;

    ----------------------------------------------------------------------------

    -- boolean isFunction(val : any)
    procedure IsFunction( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Function );
    end IsFunction;

    ----------------------------------------------------------------------------

    -- boolean isId(val : any)
    procedure isId( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Id );
    end isId;

    ----------------------------------------------------------------------------

    -- boolean isList(val : any)
    procedure IsList( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_List );
    end IsList;

    ----------------------------------------------------------------------------

    -- boolean isMap(val : any)
    procedure IsMap( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Map );
    end IsMap;

    ----------------------------------------------------------------------------

    -- boolean isNumber(val : any)
    procedure IsNumber( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_Number );
    end IsNumber;

    ----------------------------------------------------------------------------

    -- boolean isString(val : any)
    procedure IsString( state : in out Scribble_State ) is
    begin
        state.result := Create( state.args(1).Is_String );
    end IsString;

    ----------------------------------------------------------------------------

    -- null print(val : any)
    procedure Print( state : in out Scribble_State ) is
    begin
        if state.args(1).Is_String then
            Print_Line( state.args(1).Str.To_String );
        else
            Print_Line( Image( state.args(1) ) );
        end if;
    end Print;

    ----------------------------------------------------------------------------

    -- null print(format : string, ...)
    procedure Printf( state : in out Scribble_State ) is null;

    ----------------------------------------------------------------------------

    -- null sleep(millis : number)
    procedure Sleep_ms( state : in out Scribble_State ) is
    begin
        if not state.args(1).Is_Number then
            state.result := Create( Type_Mismatch, "sleep()" );
            return;
        end if;
        delay Duration(state.args(1).To_Long_Float) / 1000.0;
    end Sleep_ms;

    ----------------------------------------------------------------------------

    -- string type(val : any)
    procedure Type_Val( state : in out Scribble_State ) is
    begin
        state.result := Create( Image( state.args(1).Get_Type ) );
    end Type_Val;

end Scribble.Stdlib;
