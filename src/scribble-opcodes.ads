--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

package Scribble.Opcodes is

    type Instruction is (
     -- Operation   Argument Type  Operands Results Semantics
     -- ---------   -------- ----- -------- ------- ---------
        NOOP,    --                                 No operation. Does nothing.

        PUSHC,   -- U        load           1       Pushes a copy of a constant value from program data. The U argument
                 --                                 specifies the address in the program data array.

        PUSHI,   -- S        load           1       Pushes an integer onto the stack. The S argument specifies the
                 --                                 integer directly in the opcode (conserves program data space).

        PUSHN,   --          load  2        1       Pushes a reference to a value in a namespace. Two operands are
                 --                                 popped: the namespace (string), and the name to read (string).
                 --                                 The reference into the namespace, or an Error, is pushed as the
                 --                                 result.

        PUSHND,  --          load  2        1       Pushes a value retrieved (by copy) from a namespace. Two operands
                 --                                 are popped: the namespace (string), and the name to read (string).
                 --                                 The value read, or an Error, is pushed as the result.

        PUSHNUL, -- U        load           n       Pushes 'n' number of Null values. The U argument contains the
                 --                                 number of Nulls to push. This serves as a useful optimization when
                 --                                 a series of Null references are pushed as part of an activation
                 --                                 record.

        PUSHS,   -- S        load           1       Pushes a reference to a value from the stack. The S argument
                 --                                 indicates the offset of the slot, relative to the frame pointer.
                 --                                 If the stack slot contains a reference, it will be pushed onto the
                 --                                 top of the stack. Otherwise, a reference to the stack slot itself
                 --                                 will be pushed onto the stack. This is used for assigning to local
                 --                                 variables and parameters, which are stored on the stack as values
                 --                                 and/or references.

        PUSHSD,  -- S        load           1       Pushes a value from the stack. No copy is made. The S argument
                 --                                 indicates the offset of the slot, relative to the frame ponter. If
                 --                                 the stack slot contains a reference, it will be dereferenced and
                 --                                 its target value will be pushed. Otherwise, the value in the slot
                 --                                 itself will be pushed onto the stack. This is used to read local
                 --                                 variables and parameters, which are stored on the stack as values
                 --                                 and/or references.

        PUSHT,   --          load  1        2       Pushes the value on top of the stack back onto the stack without
                 --                                 popping it, effectively creating a duplicate. No copy is made.

        STORE,   -- U        store 2        1       Pops the reference on the top of the stack and replaces its target
                 --                                 value with the new top of the stack, directly underneath it. The U
                 --                                 argument specifies the assignment operation, e.g. direct store, add
                 --                                 and store, multiply and store, etc. The computed value that is
                 --                                 stored into the reference will be left on the stack.

        STORENUL,-- S        store                  Stores a Null into a Reference value on the stack. The S argument
                 --                                 stores the stack offset to write into, relative to the frame
                 --                                 pointer. This is used to clear local variable slots for reuse.

        DEREF,   --          ref   1        1       Pops the reference on the top of the stack and pushes the value
                 --                                 that it references. This is currently unused.

        REF,     --                                 Currently unused; illegal instruction.

        POP,     --                1                Pops the top of the stack and discards it.

        SWAP,    --                2        2       Swaps the two elements on top of the stack.

        INVOKE,  -- U        call  n+1      1       Invokes an internal application function specified by the function
                 --                                 id on top of the stack. The following 'n' operands are passed
                 --                                 (without copy) as arguments, last on top. The opcode's U argument
                 --                                 specifies the number of function arguments passed. The function id
                 --                                 and all arguments are popped before execution returns. The
                 --                                 function's result, or an Error, is pushed as the result.

        EVAL,    -- U        call  n+1      1       Evaluates the Scribble Function value on the top of the stack. The
                 --                                 following 'n' operands are passed (without copy) as arguments, last
                 --                                 on top. The opcode's U argument specifies the number of function
                 --                                 arguments to be passed. All arguments must Reference values to the
                 --                                 actual argument values. The Function and all argument operands are
                 --                                 popped before execution returns. The function's result, or an
                 --                                 Error, is pushed as the result.

        RET,     --                                 Ends evaluation of the current function, returning the top of the
                 --                                 stack as the evaluation result. The stack will be unwound up to,
                 --                                 and including, the caller's arguments. The Function's resultant
                 --                                 value will be on the top of the stack after evaluation. If this is
                 --                                 the initial function in the execution thread, evaluation is
                 --                                 complete.

        JUMP,    -- S        jump                   Jumps unconditionally to another instruction. The S argument
                 --                                 specifies the number of instructions to jump, relative to the
                 --                                 program counter.

        JUMPZ,   -- S        jump  1                Conditionally jumps to another instruction. The value on top of the
                 --                                 stack will be popped. If it equals 0, False, or an Error, the jump
                 --                                 will be performed. The S argument specifies the number of
                 --                                 instructions to jump, relative to the program counter.

        EQ,      --          arith 2        1       Pops two operands, pushing a Boolean indicating if they were equal.

        NEQ,     --          arith 2        1       Pops two operands, pushing a Boolean indicating if they were unequal.

        LT,      --          arith 2        1       Pops two operands, pushing a Boolean indicating if the second was
                 --                                 less than the first.

        GT,      --          arith 2        1       Pops two operands, pushing a Boolean indicating if the second was
                 --                                 greater than the first.

        LTE,     --          arith 2        1       Pops two operands, pushing a Boolean indicating if the second was
                 --                                 less than or equal to the first.

        GTE,     --          arith 2        1       Pops two operands, pushing a Boolean indicating if the second was
                 --                                 greater than or equal to the first.

        BNOT,    --          arith 1        1       Pops one operand and performs a Not operation, pushing the result.

        NEG,     --          arith 1        1       Pops one operand and performs a negation, pushing the result.

        BAND,    --          arith 2        1       Pops two operands and performs an And operation, pushing the result.

        BOR,     --          arith 2        1       Pops two operands and performs an Or operation, pushing the result.

        BXOR,    --          arith 2        1       Pops two operands and performs an Xor operation, pushing the result.

        ADD,     --          arith 2        1       Pops two operands and adds them, pushing the result.

        SUB,     --          arith 2        1       Pops two operands and subtracts them, pushing the result.

        MUL,     --          arith 2        1       Pops two operands and multiplies them, pushing the result.

        DIV,     --          arith 2        1       Pops two operands and divides them, pushing the result.

        MODULO,  --          arith 2        1       Pops two operands and performs a modulo operation, pushing the result.

        POW,     --          arith 2        1       Pops two operands and performs exponentiation, pushing the result.

        CONCAT,  --          arith 2        1       Pops two operands and performs string concatenation, pushing the result.

        SIGN,    --          arith 1        1       Pops one operand and performs a Sign operation, pushing the result.

        YIELD    --                                 Yields execution control back to the VM.
    );
    for Instruction use (
        NOOP     =>  0,
        PUSHC    =>  1,
        PUSHI    =>  2,
        PUSHN    =>  3,
        PUSHND   =>  4,
        PUSHNUL  =>  5,
        PUSHS    =>  6,
        PUSHSD   =>  7,
        PUSHT    =>  8,
        STORE    =>  9,
        STORENUL => 10,
        DEREF    => 11,
        REF      => 12,
        POP      => 13,
        SWAP     => 14,
        INVOKE   => 15,
        EVAL     => 16,
        RET      => 17,
        JUMP     => 18,
        JUMPZ    => 19,
        EQ       => 20,
        NEQ      => 21,
        LT       => 22,
        GT       => 23,
        LTE      => 24,
        GTE      => 25,
        BNOT     => 26,
        NEG      => 27,
        BAND     => 28,
        BOR      => 29,
        BXOR     => 30,
        ADD      => 31,
        SUB      => 32,
        MUL      => 33,
        DIV      => 34,
        MODULO   => 35,
        POW      => 36,
        CONCAT   => 37,
        SIGN     => 38,
        YIELD    => 39
    );
    for Instruction'Size use 8;

    type Args_Format is (Format_S, Format_U, Format_None);

    -- Returns the arguments format used by Instruction 'inst'.
    function Instruction_Format( inst : Instruction ) return Args_Format;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A Scribble Opcode is a 32 bit number comprised of a 6 bit Instruction and
    -- one argument. The type of the argument depends on the Instruction. Opcode
    -- formats are as follows:
    --
    -- [ inst ] [ unused               ]
    -- [ inst ] [ S argument           ]  S = 26 bit signed integer
    -- [ inst ] [ U argument           ]  U = 26 bit unsigned integer
    -- 0      5 6                     31
    --         <-- 32 Bits -->
    --
    -- The signed S argument is actually stored as an unsigned integer with a
    -- bias of 2**25-1. Thus, 0 is stored as 33,554,431.

    subtype Opcode is Unsigned_32;

    -- Creates and returns an Opcode without an argument.
    function To_Opcode( inst : Instruction ) return Opcode;

    -- Creates and returns an S-format Opcode. A Constraint_Error will be raised
    -- if 's' is too large or small to fit.
    function To_Opcode_S( inst : Instruction; s : Integer ) return Opcode;

    -- Creates and returns a U-format Opcode. A Constraint_Error will be raised
    -- if 'u' is too large to fit.
    function To_Opcode_U( inst : Instruction; u : Natural ) return Opcode;

    -- Returns the opcode's instruction.
    function Get_Instruction( op : Opcode ) return Instruction with Inline;

    -- Returns the opcode's S argument as a signed integer.
    function Get_S( op : Opcode ) return Integer with Inline;

    -- Returns the opcode's U argument as an unsigned integer.
    function Get_U( op : Opcode ) return Natural with Inline;

    -- Returns a string representation of the Opcode for debugging purposes.
    function Image( op : Opcode ) return String;

    -- Returns the maximum absolute value that can fit in the S argument of an
    -- Opcode.
    function Max_S return Integer with Inline;

    -- Returns the maximum value that can fit in the U argument of an Opcode.
    function Max_U return Integer with Inline;

    -- Sets an opcode's instruction.
    procedure Set_Instruction( op : in out Opcode; inst : Instruction ) with Inline;

    -- Sets an opcode's 'S' argument, a 26 bit signed integer. A
    -- Constraint_Error will be raised if 's' is too large or small to fit.
    procedure Set_S( op : in out Opcode; s : Integer ) with Inline;

    -- Sets an opcode's 'U' argument, a 26 bit unsigned integer. A
    -- Constraint_Error will be raised if 'u' is too large to fit.
    procedure Set_U( op : in out Opcode; u : Natural ) with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Opcode_Array is array (Integer range <>) of Opcode;
    type A_Opcode_Array is access all Opcode_Array;

    procedure Delete( oa : in out A_Opcode_Array );

end Scribble.Opcodes;
