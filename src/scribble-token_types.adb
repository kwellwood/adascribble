--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

package body Scribble.Token_Types is

    subtype Character_Type is Unsigned_32;

    CT_NORMAL     : constant Character_Type := 0;
    CT_DELIMITER  : constant Character_Type := 1;

    type Character_Type_Array is array (0..255) of Character_Type;

    charTypes : constant Character_Type_Array := Character_Type_Array'(
        -- ASCII characters
          0 => CT_NORMAL                ,  --   0 NUL
          1 => CT_NORMAL                ,  --   1
          2 => CT_NORMAL                ,  --   2
          3 => CT_NORMAL                ,  --   3
          4 => CT_NORMAL                ,  --   4
          5 => CT_NORMAL                ,  --   5
          6 => CT_NORMAL                ,  --   6
          7 => CT_NORMAL                ,  --   7
          8 => CT_NORMAL                ,  --   8
          9 => CT_NORMAL or CT_DELIMITER,  --   9 TAB
         10 => CT_NORMAL or CT_DELIMITER,  --  10 LF
         11 => CT_NORMAL                ,  --  11
         12 => CT_NORMAL                ,  --  12
         13 => CT_NORMAL or CT_DELIMITER,  --  13 CR
         14 => CT_NORMAL                ,  --  14
         15 => CT_NORMAL                ,  --  15
         16 => CT_NORMAL                ,  --  16
         17 => CT_NORMAL                ,  --  17
         18 => CT_NORMAL                ,  --  18
         19 => CT_NORMAL                ,  --  19
         20 => CT_NORMAL                ,  --  20
         21 => CT_NORMAL                ,  --  21
         22 => CT_NORMAL                ,  --  22
         23 => CT_NORMAL                ,  --  23
         24 => CT_NORMAL                ,  --  24
         25 => CT_NORMAL                ,  --  25
         26 => CT_NORMAL                ,  --  26
         27 => CT_NORMAL                ,  --  27
         28 => CT_NORMAL                ,  --  28
         29 => CT_NORMAL                ,  --  29
         30 => CT_NORMAL                ,  --  30
         31 => CT_NORMAL                ,  --  31
         32 => CT_NORMAL or CT_DELIMITER,  --  32 Space
         33 => CT_NORMAL or CT_DELIMITER,  --  33 !
         34 => CT_NORMAL                ,  --  34 "
         35 => CT_NORMAL                ,  --  35 #
         36 => CT_NORMAL                ,  --  36 $
         37 => CT_NORMAL or CT_DELIMITER,  --  37 %
         38 => CT_NORMAL or CT_DELIMITER,  --  38 &
         39 => CT_NORMAL                ,  --  39 '
         40 => CT_NORMAL or CT_DELIMITER,  --  40 (
         41 => CT_NORMAL or CT_DELIMITER,  --  41 )
         42 => CT_NORMAL or CT_DELIMITER,  --  42 *
         43 => CT_NORMAL or CT_DELIMITER,  --  43 +
         44 => CT_NORMAL or CT_DELIMITER,  --  44 ,
         45 => CT_NORMAL or CT_DELIMITER,  --  45 -
         46 => CT_NORMAL or CT_DELIMITER,  --  46 .
         47 => CT_NORMAL or CT_DELIMITER,  --  47 /
         48 => CT_NORMAL                ,  --  48 0
         49 => CT_NORMAL                ,  --  49 1
         50 => CT_NORMAL                ,  --  50 2
         51 => CT_NORMAL                ,  --  51 3
         52 => CT_NORMAL                ,  --  52 4
         53 => CT_NORMAL                ,  --  53 5
         54 => CT_NORMAL                ,  --  54 6
         55 => CT_NORMAL                ,  --  55 7
         56 => CT_NORMAL                ,  --  56 8
         57 => CT_NORMAL                ,  --  57 9
         58 => CT_NORMAL or CT_DELIMITER,  --  58 :
         59 => CT_NORMAL or CT_DELIMITER,  --  59 ;
         60 => CT_NORMAL or CT_DELIMITER,  --  60 <
         61 => CT_NORMAL or CT_DELIMITER,  --  61 =
         62 => CT_NORMAL or CT_DELIMITER,  --  62 >
         63 => CT_NORMAL or CT_DELIMITER,  --  63 ?
         64 => CT_NORMAL or CT_DELIMITER,  --  64 @
         65 => CT_NORMAL                ,  --  65 A
         66 => CT_NORMAL                ,  --  66 B
         67 => CT_NORMAL                ,  --  67 C
         68 => CT_NORMAL                ,  --  68 D
         69 => CT_NORMAL                ,  --  69 E
         70 => CT_NORMAL                ,  --  70 F
         71 => CT_NORMAL                ,  --  71 G
         72 => CT_NORMAL                ,  --  72 H
         73 => CT_NORMAL                ,  --  73 I
         74 => CT_NORMAL                ,  --  74 J
         75 => CT_NORMAL                ,  --  75 K
         76 => CT_NORMAL                ,  --  76 L
         77 => CT_NORMAL                ,  --  77 M
         78 => CT_NORMAL                ,  --  78 N
         79 => CT_NORMAL                ,  --  79 O
         80 => CT_NORMAL                ,  --  80 P
         81 => CT_NORMAL                ,  --  81 Q
         82 => CT_NORMAL                ,  --  82 R
         83 => CT_NORMAL                ,  --  83 S
         84 => CT_NORMAL                ,  --  84 T
         85 => CT_NORMAL                ,  --  85 U
         86 => CT_NORMAL                ,  --  86 V
         87 => CT_NORMAL                ,  --  87 W
         88 => CT_NORMAL                ,  --  88 X
         89 => CT_NORMAL                ,  --  89 Y
         90 => CT_NORMAL                ,  --  90 Z
         91 => CT_NORMAL or CT_DELIMITER,  --  91 [
         92 => CT_NORMAL                ,  --  92 backslash
         93 => CT_NORMAL or CT_DELIMITER,  --  93 ]
         94 => CT_NORMAL or CT_DELIMITER,  --  94 ^
         95 => CT_NORMAL                ,  --  95 _
         96 => CT_NORMAL or CT_DELIMITER,  --  96 `
         97 => CT_NORMAL                ,  --  97 a
         98 => CT_NORMAL                ,  --  98 b
         99 => CT_NORMAL                ,  --  99 c
        100 => CT_NORMAL                ,  -- 100 d
        101 => CT_NORMAL                ,  -- 101 e
        102 => CT_NORMAL                ,  -- 102 f
        103 => CT_NORMAL                ,  -- 103 g
        104 => CT_NORMAL                ,  -- 104 h
        105 => CT_NORMAL                ,  -- 105 i
        106 => CT_NORMAL                ,  -- 106 j
        107 => CT_NORMAL                ,  -- 107 k
        108 => CT_NORMAL                ,  -- 108 l
        109 => CT_NORMAL                ,  -- 109 m
        110 => CT_NORMAL                ,  -- 110 n
        111 => CT_NORMAL                ,  -- 111 o
        112 => CT_NORMAL                ,  -- 112 p
        113 => CT_NORMAL                ,  -- 113 q
        114 => CT_NORMAL                ,  -- 114 r
        115 => CT_NORMAL                ,  -- 115 s
        116 => CT_NORMAL                ,  -- 116 t
        117 => CT_NORMAL                ,  -- 117 u
        118 => CT_NORMAL                ,  -- 118 v
        119 => CT_NORMAL                ,  -- 119 w
        120 => CT_NORMAL                ,  -- 120 x
        121 => CT_NORMAL                ,  -- 121 y
        122 => CT_NORMAL                ,  -- 122 z
        123 => CT_NORMAL or CT_DELIMITER,  -- 123 {
        124 => CT_NORMAL                ,  -- 124 |
        125 => CT_NORMAL or CT_DELIMITER,  -- 125 }
        126 => CT_NORMAL                ,  -- 126 ~
        127 => CT_NORMAL                ,  -- 127

        -- Extended Codes...
        others => CT_NORMAL
    );

    ----------------------------------------------------------------------------

    function Image( t : Token_Type ) return String is (
        case t is
            when TK_CONSTANT       => "constant",
            when TK_FUNCTION       => "function",
            when TK_DEFAULT        => "default",
            when TK_INCLUDE        => "include",
            when TK_REQUIRE        => "require",
            when TK_MEMBER         => "member",
            when TK_RETURN         => "return",
            when TK_BLOCK          => "block",
            when TK_ELSIF          => "elsif",
            when TK_EVERY          => "every",
            when TK_LOCAL          => "local",
            when TK_TIMER          => "timer",
            when TK_WHILE          => "while",
            when TK_YIELD          => "yield",
            when TK_ELSE           => "else",
            when TK_EXIT           => "exit",
            when TK_LOOP           => "loop",
            when TK_SELF           => "self",
            when TK_STEP           => "step",
            when TK_THEN           => "then",
            when TK_THIS           => "this",
            when TK_WHEN           => "when",
            when TK_AND            => "and",
            when TK_END            => "end",
            when TK_FOR            => "for",
            when TK_XOR            => "xor",
            when TK_IF             => "if",
            when TK_IN             => "in",
            when TK_IS             => "is",
            when TK_OF             => "of",
            when TK_OR             => "or",
            when TK_TO             => "to",
            when TK_COLON_EQUALS   => ":=",
            when TK_PLUS_EQUALS    => "+=",
            when TK_MINUS_EQUALS   => "-=",
            when TK_STAR_EQUALS    => "*=",
            when TK_SLASH_EQUALS   => "/=",
            when TK_AMP_EQUALS     => "&=",
            when TK_GREATER_EQUALS => ">=",
            when TK_LESS_EQUALS    => "<=",
            when TK_BANG_EQUALS    => "!=",
            when TK_DOUBLE_LESS    => "<<",
            when TK_DOUBLE_GREATER => ">>",
            when TK_DOUBLE_COLON   => "::",
            when TK_GREATER        => ">",
            when TK_LESS           => "<",
            when TK_EQUALS         => "=",
            when TK_MINUS          => "-",
            when TK_BANG           => "!",
            when TK_STAR           => "*",
            when TK_SLASH          => "/",
            when TK_PERCENT        => "%",
            when TK_PLUS           => "+",
            when TK_AMP            => "&",
            when TK_AT             => "@",
            when TK_CARAT          => "^",
            when TK_LPAREN         => "(",
            when TK_RPAREN         => ")",
            when TK_LBRACE         => "{",
            when TK_RBRACE         => "}",
            when TK_LBRACKET       => "[",
            when TK_RBRACKET       => "]",

            when TK_QUESTION       => "?",
            when TK_COLON          => ":",
            when TK_SEMICOLON      => ";",
            when TK_COMMA          => ",",
            when TK_DOT            => ".",

            when TK_VALUE          => "",
            when TK_SYMBOL         => "",

            when TK_EOL            => String'(1 => ASCII.LF),

            when TK_EOF            => ""
    );

    ----------------------------------------------------------------------------

    function Name( t : Token_Type ) return String is (
        case t is
            when TK_CONSTANT       => "constant",
            when TK_FUNCTION       => "function",
            when TK_DEFAULT        => "default",
            when TK_INCLUDE        => "include",
            when TK_REQUIRE        => "require",
            when TK_MEMBER         => "member",
            when TK_RETURN         => "return",
            when TK_BLOCK          => "block",
            when TK_ELSIF          => "elsif",
            when TK_EVERY          => "every",
            when TK_LOCAL          => "local",
            when TK_TIMER          => "timer",
            when TK_WHILE          => "while",
            when TK_YIELD          => "yield",
            when TK_ELSE           => "else",
            when TK_EXIT           => "exit",
            when TK_LOOP           => "loop",
            when TK_SELF           => "self",
            when TK_STEP           => "step",
            when TK_THEN           => "then",
            when TK_THIS           => "this",
            when TK_WHEN           => "when",
            when TK_AND            => "and",
            when TK_END            => "end",
            when TK_FOR            => "for",
            when TK_XOR            => "xor",
            when TK_IF             => "if",
            when TK_IN             => "in",
            when TK_IS             => "is",
            when TK_OF             => "of",
            when TK_OR             => "or",
            when TK_TO             => "to",
            when TK_COLON_EQUALS   => ":=",
            when TK_PLUS_EQUALS    => "+=",
            when TK_MINUS_EQUALS   => "-=",
            when TK_STAR_EQUALS    => "*=",
            when TK_SLASH_EQUALS   => "/=",
            when TK_AMP_EQUALS     => "&=",
            when TK_GREATER_EQUALS => ">=",
            when TK_LESS_EQUALS    => "<=",
            when TK_BANG_EQUALS    => "!=",
            when TK_DOUBLE_LESS    => "<<",
            when TK_DOUBLE_GREATER => ">>",
            when TK_DOUBLE_COLON   => "::",
            when TK_GREATER        => ">",
            when TK_LESS           => "<",
            when TK_EQUALS         => "=",
            when TK_MINUS          => "-",
            when TK_BANG           => "!",
            when TK_STAR           => "*",
            when TK_SLASH          => "/",
            when TK_PERCENT        => "%",
            when TK_PLUS           => "+",
            when TK_AMP            => "&",
            when TK_AT             => "@",
            when TK_CARAT          => "^",
            when TK_LPAREN         => "(",
            when TK_RPAREN         => ")",
            when TK_LBRACE         => "{",
            when TK_RBRACE         => "}",
            when TK_LBRACKET       => "[",
            when TK_RBRACKET       => "]",

            when TK_QUESTION       => "?",
            when TK_COLON          => ":",
            when TK_SEMICOLON      => ";",
            when TK_COMMA          => ",",
            when TK_DOT            => ".",

            when TK_VALUE          => "value",
            when TK_SYMBOL         => "symbol",

            when TK_EOL            => "linefeed",

            when TK_EOF            => "EOF"
    );

    ----------------------------------------------------------------------------

    procedure To_Token_Type( img : String; t : out Token_Type; valid : out Boolean ) is
    begin
        t := TK_EOF;
        valid := False;

        for tt in Token_Type'Range loop
            if img = Image( tt ) then
                t := tt;
                valid := True;
                exit;
            end if;
        end loop;
    end To_Token_Type;

    ----------------------------------------------------------------------------

    function Is_Delimiter( c : Character ) return Boolean is ((charTypes(Character'Pos( c )) and CT_DELIMITER) = CT_DELIMITER);

end Scribble.Token_Types;
