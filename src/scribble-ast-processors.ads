--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;

package Scribble.Ast.Processors is

    -- The Ast_Processor interface class examines nodes in the abstract syntax
    -- tree structure, following the Visitor design pattern. Traversal must be
    -- implemented by the subclass.
    --
    -- Examination begins by passing an Ast_Processor to the Process() procedure
    -- of an Ast_Node.
    type Ast_Processor is limited interface;
    type A_Ast_Processor is access all Ast_Processor'Class;

    -- expressions: operands
    procedure Process_Literal( this : access Ast_Processor; node : A_Ast_Literal ) is null;
    procedure Process_Map( this : access Ast_Processor; node : A_Ast_Map ) is abstract;
    procedure Process_List( this : access Ast_Processor; node : A_Ast_List ) is abstract;
    procedure Process_Function_Call( this : access Ast_Processor; node : A_Ast_Function_Call ) is abstract;
    procedure Process_Member_Call( this : access Ast_Processor; node : A_Ast_Member_Call ) is abstract;
    procedure Process_Self( this : access Ast_Processor; node : A_Ast_Self ) is abstract;
    procedure Process_This( this : access Ast_Processor; node : A_Ast_This ) is abstract;
    procedure Process_Builtin( this : access Ast_Processor; node : A_Ast_Builtin ) is abstract;
    procedure Process_Identifier( this : access Ast_Processor; node : A_Ast_Identifier ) is abstract;
    procedure Process_Membership( this : access Ast_Processor; node : A_Ast_Membership ) is abstract;
    procedure Process_Name_Expr( this : access Ast_Processor; node : A_Ast_Name_Expr ) is abstract;

    -- expressions: operators
    procedure Process_Unary_Op( this : access Ast_Processor; node : A_Ast_Unary_Op ) is abstract;
    procedure Process_Reference( this : access Ast_Processor; node : A_Ast_Reference ) is abstract;
    procedure Process_Binary_Op( this : access Ast_Processor; node : A_Ast_Binary_Op ) is abstract;
    procedure Process_Assign( this : access Ast_Processor; node : A_Ast_Assign ) is abstract;
    procedure Process_Index( this : access Ast_Processor; node : A_Ast_Index ) is abstract;
    procedure Process_Conditional( this : access Ast_Processor; node : A_Ast_Conditional ) is abstract;

    -- expressions: function definition
    procedure Process_Function_Definition( this : access Ast_Processor; node : A_Ast_Function_Definition ) is abstract;

    -- statements
    procedure Process_Block( this : access Ast_Processor; node : A_Ast_Block ) is abstract;
    procedure Process_Exit( this : access Ast_Processor; node : A_Ast_Exit ) is abstract;
    procedure Process_Expression_Statement( this : access Ast_Processor; node : A_Ast_Expression_Statement ) is abstract;
    procedure Process_If( this : access Ast_Processor; node : A_Ast_If ) is abstract;
    procedure Process_Loop( this : access Ast_Processor; node : A_Ast_Loop ) is abstract;
    procedure Process_Return( this : access Ast_Processor; node : A_Ast_Return ) is abstract;
    procedure Process_Var( this : access Ast_Processor; node : A_Ast_Var ) is abstract;
    procedure Process_Yield( this : access Ast_Processor; node : A_Ast_Yield ) is abstract;

end Scribble.Ast.Processors;
