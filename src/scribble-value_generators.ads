--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Code_Generators;          use Scribble.Code_Generators;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values;                            use Values;
with Values.Functions;                  use Values.Functions;

private package Scribble.Value_Generators is

    -- The Scribble value generator builds a Value from a fully verified
    -- abstract syntax tree.
    type Value_Generator is limited new Ast_Processor with private;
    type A_Value_Generator is access all Value_Generator'Class;

    -- Creates a new instance of the Scribble value generator. The Scribble
    -- runtime provides information about the existence of directly exported
    -- Ada functions.
    function Create_Value_Generator( runtime : not null A_Scribble_Runtime ) return A_Value_Generator;
    pragma Postcondition( Create_Value_Generator'Result /= null );

    -- Generates and returns a value from literal expression AST 'node'. If
    -- 'enableDebug' is True, debugging information will be included in all
    -- generated Functions. A Parse_Error will be raised if an error is
    -- encountered during code generation (e.g. max program data size is exceeded.)
    --
    -- The AST node is expected to have been parsed from a Scribble Value term;
    -- it must be an Ast_Literal, Ast_List, Ast_Map, or Ast_Function_Definition.
    function Generate( this        : not null access Value_Generator'Class;
                       node        : not null A_Ast_Expression;
                       enableDebug : Boolean := False ) return Value;
    pragma Precondition( node.Get_Type in Ast_Value_Type );

    procedure Delete( this : in out A_Value_Generator );
    pragma Postcondition( this = null );

private

    type Value_Generator is limited new Ast_Processor with
        record
            runtime     : A_Scribble_Runtime := null;
            codeGen     : A_Code_Generator := null;
            enableDebug : Boolean := False;
            result      : Value;
        end record;

    -- expressions: operands
    procedure Process_Literal( this : access Value_Generator; node : A_Ast_Literal );
    procedure Process_Map( this : access Value_Generator; node : A_Ast_Map );
    procedure Process_List( this : access Value_Generator; node : A_Ast_List );
    procedure Process_Function_Call( this : access Value_Generator; node : A_Ast_Function_Call ) is null;
    procedure Process_Member_Call( this : access Value_Generator; node : A_Ast_Member_Call ) is null;
    procedure Process_Self( this : access Value_Generator; node : A_Ast_Self ) is null;
    procedure Process_This( this : access Value_Generator; node : A_Ast_This ) is null;
    procedure Process_Builtin( this : access Value_Generator; node : A_Ast_Builtin ) is null;
    procedure Process_Identifier( this : access Value_Generator; node : A_Ast_Identifier ) is null;
    procedure Process_Membership( this : access Value_Generator; node : A_Ast_Membership ) is null;
    procedure Process_Name_Expr( this : access Value_Generator; node : A_Ast_Name_Expr ) is null;

    -- expressions: operators
    procedure Process_Unary_Op( this : access Value_Generator; node : A_Ast_Unary_Op ) is null;
    procedure Process_Reference( this : access Value_Generator; node : A_Ast_Reference ) is null;
    procedure Process_Binary_Op( this : access Value_Generator; node : A_Ast_Binary_Op ) is null;
    procedure Process_Assign( this : access Value_Generator; node : A_Ast_Assign ) is null;
    procedure Process_Index( this : access Value_Generator; node : A_Ast_Index ) is null;
    procedure Process_Conditional( this : access Value_Generator; node : A_Ast_Conditional ) is null;

    -- expressions: function definition
    procedure Process_Function_Definition( this : access Value_Generator; node : A_Ast_Function_Definition );

    -- statements
    procedure Process_Block( this : access Value_Generator; node : A_Ast_Block ) is null;
    procedure Process_Exit( this : access Value_Generator; node : A_Ast_Exit ) is null;
    procedure Process_Expression_Statement( this : access Value_Generator; node : A_Ast_Expression_Statement ) is null;
    procedure Process_If( this : access Value_Generator; node : A_Ast_If ) is null;
    procedure Process_Loop( this : access Value_Generator; node : A_Ast_Loop ) is null;
    procedure Process_Return( this : access Value_Generator; node : A_Ast_Return ) is null;
    procedure Process_Var( this : access Value_Generator; node : A_Ast_Var ) is null;
    procedure Process_Yield( this : access Value_Generator; node : A_Ast_Yield ) is null;

end Scribble.Value_Generators;
