--
-- Copyright (c) 2013-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Operations.Generics is

    package Unary_Negate is new Generic_Unary( Negate_Op );
    package Unary_Not    is new Generic_Unary( Not_Op );
    package Unary_Sign   is new Generic_Unary( Sign_Op );

    ----------------------------------------------------------------------------

    package Binary_Add       is new Generic_Binary( Add_Op );
    package Binary_And       is new Generic_Binary( And_Op );
    package Binary_Concat    is new Generic_Binary( Concat_Op );
    package Binary_Divide    is new Generic_Binary( Divide_Op );
    package Binary_In        is new Generic_Binary( In_Op );
    package Binary_Index     is new Generic_Binary( Index_Op );
    package Binary_Index_Ref is new Generic_Binary( Index_Ref_Op );
    package Binary_Modulo    is new Generic_Binary( Modulo_Op );
    package Binary_Multiply  is new Generic_Binary( Multiply_Op );
    package Binary_Or        is new Generic_Binary( Or_Op );
    package Binary_Power     is new Generic_Binary( Power_Op );
    package Binary_Subtract  is new Generic_Binary( Subtract_Op );
    package Binary_Xor       is new Generic_Binary( Xor_Op );

end Values.Operations.Generics;
