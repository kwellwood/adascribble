--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Indirects is

    generic
        type Storage (<>) is abstract tagged limited private;
    package Generics is

        -- Member function of Storage class that queries a value stored by
        -- 'object'. The specific value with in the storage is implied by the
        -- callback.
        type A_Implicit_Reader is access
            function( object : not null access Storage'Class ) return Value;

        -- Member function of Storage class that sets a specific value stored by
        -- 'object'. The specific value with in the storage is implied by the
        -- callback.
        type A_Implicit_Writer is access
            procedure( object : not null access Storage'Class; val : Value'Class );

        -- Member function of Storage class that queries 'object' for a value
        -- that is explicitly identified by 'name'.
        type A_Explicit_Reader is access
            function( object : not null access Storage'Class;
                      name   : String ) return Value;

        -- Member function Storage class that sets a value stored by 'object'
        -- that is explictly identified by 'name'.
        type A_Explicit_Writer is access
            procedure( object : not null access Storage'Class;
                       name   : String;
                       val    : Value'Class );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        -- Creates a new indirect reference value that redirects read and write
        -- operations (.Deref and .Replace) to 'reader' and 'writer' callbacks.
        -- Pass 'writer' as null to make the value read-only.
        function Create_Indirect( obj    : access Storage'Class;
                                  reader : not null A_Implicit_Reader;
                                  writer : A_Implicit_Writer ) return Value;

        -- Creates a new indirect reference value that redirects read and write
        -- operations (.Deref and .Replace) to 'reader' and 'writer' callbacks.
        -- Pass 'writer' as null to make the value read-only. 'name' will be
        -- passed to 'reader' and 'writer' to explicitly specify a value within
        -- the storage object for the operation.
        function Create_Indirect( obj    : access Storage'Class;
                                  reader : not null A_Explicit_Reader;
                                  writer : A_Explicit_Writer;
                                  name   : String ) return Value;

    end Generics;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    procedure Delete_Indirect( obj : Address );

    function Image_Indirect( this : Value'Class ) return String;
    pragma Precondition( this.Is_Ref );

    function Read_Indirect( this : Value'Class ) return Value;

    procedure Write_Indirect( this : Value'Class; val : Value'Class );

private

    type Indirection is tagged null record;
    type A_Indirection is access all Indirection'Class;
    pragma No_Strict_Aliasing( A_Indirection );

    function Read( this : Indirection ) return Value is (Null_Value);

    procedure Write( this : Indirection; val : Value'Class ) is null;

end Values.Indirects;
