--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Containers.Ordered_Maps;
with System.Address_To_Access_Conversions;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Streaming;                  use Values.Streaming;
with Values.Strings;                    use Values.Strings;

package body Values.Maps is

    package Value_Maps is new Ada.Containers.Ordered_Maps( Unbounded_String, A_Value, "<", "=" );
    use Value_Maps;

    type Map_Data is
        record
            obj   : Object_Refs;
            pairs : Value_Maps.Map;
        end record;
    for Map_Data use
        record
            obj at 0 range 0..31;
        end record;

    ----------------------------------------------------------------------------

    package Accesses is new System.Address_To_Access_Conversions( Map_Data );
    use Accesses;

    --==========================================================================

    function Create_Map return Value is
        this : Value := Value'(Controlled with v => TAG_MAP);
        data : constant Object_Pointer := new Map_Data;
    begin
        this.Set_Object( To_Address( data ) );
        return this;
    end Create_Map;

    ----------------------------------------------------------------------------

    function Create( pairs : Pair_Array; consume : Boolean := False ) return Value is
        data : constant Object_Pointer := new Map_Data;
        this : Value := Value'(Controlled with v => TAG_MAP);
        val  : A_Value;
    begin
        for i in pairs'Range loop
            if not pairs(i).val.Is_Null then
                val := new Value'(if consume then pairs(i).val else Clone( pairs(i).val ));
                -- raises an exception if the key already exists
                data.pairs.Insert( pairs(i).key, val );
            end if;
        end loop;
        this.Set_Object( To_Address( data ) );
        return this;
    exception
        when others =>
            Free( val );
            Delete_Map( To_Address( data ) );
            val := null;
            raise;
    end Create;

    --==========================================================================

    function As_Map( this : Value'Class ) return Map_Value is
        (Map_Value'(Value'(Value(this)) with data => A_Map_Data(To_Pointer( this.Get_Address ))));

    ----------------------------------------------------------------------------

    function Contains( this : Map_Value'Class; field : Unbounded_String ) return Boolean
        is (this.data.pairs.Contains( field ));

    ----------------------------------------------------------------------------

    function Get( this : Map_Value'Class; field : Unbounded_String ) return Value is
        pos : constant Cursor := this.data.pairs.Find( field );
    begin
        if Has_Element( pos ) then
            return Element( pos ).all;
        end if;
        return Null_Value;
    end Get;

    ----------------------------------------------------------------------------

    function Get_Boolean( this    : Map_Value'Class;
                          field   : Unbounded_String;
                          default : Boolean := False ) return Boolean
        is (Cast_Boolean( this.Get( field ), default ));

    function Get_Boolean( this    : Map_Value'Class;
                          field   : String;
                          default : Boolean := False ) return Boolean
        is (Cast_Boolean( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_Int( this    : Map_Value'Class;
                      field   : Unbounded_String;
                      default : Integer := 0 ) return Integer
        is (Cast_Int( this.Get( field ), default ));

    function Get_Int( this    : Map_Value'Class;
                      field   : String;
                      default : Integer := 0 ) return Integer
        is (Cast_Int( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_Float( this    : Map_Value'Class;
                        field   : Unbounded_String;
                        default : Float := 0.0 ) return Float
        is (Cast_Float( this.Get( field ), default ));

    function Get_Float( this    : Map_Value'Class;
                        field   : String;
                        default : Float := 0.0 ) return Float
        is (Cast_Float( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_Long_Float( this    : Map_Value'Class;
                             field   : Unbounded_String;
                             default : Long_Float := 0.0 ) return Long_Float
        is (Cast_Long_Float( this.Get( field ), default ));

    function Get_Long_Float( this    : Map_Value'Class;
                             field   : String;
                             default : Long_Float := 0.0 ) return Long_Float
        is (Cast_Long_Float( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_String( this    : Map_Value'Class;
                         field   : Unbounded_String;
                         default : String := "" ) return String
        is (Cast_String( this.Get( field ), default ));

    function Get_String( this    : Map_Value'Class;
                         field   : String;
                         default : String := "" ) return String
        is (Cast_String( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_Unbounded_String( this    : Map_Value'Class;
                                   field   : Unbounded_String;
                                   default : String := "" ) return Unbounded_String
        is (Cast_Unbounded_String( this.Get( field ), default ));

    function Get_Unbounded_String( this    : Map_Value'Class;
                                   field   : String;
                                   default : String := "" ) return Unbounded_String
        is (Cast_Unbounded_String( this.Get( To_Unbounded_String( field ) ), default ));

    function Get_Unbounded_String( this    : Map_Value'Class;
                                   field   : Unbounded_String;
                                   default : Unbounded_String ) return Unbounded_String
        is (Cast_Unbounded_String( this.Get( field ), default ));

    function Get_Unbounded_String( this    : Map_Value'Class;
                                   field   : String;
                                   default : Unbounded_String ) return Unbounded_String
        is (Cast_Unbounded_String( this.Get( To_Unbounded_String( field ) ), default ));

    ----------------------------------------------------------------------------

    function Get_Keys( this : Map_Value'Class ) return Value is
        keys : Value_Array(1..Integer(this.data.pairs.Length));
        pos  : Cursor := this.data.pairs.First;
    begin
        for i in keys'Range loop
            keys(i) := Create( Key( pos ) );
            Next( pos );
        end loop;
        return Create_List( keys, consume => True );
    end Get_Keys;

    ----------------------------------------------------------------------------

    function Has_Value( this : Map_Value'Class; val : Value'Class ) return Boolean is
    begin
        for v of this.data.pairs loop
            if Value(val) = v.all then
                return True;
            end if;
        end loop;
        return False;
    end Has_Value;

    ----------------------------------------------------------------------------

    function Is_Empty( this : Map_Value'Class ) return Boolean is (this.data.pairs.Is_Empty);

    ----------------------------------------------------------------------------

    procedure Iterate( this    : Map_Value'Class;
                       examine : not null access procedure( key : String;
                                                            val : Value ) ) is
        pos : Cursor := this.data.pairs.First;
    begin
        while Has_Element( pos ) loop
            examine.all( To_String( Key( pos ) ), Element( pos ).all );
            Next( pos );
        end loop;
    end Iterate;

    ----------------------------------------------------------------------------

    procedure Merge( this : Map_Value'Class; from : Value'Class ) is
        fromPos : Cursor;
    begin
        if not from.Is_Map then
            return;
        end if;
        fromPos := As_Map( from ).data.pairs.First;
        while Has_Element( fromPos ) loop
            this.Set( Key( fromPos ), Element( fromPos ).all, consume => False );
            Next( fromPos );
        end loop;
    end Merge;

    ----------------------------------------------------------------------------

    function Merge( base : Map_Value'Class; overrides : Value'Class ) return Value is
        val  : constant Value := Create_Map;
        data : constant Object_Pointer := To_Pointer( val.Get_Address );
        pos  : Cursor := base.data.pairs.First;
    begin
        -- create a clone of base's data
        while Has_Element( pos ) loop
            data.pairs.Insert( Key( pos ), new Value'(Clone( Element( pos ).all )) );
            Next( pos );
        end loop;

        As_Map( val ).Merge( overrides );
        return val;
    end Merge;

    ----------------------------------------------------------------------------

    procedure Recursive_Merge( this : Map_Value'Class; from : Value'Class ) is
        fromPos, toPos : Cursor;
    begin
        if not from.Is_Map then
            return;
        end if;

        fromPos := As_Map( from ).data.pairs.First;
        while Has_Element( fromPos ) loop
            toPos := this.data.pairs.Find( Key( fromPos ) );
            if Has_Element( toPos ) then
                if Element( toPos ).Is_Map and then Element( fromPos ).Is_Map then
                    -- merge matching map into this
                    As_Map( Element( toPos ).all ).Recursive_Merge( Element( fromPos ).all );
                else
                    -- override matching field in this
                    Element( toPos ).all := Clone( Element( fromPos ).all );
                end if;
            else
                -- include unmatched field in this
                this.data.pairs.Insert( Key( fromPos ), new Value'(Clone( Element( fromPos ).all )) );
            end if;

            Next( fromPos );
        end loop;
    end Recursive_Merge;

    ----------------------------------------------------------------------------

    function Recursive_Merge( base : Map_Value'Class; overrides : Value'Class ) return Value is
        val  : constant Value := Create_Map;
        data : constant Object_Pointer := To_Pointer( val.Get_Address );
        pos  : Cursor := base.data.pairs.First;
    begin
        -- create a clone of base's data
        while Has_Element( pos ) loop
            data.pairs.Insert( Key( pos ), new Value'(Clone( Element( pos ).all )) );
            Next( pos );
        end loop;

        As_Map( val ).Recursive_Merge( overrides );
        return val;
    end Recursive_Merge;

    ----------------------------------------------------------------------------

    function Reference( this          : Map_Value'Class;
                        field         : Unbounded_String;
                        createMissing : Boolean := True ) return Value is
        pos      : Cursor := this.data.pairs.Find( field );
        inserted : Boolean;
    begin
        if Has_Element( pos ) then
            return Create_Ref( Element( pos ) );
        elsif createMissing then
            this.data.pairs.Insert( field, new Value'(Null_Value), pos, inserted );
            pragma Assert( inserted );
            return Create_Ref( Element( pos ) );
        end if;
        return Null_Value;
    end Reference;

    ----------------------------------------------------------------------------

    procedure Remove( this : Map_Value'Class; field : Unbounded_String ) is
        pos : Cursor := this.data.pairs.Find( field );
    begin
        if Has_Element( pos ) then
            Free( this.data.pairs.Reference( pos ) );
            this.data.pairs.Delete( pos );
        end if;
    end Remove;

    ----------------------------------------------------------------------------

    procedure Remove( this : Map_Value'Class; field : String ) is
    begin
        this.Remove( To_Unbounded_String( field ) );
    end Remove;

    ----------------------------------------------------------------------------

    procedure Set( this    : Map_Value'Class;
                   field   : Unbounded_String;
                   val     : Value'Class;
                   consume : Boolean := False ) is
        pos : Cursor := this.data.pairs.Find( field );
    begin
        if Has_Element( pos ) then
            if not val.Is_Null then
                -- replace the existing value
                Element( pos ).all := (if consume then Value(val) else Clone( val ));
            else
                -- remove 'field' (value is Null)
                Free( this.data.pairs.Reference( pos ) );
                this.data.pairs.Delete( pos );
            end if;
        elsif not val.Is_Null then
            -- insert the value
            this.data.pairs.Insert( field, new Value'(if consume then Value(val) else Clone( val )) );
        end if;
    end Set;

    ----------------------------------------------------------------------------

    procedure Set( this    : Map_Value'Class;
                   field   : String;
                   val     : Value'Class;
                   consume : Boolean := False ) is
    begin
        this.Set( To_Unbounded_String( field ), val, consume );
    end Set;

    ----------------------------------------------------------------------------

    function Size( this : Map_Value'Class ) return Natural is (Natural(this.data.pairs.Length));

    --==========================================================================

    function Clone_Map( this : Value'Class ) return Value is
        thisData : constant Object_Pointer := To_Pointer( this.Get_Address );
        copyData : constant Object_Pointer := new Map_Data;
        result   : Value := Value'(Controlled with v => TAG_MAP);
        pos      : Cursor := thisData.pairs.First;
    begin
        while Has_Element( pos ) loop
            copyData.pairs.Insert( Key( pos ), new Value'(Clone( Element( pos ).all )) );
            Next( pos );
        end loop;
        result.Set_Object( To_Address( copyData ) );
        return result;
    end Clone_Map;

    ----------------------------------------------------------------------------

    function Compare_Maps( left, right : Value'Class ) return Integer is
        leftData  : constant Object_Pointer := To_Pointer( left.Get_Address );
        rightData : constant Object_Pointer := To_Pointer( right.Get_Address );
        leftPos   : Cursor;
        rightPos  : Cursor;
        result    : Integer;
    begin
        if leftData.pairs.Length > rightData.pairs.Length then
            return 1;
        elsif leftData.pairs.Length < rightData.pairs.Length then
            return -1;
        end if;

        leftPos := leftData.pairs.First;
        rightPos := rightData.pairs.First;
        while Has_Element( leftPos ) loop
            -- compare the keys
            if Key( leftPos ) > Key( rightPos ) then
                return 1;
            elsif Key( leftPos ) < Key( rightPos ) then
                return -1;
            end if;

            -- compare the values
            result := Element( leftPos ).Compare( Element( rightPos ).all );
            if result /= 0 then
                return result;
            end if;

            Next( leftPos );
            Next( rightPos );
        end loop;

        -- both maps have the same keys and values
        return 0;
    end Compare_Maps;

    ----------------------------------------------------------------------------

    procedure Delete_Map( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( Map_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        -- looping over values
        for val of data.pairs loop
            Free( val );    -- delete each dynamically allocated Value
        end loop;
        Free( data );
    end Delete_Map;

    ----------------------------------------------------------------------------

    function Image_Map( this : Value'Class ) return String is
        data   : constant Object_Pointer := To_Pointer( this.Get_Address );
        pos    : Cursor := data.pairs.First;
        nxt    : Cursor;
        result : Unbounded_String;
    begin
        Append( result, '{' );
        while Has_Element( pos ) loop
            Append( result, '"' );
            Append( result, Values.Strings.Escape( To_String( Key( pos ) ) ) );
            Append( result, """:" );
            Append( result, Element( pos ).Image );

            nxt := Next( pos );
            if Has_Element( nxt ) then
                Append( result, ',' );
            end if;

            pos := nxt;
        end loop;
        Append( result, '}' );
        return To_String( result );
    end Image_Map;

    ----------------------------------------------------------------------------

    procedure Read_Map( stream : access Root_Stream_Type'Class;
                        this   : in out Value'Class ) is
        data : Object_Pointer;
        len  : Integer;
        key  : Unbounded_String;
        elem : Value;
    begin
        data := new Map_Data;
        this.Set_Object( To_Address( data ) );

        len := Integer'Input( stream );
        for i in 1..len loop
            key := Read_String( stream );
            elem := Value_Input( stream );
            data.pairs.Insert( key, new Value'(elem) );
        end loop;
    end Read_Map;

    ----------------------------------------------------------------------------

    procedure Write_Map( stream : access Root_Stream_Type'Class;
                         this   : Value'Class ) is
        data : constant Object_Pointer := To_Pointer( this.Get_Address );
        pos  : Cursor;
    begin
        Integer'Output( stream, Integer(data.pairs.Length) );
        pos := data.pairs.First;
        while Has_Element( pos ) loop
            Write_String( stream, Key( pos ) );
            Value_Output( stream, Element( pos ).all );
            Next( pos );
        end loop;
    end Write_Map;

end Values.Maps;

