--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Scribble.Generic_Tokens;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Streams;                       use Ada.Streams;
with Scribble.Token_Locations;          use Scribble.Token_Locations;

generic
    with package Tokens is new Scribble.Generic_Tokens(<>);
package Scribble.Tokenizers is

    use Tokens;

    -- A Tokenizer reads from a character stream and splits the text, using a
    -- set of delimiter characters, to return a series of Token objects. If the
    -- stream contains character sequences that do not compose valid,
    -- recognizable tokens, an exception will be thrown and the erroneous text
    -- will be discarded.
    --
    -- Tokens may be pushed back to the Tokenizer, providing support for LL(n)
    -- parsers.
    type Tokenizer is tagged limited private;
    type A_Tokenizer is access all Tokenizer'Class;

    -- Creates a new Tokenizer with no input stream.
    function Create_Tokenizer return A_Tokenizer;
    pragma Postcondition( Create_Tokenizer'Result /= null );

    -- Returns the source code that has been tokenized up to this point.
    function Get_Source( this : not null access Tokenizer'Class ) return Unbounded_String;

    -- Returns the Tokenizer's current location in the input stream.
    function Location( this : not null access Tokenizer'Class ) return Token_Location;

    -- Returns the next Token read from the input stream. If Tokens were
    -- previously pushed back to the Tokenizer, the most recent will be returned
    -- instead. If 'skipEOL' is True, all end-of-line tokens before the next
    -- token will be discarded. A Parse_Error will be raised if a token is not
    -- recognized or if the token is malformed.
    function Next( this : not null access Tokenizer'Class; skipEOL : Boolean := True ) return Token_Ptr;
    pragma Postcondition( Next'Result.Not_Null );

    -- Sets the input stream for reading characters. The state of the Tokenizer
    -- will also be reset. If 'input' is null, the Tokenizer's input will be
    -- cleared. 'filepath' is the path of the file being tokenized (when
    -- available), or an empty string if none.
    procedure Set_Input( this     : not null access Tokenizer'Class;
                         input    : access Root_Stream_Type'Class;
                         filepath : String );

    -- Steps backward in the token list. The token most recently returned by
    -- Next() will be pushed back onto the token list, allowing Next() to return
    -- it again. This procedure cannot be called twice in a row; The tokenizer
    -- can only go backward one step.
    procedure Step_Back( this : not null access Tokenizer'Class );

    -- Deletes the Tokenizer.
    procedure Delete( this : in out A_Tokenizer );
    pragma Postcondition( this = null );

private

    package Character_Vectors is new Ada.Containers.Vectors( Natural, Character, "=" );

    type Token_Array is array (Integer range <>) of Token_Ptr;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Tokenizer is tagged limited
        record
            strm         : access Root_Stream_Type'Class;
            source       : Unbounded_String;    -- source code buffer
            loc          : Token_Location;
            tokenLoc     : Token_Location := UNKNOWN_LOCATION;
            pushedChars  : Character_Vectors.Vector;

            -- the token queue, supporting 1 token look ahead. it hold 4 elements
            -- instead of 2 because end-of-line tokens do not count toward the
            -- look ahead limit. (the tokenizer can actually look ahead 2 tokens
            -- if the first token is an EOL.)
            --
            -- The token most recently returned by Next() is stored at (0) and
            -- the token returned before that is stored at (1). Storing these
            -- look ahead tokens in the queue allows Step_Back() to push look
            -- ahead tokens back into the queue without a reference from the
            -- caller. The token that was most recently pushed back into the
            -- queue is at (-1) and the token pushed before that is at slot (-2).
            --
            -- A second lookahead token is only kept if it's an EOL. When
            -- Step_Back() is called, it will push back one token. But, if a
            -- second lookahead token (an EOL) exists, it will be pushed back too.
            --
            --      +-----+-----+-----+-----+
            --      | -2  | -1  |  0  |  1  |
            --      +-----+-----+-----+-----+
            --         ^     ^     ^     ^
            --         |     |     |     previous look ahead token (EOL only)
            --         |     |     look ahead token
            --         |     pushed back token
            --         previous pushed back token (only when -1 is EOL)
            --
            tokenQueue   : Token_Array(-2..1);
        end record;

    -- Returns true if the next character in the input stream is argument 'c'.
    -- If the character equals 'c', it will be discarded. Otherwise, nothing
    -- will be removed from the input stream and false will be returned. If the
    -- input stream is empty, false will be returned.
    function Expect( this : not null access Tokenizer'Class; c : Character ) return Boolean;

    -- Saves the tokenizer's current location into .tokenLoc, returning the
    -- location as well.
    procedure Mark_Location( this : not null access Tokenizer'Class );
    function  Mark_Location( this : not null access Tokenizer'Class ) return Token_Location;

    -- The following Parse_* functions are called by Next() to parse a specific
    -- token type from the input stream. If the correct token is not found, a
    -- Parse_Error will be raised. The Parse_* functions do not return null.
    function Parse_Color_Literal( this : not null access Tokenizer'Class ) return Token_Ptr;
    function Parse_Id_Literal( this : not null access Tokenizer'Class ) return Token_Ptr;
    function Parse_Number_Literal( this : not null access Tokenizer'Class ) return Token_Ptr;
    function Parse_Simple_Token( this : not null access Tokenizer'Class ) return Token_Ptr;
    function Parse_String_Literal( this : not null access Tokenizer'Class ) return Token_Ptr;
    function Parse_Symbol( this : not null access Tokenizer'Class ) return Token_Ptr;

    -- Reads the next character from the input stream into 'c'. An exception
    -- will be raised if the stream fails. Otherwise, true will be returned on
    -- success and false will be returned if the end-of-File is encountered.
    -- 'c' will not be modified unless the read is successful.
    function Read( this : not null access Tokenizer'Class; c : out Character ) return Boolean;

    -- Unreads the character mostly recently returned by Read, pushing it back
    -- to be returned again by the next call to Read.
    procedure Unread( this : not null access Tokenizer'Class; c : Character );

    -- raised internally when the input stream fails
    Stream_Error : exception;

end Scribble.Tokenizers;
