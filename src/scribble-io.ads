--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Calendar;                      use Ada.Calendar;
with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Values;                            use Values;

package Scribble.IO is

    type Content_Status is (None, Loaded, Unrecognized, Unsupported, Corrupt);

    -- Contains the contents of a Scribble compiled object (.sco) file
    --
    -- If .valid is False, the file was not loaded succesfully and the other
    -- fields are considered invalid.
    type File_Content is
        record
            status      : Content_Status := None;
            sourcePath  : Unbounded_String;    -- original source filepath
            compileDate : Ada.Calendar.Time;   -- UTC
            val         : Value;
        end record;

    -- Reads a Scribble compiled object (.sco) from an input stream. The
    -- contents of the file are returned in 'result'. On success, the .status
    -- field will be set to Loaded, otherwise it will indicate the error and no
    -- other fields will be populated.
    --
    -- Failures are caused by an unsupported file version or corrupt data.
    procedure Read( stream : not null access Root_Stream_Type'Class;
                    result : out File_Content );
    pragma Postcondition( result.status /= None );

    -- Writes a Scribble value to a stream using the Scribble compiled object
    -- (.sco) format. 'sourcePath' is the optional filepath of the source code
    -- that the value was originally parsed from.
    procedure Write( stream     : not null access Root_Stream_Type'Class;
                     val        : Value'Class;
                     sourcePath : String := "" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Reads Scribble source file 'filepath', returning its contents in 'source'.
    -- Lines will be delimited by ASCII.LF, regardless of platform. Returns True
    -- on success or False if 'filepath' cannot be opened.
    function Read_Source( filepath : String; source : out Unbounded_String ) return Boolean;

end Scribble.IO;
