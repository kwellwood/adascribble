--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Construction is

    -- Returns a deep copy of the value, or the value itself if immutable.
    --
    -- The usage pattern for copying Values is as follows:
    --
    -- 1. Always copy a Value when passing it between threads.
    -- 2. When storing a Value inside another object, always store a copy.
    -- 3. When a Value is requested, return it without making a copy (unless the
    --      value may be passed between threads).
    -- 4. Never modify a value passed into a routine; make a copy first. Unless
    --      explicitly indicated, the caller maintains ownership of Value_Ptr
    --      arguments and expects they will remain unmodified.
    -- 5. Conversely, there is no need to make a copy of a Value before passing
    --      it to a routine, unless the comments state that it may be modified.
    function Clone( this : Value'Class ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Numbers

    -- Creates a new number value from integer 'num'.
    function Create( num : Integer ) return Value;

    -- Creates a new number value from float 'num'.
    function Create( num : Float ) return Value;

    -- Creates a new number value from long float 'num'.
    function Create( num : Long_Float ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Booleans

    -- Creates a new boolean value from 'bool'.
    function Create( bool : Boolean ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Colors

    -- Creates a new RGBA color value.
    function Create_Color( r, g, b : Float; a : Float := 1.0 ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Ids

    -- Creates a new 48-bit identifier value. The upper 16-bits of 'id' will be
    -- ignored.
    function Create_Id( id : Unsigned_64 ) return Value;
    pragma Precondition( id < 2**48 );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- References

    -- A reference is a Value that is itself a reference to another Value. This
    -- allows a value stored in a specific location to be updated in-place; for
    -- example, inside a list or map.
    function Create_Ref( target : not null A_Value ) return Value;

end Values.Construction;
