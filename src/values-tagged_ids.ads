--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values;                            use Values;
with Values.Casting;

package Values.Tagged_Ids is

    -- Uniquely identifies an object of a type.
    --
    -- The id is composed if a 4 bit tag specifying the object's type (or class),
    -- and a 44 bit identifier to uniquely identify an object of its type.
    type Tagged_Id is private;

    -- Type of object referenced by an id. This is the object's "class",
    -- specified as a 4-bit tag (16 possible object types).
    type Id_Tag is mod 16;

    -- Default value for no object.
    NULL_ID : constant Tagged_Id;

    -- Compares Tagged_Id values to support ordering.
    function "<"( l, r : Tagged_Id ) return Boolean with Inline;

    -- Returns a string representation of 'id'.
    function Image( id : Tagged_Id ) return String with Inline;

    -- Casts a Value to an Tagged_Id. Returns 'default' if 'val' is not an id.
    function Cast_Tagged_Id( val     : Value'Class;
                             default : Tagged_Id := NULL_ID ) return Tagged_Id;

    -- Returns an object of type 'tag', identified by unique bits 'x'. Only the
    -- 44 least significant bits of 'x' are included in the id.
    function Create_Tagged_Id( tag : Id_Tag; x : Unsigned_64 ) return Tagged_Id;
    pragma Precondition( x < 2 ** 44 );

    -- Returns a tagged id created from the raw bits of 'rawId', which contains
    -- a tag and id already.
    function Create_Tagged_Id( rawId : Unsigned_64 ) return Tagged_Id;
    pragma Precondition( rawId < 2 ** 48 );

    -- Returns a Tagged_Id's tag component (4 bits).
    function Get_Tag( id : Tagged_Id ) return Id_Tag with Inline;

    -- Returns a Tagged_Id's identifier component (44 bits).
    function Get_Id( id : Tagged_Id ) return Unsigned_64;
    pragma Postcondition( Get_Id'Result < 2 ** 44 );

    -- Returns True if 'id' represents a null object. Null object identifiers
    -- are all zeros following the id tag.
    function Is_Null( id : Tagged_Id ) return Boolean with Inline;

    -- Converts an id Value to an Tagged_Id. NULL_ID will be returned if 'val'
    -- is not an id Value.
    function To_Tagged_Id( val : Value'Class ) return Tagged_Id with Inline;

    -- Converts an Tagged_Id to an id Value.
    function To_Id_Value( id : Tagged_Id ) return Value;

    -- Returns a unique Tagged_Id. The id is built using a random number, so it
    -- theoretically possible to generate duplicate ids, though unlikely with
    -- 44 bits of uniqueness.
    function Unique_Tagged_Id( tag : Id_Tag ) return Tagged_Id;

private

    type Tagged_Id is new Unsigned_64;

    NULL_ID      : constant Tagged_Id := 16#0000_F_00000000000#;
    DUPLICATE_ID : exception;

    function Cast_Id is new Casting.Cast_Id( Tagged_Id );

    function Cast_Tagged_Id( val     : Value'Class;
                             default : Tagged_Id := NULL_ID ) return Tagged_Id
    is (Cast_Id( val, default ));

end Values.Tagged_Ids;
