--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Ada.Calendar.Formatting;
with Interfaces;                        use Interfaces;
with Values.Streaming;                  use Values.Streaming;

package body Scribble.IO is

    -- File format version
    SCO_VERSION_READ  : constant := 1 * 100 + Values.STREAM_VERSION_READ;
    SCO_VERSION_WRITE : constant := 1 * 100 + Values.STREAM_VERSION_WRITE;

    -- Scribble language version
    LANGUAGE_VERSION_READ  : constant := LANGUAGE_VERSION;
    LANGUAGE_VERSION_WRITE : constant := LANGUAGE_VERSION;

    ----------------------------------------------------------------------------

    procedure Read( stream : not null access Root_Stream_Type'Class;
                    result : out File_Content ) is
    begin
        result.status := None;
        result.sourcePath := To_Unbounded_String( "" );
        result.compileDate := Clock;
        result.val := Null_Value;

        -- verify the file format identifier
        if Character'Input( stream ) /= 'S' or else
           Character'Input( stream ) /= 'C' or else
           Character'Input( stream ) /= 'O'
        then
            result.status := Unrecognized;
            return;
        end if;

        -- verify the file format version
        if Unsigned_16'Input( stream ) /= SCO_VERSION_READ then
            result.status := Unsupported;
            return;
        end if;

        -- verify the language version
        if Unsigned_8'Input( stream ) /= LANGUAGE_VERSION_READ then
            result.status := Unsupported;
            return;
        end if;

        -- read the remaining file contents
        result.sourcePath := To_Unbounded_String( String'Input( stream ) );
        result.compileDate := Ada.Calendar.Formatting.Value( String'Input( stream ) );
        result.val := Value_Input( stream );
        result.status := Loaded;
    exception
        when others =>
            result.status := Corrupt;
            result.sourcePath := To_Unbounded_String( "" );
            result.compileDate := Clock;
            result.val := Null_Value;
            return;
    end Read;

    ----------------------------------------------------------------------------

    function Read_Source( filepath : String; source : out Unbounded_String ) return Boolean is
        input : Ada.Text_IO.File_Type;
    begin
        source := Null_Unbounded_String;

        begin
            Ada.Text_IO.Open( input, Ada.Text_IO.In_File, filepath );
            if not Ada.Text_IO.Is_Open( input ) then
                return False;
            end if;
        exception
            when others =>
                return False;
        end;

        while not Ada.Text_IO.End_Of_File( input ) loop
            Append( source, String'(Ada.Text_IO.Get_Line( input )) & ASCII.LF );
        end loop;
        Ada.Text_IO.Close( input );

        return True;
    end Read_Source;

    ----------------------------------------------------------------------------

    procedure Write( stream     : not null access Root_Stream_Type'Class;
                     val        : Value'Class;
                     sourcePath : String := "" ) is
        use Ada.Calendar.Formatting;
    begin
        -- file type identifier
        Character'Output( stream, 'S' );                        -- 'S'
        Character'Output( stream, 'C' );                        -- 'C'
        Character'Output( stream, 'O' );                        -- 'O'
        Unsigned_16'Output( stream, SCO_VERSION_WRITE );        -- file format version
        Unsigned_8'Output( stream, LANGUAGE_VERSION_WRITE );    -- Scribble language version

        -- file contents
        String'Output( stream, sourcePath );                    -- source file path
        String'Output( stream, Image( Ada.Calendar.Clock ) );   -- compilation date/time
        Value_Output( stream, val );                            -- compiled value
    end Write;

end Scribble.IO;
