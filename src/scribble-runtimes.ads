--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Strings.Hash;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.States;                   use Scribble.States;
with Values;                            use Values;

private with Ada.Containers.Indefinite_Ordered_Maps;
private with Ada.Containers.Ordered_Maps;

package Scribble.Runtimes is

    type A_Ada_Function is access
        procedure( scribble : in out Scribble_State );

    type Prototype is
        record
            func     : A_Ada_Function := null;
            minArgs  : Natural := 0;
            maxArgs  : Natural := 0;
            exported : Boolean := False;
            name     : Unbounded_String;
        end record;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    subtype Function_Id is Ada.Containers.Hash_Type;

    Invalid_FID : constant Function_Id := Ada.Strings.Hash( "" );

    function Fid_Hash( str : String ) return Function_Id renames Ada.Strings.Hash;

    -- Converts an Id_Value pointer to a Function_Id. If 'val' is a null pointer,
    -- Invalid_FID will be returned.
    function To_Function_Id( val : Value'Class ) return Function_Id with Inline;

    -- Converts a Function_Id to an Id value.
    function To_Id_Value( id : Function_Id ) return Value with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- A Scribble Runtime contains a registry of bindings to Ada functions that
    -- can be called from Scribble.
    type Scribble_Runtime is tagged limited private;
    type A_Scribble_Runtime is access all Scribble_Runtime'Class;

    -- Creates a new Scribble Runtime with only the built-in operators functions
    -- registered. Additional functions providing access to the applications can
    -- be added with Define(). All bindings must be defined  before using the
    -- runtime with the Scribble compiler or the Scribble VM.
    function Create_Scribble_Runtime return A_Scribble_Runtime;
    pragma Postcondition( Create_Scribble_Runtime'Result /= null );

    -- Defines an Ada function that is accessible via the Scribble runtime as
    -- a function called 'name'. 'minArgs' and 'maxArgs' are the minimum and
    -- maximum number of arguments accepted by the function. If 'exported' is
    -- True, the function will be automatically exported to Scribble code as a
    -- built-in function and can be called directly without creating a Scribble
    -- binding function. Otherwise, a binding function bound to 'name' will have
    -- be created to access the implementation (ex: "function myName() : name").
    -- Names of built-in functions cannot be used as variable or parameter names
    -- in any Scribble functions.
    --
    -- If a matching function name has already defined, it will be overridden by
    -- this definition.
    procedure Define( this     : not null access Scribble_Runtime'Class;
                      name     : String;
                      func     : not null A_Ada_Function;
                      minArgs  : Natural;
                      maxArgs  : Natural;
                      exported : Boolean := False );
    pragma Precondition( minArgs <= maxArgs );
    pragma Precondition( maxArgs <= 255 );

    -- Returns a string representation of the Scribble_Runtime environment,
    -- including namespaces, functions, their argument count, and export state.
    -- The string is useful for debugging purposes only.
    function Get_Environment( this : not null access Scribble_Runtime'Class ) return Unbounded_String;

    -- Returns the namespace named 'name' registered with this thread. If it has
    -- not been registered, null will be returned.
    function Get_Namespace( this : not null access Scribble_Runtime'Class;
                            name : String ) return A_Scribble_Namespace;

    -- Returns the Prototype defined for function 'fid' by id. If 'fid' is not
    -- defined, 'proto.func' will be null.
    procedure Get_Prototype( this  : not null access Scribble_Runtime'Class;
                             fid   : Function_Id;
                             proto : out Prototype );

    -- Returns True if function 'fid' is defined in the registry.
    function Is_Defined( this : not null access Scribble_Runtime'Class;
                         fid  : Function_Id ) return Boolean;

    -- Returns True if 'namespace' is the name of a registered namespace.
    function Is_Namespace( this      : not null access Scribble_Runtime'Class;
                           namespace : String ) return Boolean;

    -- Registers a namespace 'name', making its contents accessible to Scribble
    -- code executed with this runtime. If 'name' has already been registered,
    -- 'namespace' will replace it. If 'namespace' is null, then 'name' will be
    -- unregistered.
    procedure Register_Namespace( this      : not null access Scribble_Runtime'Class;
                                  name      : String;
                                  namespace : access Scribble_Namespace'Class );

    -- Reserves a namespace named 'name' within the runtime, allowing the name
    -- to be declared without providing an implementation. This is useful for
    -- compiling code that references namespaces but it doesn't need to run yet,
    -- like a compiler in one application compiling for a VM in another. If a
    -- reserved namespace is accessed by a VM during execution, every name will
    -- return an Error value.
    --
    -- If the namespace 'name' has already been registered, it will be unaffected.
    procedure Reserve_Namespace( this : not null access Scribble_Runtime'Class;
                                 name : String );

    -- Unregisters a namespace 'name', if it was registered or reserved. This is
    -- identical to calling Register_Namespace( name, null ).
    procedure Unregister_Namespace( this : not null access Scribble_Runtime'Class;
                                    name : String );

    procedure Delete( this : in out A_Scribble_Runtime );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    FUNC_INDEX        : constant String;
    FUNC_INDEX_REF    : constant String;
    FUNC_MEMBER       : constant String;
    FUNC_MEMBER_REF   : constant String;
    FUNC_ISLIST       : constant String;
    FUNC_ISMAP        : constant String;
    FUNC_KEYS         : constant String;
    FUNC_LENGTH       : constant String;

    FID_BUILD_LIST    : constant Function_Id;
    FID_BUILD_MAP     : constant Function_Id;
    FID_IN            : constant Function_Id;
    FID_INDEX         : constant Function_Id;
    FID_INDEX_REF     : constant Function_Id;
    FID_MEMBER        : constant Function_Id;
    FID_MEMBER_REF    : constant Function_Id;

private

    FUNC_INDEX        : constant String := "$index";
    FUNC_INDEX_REF    : constant String := "$indexRef";
    FUNC_MEMBER       : constant String := "$member";
    FUNC_MEMBER_REF   : constant String := "$memberRef";
    FUNC_ISLIST       : constant String := "isList";
    FUNC_ISMAP        : constant String := "isMap";
    FUNC_KEYS         : constant String := "keys";
    FUNC_LENGTH       : constant String := "length";

    FID_BUILD_LIST    : constant Function_Id := Fid_Hash( "$buildList" );
    FID_BUILD_MAP     : constant Function_Id := Fid_Hash( "$buildMap" );
    FID_IN            : constant Function_Id := Fid_Hash( "$in" );
    FID_INDEX         : constant Function_Id := Fid_Hash( FUNC_INDEX );
    FID_INDEX_REF     : constant Function_Id := Fid_Hash( FUNC_INDEX_REF );
    FID_MEMBER        : constant Function_Id := Fid_Hash( FUNC_MEMBER );
    FID_MEMBER_REF    : constant Function_Id := Fid_Hash( FUNC_MEMBER_REF );

    ----------------------------------------------------------------------------

    package Prototype_Maps is new Ada.Containers.Ordered_Maps( Function_Id, Prototype, "<", "=" );

    package Namespace_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Scribble_Namespace, "<", "=" );

    type Scribble_Runtime is tagged limited
        record
            prototypes : Prototype_Maps.Map;
            namespaces : Namespace_Maps.Map;      -- not owned
        end record;

    ----------------------------------------------------------------------------

    type Reserved_Namespace is new Scribble_Namespace with null record;

    -- Not intended for use.
    overriding
    function Get_Namespace_Name( this : access Reserved_Namespace; name : String ) return Value is (Null_Value);

    -- Not intended for use.
    overriding
    function Get_Namespace_Ref( this : access Reserved_Namespace; name : String ) return Value is (Null_Value);

end Scribble.Runtimes;
