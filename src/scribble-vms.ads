--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Values;                            use Values;
with Values.Functions;                  use Values.Functions;

package Scribble.VMs is

    type Thread(stackSize : Positive) is tagged limited private;
    type A_Thread is access all Thread'Class;

    -- Creates a new evaluation thread to run on a VM. 'stackSize' is the size
    -- of the thread's stack in elements.
    function Create_Thread( stackSize : Positive := 1024 ) return A_Thread;
    pragma Postcondition( Create_Thread'Result /= null );

    -- Returns the name of the first execution frame in the thread. This is the
    -- 'baseFrame' that was passed to Load(), or the original function's name.
    -- Returns an empty string if no function is loaded.
    function Get_First_Frame( this : Thread'Class ) return String;

    -- Returns the result of the thread's initial function. null will be
    -- returned if the thread is still running, or an Error value if the thread
    -- encountered an error.
    function Get_Result( this : Thread'Class ) return Value;

    -- Returns True if the thread is CURRENTLY being executed. This is only True
    -- while the VM has this thread open on the Ada stack and dispatched to a
    -- built-in Ada function, which then recursed back to call this. Prevents
    -- loading a new function while the thread is busy.
    function Is_Busy( this : Thread'Class ) return Boolean with Inline_Always;

    -- Returns True if the thread encountered an unrecoverable error and had to
    -- be terminated. Get_Result will return a descriptive Error value.
    function Is_Errored( this : Thread'Class ) return Boolean with Inline_Always;

    -- Returns True if the currently loaded function has not yet finished.
    function Is_Running( this : Thread'Class ) return Boolean with Inline_Always;

    -- Iterates backwards through the threads execution trace, examining each
    -- frame with 'examine'. The 'str' argument will be in the format
    -- "FunctionName line:col"
    procedure Iterate_Trace( this : Thread'Class; examine : access procedure( str : String ) );

    -- Loads 'func' with arguments 'args' to begin executing on this thread. Any
    -- previous execution on the thread will be abandoned. If the length of
    -- 'args' does not match the function's argument requirements, then a
    -- Runtime_Error will be raised and the thread's state will not be modified.
    -- 'baseFrame' is the name of the base execution frame starting in 'func',
    -- otherwise it will default to the function's name.
    procedure Load( this      : in out Thread'Class;
                    func      : Function_Value;
                    args      : Value_Array;
                    baseFrame : String := "" );
    pragma Precondition( not this.Is_Busy );

    -- Loads 'func' to begin executing on this thread. Any previous execution on
    -- the thread will be abandoned. If the function requires more than zero
    -- arguments, a Runtime_Error will be raised and the thread's state will not
    -- be modified. 'baseFrame' is the name of the base execution frame starting
    -- in 'func', otherwise it will default to the function's name.
    procedure Load( this      : in out Thread'Class;
                    func      : Function_Value;
                    baseFrame : String := "" );

    -- Registers a namespace 'name', making its contents accessible to Scribble
    -- code executed with this thread. This provides the same behavior as
    -- Scribble_Runtime.Register_Namespace, but the namespace is accessible only
    -- to this thread. If a Namespace with the same name has already been
    -- registered, 'namespace' will replace it. If 'namespace' is null, then the
    -- Namespace 'name' will be unregistered. A Constraint_Error will be raised
    -- if the thread has already begun executing.
    procedure Register_Namespace( this      : in out Thread'Class;
                                  name      : String;
                                  namespace : access Scribble_Namespace'Class );

    procedure Delete( this : in out A_Thread );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Run_Mode is (Run_Complete, Run_Yield, Run_Step);

    type Scribble_VM is tagged limited private;
    type A_Scribble_VM is access all Scribble_VM'Class;

    -- Creates and returns a new Scribble Virtual Machine. The Scribble runtime
    -- provides implementations for langauge operators and Ada functions
    -- exported to Scribble code.
    function Create_Scribble_VM( runtime : not null A_Scribble_Runtime ) return A_Scribble_VM;
    pragma Postcondition( Create_Scribble_VM'Result /= null );

    -- Continues execution of 'thread', stopping at a point determined by the
    -- execution mode 'mode'. The following execution modes are available:
    --
    -- Run_Complete : execute until evaluation is complete or an error is
    --                encountered.
    --
    -- Run_Yield    : execute until a yield instruction is encountered or
    --                evaluation completes, whichever occurs first. check the
    --                thread's Is_Running() method to determine if the thread
    --                completed.
    --
    -- Run_Step     : execute just one instruction. check the thread's
    --                Is_Running() method to determine if the thread can be
    --                stepped again (True) or if it has ended (False).
    --
    procedure Run( this   : not null access Scribble_VM'Class;
                   thread : not null A_Thread;
                   mode   : Run_Mode );

    procedure Delete( this : in out A_Scribble_VM );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Writes a line of text to the debug output procedure.
    procedure Debug_Line( str : String );

    -- Writes a line of text to the standard output procedure.
    procedure Print_Line( str : String );

    type A_Output_Proc is access procedure( str : String );

    -- Sets the optional output procedure used by all VMs to write debugging
    -- output to the application. This procedure should only be called once,
    -- during application startup.
    procedure Set_Debug_Output( proc : A_Output_Proc );

    -- Sets the standard output procedure used by all VMs to write output to the
    -- application. The Scribble print() and printf() functions will use it.
    -- This procedure should only be called once, during application startup.
    procedure Set_Output( proc : A_Output_Proc );

    -- Raised when the virtual machine encounters an error during evaluation.
    Runtime_Error : exception;

    MIN_STACK_SIZE : constant := 512;
    MAX_STACK_SIZE : constant := 65535;

private

    type Trace_Frame is
        record
            name : Unbounded_String;
            path : Unbounded_String;
            loc  : Token_Location := UNKNOWN_LOCATION;
        end record;

    package Trace_Frames is new Ada.Containers.Vectors( Positive, Trace_Frame, "=" );

    package Namespace_Maps is new Ada.Containers.Indefinite_Ordered_Maps( String, A_Scribble_Namespace, "<", "=" );

    type Thread(stackSize : Positive) is tagged limited
        record
            busy       : Boolean := False;
            running    : Boolean := False;
            err        : Boolean := False;        -- encountered an error?
            trace      : Trace_Frames.Vector;
            firstFrame : Unbounded_String;
            result     : Value;                   -- result after evaluation
            stack      : Value_Array(1..stackSize) := (others => Null_Value);
            namespaces : Namespace_Maps.Map;      -- not owned

            -- registers
            ts : Integer := 0;          -- top of the stack (0 if empty stack)
            fp : Integer := 0;          -- stack frame pointer
            pc : Integer := 0;          -- program counter
            fn : Function_Value;        -- current function
        end record;

    -- Pushes an activation record to evaluate 'func'. The arguments to pass
    -- must be on the stack already. The thread will enter the error state on
    -- failure.
    procedure Enter_Function( this : in out Thread'Class;
                              func : Function_Value ) with Inline;

    -- Returns the namespace 'name' registered with this thread. If it has not
    -- been registered, null will be returned.
    function Get_Namespace( this : Thread'Class;
                            name : String ) return A_Scribble_Namespace;

    -- Invokes function 'fid' in the VM's runtime, parameterized with 'argCount'
    -- args already on the stack.
    function Invoke( this     : not null access Scribble_VM'Class;
                     thread   : not null A_Thread;
                     fid      : Function_Id;
                     argCount : Natural ) return Value;

    -- Pops the top of the stack and returns it as-is (without copying).
    procedure Pop( this : in out Thread'Class;
                   top  : out Value ) with Inline_Always;

    -- Pops the top frame of the trace stack.
    procedure Pop_Frame( this : in out Thread'Class );

    -- Pushes 'val' directly onto the stack. No copy is made. The thread will
    -- enter the error state on failure.
    procedure Push( this : in out Thread'Class;
                    val  : Value'Class ) with Inline_Always;

    -- Pushes a new frame onto the trace stack.
    procedure Push_Frame( this : in out Thread'Class; name : String; path : String );

    -- Terminates the thread's execution with an Error result.
    procedure Set_Error( this : in out Thread'Class; err : Value'Class ) with Inline;

    -- Sets the result of the thread's evaluation. This can only be set once.
    procedure Set_Result( this : in out Thread'Class; result : Value'Class ) with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    package Thread_Vectors is new Ada.Containers.Vectors( Positive, A_Thread, "=" );

    type Scribble_VM is tagged limited
        record
            runtime : A_Scribble_Runtime := null;    -- not owned
            trace   : Thread_Vectors.Vector;
        end record;

    -- Reads a name in a registered Scribble namespace. If 'reference' is True,
    -- a Reference to the value of the name will be returned, otherwise the
    -- name's value will be returned directly. If 'name' is not defined in the
    -- namespace, then it will automatically be defined with a value of Null, if
    -- possible. If 'name' cannot be defined, an Error will be returned.
    -- Any namespace registered with 'thread' will override a matching one
    -- registered the VM's runtime. An Error value will be returned if
    -- 'namespace' is not registered.
    function Namespace_Read( this      : not null access Scribble_VM'Class;
                             thread    : not null A_Thread;
                             namespace : String;
                             name      : String;
                             reference : Boolean ) return Value;

    procedure Pop_Thread( this : not null access Scribble_VM'Class );

    procedure Push_Thread( this : not null access Scribble_VM'Class; thread : not null A_Thread );

end Scribble.VMs;
