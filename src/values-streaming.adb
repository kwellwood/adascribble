--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Errors;
with Values.Functions;
with Values.Lists;
with Values.Maps;
with Values.Strings;

pragma Elaborate( Values.Errors );
pragma Elaborate( Values.Functions );
pragma Elaborate( Values.Lists );
pragma Elaborate( Values.Maps );
pragma Elaborate( Values.Strings );

package body Values.Streaming is

    type Stream_Reader is access
        procedure( stream : access Root_Stream_Type'Class;
                   this   : in out Value'Class );

    type Stream_Reader_Array is array (Integer range 2#0_000#..2#1_111#) of Stream_Reader;

    pragma Suppress(Elaboration_Check, Strings.Read_String);
    pragma Suppress(Elaboration_Check, Lists.Read_List);
    pragma Suppress(Elaboration_Check, Maps.Read_Map);
    pragma Suppress(Elaboration_Check, Functions.Read_Function);
    pragma Suppress(Elaboration_Check, Errors.Read_Error);

    stream_readers : constant Stream_Reader_Array :=
    (
        STRING_TYPE   => Strings.Read_String'Access,
        LIST_TYPE     => Lists.Read_List'Access,
        MAP_TYPE      => Maps.Read_Map'Access,
        FUNCTION_TYPE => Functions.Read_Function'Access,
        ERROR_TYPE    => Errors.Read_Error'Access,
        others        => null
    );

    ----------------------------------------------------------------------------

    type Stream_Writer is access
        procedure( stream : access Root_Stream_Type'Class;
                   this   : Value'Class );

    type Stream_Writer_Array is array (Integer range 2#0_000#..2#1_111#) of Stream_Writer;

    pragma Suppress(Elaboration_Check, Strings.Write_String);
    pragma Suppress(Elaboration_Check, Lists.Write_List);
    pragma Suppress(Elaboration_Check, Maps.Write_Map);
    pragma Suppress(Elaboration_Check, Functions.Write_Function);
    pragma Suppress(Elaboration_Check, Errors.Write_Error);

    stream_writers : constant Stream_Writer_Array :=
    (
        STRING_TYPE   => Strings.Write_String'Access,
        LIST_TYPE     => Lists.Write_List'Access,
        MAP_TYPE      => Maps.Write_Map'Access,
        FUNCTION_TYPE => Functions.Write_Function'Access,
        ERROR_TYPE    => Errors.Write_Error'Access,
        others        => null
    );

    --==========================================================================

    function Value_Input( stream : access Root_Stream_Type'Class ) return Value is
        this : Value;
    begin
        this.v := Unsigned_64'Input( stream );
        if not this.Is_Number and then stream_readers(this.Get_Type) /= null then
            this.v := this.v and not PAYLOAD_MASK;                       -- zero the address
            stream_readers(this.Get_Type).all( stream, this );
        end if;
        return this;
    end Value_Input;

    ----------------------------------------------------------------------------

    procedure Value_Output( stream : access Root_Stream_Type'Class; this : Value'Class ) is
    begin
        if this.Is_Number or else stream_writers(this.Get_Type) = null then
            -- boxed types
            Unsigned_64'Output( stream, this.v );
        else
            Unsigned_64'Output( stream, this.v and not PAYLOAD_MASK );   -- zero the address
            stream_writers(this.Get_Type).all( stream, this );
        end if;
    end Value_Output;

end Values.Streaming;
