--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;

package body Scribble.Opcodes is

    -- Opcode formats:
    --
    -- UNSIGNED ARGUMENT                      SIGNED ARGUMENT
    -- Bits 0..5  - Instruction               Bits 0..5  - Instruction
    -- Bits 6..31 - U (unsigned integer)      Bits 6..31 - S (signed integer)

    INST_SIZE  : constant := 6;
    INST_POS   : constant := 0;
    INST_MASK1 : constant Unsigned_32 := Shift_Left( 2**INST_SIZE - 1, INST_POS );

    U_SIZE  : constant := 26;
    U_POS   : constant := INST_POS + INST_SIZE;
    U_MAX   : constant Unsigned_32 := 2**U_SIZE - 1;
    U_MASK1 : constant Unsigned_32 := Shift_Left( 2**U_SIZE - 1, U_POS );

    S_POS   : constant := U_POS;
    S_MAX   : constant Unsigned_32 := Shift_Right( U_MAX, 1 );
    S_MASK1 : constant Unsigned_32 := U_MASK1;

    ----------------------------------------------------------------------------

    function To_Opcode( inst : Instruction ) return Opcode is
        op : Opcode := 0;
    begin
        Set_Instruction( op, inst );
        return op;
    end To_Opcode;

    ----------------------------------------------------------------------------

    function To_Opcode_S( inst : Instruction; s : Integer ) return Opcode is
        op : Opcode := 0;
    begin
        Set_Instruction( op, inst );
        Set_S( op, s );
        return op;
    end To_Opcode_S;

    ----------------------------------------------------------------------------

    function To_Opcode_U( inst : Instruction; u : Natural ) return Opcode is
        op : Opcode := 0;
    begin
        Set_Instruction( op, inst );
        Set_U( op, u );
        return op;
    end To_Opcode_U;

    ----------------------------------------------------------------------------

    type Args_Format_Array is array (Instruction) of Args_Format;
    ARGS_FORMATS : constant Args_Format_Array := (
        NOOP     => Format_None,
        PUSHC    => Format_U,
        PUSHI    => Format_S,
        PUSHN    => Format_None,
        PUSHND   => Format_None,
        PUSHNUL  => Format_U,
        PUSHS    => Format_S,
        PUSHSD   => Format_S,
        PUSHT    => Format_None,
        STORE    => Format_U,
        STORENUL => Format_S,
        REF      => Format_None,
        DEREF    => Format_None,
        POP      => Format_None,
        SWAP     => Format_None,
        INVOKE   => Format_U,
        EVAL     => Format_U,
        RET      => Format_None,
        JUMP     => Format_S,
        JUMPZ    => Format_S,
        EQ       => Format_None,
        NEQ      => Format_None,
        LT       => Format_None,
        GT       => Format_None,
        LTE      => Format_None,
        GTE      => Format_None,
        BNOT     => Format_None,
        NEG      => Format_None,
        BAND     => Format_None,
        BOR      => Format_None,
        BXOR     => Format_None,
        ADD      => Format_None,
        SUB      => Format_None,
        MUL      => Format_None,
        DIV      => Format_None,
        MODULO   => Format_None,
        POW      => Format_None,
        CONCAT   => Format_None,
        SIGN     => Format_None,
        YIELD    => Format_None
    );

    function Instruction_Format( inst : Instruction ) return Args_Format is (ARGS_FORMATS(inst));

    --==========================================================================

    function Get_Instruction( op : Opcode ) return Instruction
    is (Instruction'Val( op and INST_MASK1 ));

    ----------------------------------------------------------------------------

    function Get_S( op : Opcode ) return Integer
    is (Integer(Shift_Right( op and S_MASK1, S_POS )) - Integer(S_MAX));

    ----------------------------------------------------------------------------

    function Get_U( op : Opcode ) return Natural
    is (Natural(Shift_Right( op and U_MASK1, U_POS )));

    ----------------------------------------------------------------------------

    function Image( op : Opcode ) return String is
        inst : constant Instruction := Get_Instruction( op );
    begin
        case Instruction_Format( inst ) is
            when Format_S    => return Instruction'Image( inst ) & "(" & Trim( Integer'Image( Get_S( op ) ), Left ) & ")";
            when Format_U    => return Instruction'Image( inst ) & "(" & Trim( Natural'Image( Get_U( op ) ), Left ) & ")";
            when Format_None => return Instruction'Image( inst );
        end case;
    end Image;

    ----------------------------------------------------------------------------

    function Max_S return Integer is (Integer(S_MAX));

    ----------------------------------------------------------------------------

    function Max_U return Integer is (Integer(U_MAX));

    ----------------------------------------------------------------------------

    procedure Set_Instruction( op : in out Opcode; inst : Instruction ) is
    begin
        op := (op and not INST_MASK1) or
              (Unsigned_32(Instruction'Pos( inst )) and INST_MASK1);
    end Set_Instruction;

    ----------------------------------------------------------------------------

    procedure Set_S( op : in out Opcode; s : Integer ) is
    begin
        if s > Integer(S_MAX) or else s < -Integer(S_MAX) then
            raise Constraint_Error with "Opcode argument out of range";
        end if;
        op := (op and not S_MASK1) or
              (Shift_Left( Unsigned_32(s + Integer(S_MAX)), S_POS ) and S_MASK1);
    end Set_S;

    ----------------------------------------------------------------------------

    procedure Set_U( op : in out Opcode; u : Natural ) is
    begin
        if u > Natural(U_MAX) then
            raise Constraint_Error with "Opcode argument out of range";
        end if;
        op := (op and not U_MASK1) or
              (Shift_Left( Unsigned_32(u), U_POS ) and U_MASK1);
    end Set_U;

    --==========================================================================

    procedure Delete( oa : in out A_Opcode_Array ) is
        procedure Free is new Ada.Unchecked_Deallocation( Opcode_Array, A_Opcode_Array );
    begin
        Free( oa );
    end Delete;

end Scribble.Opcodes;
