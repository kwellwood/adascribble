--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Scribble.IO;                       use Scribble.IO;

package body Scribble.Parse_Errors is

    package Location_Vectors is new Ada.Containers.Vectors(Positive, Token_Location, "=");
    use Location_Vectors;

    type Error_Report is
        record
            msg        : Unbounded_String;           -- error messages
            loc        : Token_Location;             -- location of the error
            includedBy : Location_Vectors.Vector;    -- inclusion chain traceback; (N) is included by (N+1)
        end record;

    ----------------------------------------------------------------------------

    procedure To_Error_Report( emsg : String; report : out Error_Report ) is
        lf    : Integer;
        colon : Integer;
        last  : Integer;
        loc   : Token_Location;
    begin
        report.msg := Null_Unbounded_String;
        report.loc := UNKNOWN_LOCATION;
        report.includedBy.Clear;

        last := emsg'Last;
        lf := Index( emsg, String'(1 => ASCII.LF), last, Backward );

        if lf < emsg'First then
            -- if no ASCII.LF then 'emsg' isn't a valid Parse_Error message
            report.msg := To_Unbounded_String( emsg );
            return;
        end if;

        -- parse the error message
        report.msg := To_Unbounded_String( emsg(lf+1..last) );
        last := lf - 1;

        loop
            -- parse the column number
            colon := Index( emsg, ":", last, Backward );
            begin
                loc.column := Integer'Value( emsg(colon+1..last) );
                last := colon - 1;
            exception
                when others => return;
            end;

            -- parse the line number
            colon := Integer'Max( Index( emsg, String'(1 => ASCII.LF), last, Backward ),
                                  Index( emsg, ":", last, Backward ) );
            begin
                loc.line := Integer'Value( emsg(colon+1..last) );
                last := colon - 1;
            exception
                when others => return;
            end;

            -- parse the optional path
            -- a colon preceeding the line number indicates it exists
            if last >= emsg'First and then emsg(last+1) = ':' then
                lf := Index( emsg, String'(1 => ASCII.LF), last, Backward );
                loc.path := To_Unbounded_String( emsg(lf+1..last) );
                last := lf - 1;
            end if;

            report.includedBy.Append( loc );

            exit when last <= emsg'First;
        end loop;

        -- the first location seen (in reverse) is the actual error location
        report.loc := report.includedBy.First_Element;
        report.includedBy.Delete_First;
    end To_Error_Report;

    -- END PRIVATE DECLARATIONS --
    --==========================================================================

    function Format_Parse_Error( emsg      : String;
                                 source    : Unbounded_String := Null_Unbounded_String;
                                 omitLines : Natural := 0;
                                 omitCols  : Natural := 0
                               ) return String is

        ------------------------------------------------------------------------

        function Get_Line( text : Unbounded_String; line : Positive ) return Unbounded_String is
            first : Integer := 1;
            last  : Integer := Index( text, "" & ASCII.LF, first );
        begin
            for i in 2..line loop
                first := last + 1;
                if first > Length( text ) then
                    return Null_Unbounded_String;
                end if;
                last := Index( text, "" & ASCII.LF, first );
            end loop;

            if last < first then
                last := Length( text );
            else
                last := last - 1;      -- discard the LF
                if Element( text, last ) = ASCII.CR then
                    last := last - 1;
                end if;                -- discard the CR
            end if;

            return Unbounded_Slice( text, first, last );
        end Get_Line;

        ------------------------------------------------------------------------

        result     : Unbounded_String;
        report     : Error_Report;
        loc        : Token_Location;
        source2    : Unbounded_String;
        sourceLine : Unbounded_String;
        ok         : Boolean;
        pragma Warnings( Off, ok );
    begin
        -- parse the Parse_Error exception message
        To_Error_Report( emsg, report );
        -- locate the offending line of source code
        if Length( report.loc.path ) = 0 then
            -- use the given source code in 'source'; this is for when code is
            -- compiled directly, without reading it from a file.
            source2 := source;
        else
            -- read the source from a file on disk
            ok := Read_Source( To_String( report.loc.path ), source2 );
        end if;
        if report.loc.line > 0 then
            sourceLine := Get_Line( source2, report.loc.line );
        end if;

        if not report.includedBy.Is_Empty then
            -- if the top level of compilation is 'source' (not compiled from a
            -- file), then adjust the reported inclusion location.
            loc := report.includedBy.Last_Element;
            if Length( loc.path ) = 0 then
                loc.line := Integer'Max( loc.line - omitLines, 1 );
                loc.column := Integer'Max( loc.column - omitCols, 1 );
                report.includedBy.Replace_Element( report.includedBy.Last_Index, loc );
            end if;

            -- show the file inclusion chain
            Append( result, "In file included from " & Image( report.includedBy.First_Element ) & ASCII.LF );
            for i in 2..Integer(Length( report.includedBy )) loop
                Append( result, "                 from " & Image( report.includedBy.Element( i ) ) & ASCII.LF );
            end loop;
        else
            -- if error occured in 'source' (not compiled from a file), then
            -- adjust the reported error location.
            if Length( report.loc.path ) = 0 then
                report.loc.line := Integer'Max( report.loc.line - omitLines, 1 );
                report.loc.column := Integer'Max( report.loc.column - omitCols, 1 );

                -- when the error's column is changed, the head of the source
                -- line to be displayed also needs to be trimmed.
                if omitCols > 0 then
                    sourceLine := Tail( sourceLine, Length( sourceLine ) - omitCols );
                end if;
            end if;
        end if;

        -- show the error message and location
        Append( result, Image( report.loc ) );
        if report.loc.line = 0 and report.loc.column = 0 then
            -- the error location is at 0:0, which is not actually in the file.
            -- omit the line and column.
            Delete( result, Length( result ) - 2, Length( result ) );
        end if;
        Append( result, " " & report.msg );

        -- point to the error in source code
        if report.loc.column > 0 and Length( sourceLine ) > 0 then
            Append( result, String'(1=>ASCII.LF) );
            Append( result, String'("  " & To_String( sourceLine ) & ASCII.LF) );
            Append( result, String'((2 + report.loc.column - 1) * ".") & "^" );
        end if;

        return To_String( result );
    end Format_Parse_Error;

    ----------------------------------------------------------------------------

    procedure Raise_Parse_Error( msg : String; loc : Token_Location ) is
    begin
        raise Parse_Error with Image( loc ) & ASCII.LF & msg;
    end Raise_Parse_Error;

end Scribble.Parse_Errors;
