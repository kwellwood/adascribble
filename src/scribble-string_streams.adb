--
-- Copyright (c) 2012 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;

package body Scribble.String_Streams is

    function Stream( str : Unbounded_String ) return A_String_Stream is
        strm : constant A_String_Stream := new String_Stream;
    begin
        strm.buffer := str;
        strm.readPos := 1;
        return strm;
    end Stream;

    ----------------------------------------------------------------------------

    procedure Close( stream : in out A_String_Stream ) is
        procedure Free is new Ada.Unchecked_Deallocation( String_Stream'Class, A_String_Stream );
    begin
        if stream /= null then
            Free( stream );
        end if;
    end Close;

    ----------------------------------------------------------------------------

    overriding
    procedure Read( stream : in out String_Stream;
                    item   : out Stream_Element_Array;
                    last   : out Stream_Element_Offset ) is
    begin
        if stream.readPos <= Length( stream.buffer ) then
            last := item'First - 1;
            for i in item'Range loop
                exit when stream.readPos > Length( stream.buffer );
                item(i) := Stream_Element(Character'Pos( Element( stream.buffer, stream.readPos ) ));
                last := i;
                stream.readPos := stream.readPos + 1;
            end loop;
        else
            last := item'First - 1;
        end if;
    end Read;

    ----------------------------------------------------------------------------

    overriding
    procedure Write( stream : in out String_Stream;
                     item   : Stream_Element_Array ) is
        pragma Unreferenced( stream, item );
    begin
        raise STREAM_ERROR with "Writing to String_Stream is not supported";
    end Write;

end Scribble.String_Streams;
