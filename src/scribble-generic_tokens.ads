--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Finalization;                  use Ada.Finalization;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Interfaces;                        use Interfaces;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Values;                            use Values;

generic
    type Token_Enum is (<>);

    -- Returns the name of 'tok' in readable form. Used for error messages.
    with function Name( tok : Token_Enum ) return String;

    -- Converts a string 'img' to a Token_Enum, returning 'valid' as True on
    -- success, or False on failure. This procedure does not support tokens that
    -- do not map directly to a single string.
    with procedure To_Token_Enum( img : String; tok : out Token_Enum; valid : out Boolean );

    TOKEN_SYMBOL : Token_Enum;     -- Symbol/identifier token
    TOKEN_VALUE  : Token_Enum;     -- Literal value token
    TOKEN_EOL    : Token_Enum;     -- End-of-Line token
    TOKEN_EOF    : Token_Enum;     -- Eod-of-File token

    -- Returns true if 'c' is a delimiter character, otherwise false.
    with function Is_Delimiter( c : Character ) return Boolean;

    -- Suppress "Unreferenced" warnings because they are used by clients of this
    -- package, just not directly here.
    pragma Warnings( Off, Name );
    pragma Warnings( Off, To_Token_Enum );
    pragma Warnings( Off, TOKEN_EOL );
    pragma Warnings( Off, TOKEN_EOF );
    pragma Warnings( Off, Is_Delimiter );

package Scribble.Generic_Tokens is

    type Token_Enums is array (Integer range <>) of Token_Enum;

    -- Returns true if 'c' is an end-of-line character.
    function Is_EOL( c : Character ) return Boolean;

    -- Returns true if 'c' is a whitespace character, otherwise false.
    function Is_Whitespace( c : Character ) return Boolean;

    ----------------------------------------------------------------------------

    type Token is tagged limited private;
    type Token_Ptr is tagged private;

    function Get_Type( this : not null access Token'Class ) return Token_Enum;

    function Location( this : not null access Token'Class ) return Token_Location;

    -- Creates a new Token of type 't', located in the stream at 'loc'.
    function Create_Token( t : Token_Enum; loc : Token_Location ) return Token_Ptr;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Returns a reference to the pointer's target Token.
    function Get( this : Token_Ptr ) return access Token'Class with Inline;

    -- Returns True if the pointer's target is null.
    function Is_Null( this : Token_Ptr'Class ) return Boolean with Inline;

    -- Returns True if the pointer does reference a Token.
    function Not_Null( this : Token_Ptr'Class ) return Boolean with Inline;

    -- Sets the pointer's target back to null.
    procedure Unset( this : in out Token_Ptr'Class );

    -- Returns True if the pointers reference the same Token.
    function "="( l, r : Token_Ptr ) return Boolean;

    Null_Token : constant Token_Ptr;

    ----------------------------------------------------------------------------

    type Symbol_Token is new Token with private;

    -- Returns the name of the symbol.
    function Get_Name( this : not null access Symbol_Token'Class ) return String;

    -- Creates a new token for a symbol 'name'.
    function Create_Symbol( name : String; loc : Token_Location ) return Token_Ptr;

    ----------------------------------------------------------------------------

    type Value_Token is new Token with private;

    -- Returns a pointer to the value of the literal value token.
    function Get_Value( this : not null access Value_Token'Class ) return Value;

    -- Creates a new Value_Token representing the literal value 'val'. 'val'
    -- will be copied.
    function Create_Value( val : Value'Class; loc : Token_Location ) return Token_Ptr;

private

    type Token is tagged limited
        record
            refs : aliased Integer_32 := 0;
            t    : Token_Enum;
            loc  : Token_Location;
        end record;
    type A_Naked_Token is access all Token'Class;

    procedure Delete( this : in out Token ) is null;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Token_Ptr is new Ada.Finalization.Controlled with
        record
            target : A_Naked_Token := null;
        end record;

    overriding
    procedure Adjust( this : in out Token_Ptr );

    overriding
    procedure Finalize( this : in out Token_Ptr );

    procedure Set( this : in out Token_Ptr; target : access Token'Class );

    Null_Token : constant Token_Ptr := (Controlled with target => null);

    ----------------------------------------------------------------------------

    type Symbol_Token is new Token with
        record
            name : Unbounded_String;
        end record;

    ----------------------------------------------------------------------------

    type Value_Token is new Token with
        record
            val : Value;
        end record;

end Scribble.Generic_Tokens;
