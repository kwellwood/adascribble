--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Interfaces;                        use Interfaces;

package Scribble.Debugging is

    type Integer_Array is array (Integer range <>) of Integer;
    type A_Integer_Array is access all Integer_Array;

    type String_Array is array (Integer range <>) of Unbounded_String;
    type A_String_Array is access all String_Array;

    type Var_Use_Rec is
        record
            line : Natural;
            name : Unbounded_String;
        end record;
    type Var_Use_Array is array (Integer range <>) of Var_Use_Rec;
    type A_Var_Use_Array is access all Var_Use_Array;

    type Var_Name_Array is array (Integer range <>) of A_Var_Use_Array;
    type A_Var_Name_Array is access all Var_Name_Array;

    type Func_Name_Rec is
        record
            fid  : Unsigned_32 := 0;
            name : Unbounded_String;
        end record;
    type Func_Name_Array is array (Integer range <>) of Func_Name_Rec;
    type A_Func_Name_Array is access all Func_Name_Array;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Debug_Info is
        record
            path       : Unbounded_String;
            source     : Unbounded_String;
            firstLine  : Natural := 0;
            lines      : A_Integer_Array := null;
            paramNames : A_String_Array := null;
            varNames   : A_Var_Name_Array := null;
            funcNames  : A_Func_Name_Array := null;
        end record;
    type A_Debug_Info is access all Debug_Info;

    procedure Delete( info : in out A_Debug_Info );

end Scribble.Debugging;
