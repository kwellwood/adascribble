--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Values.Casting;                   use Values.Casting;
with Values.Construction;              use Values.Construction;
with Values.Lists;                     use Values.Lists;
with Values.Maps;                      use Values.Maps;
with Values.Strings;                   use Values.Strings;

package body Scribble.Code_Generators is

    MAX_PROGRAM_DATA : constant := 2 ** 26 - 1;  -- limited by Opcode's U argument size

    -- known offsets of activation record fields, relative to the frame pointer
    FP_OFFSET_FIRST_LOCAL_VAR : constant :=  1;
    FP_OFFSET_LAST_ARGUMENT   : constant := -3;

    -- pre-defined label names
    LABEL_COND_FALSE : constant := 1;      -- false case of conditional operator
    LABEL_COND_END   : constant := 2;      -- end of conditional operator
    LABEL_IF_CASE    : constant := 3;      -- the next case of an if statement
    LABEL_IF_END     : constant := 4;      -- end of an if statement
    LABEL_LOOP_END   : constant := 5;      -- end of a loop statement

    --==========================================================================

    function Create_Code_Generator( runtime : not null A_Scribble_Runtime ) return A_Code_Generator is
        this : constant A_Code_Generator := new Code_Generator;
    begin
        this.runtime := runtime;
        return this;
    end Create_Code_Generator;

    ----------------------------------------------------------------------------

    function Add_Data( this : not null access Code_Generator'Class;
                       val  : Value'Class ) return Positive is
        crsr : constant Value_Vectors.Cursor := this.data.Find( Value(val) );
    begin
        if Value_Vectors.Has_Element( crsr ) then
            return Value_Vectors.To_Index( crsr );
        end if;

        if this.data.Length >= MAX_PROGRAM_DATA then
            raise Constraint_Error with "Program data too large";
        end if;

        this.data.Append( Value(val) );
        return Integer(this.data.Length);
    end Add_Data;

    ----------------------------------------------------------------------------

    function Current_Function_Frame_Offset( this : not null access Code_Generator'Class ) return Integer
    is (FP_OFFSET_LAST_ARGUMENT - this.params);

    ----------------------------------------------------------------------------

    procedure Define_Label( this : not null access Code_Generator'Class;
                            name : Positive ) is
    begin
        this.fjt.Append( Label'(name => name, others => <>) );
    end Define_Label;

    ----------------------------------------------------------------------------

    function Find_Label_Index( this : not null access Code_Generator'Class;
                               name : Positive ) return Positive is
        crsr : constant Label_Vectors.Cursor := this.fjt.Reverse_Find( Label'(name, others => <>) );
    begin
        if Label_Vectors.Has_Element( crsr ) then
            return Label_Vectors.To_Index( crsr );
        end if;

        -- Due to the semantics of code generating forward jumps, the label
        -- named 'name' must have already been defined and not yet resolved
        -- (have an unknown address). Since it could not be found, there must be
        -- a bug in the code generator where a label is not defined, or defined
        -- too late, etc.
        raise Constraint_Error with "Scribble code generator bug: Label not found";
    end Find_Label_Index;

    ----------------------------------------------------------------------------

    procedure Set_Label_Address( this    : not null access Code_Generator'Class;
                                 name    : Positive;
                                 address : Natural ) is
        -- matches 'name' and it's unresolved
        crsr : constant Label_Vectors.Cursor := this.fjt.Reverse_Find( Label'(name, others => <>) );
    begin
        if Label_Vectors.Has_Element( crsr ) then
            this.fjt.Replace_Element( crsr, Label'(name, address) );
        end if;
    end Set_Label_Address;

    ----------------------------------------------------------------------------

    function Generate( this        : not null access Code_Generator'Class;
                       node        : not null A_Ast_Function_Definition;
                       enableDebug : Boolean := False ) return Function_Value is
        op           : Opcode;
        compiledOps  : A_Opcode_Array := null;
        compiledData : A_Value_Array := null;
        argDefaults  : A_Value_Array := null;
        debugInfo    : A_Debug_Info := null;
        result       : Function_Value;
    begin
        this.params := 0;
        this.instructions.Clear;
        this.data.Clear;
        this.scope := null;
        this.fjt.Clear;
        this.enableDebug := enableDebug;
        -- for debugging info
        this.lines.Clear;
        this.stmtLine := 0;
        this.funcNames.Clear;
        for list of this.varNames loop
            list.Clear;
        end loop;
        this.varNames.Clear;

        this.params := node.Parameters_Count;

        --
        -- Generate program instructions and data from the function definition
        --
        if node.Get_Internal_Name'Length > 0 then
            -- code generate a Function bound to an internal implementation

            -- push each of the arguments passed to the function onto the stack
            -- as arguments to the internal function
            for i in 1..this.params loop
                -- argument indexes are negative
                this.Op_PUSHSD( this.Ident_Index_To_Frame_Offset( -i ), 1 );
            end loop;

            -- invoke the function
            this.Op_INVOKE( node.Get_Internal_Name, Fid_Hash( node.Get_Internal_Name ), node.Parameters_Count, 1 );

            -- return the result
            this.Op_RET( 1 );
        else
            -- code generate a Function with a Scribble implementation
            this.varNames.Set_Length( Count_Type(node.Get_Body.Reserved_Variables) );
            node.Get_Body.Process( A_Ast_Processor(this) );
        end if;

        --
        -- Build the instruction list
        --
        compiledOps := new Opcode_Array(1..Integer(this.instructions.Length));
        for i in 1..Integer(this.instructions.Length) loop
            op := this.instructions.Element( i );

            -- check for a forward jump instruction
            if Get_Instruction( op ) = JUMP or else Get_Instruction( op ) = JUMPZ then
                if Get_S( op ) >= 0 then
                    -- resolve the jump distance using the target label's address
                    Set_S( op, this.fjt.Element( Get_S( op ) ).address - i );
                end if;
            end if;

            compiledOps(i) := op;
        end loop;

        --
        -- Build the program data array
        --
        compiledData := new Value_Array(1..Integer(this.data.Length));
        for i in compiledData'Range loop
            compiledData(i) := this.data.Element( i );
        end loop;

        --
        -- Build the argument defaults array
        --
        for i in 1..node.Parameters_Count loop
            if argDefaults = null and then node.Get_Parameter_Default( i ) /= null then
                argDefaults := new Value_Array(i..node.Parameters_Count);
            end if;
            if argDefaults /= null then
                pragma Assert( node.Get_Parameter_Default( i ) /= null,
                               "Ast_Function_Definition returned null parameter default" );
                argDefaults(i) := A_Ast_Literal(node.Get_Parameter_Default( i )).Get_Value;
            end if;
        end loop;
        if argDefaults = null then
            -- no default arguments
            argDefaults := new Value_Array(1..0);
        end if;

        --
        -- Create the debugging information
        --
        if this.enableDebug then
            debugInfo := this.Generate_Debug_Info( node );
        end if;

        --
        -- Create the Function value
        --
        result := Create_Function( node.Parameters_Count, argDefaults.all,
                                   compiledOps.all, compiledData.all,
                                   node.Is_Member,
                                   (if node.Get_Name /= null then node.Get_Name.Get_Name else ""),
                                   debugInfo ).Func;  -- 'debugInfo' is consumed

        --
        -- Clean up
        --
        Delete( compiledOps );
        Delete( compiledData );
        Delete( argDefaults );
        this.params := 0;
        this.instructions.Clear;
        this.data.Clear;
        this.scope := null;
        this.fjt.Clear;
        -- for debugging info
        this.lines.Clear;
        this.stmtLine := 0;
        this.funcNames.Clear;
        for list of this.varNames loop
            list.Clear;
        end loop;
        this.varNames.Clear;

        return result;
    end Generate;

    ----------------------------------------------------------------------------

    function Generate_Debug_Info( this : not null access Code_Generator'Class;
                                  node : not null A_Ast_Function_Definition ) return A_Debug_Info is

        ------------------------------------------------------------------------

        -- Subtracts 'offset' from location 'l'. The resulting location will be
        -- no less than 1:1 at absolute position 1. The column will only be
        -- adjusted if the line of the offset matches the line of the location 'l'.
        function Subtract_Offset( l, offset : Token_Location ) return Token_Location is
            result : Token_Location;
        begin
            result.line := Integer'Max( l.line - offset.line, 1 );
            result.column := (if result.line > 1 then offset.column else Integer'Max( l.column - offset.column, 1 ));
            result.absolute := Integer'Max( l.absolute - offset.absolute, 1 );
            result.path := l.path;
            return result;
        end Subtract_Offset;

        ------------------------------------------------------------------------

        debugInfo : constant A_Debug_Info := new Debug_Info;
    begin
        -- instruction lines
        debugInfo.firstLine := node.Location.line;
        debugInfo.lines := new Integer_Array(1..Integer(this.lines.Length));
        pragma Assert( debugInfo.lines'Length = this.instructions.Length, "Invalid lines length" );
        for i in debugInfo.lines'Range loop
            if this.lines.Element( i ) > 0 then
                debugInfo.lines(i) := Subtract_Offset( (line     => this.lines.Element( i ),
                                                        column   => 1,
                                                        absolute => 1,
                                                        path     => node.Location.path),
                                                       node.Get_Source_Offset ).line;
            else
                debugInfo.lines(i) := 0;
            end if;
        end loop;

        -- parameter names
        debugInfo.paramNames := new String_Array(1..node.Parameters_Count);
        for i in debugInfo.paramNames'Range loop
            debugInfo.paramNames(i) := To_Unbounded_String( node.Get_Parameter( i ).Get_Name );
        end loop;

        -- variable names
        debugInfo.varNames := new Var_Name_Array(1..Integer(this.varNames.Length));
        for i in debugInfo.varNames'Range loop
            declare
                use Var_Lists;
                varList : constant Var_Lists.List := this.varNames.Element( i );
                crsr    : Cursor := varList.First;
            begin
                debugInfo.varNames(i) := new Var_Use_Array(1..Integer(varList.Length));
                for j in debugInfo.varNames(i)'Range loop
                    -- calculate the line of the variable declaration statement,
                    -- relative to the function's source code snippet.
                    debugInfo.varNames(i).all(j).line := Subtract_Offset( Element( crsr ).loc,
                                                                          node.Get_Source_Offset ).line;
                    debugInfo.varNames(i).all(j).name := To_Unbounded_String( Element( crsr ).ident.Get_Name );
                    Next( crsr );
                end loop;
            end;
        end loop;

        -- function names
        debugInfo.funcNames := new Func_Name_Array(1..Integer(this.funcNames.Length));
        declare
            use Func_Name_Maps;
            crsr : Cursor := this.funcNames.First;
        begin
            for i in debugInfo.funcNames'Range loop
                debugInfo.funcNames(i).fid := Unsigned_32(Key( crsr ));
                debugInfo.funcNames(i).name := Element( crsr );
                Next( crsr );
            end loop;
        end;

        -- source code
        debugInfo.source := node.Get_Source;
        debugInfo.firstLine := node.Location.line;

        debugInfo.path := To_Unbounded_String( node.Get_Path );

        return debugInfo;
    end Generate_Debug_Info;

    ----------------------------------------------------------------------------

    function Ident_Index_To_Frame_Offset( this  : not null access Code_Generator'Class;
                                          index : Integer ) return Integer is
    begin
        --
        -- Organization of values within the stack, relative to the frame pointer:
        --
        --                                      FP_OFFSET_FIRST_LOCAL_VAR
        --                                      \/
        --          +----+----+----+   +--+   +----+----+----+
        --       ...|arg1|arg2|arg3|...|  |...|var1|var2|var3|...
        --          +----+----+----+   +--+   +----+----+----+
        --                      /\     /\
        -- FP_OFFSET_LAST_ARGUMENT     Frame pointer
        --
        if index < 0 then
            -- argument index; located before the frame pointer
            return (FP_OFFSET_LAST_ARGUMENT - this.params) - index;
        else -- index > 0
            -- local variable index; located after the frame pointer
            return FP_OFFSET_FIRST_LOCAL_VAR + (index - 1);
        end if;
    end Ident_Index_To_Frame_Offset;

    ----------------------------------------------------------------------------

    procedure Op_DEREF( this : not null access Code_Generator'Class;
                        line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( DEREF ) );
        this.lines.Append( line );
    end Op_DEREF;

    ----------------------------------------------------------------------------

    procedure Op_EVAL( this : not null access Code_Generator'Class;
                       args : Natural;
                       line : Natural ) is
    begin
        this.instructions.Append( To_Opcode_U( EVAL, args ) );
        this.lines.Append( line );
    end Op_EVAL;

    ----------------------------------------------------------------------------

    procedure Op_INVOKE( this : not null access Code_Generator'Class;
                         name : String;
                         fid  : Function_Id;
                         args : Natural;
                         line : Natural ) is
    begin
        this.Op_PUSHC( To_Id_Value( fid ), line );
        this.instructions.Append( To_Opcode_U( INVOKE, args ) );
        this.lines.Append( line );
        this.funcNames.Include( fid, To_Unbounded_String( name ) );
    end Op_INVOKE;

    ----------------------------------------------------------------------------

    procedure Op_JUMP( this   : not null access Code_Generator'Class;
                       target : Integer;
                       line   : Natural ) is
    begin
        this.instructions.Append( To_Opcode_S( JUMP, (if target < 0 then target else this.Find_Label_Index( target )) ) );
        this.lines.Append( line );
    end Op_JUMP;

    ----------------------------------------------------------------------------

    procedure Op_JUMPZ( this   : not null access Code_Generator'Class;
                        target : Integer;
                        line   : Natural ) is
    begin
        this.instructions.Append( To_Opcode_S( JUMPZ, (if target < 0 then target else this.Find_Label_Index( target )) ) );
        this.lines.Append( line );
    end Op_JUMPZ;

    ----------------------------------------------------------------------------

    procedure Op_NOOP( this : not null access Code_Generator'Class;
                       line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( NOOP ) );
        this.lines.Append( line );
    end Op_NOOP;

    ----------------------------------------------------------------------------

    procedure Op_YIELD( this : not null access Code_Generator'Class;
                        line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( YIELD ) );
        this.lines.Append( line );
    end Op_YIELD;

    ----------------------------------------------------------------------------

    procedure Op_POP( this : not null access Code_Generator'Class;
                      line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( POP ) );
        this.lines.Append( line );
    end Op_POP;

    ----------------------------------------------------------------------------

    procedure Op_PUSHC( this : not null access Code_Generator'Class;
                        val  : Value'Class;
                        line : Natural ) is
        op : Opcode;
    begin
        if val.Is_Number and then
           val.Is_Int and then
           val.To_Int >= -Opcodes.Max_S and then val.To_Int <= Opcodes.Max_S
        then
            op := To_Opcode_S( PUSHI, val.To_Int );
        elsif val.Is_Null then
            op := To_Opcode_U( PUSHNUL, 1 );
        else
            op := To_Opcode_U( PUSHC, this.Add_Data( val ) );
        end if;
        this.instructions.Append( op );
        this.lines.Append( line );
    end Op_PUSHC;

    ----------------------------------------------------------------------------

    procedure Op_PUSHN( this      : not null access Code_Generator'Class;
                        namespace : String;
                        name      : String;
                        line      : Natural ) is
    begin
        if name /= "" then
             this.Op_PUSHC( Strings.Create( name ), line );
        end if;
        this.Op_PUSHC( Strings.Create( namespace ), line );
        this.instructions.Append( To_Opcode( PUSHN ) );
        this.lines.Append( line );
    end Op_PUSHN;

    ----------------------------------------------------------------------------

    procedure Op_PUSHND( this      : not null access Code_Generator'Class;
                         namespace : String;
                         name      : String;
                         line      : Natural ) is
    begin
        if name /= "" then
             this.Op_PUSHC( Strings.Create( name ), line );
        end if;
        this.Op_PUSHC( Strings.Create( namespace ), line );
        this.instructions.Append( To_Opcode( PUSHND ) );
        this.lines.Append( line );
    end Op_PUSHND;

    ----------------------------------------------------------------------------

    procedure Op_PUSHNUL( this  : not null access Code_Generator'Class;
                          count : Integer;
                          line  : Natural ) is
    begin
        if count /= 0 then
            this.instructions.Append( To_Opcode_U( PUSHNUL, count ) );
            this.lines.Append( line );
        end if;
    end Op_PUSHNUL;

    ----------------------------------------------------------------------------

    procedure Op_PUSHS( this   : not null access Code_Generator'Class;
                         offset : Integer;
                         line   : Natural ) is
    begin
        this.instructions.Append( To_Opcode_S( PUSHS, offset ) );
        this.lines.Append( line );
    end Op_PUSHS;

    ----------------------------------------------------------------------------

    procedure Op_PUSHSD( this   : not null access Code_Generator'Class;
                         offset : Integer;
                         line   : Natural ) is
    begin
        this.instructions.Append( To_Opcode_S( PUSHSD, offset ) );
        this.lines.Append( line );
    end Op_PUSHSD;

    ----------------------------------------------------------------------------

    procedure Op_PUSHT( this : not null access Code_Generator'Class;
                        line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( PUSHT ) );
        this.lines.Append( line );
    end Op_PUSHT;

    ----------------------------------------------------------------------------

    procedure Op_RET( this : not null access Code_Generator'Class;
                      line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( RET ) );
        this.lines.Append( line );
    end Op_RET;

    ----------------------------------------------------------------------------

    procedure Op_STORE( this     : not null access Code_Generator'Class;
                        assignOp : Assign_Op;
                        line     : Natural ) is
    begin
        this.instructions.Append( To_Opcode_U( STORE, Assign_Op'Pos( assignOp ) ) );
        this.lines.Append( line );
    end Op_STORE;

    ----------------------------------------------------------------------------

    procedure Op_STORENUL( this   : not null access Code_Generator'Class;
                           offset : Integer;
                           line   : Natural ) is
    begin
        this.instructions.Append( To_Opcode_S( STORENUL, offset ) );
        this.lines.Append( line );
    end Op_STORENUL;

    ----------------------------------------------------------------------------

    procedure Op_SWAP( this : not null access Code_Generator'Class;
                       line : Natural ) is
    begin
        this.instructions.Append( To_Opcode( SWAP ) );
        this.lines.Append( line );
    end Op_SWAP;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Literal( this : access Code_Generator; node : A_Ast_Literal ) is
    begin
        this.Op_PUSHC( node.Get_Value, this.stmtLine );
    end Process_Literal;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Map( this : access Code_Generator; node : A_Ast_Map ) is
        val : constant Map_Value := Create_Map.Map;
    begin
        -- A literal map is generated as a push of the list with the literal
        -- key/value pairs filled in, followed by a series of Assign-Index
        -- operations to evaluate the keys and values of pairs that could not be
        -- fully known at compile time (contain expressions).

        -- push a map containing all its literal key/value pairs
        for i in 1..node.Length loop
            if node.Get_Key( i ).Get_Type = LITERAL and then
               node.Get_Value( i ).Get_Type = LITERAL
            then
               -- insert the literal pair into the map
               val.Set( Cast_String( A_Ast_Literal(node.Get_Key( i )).Get_Value ),
                        A_Ast_Literal(node.Get_Value( i )).Get_Value,
                        consume => True );
            end if;
        end loop;
        this.Op_PUSHC( Value(val), this.stmtLine );

        -- find all the expression pairs and generate them as inserts into the
        -- the map
        for i in 1..node.Length loop
            if node.Get_Key( i ).Get_Type /= LITERAL or else
               node.Get_Value( i ).Get_Type /= LITERAL
            then
                node.Get_Key( i ).Process( A_Ast_Processor(this) );
                node.Get_Value( i ).Process( A_Ast_Processor(this) );

                -- $buildMap( target : map, key : string, value : any ) return map
                this.Op_INVOKE( "$buildMap", FID_BUILD_MAP, 3, this.stmtLine );
            end if;
        end loop;
    end Process_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_List( this : access Code_Generator; node : A_Ast_List ) is
        elems : Value_Array(1..node.Length);
    begin
        -- A literal List is generated as a push of the list with the literal
        -- elements filled in, followed by a series of Assign-to-Index
        -- operations to evaluate and set the elements that could not be known
        -- at compile time. The actual size of the List is known at compile
        -- time, so the dynamic elements yet to be assigned are stored as Null
        -- elements.

        -- push a list containing all its literal values with Null placeholders
        for i in 1..node.Length loop
            if node.Get_Element( i ).Get_Type = LITERAL then
                -- insert the literal value into the list
                elems(i) := A_Ast_Literal(node.Get_Element( i )).Get_Value;
            else
                -- insert a Null placeholder
                elems(i) := Null_Value;
            end if;
        end loop;
        this.Op_PUSHC( Create_List( elems, consume => True ), this.stmtLine );

        -- find all the expression elements and generate them as assignments
        -- into the list
        for i in 1..node.Length loop
            if node.Get_Element( i ).Get_Type /= LITERAL then
                this.Op_PUSHC( Create( i ), this.stmtLine );
                node.Get_Element( i ).Process( A_Ast_Processor(this) );

                -- $buildList( target : list, index : number, value : any ) return list
                this.Op_INVOKE( "$buildList", FID_BUILD_LIST, 3, this.stmtLine );
            end if;
        end loop;
    end Process_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Call( this : access Code_Generator; node : A_Ast_Function_Call ) is
    begin
        -- calling convention for Ada functions
        if node.Get_Call_Expression.Get_Type = BUILTIN then
            for i in 1..node.Arguments_Count loop
                node.Get_Argument( i ).Process( A_Ast_Processor(this) );
            end loop;
            this.Op_INVOKE( A_Ast_Builtin(node.Get_Call_Expression).Get_Name,
                            A_Ast_Builtin(node.Get_Call_Expression).Get_Fid,
                            node.Arguments_Count,
                            this.stmtLine );

        -- calling convention for Scribble functions
        else
            node.Get_Call_Expression.Process( A_Ast_Processor(this) );
            for i in 1..node.Arguments_Count loop
                node.Get_Argument( i ).Process( A_Ast_Processor(this) );
                -- If the argument expression is a reference operator, then
                -- the result of evaluating the argument expression will
                -- be a reference to some storage location and we'll just
                -- push it on the stack (pass-by-reference).
                -- Otherwise, the expression will result in a value to be
                -- passed as an argument (pass-by-value).
            end loop;
            this.Op_EVAL( node.Arguments_Count, this.stmtLine );
        end if;
    end Process_Function_Call;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Member_Call( this : access Code_Generator; node : A_Ast_Member_Call ) is
    begin
        -- 1. push a reference to the object onto the stack
        node.Get_Call_Expression.Process( A_Ast_Processor(this) );

        -- 2. duplicate the reference value on the top. this duplicate will be
        -- consumed by the $member2 operation.
        this.Op_PUSHT( this.stmtLine );

        -- 3. push the member name for the MEMBER_REF operation.
        this.Op_PUSHC( Create( node.Get_Member_Name ), this.stmtLine );

        -- 4. consume the object reference and the member name on the stack to
        -- produce the function value to invoke.
        this.Op_INVOKE( FUNC_MEMBER, FID_MEMBER, 2, this.stmtLine );

        -- 5. swaps the object reference and the function such that the object
        -- reference is now on top of the stack. it becomes the first argument
        -- to the function (pass-by-reference).
        this.Op_SWAP( this.stmtLine );

        -- 6. push the remaining arguments
        for i in 1..node.Arguments_Count loop
            node.Get_Argument( i ).Process( A_Ast_Processor(this) );
            -- If the argument expression is a reference operator, then
            -- the result of evaluating the argument expression will
            -- be a reference to some storage location and we'll just
            -- push it on the stack (pass-by-reference).
            -- Otherwise, the expression will result in a value to be
            -- passed as an argument (pass-by-value).
        end loop;

        -- 7. invoke the member function
        this.Op_EVAL( 1 + node.Arguments_Count, this.stmtLine );
    end Process_Member_Call;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Self( this : access Code_Generator; node : A_Ast_Self ) is
        pragma Unreferenced( node );
    begin
        -- push the currently executing function from the activation record
        this.Op_PUSHSD( this.Current_Function_Frame_Offset, this.stmtLine );
    end Process_Self;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_This( this : access Code_Generator; node : A_Ast_This ) is
    begin
        -- push the first argument
        if node.Is_Reference_Expression then
            this.Op_PUSHS( this.Ident_Index_To_Frame_Offset( -1 ), this.stmtLine );
        else
            this.Op_PUSHSD( this.Ident_Index_To_Frame_Offset( -1 ), this.stmtLine );
        end if;
    end Process_This;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Identifier( this : access Code_Generator; node : A_Ast_Identifier ) is
        index : constant Integer := this.scope.Resolve_Identifier_Index( node );
    begin
        if node.Is_Reference_Expression then
            -- code generating an assignment instead of a value
            if index /= 0 then
                -- reference to a local variable or parameter
                this.Op_PUSHS( this.Ident_Index_To_Frame_Offset( index ), this.stmtLine );
            else
                -- reference into a namespace
                this.Op_PUSHN( node.Get_Namespace, node.Get_Name, this.stmtLine );
            end if;
        else
            -- code generate for a value read
            if index /= 0 then
                -- reading a local variable or parameter
                this.Op_PUSHSD( this.Ident_Index_To_Frame_Offset( index ), this.stmtLine );
            else
                -- reading a name from a namespace
                this.Op_PUSHND( node.Get_Namespace, node.Get_Name, this.stmtLine );
            end if;
        end if;
    end Process_Identifier;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Membership( this : access Code_Generator; node : A_Ast_Membership ) is
    begin
        node.Get_Expression.Process( A_Ast_Processor(this) );
        this.Op_PUSHC( Strings.Create( node.Get_Name ), this.stmtLine );
        if node.Is_Reference_Expression then
            this.Op_INVOKE( FUNC_MEMBER_REF, FID_MEMBER_REF, 2, this.stmtLine );
        else
            this.Op_INVOKE( FUNC_MEMBER, FID_MEMBER, 2, this.stmtLine );
        end if;
    end Process_Membership;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Name_Expr( this : access Code_Generator; node : A_Ast_Name_Expr ) is
    begin
        node.Get_Name.Process( A_Ast_Processor(this) );
        if node.Is_Reference_Expression then
            this.Op_PUSHN( node.Get_Namespace, "", this.stmtLine );
        else
            this.Op_PUSHND( node.Get_Namespace, "", this.stmtLine );
        end if;
    end Process_Name_Expr;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Unary_Op( this : access Code_Generator; node : A_Ast_Unary_Op ) is
    begin
        node.Get_Right.Process( A_Ast_Processor(this) );
        if node.Get_Instruction /= NOOP then
            -- implemented with a machine instruction
            this.instructions.Append( To_Opcode( node.Get_Instruction ) );
            this.lines.Append( this.stmtLine );
        else
            -- implemented with a runtime function
            this.Op_INVOKE( '"' & node.Get_Function_Name & '"',
                            node.Get_Function_Id, 1, this.stmtLine );
        end if;
    end Process_Unary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Reference( this : access Code_Generator; node : A_Ast_Reference ) is
    begin
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Reference;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Binary_Op( this : access Code_Generator; node : A_Ast_Binary_Op ) is
    begin
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );
        if node.Get_Instruction /= NOOP then
            -- implemented with a machine instruction
            this.instructions.Append( To_Opcode( node.Get_Instruction ) );
            this.lines.Append( this.stmtLine );
        else
            -- implemented with a runtime function
            this.Op_INVOKE( '"' & node.Get_Function_Name & '"',
                            node.Get_Function_Id, 2, this.stmtLine );
        end if;
    end Process_Binary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Assign( this : access Code_Generator; node : A_Ast_Assign ) is
    begin
        node.Get_Right.Process( A_Ast_Processor(this) );   -- push the value to assign
        node.Get_Left.Process( A_Ast_Processor(this) );    -- push the reference to assign into
        this.Op_STORE( node.Get_Operation, this.stmtLine );
    end Process_Assign;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Index( this : access Code_Generator; node : A_Ast_Index ) is
    begin
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );

        if node.Is_Reference_Expression then
            this.Op_INVOKE( FUNC_INDEX_REF, FID_INDEX_REF, 2, this.stmtLine );   -- returns a reference to index
        else
            this.Op_INVOKE( FUNC_INDEX, FID_INDEX, 2, this.stmtLine );           -- returns value at index
        end if;
    end Process_Index;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Conditional( this : access Code_Generator; node : A_Ast_Conditional ) is
    begin
        this.Define_Label( LABEL_COND_END );
        this.Define_Label( LABEL_COND_FALSE );

        -- evaluate the condition and jump
        node.Get_Condition.Process( A_Ast_Processor(this) );
        this.Op_JUMPZ( LABEL_COND_FALSE, this.stmtLine );

        -- process the True case and jump to the end
        node.Get_True_Case.Process( A_Ast_Processor(this) );
        this.Op_JUMP( LABEL_COND_END, this.stmtLine );

        -- process the False case
        this.Set_Label_Address( LABEL_COND_FALSE, this.instructions.Last_Index + 1 );
        node.Get_False_Case.Process( A_Ast_Processor(this) );   -- label: LABEL_COND_FALSE

        -- use a no-op as a landing instruction
        this.Op_NOOP( this.stmtLine );                          -- label: LABEL_COND_END
        this.Set_Label_Address( LABEL_COND_END, this.instructions.Last_Index );
    end Process_Conditional;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Definition( this : access Code_Generator; node : A_Ast_Function_Definition ) is
    begin
        if this.child = null then
            this.child := Create_Code_Generator( this.runtime );
        end if;
        this.Op_PUSHC( this.child.Generate( node, this.enableDebug ), this.stmtLine );
    end Process_Function_Definition;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Block( this : access Code_Generator; node : A_Ast_Block ) is
        parent    : constant A_Ast_Block := this.scope;
        offset    : Natural;
        statement : A_Ast_Statement;
    begin
        -- node MUST be a child scope of the current scope
        -- if it's not, the AST is inconsistent and there is a bug in the compiler
        pragma Assert( node.Get_Parent = parent, "parent/scope mismatch in AST" );

        this.stmtLine := node.Location.line;
        this.scope := node;           -- push scope

        -- record the variables declared in this block (for debugging info)
        for i in 1..node.Variable_Count loop
            declare
                procedure Add_Use( varList : in out Var_Lists.List ) is
                begin
                    varList.Append( Var_Use_Rec'(node.Get_Nth_Variable( i ),
                                                 node.Get_Nth_Variable_Location( i )) );
                end Add_Use;
            begin
                this.varNames.Update_Element( node.Variables_Offset + i, Add_Use'Access );
            end;
        end loop;

        if parent = null then
            -- this is the root scope; reserve space for all local variables on the stack
            this.Op_PUSHNUL( node.Reserved_Variables, this.stmtLine );
        else
            -- this is a nested scope so we should clear the local variables on
            -- the stack that are to be used. they may have been previously used
            -- by a sibling scope and need to be reinitialized.
            if node.Variable_Count > 0 then
                offset := node.Variables_Offset;
                for i in 1..node.Variable_Count loop
                    this.Op_STORENUL( FP_OFFSET_FIRST_LOCAL_VAR + (offset + i - 1), this.stmtLine );
                end loop;
            end if;
        end if;

        statement := node.Get_First;
        while statement /= null loop
            statement.Process( A_Ast_Processor(this) );
            statement := statement.Get_Next;
        end loop;

        this.scope := parent;         -- pop scope
    end Process_Block;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Exit( this : access Code_Generator; node : A_Ast_Exit ) is
    begin
        -- jump to the end of the innermost loop
        -- the semantic analyzer ensures that the exit statement is within a loop
        this.stmtLine := node.Location.line;
        this.Op_JUMP( LABEL_LOOP_END, this.stmtLine );
    end Process_Exit;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Expression_Statement( this : access Code_Generator; node : A_Ast_Expression_Statement ) is
    begin
        this.stmtLine := node.Location.line;
        node.Get_Expression.Process( A_Ast_Processor(this) );
        this.Op_POP( this.stmtLine );       -- discard the result
    end Process_Expression_Statement;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_If( this : access Code_Generator; node : A_Ast_If ) is
    begin
        --
        -- An if-elsif-else instruction is generated with the following format:
        --
        -- OPERATION                        LABEL
        -- ---------                        -----
        -- Evaluate condition 0             (no label)
        -- ...
        -- Evaluate condition N             LABEL_IF_CASE (jump from condition N-1)
        -- Jump if false to condition N+1
        --     Execute case N
        --     Jump to end
        -- Evaluate condition N+1           LABEL_IF_CASE (jump from condition N)
        -- Jump if false to condition N+2
        --     Execute case N+1
        --     Jump to end
        -- ...
        -- Execute case else                LABEL_IF_CASE (jump from last condition)
        -- End (no-op)                      LABEL_IF_END  (jump from end of each case block)
        --

        -- define the label for the end of the if-statement
        this.Define_Label( LABEL_IF_END );

        for i in 1..node.Case_Count loop
            -- the address of this case's label is now known; case 1 doesn't
            -- need a label
            if i > 1 then
                this.Set_Label_Address( LABEL_IF_CASE, this.instructions.Last_Index + 1 );
            end if;

            -- define the label for the next case that follows this one. if no
            -- more cases follow this, then it will share its location with
            -- LABEL_IF_END.
            this.Define_Label( LABEL_IF_CASE );

            -- evaluate the case's condition and jump to the next case when the
            -- result is false
            this.stmtLine := node.Get_Case_Location( i ).line;
            node.Get_Case_Condition( i ).Process( A_Ast_Processor(this) );      -- label: LABEL_IF_CASE
            this.Op_JUMPZ( LABEL_IF_CASE, this.stmtLine );

            -- execute the case's block when its condition tests true
            node.Get_Case_Block( i ).Process( A_Ast_Processor(this) );

            -- jump to the end of the statement if more cases follow this one
            if i < node.Case_Count or else node.Get_Else_Block /= null then
                -- sharing the line number of the previous statement
                this.Op_JUMP( LABEL_IF_END, this.stmtLine );
            end if;
        end loop;

        -- generate the else case as the final case, without a condition
        if node.Get_Else_Block /= null then
            -- the address of the else case's label is now known
            this.Set_Label_Address( LABEL_IF_CASE, this.instructions.Last_Index + 1 );

            -- generate the block to execute when all previous conditions
            -- tested false
            node.Get_Else_Block.Process( A_Ast_Processor(this) );               -- label: LABEL_IF_CASE

            -- define the label at the end of the if statement as the next case.
            -- this isn't actually necessary since, since we won't jump to it,
            -- but it simplifies the logic in the loop above by avoiding the
            -- need to know if there is a next case at the point where we define
            -- the label for the next case.
            this.Define_Label( LABEL_IF_CASE );
        end if;

        -- use a no-op as a landing instruction for the end of the statement
        this.Set_Label_Address( LABEL_IF_CASE, this.instructions.Last_Index + 1 );
        this.Set_Label_Address( LABEL_IF_END, this.instructions.Last_Index + 1 );
        this.Op_NOOP( line => 0 );                                              -- label: LABEL_IF_END
    end Process_If;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Loop( this : access Code_Generator; node : A_Ast_Loop ) is
        loopStartAddress : Natural;
    begin
        this.Define_Label( LABEL_LOOP_END );

        loopStartAddress := this.instructions.Last_Index + 1;
        node.Get_Body.Process( A_Ast_Processor(this) );                                        -- start of the loop
        pragma Assert( this.instructions.Last_Index >= loopStartAddress,                       -- can't have an empty loop
                       "no instructions in loop" );
        this.Op_JUMP( loopStartAddress - (this.instructions.Last_Index + 1), this.stmtLine );  -- jump back to the start

        -- use a no-op as a landing instruction after the loop
        this.Op_NOOP( line => 0 );                                              -- label: LABEL_LOOP_END
        this.Set_Label_Address( LABEL_LOOP_END, this.instructions.Last_Index );
    end Process_Loop;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Return( this : access Code_Generator; node : A_Ast_Return ) is
    begin
        this.stmtLine := node.Location.line;
        node.Get_Expression.Process( A_Ast_Processor(this) );
        this.Op_RET( this.stmtLine );
    end Process_Return;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Var( this : access Code_Generator; node : A_Ast_Var ) is
    begin
        -- evaluate the variable's initial value and assign it into the stack
        this.stmtLine := node.Location.line;
        node.Get_Expression.Process( A_Ast_Processor(this) );
        this.Op_POP( this.stmtLine );
    end Process_Var;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Yield( this : access Code_Generator; node : A_Ast_Yield ) is
    begin
        this.stmtLine := node.Location.line;
        this.Op_YIELD( this.stmtLine );
    end Process_Yield;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Code_Generator ) is
        procedure Free is new Ada.Unchecked_Deallocation( Code_Generator'Class, A_Code_Generator );
    begin
        if this /= null then
            Delete( this.child );
            Free( this );
        end if;
    end Delete;

end Scribble.Code_Generators;
