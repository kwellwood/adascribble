--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.CLI;                      use Scribble.CLI;
with Scribble.VMs;
with Values.Lists;                      use Values.Lists;
with Values.Strings;                    use Values.Strings;

procedure Main is

    procedure Display_Title is
    begin
        Output_Line( "" );
        Output_Line( "scribble v0.6.0 - Kevin Wellwood Copyright (C) 2014-2019" );
        Output_Line( "language version" & Integer'Image( Scribble.LANGUAGE_VERSION ) );
        Output_Line( "" );
    end Display_Title;

    ----------------------------------------------------------------------------

    procedure Display_Usage is
    begin
        Display_Title;
        Output_Line( "Usage:" );
        Output_Line( "  scribble -a <compiled-file> [-sub <indices>]" );
        Output_Line( "  scribble -c [switches] <source-file> <compiled-file>" );
        Output_Line( "  scribble -p <compiled-file> [-sub <indices>]" );
        Output_Line( "  scribble -i [switches]" );
        Output_Line( "" );
        Output_Line( "Where:" );
        Output_Line( "  <compiled-file> : Path of the compiled Scribble file (*.sco)" );
        Output_Line( "  <source-file>   : Path of the Scribble source file (*.sv, *.sp)" );
        Output_Line( "" );
        Output_Line( "Operations:" );
        Output_Line( "  -a : Displays instruction disassembly for <compiled-file>" );
        Output_Line( "       -sub : Index into sub value of <compiled-file> with <indices>" );
        Output_Line( "  -c : Compiles Scribble <source-file> into <compiled-file>" );
        Output_Line( "       Switches:" );
        Output_Line( "         -nXYZ : Declare XYZ as a valid namespace" );
        Output_Line( "         -d    : Include debugging info" );
        Output_Line( "  -p : Prints the source code of <compiled-file> (if available)" );
        Output_Line( "       -sub : Index into sub value of <compiled-file> with <indices>" );
        Output_Line( "  -i : Interactive mode" );
        Output_Line( "       Switches:" );
        Output_Line( "         -nXYZ : Declare XYZ as a valid namespace" );
        Output_Line( "" );
    end Display_Usage;

    ----------------------------------------------------------------------------

    function Match_Arg( arg : String; head : String ) return Boolean is
        larg : constant String := To_Lower( arg );
    begin
        return larg'Length >= head'Length and then
               larg(larg'First..larg'First+head'Length-1) = To_Lower( head );
    end Match_Arg;

    ----------------------------------------------------------------------------

    sourcePath   : Unbounded_String;
    namespaces   : String_Vectors.Vector;
    enableDebug  : Boolean := False;
    compiledPath : Unbounded_String;
    stat         : Exit_Status := Bad_Arguments;
    indices      : List_Value;
begin
    Scribble.VMs.Set_Output( Output_Line'Access );
    Scribble.VMs.Set_Debug_Output( null );

    if Argument_Count < 1 then
        Display_Usage;
        Set_Exit_Status( Success );
        return;
    end if;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Compile a source file into an object file
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if Argument( 1 ) = "-c" then
        if Argument_Count < 2 then
            Output_Line( "Missing <source-file> argument" );
            Display_Usage;
            Set_Exit_Status( stat );
            return;
        end if;

        for i in 2..Argument_Count loop
            if Argument( i ) = "-n" then
                namespaces.Append( "" );   -- global namespace
            elsif Match_Arg( Argument( i ), "-n" ) then
                namespaces.Append( Tail( Argument( i ), Argument( i )'Length - 2 ) );
            elsif Argument( i ) = "-d" then
                enableDebug := True;
            elsif Match_Arg( Argument( i ), "-" ) then
                Output_Line( "Unrecognized switch: " & Argument( i ) );
                Set_Exit_Status( stat );
                return;
            else
                -- found the source file argument
                sourcePath := To_Unbounded_String( Argument( i ) );
                compiledPath := To_Unbounded_String( Replace_Extension( To_String( sourcePath ), "sco" ) );
                if i < Argument_Count then
                    -- found the compiled file argument
                    compiledPath := To_Unbounded_String( Argument( i + 1 ) );
                end if;
                if i + 1 < Argument_Count then
                    Output_Line( "Unexpected arguments" );
                    Set_Exit_Status( stat );
                    return;
                end if;
                exit;
            end if;
        end loop;

        if Length( sourcePath ) = 0 then
            Output_Line( "Missing <source-file> argument" );
            Set_Exit_Status( stat );
            return;
        end if;

        stat := Compile_Object( To_String( sourcePath ),
                                To_String( compiledPath ),
                                namespaces,
                                enableDebug );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Display the source of an object file
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    elsif Argument( 1 ) = "-p" then
        if Argument_Count < 2 then
            Output_Line( "Missing <compiled-file> argument" );
            Display_Usage;
            Set_Exit_Status( stat );
            return;
        elsif Argument_Count > 2 then
            if Argument( 3 ) /= "-sub" or else Argument_Count < 4 then
                Output_Line( "Unexpected arguments" );
                Display_Usage;
                Set_Exit_Status( stat );
                return;
            end if;
            indices := Create_List.Lst;
            for i in 4..Argument_Count loop
                indices.Append( Create( Argument( i ) ) );
            end loop;
        end if;
        stat := Display_Source( Argument( 2 ), indices );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Interactive interpretor
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    elsif Argument( 1 ) = "-i" then
        for i in 2..Argument_Count loop
            if Argument( i ) = "-n" then
                namespaces.Append( "" );   -- global namespace
            elsif Match_Arg( Argument( i ), "-n" ) then
                namespaces.Append( Tail( Argument( i ), Argument( i )'Length - 2 ) );
            else
                Output_Line( "Unrecognized argument: " & Argument( i ) );
                Set_Exit_Status( stat );
                return;
            end if;
        end loop;
        namespaces.Append( "global" );

        Display_Title;
        Interpretor_Prompt( namespaces );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Display the disassembly of an object file
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    elsif Argument( 1 ) = "-a" then
        if Argument_Count < 2 then
            Output_Line( "Missing <compiled-file> argument" );
            Display_Usage;
            Set_Exit_Status( stat );
            return;
        end if;
        if Argument_Count > 2 then
            if Argument( 3 ) /= "-sub" or else Argument_Count < 4 then
                Output_Line( "Unexpected arguments" );
                Display_Usage;
                Set_Exit_Status( stat );
                return;
            end if;
            indices := Create_List.Lst;
            for i in 4..Argument_Count loop
                indices.Append( Create( Argument( i ) ) );
            end loop;
        end if;
        stat := Disassemble( Argument( 2 ), indices );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Invalid arguments
    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    else
        Output_Line( "Invalid argument: " & Argument( 1 ) );
        Display_Usage;
        Set_Exit_Status( stat );
        return;
    end if;

    Set_Exit_Status( stat );
exception
    when e : others =>
        Output_Line( "Exception: " & Exception_Name( e ) & " -- " & Exception_Message( e ) );
        Set_Exit_Status( Exception_Fail );
end Main;
