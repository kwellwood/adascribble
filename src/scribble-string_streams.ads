--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Streams;                       use Ada.Streams;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;

package Scribble.String_Streams is

    -- A String_Stream is a stream implementation backed by a read-only
    -- Unbounded_String object.
    type String_Stream is new Root_Stream_Type with private;
    type A_String_Stream is access all String_Stream'Class;

    -- Creates a String_Stream wrapper around 'str'. It is recommended that only
    -- Characters be read from this type of stream.
    function Stream( str : Unbounded_String ) return A_String_Stream;
    pragma Postcondition( Stream'Result /= null );

    -- Closes the stream and deallocates it.
    procedure Close( stream : in out A_String_Stream );
    pragma Postcondition( stream = null );

    -- raised by streams on error
    STREAM_ERROR : exception;

private

    type String_Stream is new Root_Stream_Type with record
        buffer  : Unbounded_String;
        readPos : Natural;
    end record;

    -- 'last' will be set to item'First - 1 when the stream is empty.
    procedure Read( stream : in out String_Stream;
                    item   : out Stream_Element_Array;
                    last   : out Stream_Element_Offset );

    -- Writing is not supported. Raises STREAM_ERROR.
    procedure Write( stream : in out String_Stream;
                     item   : Stream_Element_Array );

end Scribble.String_Streams;
