--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Streams;                       use Ada.Streams;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Values;                            use Values;
with Values.Functions;                  use Values.Functions;

private with Scribble.Value_Generators;
private with Scribble.Parsers;
private with Scribble.Semantics;

package Scribble.Compilers is

    -- The Scribble Compiler accepts Scribble source code and compiles it into a
    -- Function value class that can be evaluated by the Scribble Evaluator.
    type Scribble_Compiler is tagged limited private;
    type A_Scribble_Compiler is access all Scribble_Compiler'Class;

    -- Creates a new Scribble compiler. The Scribble runtime provides
    -- information about the available Ada functions during compilation.
    function Create_Scribble_Compiler( runtime : not null A_Scribble_Runtime ) return A_Scribble_Compiler;
    pragma Postcondition( Create_Scribble_Compiler'Result /= null );

    -- Compiles a binding function to call a Ada function 'name', defined by
    -- the compiler's runtime.
    --
    -- 'implicits' contains constant values that will always be passed as the
    -- first implicits'Length arguments to Ada. The purpose of the implicit
    -- arguments is to allow information, like an object reference, to be
    -- encapsulated in a binding function. This way, a global Ada implementation
    -- of some operation can be defined that requires an object reference as its
    -- first argument, and a binding function can be created to implicitly pass
    -- a specific instance to the global implementation.
    --
    -- Creating a map of bindings to functions that all operate on the same
    -- object allows Scribble to syntactically treat Ada objects as Scribble
    -- objects.
    --
    -- 'defaults' contains default values for the last N arguments to 'name'
    -- that do not have implicit or caller-specified values.
    --
    -- Where the target Ada function accepts a max of ADAMAX arguments, the
    -- returned Scribble binding function will require at least
    -- (ADAMAX - implicits'Length - defaults'Length) arguments, and accept not
    -- more than (ADAMAX - implicits'Length) arguments.
    --
    -- A Parse_Error will be raised if 'name' does not specify a valid Ada
    -- implementation, or if 'implicits' contains too many elements, or if
    -- 'defaults' contains too many elements.
    function Bind_Implicit( this      : not null access Scribble_Compiler'Class;
                            name      : String;
                            implicits : Value_Array;
                            defaults  : Value_Array ) return Function_Value;

    function Bind_Implicit( this      : not null access Scribble_Compiler'Class;
                            name      : String;
                            implicits : Value_Array ) return Function_Value;

    -- Compiles a binding function to call a Ada function 'name', defined by
    -- the compiler's runtime. 'defaults' contains default values for the last
    -- N arguments not specified by the caller.
    --
    -- Where the target Ada function accepts a max of ADAMAX arguments, the
    -- returned Scribble binding function will require at least
    -- (ADAMAX - defaults'Length) arguments, and accept not more than (ADAMAX)
    -- arguments.
    --
    -- A Parse_Error will be raised if 'name' does not specify a valid Ada
    -- implementation, or if 'defaults' contains too many elements.
    function Bind( this     : not null access Scribble_Compiler'Class;
                   name     : String;
                   defaults : Value_Array ) return Function_Value;

    function Bind( this : not null access Scribble_Compiler'Class;
                   name : String ) return Function_Value;

    -- Parses and returns a single literal Value written with Scribble syntax.
    -- The value's type may be any one of the base Value classes, including a
    -- named function definition. Comments are ignored.
    --
    -- 'path' is the path of the file containing 'source', or an empty string if
    -- none. If 'enableDebug is True, debugging information will be included in
    -- any Function, or embedded Function, that is returned. A Parse_Error will
    -- be raised if an error occurs during parsing, or if any characters except
    -- comments are found in the stream after the value.
    function Compile( this        : not null access Scribble_Compiler'Class;
                      source      : access Root_Stream_Type'Class;
                      path        : String;
                      enableDebug : Boolean := False ) return Value;

    -- A convenience function that compiles from a string instead of a stream.
    function Compile( this        : not null access Scribble_Compiler'Class;
                      source      : Unbounded_String;
                      path        : String;
                      enableDebug : Boolean := False ) return Value;

    -- Returns the runtime used by the compiler to resolve symbols.
    function Get_Runtime( this : not null access Scribble_Compiler'Class ) return A_Scribble_Runtime;
    pragma Postcondition( Get_Runtime'Result /= null );

    -- Deletes the compiler.
    procedure Delete( this : in out A_Scribble_Compiler );
    pragma Postcondition( this = null );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- This is a convenience wrapper for Scribble_Compiler.Compile(). A
    -- Parse_Error will be raised on error.
    --
    -- A temporary Scribble_Compiler object with a temporary Scribble_Runtime
    -- will be used for compilation. No namespaces or built-in functions will be
    -- available except for what is defined by the Scribble language.
    function Compile( source : Unbounded_String; path : String := "" ) return Value;

private

    use Scribble.Value_Generators;
    use Scribble.Parsers;
    use Scribble.Semantics;

    type Scribble_Compiler is tagged limited
        record
            runtime   : A_Scribble_Runtime := null;
            parser    : A_Parser := null;
            analyzer  : A_Semantic_Analyzer := null;
            generator : A_Value_Generator := null;
        end record;

    procedure Construct( this    : access Scribble_Compiler;
                         runtime : not null A_Scribble_Runtime );

    procedure Delete( this : in out Scribble_Compiler );

end Scribble.Compilers;
