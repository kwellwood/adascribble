--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers.Vectors;
with Ada.Unchecked_Deallocation;
with System.Address_To_Access_Conversions;
with Ada.Containers;                    use Ada.Containers;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Values.Streaming;                  use Values.Streaming;

package body Values.Lists is

    package Value_Vectors is new Ada.Containers.Vectors( Positive, A_Value, "=" );

    type List_Data is limited
        record
            obj      : Object_Refs;
            elements : Value_Vectors.Vector;
        end record;
    for List_Data use
        record
            obj at 0 range 0..31;
        end record;

    ----------------------------------------------------------------------------

    package Accesses is new System.Address_To_Access_Conversions( List_Data );
    use Accesses;

    --==========================================================================

    function Create_List return Value is
        data : constant Object_Pointer := new List_Data;
        this : Value := Value'(Controlled with v => TAG_LIST);
    begin
        this.Set_Object( To_Address( data ) );
        return this;
    end Create_List;

    ----------------------------------------------------------------------------

    function Create_List( elements : Value_Array; consume : Boolean := False ) return Value is
        data : constant Object_Pointer := new List_Data;
        this : Value := Value'(Controlled with v => TAG_LIST);
    begin
        data.elements.Reserve_Capacity( elements'Length );
        for i in elements'Range loop
            data.elements.Append( new Value'(if consume then elements(i) else Clone( elements(i) )) );
        end loop;
        this.Set_Object( To_Address( data ) );
        return this;
    end Create_List;

    --==========================================================================

    function As_List( this : Value'Class ) return List_Value is
        (List_Value'(Value'(Value(this)) with data => A_List_Data(To_Pointer( this.Get_Address ))));

    ----------------------------------------------------------------------------

    procedure Append( this : List_Value'Class; val : Value'Class; consume : Boolean := False ) is
    begin
        this.data.elements.Append( new Value'((if consume then Value(val) else Clone( val ))) );
    end Append;

    ----------------------------------------------------------------------------

    procedure Append( this : List_Value'Class; val : Boolean ) is
    begin
        this.Append( Create( val ), consume => True );
    end Append;

    procedure Append( this : List_Value'Class; val : Integer ) is
    begin
        this.Append( Create( val ), consume => True );
    end Append;

    procedure Append( this : List_Value'Class; val : Float ) is
    begin
        this.Append( Create( Long_Float(val) ), consume => True );
    end Append;

    procedure Append( this : List_Value'Class; val : Long_Float ) is
    begin
        this.Append( Create( val ), consume => True );
    end Append;

    procedure Append( this : List_Value'Class; val : String ) is
    begin
        this.Append( Strings.Create( val ), consume => True );
    end Append;

    procedure Append( this : List_Value'Class; val : Unbounded_String ) is
    begin
        this.Append( Strings.Create( val ), consume => True );
    end Append;

    ----------------------------------------------------------------------------

    procedure Append_List( this    : List_Value'Class;
                           other   : Value'Class;
                           consume : Boolean := False ) is
        otherData : constant Object_Pointer := To_Pointer( other.Get_Address );
    begin
        this.data.elements.Reserve_Capacity( this.data.elements.Length + otherData.elements.Length );
        for elem of otherData.elements loop
            this.data.elements.Append( new Value'(if consume then elem.all else Clone( elem.all )) );
        end loop;
    end Append_List;

    ----------------------------------------------------------------------------

    procedure Clear( this : List_Value'Class ) is
    begin
        this.data.elements.Clear;
    end Clear;

    ----------------------------------------------------------------------------

    function Contains( this : List_Value'Class; val : Value'Class ) return Boolean is (this.Find( val ) > 0);

    ----------------------------------------------------------------------------

    function Find( this  : List_Value'Class;
                   val   : Value'Class;
                   start : Integer := 1 ) return Natural is
    begin
        for i in start..Integer(this.data.elements.Length) loop
            if this.data.elements.Constant_Reference( i ).all = Value(val) then
                return i;
            end if;
        end loop;
        return 0;
    end Find;

    ----------------------------------------------------------------------------

    function First( this : List_Value'Class ) return Value
        is ((if this.data.elements.Length > 0 then this.data.elements.Constant_Reference( 1 ).all else Null_Value));

    ----------------------------------------------------------------------------

    function Get( this : List_Value'Class; index : Integer ) return Value
        is (if index in 1..Integer(this.data.elements.Length)
                then this.data.elements.Constant_Reference( index ).all
                else Null_Value);

    ----------------------------------------------------------------------------

    function Get_Boolean( this    : List_Value'Class;
                          index   : Integer;
                          default : Boolean := False ) return Boolean
        is (Cast_Boolean( this.Get( index ), default ));

    function Get_Int( this    : List_Value'Class;
                      index   : Integer;
                      default : Integer := 0 ) return Integer
        is (Cast_Int( this.Get( index ), default ));

    function Get_Float( this    : List_Value'Class;
                        index   : Integer;
                        default : Float := 0.0 ) return Float
        is (Cast_Float( this.Get( index ), default ));

    function Get_Long_Float( this    : List_Value'Class;
                             index   : Integer;
                             default : Long_Float := 0.0 ) return Long_Float
        is (Cast_Long_Float( this.Get( index ), default ));

    function Get_String( this    : List_Value'Class;
                         index   : Integer;
                         default : String := "" ) return String
        is (Cast_String( this.Get( index ), default ));

    function Get_Unbounded_String( this    : List_Value'Class;
                                   index   : Integer;
                                   default : String := "" ) return Unbounded_String
        is (Cast_Unbounded_String( this.Get( index ), default ));

    ----------------------------------------------------------------------------

    function Head( this : List_Value'Class; count : Integer ) return Value is (this.Slice( 1, count ));

    ----------------------------------------------------------------------------

    procedure Insert( this    : List_Value'Class;
                      val     : Value'Class;
                      index   : Integer;
                      consume : Boolean := False ) is
    begin
        if Integer(this.data.elements.Length) + 1 < index then
            -- inserting at 'index' will create empty slots in the List.
            -- Use Set() because it handles this case already.
            this.Set( index, val, consume );
        else
            this.data.elements.Insert( Integer'Max( 1, index ), new Value'((if consume then Value(val) else Clone( val ))) );
        end if;
    end Insert;

    ----------------------------------------------------------------------------

    function Last( this : List_Value'Class ) return Value
        is ((if this.data.elements.Length > 0 then this.data.elements.Last_Element.all else Null_Value));

    ----------------------------------------------------------------------------

    function Length( this : List_Value'Class ) return Natural is (Natural(this.data.elements.Length));

    ----------------------------------------------------------------------------

    procedure Prepend( this    : List_Value'Class;
                       val     : Value'Class;
                       consume : Boolean := False ) is
    begin
        this.data.elements.Prepend( new Value'((if consume then Value(val) else Clone( val ))) );
    end Prepend;

    ----------------------------------------------------------------------------

    function Reference( this          : List_Value'Class;
                        index         : Positive;
                        createMissing : Boolean := True ) return Value is
    begin
        if index < Integer(this.data.elements.Length) then
            return Create_Ref( this.data.elements.Constant_Reference( index ) );
        elsif createMissing then
            -- create the reference
            this.data.elements.Reserve_Capacity( Count_Type(index) );
            for i in Integer(this.data.elements.Length+1)..index loop
                this.data.elements.Append( new Value'(Null_Value) );
            end loop;
            return Create_Ref( this.data.elements.Constant_Reference( index ) );
        else
            return Null_Value;
        end if;
    end Reference;

    ----------------------------------------------------------------------------

    procedure Remove( this : List_Value'Class; index : Integer; count : Integer := 1 ) is
    begin
        if index >= 1 and then index <= Integer(this.data.elements.Length) then
            this.data.elements.Delete( index, Count_Type(count) );
        end if;
    end Remove;

    ----------------------------------------------------------------------------

    procedure Set( this    : List_Value'Class;
                   index   : Positive;
                   val     : Value'Class;
                   consume : Boolean := False ) is
    begin
        if index <= Integer(this.data.elements.Length) then
            this.data.elements.Reference( index ).all := (if consume then Value(val) else Clone( val ));
        else
            this.data.elements.Reserve_Capacity( Count_Type(index) );
            for i in Integer(this.data.elements.Length+1)..(index-1) loop
                this.data.elements.Append( new Value'(Null_Value) );
            end loop;
            this.data.elements.Append( new Value'((if consume then Value(val) else Clone( val ))) );
        end if;
    end Set;

    ----------------------------------------------------------------------------

    function Slice( this  : List_Value'Class;
                    from  : Integer;
                    count : Integer ) return Value is
        lFrom  : constant Positive := Integer'Max( 1, from );
        lCount : constant Natural := Integer'Max( 0, count );
        len    : constant Integer := Integer'Min( lFrom + lCount - 1, Integer(this.data.elements.Length) ) - lFrom + 1;
        vals   : Value_Array(1..len);
    begin
        for i in vals'Range loop
            vals(i) := this.data.elements.Constant_Reference( lFrom + i - 1 ).all;
        end loop;
        return Create_List( vals, consume => False );
    end Slice;

    ----------------------------------------------------------------------------

    procedure Sort( this : List_Value'Class; lt : access function( l, r : Value'Class ) return Boolean := null ) is

        function Default_Lt( l, r : A_Value ) return Boolean is (l.all < r.all);

        function User_Lt( l, r : A_Value ) return Boolean is (lt.all( l.all, r.all ));

        package Default_Sorting is new Value_Vectors.Generic_Sorting( Default_Lt );
        package User_Sorting    is new Value_Vectors.Generic_Sorting( User_Lt );
    begin
        if lt = null then
          Default_Sorting.Sort( this.data.elements );
        else
            User_Sorting.Sort( this.data.elements );
        end if;
    end Sort;

    ----------------------------------------------------------------------------

    function Tail( this : List_Value'Class; count : Integer ) return Value
        is (this.Slice( Integer'Max( Integer(this.data.elements.Length) - count + 1, 1 ), count ));

    ----------------------------------------------------------------------------

    procedure To_Array( this : List_Value'Class; output : in out Value_Array ) is
    begin
        for i in 1..this.Length loop
            exit when output'First + (i - 1) > output'Last;
            output(output'First + (i - i)) := this.data.elements.Constant_Reference( i ).all;
        end loop;
    end To_Array;

    ----------------------------------------------------------------------------

    function "&"( l : List_Value'Class; r : List_Value'Class ) return Value is
        data : constant Object_Pointer := new List_Data;
        this : Value := Value'(Controlled with v => TAG_LIST);
    begin
        data.elements.Reserve_Capacity( l.data.elements.Length + r.data.elements.Length );
        for elem of l.data.elements loop
            data.elements.Append( new Value'(Clone( elem.all )) );
        end loop;
        for elem of r.data.elements loop
            data.elements.Append( new Value'(Clone( elem.all )) );
        end loop;
        this.Set_Object( To_Address( data ) );
        return this;
    end "&";

    --==========================================================================

    function Clone_List( this : Value'Class ) return Value is
        thisData : constant Object_Pointer := To_Pointer( this.Get_Address );
        copyData : constant Object_Pointer := new List_Data;
        result   : Value := Value'(Controlled with v => TAG_LIST);
    begin
        copyData.elements.Reserve_Capacity( thisData.elements.Length );
        for v of thisData.elements loop
            copyData.elements.Append( new Value'(Clone( v.all )) );
        end loop;
        result.Set_Object( To_Address( copyData ) );
        return result;
    end Clone_List;

    ----------------------------------------------------------------------------

    function Compare_Lists( left, right : Value'Class ) return Integer is
        leftData  : constant Object_Pointer := To_Pointer( left.Get_Address );
        rightData : constant Object_Pointer := To_Pointer( right.Get_Address );
        result    : Integer;
    begin
        for i in 1..leftData.elements.Length loop
            -- comparison is equal so far, but 'left' is longer
            if i > rightData.elements.Length then
                return 1;
            end if;

            result := leftData.elements.Element( Integer(i) ).Compare( rightData.elements.Constant_Reference( Integer(i) ).all );
            if result /= 0 then
                return result;
            end if;
        end loop;

         -- comparison is equal so far, but 'right' is longer
        if rightData.elements.Length > leftData.elements.Length then
            return -1;
        end if;

        -- equivalent length and content
        return 0;
    end Compare_Lists;

    ----------------------------------------------------------------------------

    procedure Delete_List( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( List_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        for elem of data.elements loop
            Free( elem );    -- delete each dynamically allocated Value
        end loop;
        Free( data );
    end Delete_List;

    ----------------------------------------------------------------------------

    function Image_List( this : Value'Class ) return String is
        data   : constant Object_Pointer := To_Pointer( this.Get_Address );
        result : Unbounded_String;
    begin
        Append( result, '[' );
        if data.elements.Length > 0 then
            Append( result, data.elements.Element( 1 ).Image );
            for i in 2..data.elements.Length loop
                Append( result, ',' );
                Append( result, data.elements.Constant_Reference( Integer(i) ).all.Image );
            end loop;
        end if;
        Append( result, ']' );
        return To_String( result );
    end Image_List;

    ----------------------------------------------------------------------------

    procedure Read_List( stream : access Root_Stream_Type'Class;
                         this   : in out Value'Class ) is
        data : Object_Pointer;
        len  : Integer;
        elem : Value;
    begin
        data := new List_Data;
        this.Set_Object( To_Address( data ) );

        len := Integer'Input( stream );
        for i in 1..len loop
            elem := Value_Input( stream );
            data.elements.Append( new Value'(elem) );
        end loop;
    end Read_List;

    ----------------------------------------------------------------------------

    procedure Write_List( stream : access Root_Stream_Type'Class;
                          this   : Value'Class ) is
        data : constant Object_Pointer := To_Pointer( this.Get_Address );
    begin
        Integer'Output( stream, Integer(data.elements.Length) );
        for elem of data.elements loop
            Value_Output( stream, elem.all );
        end loop;
    end Write_List;

end Values.Lists;
