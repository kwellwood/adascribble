--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Scribble is

    pragma Preelaborate;

    -- Do not remove or re-order; add to the end only. The position numbers are
    -- referenced in compiled Scribble code (STORE instruction).
    type Assign_Op is (DIRECT, ADD, SUBTRACT, MULTIPLY, DIVIDE, CONCAT);

    -- Raised when an expected token is not found, or when an unexpected token
    -- is found.
    Parse_Error : exception;

    -- The current Scribble language version. This can be used to identify the
    -- language version of compiled code, for compatability purposes.
    LANGUAGE_VERSION : constant := 5;

end Scribble;
