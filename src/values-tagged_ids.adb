--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Real_Time;                     use Ada.Real_Time;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Values.Construction;               use Values.Construction;

package body Values.Tagged_Ids is

    TAG_SIZE   : constant := 4;
    TAG_OFFSET : constant := 44;
    TAG_MASK   : constant Unsigned_64 := Shift_Left( Unsigned_64(2**TAG_SIZE-1), TAG_OFFSET );

    -- for generating unique IDs
    generator : GNAT.Random_Numbers.Generator;

    --==========================================================================

    overriding
    function "<"( l, r : Tagged_Id ) return Boolean is (Unsigned_64(l) < Unsigned_64(r));

    ----------------------------------------------------------------------------

    function Create_Tagged_Id( tag : Id_Tag; x : Unsigned_64 ) return Tagged_Id
    is (Tagged_Id(Shift_Left( Unsigned_64(tag), TAG_OFFSET ) or (x and (2**TAG_OFFSET-1))));

    ----------------------------------------------------------------------------

    function Create_Tagged_Id( rawId : Unsigned_64 ) return Tagged_Id is (Tagged_Id(rawId));

    ----------------------------------------------------------------------------

    function Get_Tag( id : Tagged_Id ) return Id_Tag
    is (Id_Tag(Shift_Right( Unsigned_64(id) and TAG_MASK, TAG_OFFSET )));

    ----------------------------------------------------------------------------

    function Get_Id( id : Tagged_Id ) return Unsigned_64 is (Unsigned_64(id) and (2**TAG_OFFSET-1));

    ----------------------------------------------------------------------------

    function Image( id : Tagged_Id ) return String is
        result : constant String := Unsigned_64'Image( Unsigned_64(id) );
    begin
        return result(result'First + 1..result'Last);
    end Image;

    ----------------------------------------------------------------------------

    function Is_Null( id : Tagged_Id ) return Boolean
    is ((Unsigned_64(id) and Unsigned_64(2**TAG_OFFSET-1)) = 0);

    ----------------------------------------------------------------------------

    function To_Tagged_Id( val : Value'Class ) return Tagged_Id
    is (if val.Is_Id then Tagged_Id(val.To_Id) else NULL_ID);

    ----------------------------------------------------------------------------

    function To_Id_Value( id : Tagged_Id ) return Value
    is (Create_Id( Unsigned_64(id) and Unsigned_64(2**(TAG_OFFSET+TAG_SIZE)-1) ));

    ----------------------------------------------------------------------------

    function Unique_Tagged_Id( tag : Id_Tag ) return Tagged_Id is
        pragma Warnings( Off );
        result : Tagged_Id := NULL_ID;
        res64  : Unsigned_64;
                 for res64'Address use result'Address;
        sc     : Seconds_Count;
        ts     : Time_Span;
    begin
        loop
            Split( Clock, sc, ts );
            -- to avoid overflowing an integer (if uptime > 49.7 days) when we get
            -- the number of millis in 'ts' by division later, we must make sure
            -- that 'ts' is never more than 2,147,483 (or 2^31) seconds.
            ts := ts + Seconds( Integer(sc mod Seconds_Count(2_147_482)) );

            -- An object ID is a 44-bit unique ID based on machine up-time and a
            -- random number. The machine uptime is the lower 12 bits of the number
            -- of milliseconds since boot.
            --
            -- D = | [12 bits] uptime ms | [32 bits] random number |
            res64 := Shift_Left( Unsigned_64(ts / Milliseconds( 1 )), 32 ) or
                     Unsigned_64(Unsigned_32'(GNAT.Random_Numbers.Random( generator )));
            res64 := res64 and Unsigned_64(2**TAG_OFFSET-1);   -- chop down to 44 bits

            -- just in case a null id is generated
            exit when res64 /= 0;
        end loop;

        res64 := Shift_Left( Unsigned_64(tag), TAG_OFFSET ) or res64;
        return result;
    end Unique_Tagged_Id;

begin

    GNAT.Random_Numbers.Reset( generator );

end Values.Tagged_Ids;
