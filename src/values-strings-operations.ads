--
-- Copyright (c) 2014-2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Strings.Operations is

    -- Returns an array of strings created by splitting string 'source' on
    -- 'delim'. The delimiters are thrown away and the characters between them
    -- become the list elements. Set 'keepEmpty' to True to preserve elements in
    -- the string as empty spaces, where there are no characters between
    -- occurances of 'delim'.
    function Split( source    : Unbounded_String;
                    delim     : String;
                    keepEmpty : Boolean := True ) return Value;

end Values.Strings.Operations;
