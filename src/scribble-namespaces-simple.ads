--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values.Maps;                       use Values.Maps;

package Scribble.Namespaces.Simple is

    -- A Simple_Namespace simply writes to and reads from a map value. It can be
    -- set as read-only, which will cause all writes to return the current value
    -- of a name, instead of the assigned value.
    type Simple_Namespace is new Scribble_Namespace with private;
    type A_Simple_Namespace is access all Simple_Namespace'Class;

    -- Creates a new simple namespace, optionally setting its initial contents
    -- to 'contents'. If 'consume' is True, 'contents' will be consumed instead
    -- of copied.
    function Create_Simple_Namespace( contents : Map_Value := Create_Map.Map;
                                      consume  : Boolean := True ) return A_Simple_Namespace;
    pragma Postcondition( Create_Simple_Namespace'Result /= null );

    -- Adds 'names' to the namespace, overriding any of the same existing names.
    -- Unlike Set_Contents(), the namespace will not be cleared first. Nothing
    -- will change if the namespace is currently read-only.
    procedure Add_Contents( this  : not null access Simple_Namespace'Class;
                            names : Map_Value );

    -- Returns the value of a name within the namespace (without copying), or a
    -- null pointer if the name hasn't been defined yet.
    function Get_Namespace_Name( this : access Simple_Namespace;
                                 name : String ) return Value;

    -- Returns a reference to a value within the namespace. If the namespace is
    -- read-only and the value hasn't been defined yet, then a null pointer will
    -- be returned to indicate the name isn't readable or writable.
    function Get_Namespace_Ref( this : access Simple_Namespace;
                                name : String ) return Value;
    pragma Postcondition( Get_Namespace_Ref'Result.Is_Ref or else
                          Get_Namespace_Ref'Result.Is_Null );

    -- Returns True if the namespace is currently read-only.
    function Is_Readonly( this : not null access Simple_Namespace'Class ) return Boolean;

    -- Sets the contents of the namespace, clearing its current contents.
    -- Nothing will change if the namespace is currently read-only.
    procedure Set_Contents( this     : not null access Simple_Namespace'Class;
                            contents : Map_Value );

    -- Sets 'name' to 'val'. Nothing will change if the namespace is currently
    -- read-only.
    procedure Set_Name( this    : not null access Simple_Namespace'Class;
                        name    : String;
                        val     : Value'Class;
                        consume : Boolean := False );

    -- Allows / disallows writes to the namespace.
    procedure Set_Readonly( this     : not null access Simple_Namespace'Class;
                            readonly : Boolean );

    procedure Delete( this : in out A_Simple_Namespace );
    pragma Postcondition( this = null );

private

    type Simple_Namespace is new Scribble_Namespace with
        record
            readonly  : Boolean := False;
            namespace : Map_Value;
        end record;

end Scribble.Namespaces.Simple;
