--
-- Copyright (c) 2014-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones;           use Ada.Calendar.Time_Zones;
with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Containers;                    use Ada.Containers;
with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Streams.Stream_IO;             use Ada.Streams.Stream_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Equal_Case_Insensitive;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Interfaces;                        use Interfaces;
with Scribble.Compilers;                use Scribble.Compilers;
with Scribble.Debugging;                use Scribble.Debugging;
with Scribble.IO;                       use Scribble.IO;
with Scribble.Namespaces;               use Scribble.Namespaces;
with Scribble.Namespaces.Simple;        use Scribble.Namespaces.Simple;
with Scribble.Opcodes;                  use Scribble.Opcodes;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.Runtimes;                 use Scribble.Runtimes;
with Scribble.String_Streams;           use Scribble.String_Streams;
with Scribble.VMs;                      use Scribble.VMs;
with Values.Casting;                    use Values.Casting;
with Values.Functions;                  use Values.Functions;
use Ada;

package body Scribble.CLI is

    procedure Error_Line( str : String ) is
    begin
        Ada.Text_IO.Put_Line( Ada.Text_IO.Standard_Error, str );
    end Error_Line;

    ----------------------------------------------------------------------------

    function Compile_Object( sourcePath   : String;
                             compiledPath : String;
                             namespaces   : String_Vectors.Vector;
                             enableDebug  : Boolean ) return Exit_Status is
        output : Stream_IO.File_Type;
        source : Unbounded_String;
        val    : Value;
    begin
        -- 1. Read the source file
        if not Read_Source( sourcePath, source ) then
            Error_Line( "Cannot read file: " & sourcePath );
            return Read_Fail;
        end if;

        -- 2. Compile the file contents
        declare
            runtime  : A_Scribble_Runtime := Create_Scribble_Runtime;
            compiler : A_Scribble_Compiler := Create_Scribble_Compiler( runtime );
        begin
            for n of namespaces loop
                runtime.Reserve_Namespace( n );
            end loop;
            if Equal_Case_Insensitive( Get_Extension( sourcePath ), "sp" ) then
                source := "function() " & source & ASCII.LF & "; end function";
            end if;
            val := compiler.Compile( source, sourcePath, enableDebug );
            Delete( compiler );
            Delete( runtime );
        exception
            when e : Parse_Error =>
                Error_Line( Format_Parse_Error( Exception_Message( e ) ) & ASCII.LF &
                            "Compilation failed: " & sourcePath );
                Delete( compiler );
                Delete( runtime );
                return Parse_Fail;
        end;

        -- 3. Write the compiled file
        Create( output, Out_File, compiledPath );
        if not Is_Open( output ) then
            Error_Line( "Cannot create file: " & compiledPath );
            return Write_Fail;
        end if;

        Scribble.IO.Write( Stream( output ), val, Get_Filename( sourcePath ) );
        Close( output );

        return Success;
    end Compile_Object;

    ----------------------------------------------------------------------------

    function Disassemble( compiledPath : String; indices : List_Value ) return Exit_Status is
        input    : Stream_IO.File_Type;
        contents : File_Content;
    begin
        -- 1. Read the compiled file
        begin
            Open( input, In_File, compiledPath );
            Assert( Is_Open( input ) );
            Scribble.IO.Read( Stream( input ), contents );
            Close( input );
        exception
            when others =>
                Error_Line( "Cannot read file: " & compiledPath );
                return Read_Fail;
        end;

        if contents.status /= Loaded then
            case contents.status is
                when Unrecognized => Error_Line( "File format not recognized" );
                when Unsupported => Error_Line( "File version is not supported" );
                when Corrupt => Error_Line( "Error in file format" );
                when others => null;
            end case;
            return Read_Fail;
        end if;

        -- display the disassembly header
        Output_Line( "// Source: " & To_String( contents.sourcePath ) );
        Output_Line( "// Compiled: " & Ada.Calendar.Formatting.Image( contents.compileDate, False, UTC_Time_Offset ) );  -- display as local
        Output_Line( "// Object Type: " & contents.val.Get_Type'Img );

        -- drill down to the item to display
        if indices.Valid then
            declare
                line : Unbounded_String;
            begin
                for i in 1..indices.Length loop
                    Append( line, "[""" & Coerce_String( indices.Get( i ) ) & """]" );
                end loop;
                if indices.Length > 0 then
                    Output_Line( "// Index: " & To_String( line ) );
                end if;
            end;

            for i in 1..indices.Length loop
                if contents.val.Is_List then
                    contents.val := contents.val.Lst.Get( Coerce_Int( indices.Get(i) ) );
                elsif contents.val.Is_Map then
                    contents.val := contents.val.Map.Get( Coerce_String( indices.Get(i) ) );
                elsif contents.val.Is_Function then
                    contents.val := contents.val.Func.Get_Data( Coerce_Int( indices.Get(i) ) );
                end if;
            end loop;
        end if;

        -- disassemble a function
        if contents.val.Is_Function then
            declare
                line      : Unbounded_String;
                func      : constant Function_Value := contents.val.Func;
                argCount  : constant Natural := func.Arg_Count;
                minArgs   : constant Natural := func.Min_Args;
                debugInfo : constant A_Debug_Info := func.Get_Debug_Info;

                function Get_Inst( i : Integer ) return Instruction is (Get_Instruction( func.Get_Opcode( i ) ));
                function Get_S( i : Integer ) return Integer is (Get_S( func.Get_Opcode( i ) ));
                function Get_U( i : Integer ) return Natural is (Get_U( func.Get_Opcode( i ) ));
                function Get_Data( i : Integer ) return Value is (func.Get_Data( Get_U( i ) ));

                start   : Integer;
                lineNum : Integer := 0;
                s       : Integer;
            begin
                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- source file path (if available)
                if debugInfo /= null then
                    Output_Line( "// Path: " & To_String( debugInfo.path ) & (if debugInfo.firstLine > 1 then ":" & Trim( Integer'Image( debugInfo.firstLine ), Left ) else "") );
                end if;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- prototype section
                Output_Line( ".prototype:              //" & Integer'Image( argCount ) & " parameters;" & Integer'Image( minArgs ) & " required" );
                Output_Line( "  " & func.Prototype_Image );

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- constant data section
                Output_Line( ".data:                   //" & Integer'Image( Integer'(func.Data_Length) ) & " constants" );
                for i in 1..func.Data_Length loop
                    line := To_Unbounded_String( "  " );
                    Append( line, "[" & Tail( Trim( Integer'Image( i ), Left ), 4, '0' ) & "] " );
                    Append( line, func.Get_Data( i ).Image );
                    Output_Line( To_String( line ) );
                end loop;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- program instructions section
                Output_Line( ".program:                //" & Integer'Image( Integer'(func.Program_Length) ) & " instructions" );
                for i in 1..func.Program_Length loop
                    -- display the source line number for the following instructions
                    if debugInfo /= null then
                        if debugInfo.lines(i) /= lineNum and then debugInfo.lines(i) > 0 then
                            lineNum := debugInfo.lines(i);
                            Output_Line( "  // line" & Integer'Image( lineNum ) );
                        end if;
                    end if;

                    -- display an instruction description
                    --  [ins#] INST     ARG    // comment

                    -- column 1: instruction number
                    line := To_Unbounded_String( "  " );
                    Append( line, "[" & Tail( Trim( Integer'Image( i ), Left ), 4, '0' ) & "] " );
                    Append( line, To_Lower( Instruction'Image( Get_Inst( i ) ) ) );

                    -- column 2: instruction name
                    line := Head( line, 18, ' ' );
                    case Instruction_Format( Get_Inst( i ) ) is
                        when Format_S => Append( line, Head( Trim( Integer'Image( Get_S( i ) ), Left ), 5, ' ' ) );
                        when Format_U => Append( line, Head( Trim( Integer'Image( Get_U( i ) ), Left ), 5, ' ' ) );
                        when Format_None => null;
                    end case;

                    -- column 3: instruction argument
                    line := Head( line, 25, ' ' );
                    case Get_Inst( i ) is
                        -- PUSHC: display the value being pushed
                        when PUSHC => Append( line, "// " & Image( Get_Data( i ) ) );

                        -- JUMP: display the instruction address
                        when JUMP |
                             JUMPZ => Append( line, "// go to [" & Tail( Trim( Integer'Image( i + Get_S( i ) ), Left ), 4, '0' ) & "]" );

                        when PUSHI =>
                            Append( line, "//" & Integer'Image( Get_S( i ) ) );

                        -- PUSHN: show the namespace name being written, if possible
                        when PUSHN =>
                            if i > 2 and then Get_Inst( i - 1 ) = PUSHC and then Get_Inst( i - 2 ) = PUSHC then
                                Append( line, "// ref(" & Cast_String( Get_Data( i - 1 ) ) & "::" & Cast_String( Get_Data( i - 2 ) ) & ")" );
                            end if;

                        -- PUSHND: show the namespace name being read, if possible
                        when PUSHND =>
                            if i > 2 and then Get_Inst( i - 1 ) = PUSHC and then Get_Inst( i - 2 ) = PUSHC then
                                Append( line, "// " & Cast_String( Get_Data( i - 1 ) ) & "::" & Cast_String( Get_Data( i - 2 ) ) );
                            end if;

                        -- PUSHNUL: pushing a Null or a Null reference?
                        when PUSHNUL =>
                            s := Get_S( i );
                            if s > 0 then
                                Append( line, "// null" & (if s > 1 then (" (x" & Trim( Integer'Image( s ), Left ) & ")") else "") );
                            elsif s < 0 then
                                Append( line, "// ref(null)" & (if s < -1 then (" (x" & Trim( Integer'Image( -s ), Left ) & ")") else "") );
                            end if;

                        -- PUSHS: name of the variable/parameter being written
                        when PUSHS =>
                            if debugInfo /= null then
                                s := Get_S( i );
                                if (-s) - 1 >= 1 and then (-s) - 1 <= debugInfo.paramNames'Length then
                                    -- writing a parameter
                                    -- the last parameter is at relative stack address -2
                                    -- the first is a -2-n+1, where 'n' is the total number of parameters
                                    Append( line, "// ref(" & To_String( debugInfo.paramNames(debugInfo.paramNames'Length - ((-s) - 1) + 1) ) & ")" );
                                elsif s - 1 >= 1 and then s - 1 <= debugInfo.varNames'Length then
                                    -- writing a variable
                                    for j in reverse debugInfo.varNames(s - 1)'Range loop
                                        if lineNum >= debugInfo.varNames(s - 1).all(j).line then
                                            Append( line, "// ref(" & To_String( debugInfo.varNames(s - 1).all(j).name ) & ")" );
                                            exit;
                                        end if;
                                    end loop;
                                end if;
                            end if;

                        -- PUSHSD: name of the variable/parameter being written
                        when PUSHSD =>
                            if debugInfo /= null then
                                s := Get_S( i );
                                if (-s) - 1 >= 1 and then (-s) - 1 <= debugInfo.paramNames'Length then
                                    -- reading a parameter
                                    -- the last parameter is at relative stack address -2
                                    -- the first is a -2-n+1, where 'n' is the total number of parameters
                                    Append( line, "// " & To_String( debugInfo.paramNames(debugInfo.paramNames'Length - ((-s) - 1) + 1) ) );
                                elsif s - 1 >= 1 and then s - 1 <= debugInfo.varNames'Length then
                                    -- reading a variable
                                    for j in reverse debugInfo.varNames(s - 1)'Range loop
                                        if lineNum >= debugInfo.varNames(s - 1).all(j).line then
                                            Append( line, "// " & To_String( debugInfo.varNames(s - 1).all(j).name ) );
                                            exit;
                                        end if;
                                    end loop;
                                end if;
                            end if;

                        -- STORE: show the assignment operator
                        when STORE =>
                            case Assign_Op'Val( Get_U( i ) ) is
                                when DIRECT   => Append( line, "// :=" );
                                when ADD      => Append( line, "// +=" );
                                when SUBTRACT => Append( line, "// -=" );
                                when MULTIPLY => Append( line, "// *=" );
                                when DIVIDE   => Append( line, "// /=" );
                                when CONCAT   => Append( line, "// &=" );
                            end case;

                        -- INVOKE: translate the function id to a name, if possible
                        when INVOKE =>
                            -- invoking the constant pushed in the previous function
                            if i > 1 and then Get_Inst( i - 1 ) = PUSHC then
                                declare
                                    function Find_Function_Name( fid : Unsigned_32 ) return Unbounded_String is
                                    begin
                                        if debugInfo = null or else fid = 0 then
                                            return To_Unbounded_String( "" );
                                        end if;
                                        for i in debugInfo.funcNames'Range loop
                                            if debugInfo.funcNames(i).fid = fid then
                                                return debugInfo.funcNames(i).name;
                                            end if;
                                        end loop;
                                        return To_Unbounded_String( "" );
                                    end Find_Function_Name;

                                    function Cast_U64 is new Cast_Id( Unsigned_64 );
                                    fid  : constant Unsigned_32 := Unsigned_32(Cast_U64( Get_Data( i - 1 ), 0 ));
                                    name : constant Unbounded_String := Find_Function_Name( fid );
                                begin
                                    if Length( name ) > 0 then
                                        -- display the id constant's resolved function name
                                        Append( line, "// " & To_String( name ) );
                                    end if;
                                end;
                            end if;

                        when NOOP | YIELD | PUSHT | STORENUL | DEREF | REF | POP | SWAP | EVAL | RET => null;
                        when EQ | NEQ | LT | GT | LTE | GTE => null;
                        when BNOT | NEG | BAND | BOR | BXOR | ADD | SUB | MUL | DIV | MODULO | POW | CONCAT | SIGN => null;
                    end case;
                    Output_Line( To_String( line ) );
                end loop;

                -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                -- source code section (if available)
                if debugInfo /= null then
                    Output_Line( ".source:" );
                    line := To_Unbounded_String( "" );
                    start := 1;
                    for i in 1..Length( debugInfo.source ) loop
                        if Element( debugInfo.source, i ) = ASCII.LF then
                            if i > start then
                                -- a CR might precede the LF. drop that off
                                -- the end of the line too, if it does.
                                Output_Line( "  " & Slice( debugInfo.source,
                                                           start,
                                                           i - (if Element( debugInfo.source, i - 1 ) = ASCII.CR then 2 else 1) ) );
                            end if;
                            start := i + 1;
                        end if;
                    end loop;
                    if start <= Length( debugInfo.source ) then
                        Output_Line( "  " & Slice( debugInfo.source, start, Length( debugInfo.source ) ) );
                    end if;
                end if;
            end;
        else
            Output_Line( contents.val.Image );
        end if;

        return Success;
    end Disassemble;

    ----------------------------------------------------------------------------

    function Display_Source( compiledPath : String; indices : List_Value ) return Exit_Status is
        input    : Stream_IO.File_Type;
        contents : File_Content;
    begin
        -- 1. Read the compiled file
        begin
            Open( input, In_File, compiledPath );
            Assert( Is_Open( input ) );
            Standard.Scribble.IO.Read( Stream( input ), contents );
            Close( input );
        exception
            when others =>
                Error_Line( "Cannot read file: " & compiledPath );
                return Read_Fail;
        end;

        if contents.status /= Loaded then
            case contents.status is
                when Unrecognized => Error_Line( "File format not recognized" );
                when Unsupported => Error_Line( "File version is not supported" );
                when Corrupt => Error_Line( "Error in file format" );
                when others => null;
            end case;
            return Read_Fail;
        end if;

        -- drill down to the item to display
        if indices.Valid then
            for i in 1..indices.Length loop
                if contents.val.Is_List then
                    contents.val := contents.val.Lst.Get( Coerce_Int( indices.Get(i) ) );
                elsif contents.val.Is_Map then
                    contents.val := contents.val.Map.Get( Coerce_String( indices.Get(i) ) );
                elsif contents.val.Is_Function then
                    contents.val := contents.val.Func.Get_Data( Coerce_Int( indices.Get(i) ) );
                end if;
            end loop;
        end if;

        if contents.val.Is_Function and then contents.val.Func.Source_Code'Length > 0 then
            Error_Line( contents.val.Func.Source_Code );
        else
            Error_Line( contents.val.Image );
        end if;

        return Success;
    end Display_Source;

    ----------------------------------------------------------------------------

    procedure Interpretor_Prompt( namespaces : String_Vectors.Vector ) is
        runtime  : A_Scribble_Runtime := Create_Scribble_Runtime;
        compiler : A_Scribble_Compiler := Create_Scribble_Compiler( runtime );
        vm       : A_Scribble_VM := Create_Scribble_VM( runtime );
        thread   : A_Thread := Create_Thread( stackSize => 1024 );
        line     : Unbounded_String;
        source   : Unbounded_String;
        ready    : Boolean := True;
    begin
        Output_Line( "Interactive mode. Type 'quit' to quit." );

        if not namespaces.Is_Empty then
            Output_Text( "Namespaces: " );
            for i in 1..namespaces.Length loop
                Output_Text( (if namespaces.Element( Integer(i) )'Length > 0
                                  then namespaces.Element( Integer(i) )
                                  else "<anonymous>") );
                if i < namespaces.Length then
                    Output_Text( ", " );
                end if;
            end loop;
            Output_Line( "" );
        end if;
        Output_Line( "" );

        for n of namespaces loop
            runtime.Register_Namespace( n, A_Scribble_Namespace(Create_Simple_Namespace) );
        end loop;

        loop
            Output_Text( (if ready then ">>> " else "..> " ) );
            line := To_Unbounded_String( Ada.Text_IO.Get_Line );
            exit when line = "quit";

            if Length( line ) > 0 and then Element( line, Length( line ) ) = '\' then
                source := source & Trim( Head( line, Length( line ) - 1 ), Right ) & ASCII.LF;
                ready := False;
            else
                source := source & line;
                ready := True;
            end if;

            if ready then
                declare
                    fullSource : Unbounded_String := source;
                    func       : Function_Value;
                    strm       : A_String_Stream;
                begin
                    Insert( fullSource, 1, "function() return(" & ASCII.LF );
                    Append( fullSource, ASCII.LF & "); end function" );
                    if Length( source ) > 0 then
                        strm := Stream( fullSource );
                        func := compiler.Compile( strm, "", enableDebug => True ).Func;
                        Close( strm );
                        thread.Load( func );
                        vm.Run( thread, Run_Complete );
                        if thread.Is_Errored then
                            Output_Text( "Runtime Error: " );
                        end if;
                        if not thread.Get_Result.Is_Null then
                            Output_Line( thread.Get_Result.Image );
                        end if;
                        if thread.Is_Errored then
                            thread.Iterate_Trace( Output_Line'Access );
                        end if;
                    end if;
                exception
                    when e : Parse_Error =>
                        -- 'omitLines' is 1 because one line was prefixed to the
                        -- user's expression before compilation:
                        --
                        -- function() return(   <-- wrapping
                        -- ....                 <-- user's source
                        -- ); end function      <-- wrapping
                        Close( strm );
                        Output_Line( Format_Parse_Error( Exception_Message( e ), fullSource, omitLines => 1 ) & ASCII.LF );
                end;
                source := To_Unbounded_String( "" );
                ready := True;
            end if;
        end loop;

        Delete( thread );
        Delete( vm );
        Delete( compiler );
        Delete( runtime );
    end Interpretor_Prompt;

    ----------------------------------------------------------------------------

    function Get_Extension( path : String ) return String is
        dot : constant Integer := Index( path, ".", Backward );
    begin
        if dot > path'First and then
           dot < path'Last and then
           dot > Index( path, "/", Backward ) and then
           dot > Index( path, "\", Backward )
        then
            return path(dot+1..path'Last);
        end if;
        return "";
    end Get_Extension;

    ----------------------------------------------------------------------------

    function Get_Filename( path : String ) return String is
        pos : Integer := Index( path, "/", Backward );
    begin
        if pos < path'First then
            pos := Index( path, "\", Backward );
        end if;
        if pos >= path'First and then pos <= path'Last then
            return path(pos+1..path'Last);
        end if;
        return path;
    end Get_Filename;

    ----------------------------------------------------------------------------

    function Replace_Extension( path : String; extension : String ) return String is
        dot : constant Integer := Index( path, ".", Backward );
    begin
        if dot > path'First and then dot = path'Last then
            return path & extension;
        elsif dot > path'First and then
           dot > Index( path, "/", Backward ) and then
           dot > Index( path, "\", Backward )
        then
            return path(path'First..dot) & extension;
        end if;
        return path & "." & extension;
    end Replace_Extension;

end Scribble.CLI;
