--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Unchecked_Deallocation;
with Interfaces;                        use Interfaces;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Values;                            use Values;
with Values.Construction;               use Values.Construction;
with Values.Strings;                    use Values.Strings;
with Values.Tagged_Ids;                 use Values.Tagged_Ids;

package body Scribble.Tokenizers is

    function Is_EOL( token : Token_Ptr ) return Boolean is (token.Not_Null and then token.Get.Get_Type = TOKEN_EOL);

    ----------------------------------------------------------------------------

    -- Shifts all tokens one place to the right (out from the queue). The token
    -- in the last slot can only be an EOL. If it isn't, it will be removed.
    procedure Shift_Out( queue : in out Token_Array ) is
    begin
        for i in reverse queue'First..(queue'Last-1) loop
            queue(i+1) := queue(i);
        end loop;
        queue(queue'First).Unset;
        if not Is_EOL( queue(queue'Last) ) then
            queue(queue'Last).Unset;
        end if;
    end Shift_Out;

    ----------------------------------------------------------------------------

    -- Shifts all tokens one place to the right (out from the queue) and inserts
    -- 'token' into the queue at slot (0), as the most recently popped token.
    function Shift_Out( queue : in out Token_Array; token : Token_Ptr ) return Token_Ptr is
    begin
        -- group consecutive EOL tokens together; don't duplicate them
        if not Is_EOL( token ) or not Is_EOL( queue(0) ) then
            Shift_Out( queue );

            -- when writing 'token' into the queue, ensure that no pushed-back
            -- tokens are in the queue.
            pragma Assert( queue(queue'First).Is_Null );
            pragma Assert( queue(queue'First+1).Is_Null );
            queue(0) := token;
        end if;
        return queue(0);
    end Shift_Out;

    --==========================================================================

    function Create_Tokenizer return A_Tokenizer is (new Tokenizer);

    ----------------------------------------------------------------------------

    function Expect( this : not null access Tokenizer'Class; c : Character ) return Boolean is
        actual : Character := ASCII.NUL;
    begin
        if this.Read( actual ) then
            if c = actual then
                return True;
            end if;
            this.Unread( actual );
        end if;
        return False;
    end Expect;

    ----------------------------------------------------------------------------

    function Get_Source( this : not null access Tokenizer'Class ) return Unbounded_String is (this.source);

    ----------------------------------------------------------------------------

    function Location( this : not null access Tokenizer'Class ) return Token_Location is
    begin
        if this.tokenQueue(-1).Not_Null then
            return this.tokenQueue(-1).Get.Location;
        end if;
        return this.loc;
    end Location;

    ----------------------------------------------------------------------------

    procedure Mark_Location( this : not null access Tokenizer'Class ) is
    begin
        -- The Max(column, 1) and Max(absolute, 1) is so that an error in the
        -- very first token doesn't show as an error at column 0.
        this.tokenLoc := (this.loc.line,
                          Integer'Max( this.loc.column - 1, 1 ),
                          Integer'Max( this.loc.absolute - 1, 1 ),
                          this.loc.path);
    end Mark_Location;

    ----------------------------------------------------------------------------

    function Mark_Location( this : not null access Tokenizer'Class ) return Token_Location is
    begin
        this.Mark_Location;
        return this.tokenLoc;
    end Mark_Location;

    ----------------------------------------------------------------------------

    function Next( this : not null access Tokenizer'Class; skipEOL : Boolean := True ) return Token_Ptr is
        result : Token_Ptr;
        c, c2  : Character := ASCII.NUL;
    begin
        -- return a pushed-back token, if there are any
        while this.tokenQueue(-1).Not_Null loop
            Shift_Out( this.tokenQueue );
            if not Is_EOL( this.tokenQueue(0) ) or else not skipEOL then
                return this.tokenQueue(0);
            end if;
            -- the most recently pushed-back token was an EOL and it just got
            -- skipped. go around the loop again and see if there's another token
            -- to return before tokenizing below.
        end loop;

        -- no tokens have been pushed back in the queue
        pragma Assert( this.tokenQueue(-2).Is_Null );
        pragma Assert( this.tokenQueue(-1).Is_Null );

        if this.strm = null then
            -- no input stream; immediate end-of-file
            return Shift_Out( this.tokenQueue, Create_Token( TOKEN_EOF, this.loc ) );
        end if;

        loop
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            -- read the first character of the next token
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            -- get the first character of the token, skipping whitespace and comments
            loop
                -- skip whitespace
                loop
                    if not this.Read( c ) then
                        return Shift_Out( this.tokenQueue, Create_Token( TOKEN_EOF, this.Mark_Location ) );
                    end if;
                    exit when not Is_Whitespace( c );
                end loop;

                -- mark the location after the whitespace as the potential location
                -- for the start of the next token. Mark_Location has a side effect.
                this.Mark_Location;

                -- if it couldn't be a comment following the whitespace, break out
                -- and parse it
                if c /= '/' then
                    exit;
                end if;

                -- check the next character to see if this is a comment
                -- (2 character look-ahead)
                if not this.Read( c2 ) then
                    return Shift_Out( this.tokenQueue, Create_Token( TOKEN_EOF, this.Mark_Location ) );
                end if;

                if c2 /= '/' then
                    -- just a single slash, not a comment
                    -- put 'c2' back and go tokenize the slash we found
                    this.Unread( c2 );
                    exit;
                else
                    -- skip the rest of the line as a comment
                    loop
                        if not this.Read( c ) then
                            return Shift_Out( this.tokenQueue, Create_Token( TOKEN_EOF, this.Mark_Location ) );
                        end if;
                        exit when Is_EOL( c );
                    end loop;
                    this.Unread( c );      -- put the end-of-line back
                end if;
            end loop;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            -- decide which token type to parse
            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            -- Tokens are parsed by a state machine. The Next() function
            -- implements the home state of the parser, skipping whitespace and
            -- comments, and detecting characters that cause a state transition
            -- to one of the more specific Parse_* functions. It will unread any
            -- initial character it used to determine the next state, allowing
            -- the next state to fully parse the token.
            --
            -- The following describes the state transitions:
            --
            -- Next():
            --     whitespace -- skip whitespace --> stay at Next()
            --     "//"       -- skip comment --> stay at Next()
            --     'a'..'z',
            --     'A'..'Z'   --> Parse_Symbol()
            --     '0'..'9'   --> Parse_Number_Literal()
            --     '$'        --> Parse_Id_Literal()
            --     '#'        --> Parse_Color_Literal()
            --     '"'        --> Parse_String_Literal()
            --     other      --> Parse_Simple_Token()
            --
            --     Parse_Symbol():
            --         returns a token or raises Parse_Error (invalid symbol)
            --
            --     Parse_Number_Literal():
            --         returns TK_Value (Number) or raises Parse_Error (malformed number)
            --
            --     Parse_String_Literal():
            --         returns TK_Value (String) or raises Parse_Error (malformed string)
            --
            --     Parse_Simple_Token():
            --         returns TK_[simple] or null (unrecognized token)

            this.Unread( c );        -- put the token's first character back

            if ('a' <= c and c <= 'z') or ('A' <= c and c <= 'Z') then
                result := this.Parse_Symbol;
            elsif '0' <= c and c <= '9' then
                result := this.Parse_Number_Literal;
            elsif c = '$' then
                result := this.Parse_Id_Literal;
            elsif c = '#' then
                result := this.Parse_Color_Literal;
            elsif c = '"' then
                result := this.Parse_String_Literal;
            else
                result := this.Parse_Simple_Token;
            end if;

            -- exit now to return 'result'
            exit when not Is_EOL( result ) or else not skipEOL;

            -- shift out the EOL token but keep going because skipEOL = True
            result := Shift_Out( this.tokenQueue, result );
        end loop;

        return Shift_Out( this.tokenQueue, result );
    exception
        when Stream_Error =>
            Raise_Parse_Error( "Input stream failed", this.Mark_Location );
            raise;   -- won't happen
    end Next;

    ----------------------------------------------------------------------------

    function Parse_Color_Literal( this : not null access Tokenizer'Class ) return Token_Ptr is
        buffer : Unbounded_String;
        c      : Character := ASCII.NUL;
        eof    : Boolean := False;   -- encountered EOF
        rgba   : Unsigned_32 := 0;
    begin
        if not this.Expect( '#' ) then
            -- if Tokenizer is bug free, this will never happen
            Assert( False, "Tokenizer bug: color token error" );
            Raise_Parse_Error( "Malformed color", this.tokenLoc );
        end if;

        -- read the first digit (required)
        if not this.Read( c ) then
            -- expected at least one digit
            Raise_Parse_Error( "Invalid color", this.tokenLoc );
        elsif (c < '0' or '9' < c) and (c < 'a' or 'f' < c) and (c < 'A' or 'F' < c) then
            -- the digit must be hexadecimal
            Raise_Parse_Error( "Invalid color", this.tokenLoc );
        end if;

        -- read subsequent digits
        loop
            buffer := buffer & c;
            eof := not this.Read( c );
            exit when eof or ((c < '0' or '9' < c) and (c < 'a' or 'f' < c) and (c < 'A' or 'F' < c));
        end loop;

        -- put the look-ahead character back in the stream. the above loop
        -- should have ended just after performing a Read() operation without
        -- consuming the character into the buffer.
        if not eof then
            this.Unread( c );
            if not Tokens.Is_Delimiter( c ) then
                -- an extra non-delimiter postfixes the number
                Raise_Parse_Error( "Invalid color", this.tokenLoc );
            end if;
        end if;

        if Length( buffer ) /= 6 and Length( buffer) /= 8 then
            -- expected either RRGGBB or RRGGBBAA
            Raise_Parse_Error( "Invalid color", this.tokenLoc );
        end if;

        for i in 1..Length( buffer ) loop
            c := Element( buffer, i );
            rgba := Shift_Left( rgba, 4 );
            rgba := rgba or (Character'Pos( c ) - (if    c <= '9' then Character'Pos( '0' )
                                                   elsif c <= 'F' then Character'Pos( 'A' ) - 10
                                                   else                Character'Pos( 'a' ) - 10));
        end loop;
        if Length( buffer ) = 6 then
            -- assume full opacity when alpha is not specified
            rgba := Shift_Left( rgba, 8 ) or 16#FF#;
        end if;

        return Create_Value( Create_Color( Float(Shift_Right( rgba, 24 ) and 16#FF#) / 255.0,
                                           Float(Shift_Right( rgba, 16 ) and 16#FF#) / 255.0,
                                           Float(Shift_Right( rgba,  8 ) and 16#FF#) / 255.0,
                                           Float(             rgba       and 16#FF#) / 255.0 ), this.tokenLoc );
    end Parse_Color_Literal;

    ----------------------------------------------------------------------------

    function Parse_Id_Literal( this : not null access Tokenizer'Class ) return Token_Ptr is
        buffer : Unbounded_String;
        c      : Character := ASCII.NUL;
        eof    : Boolean := False;   -- encountered EOF
        id     : Unsigned_64 := 0;
    begin
        if not this.Expect( '$' ) then
            -- if Tokenizer is bug free, this will never happen
            Assert( False, "Tokenizer bug: id token error" );
            Raise_Parse_Error( "Malformed id", this.tokenLoc );
        end if;

        -- read the first digit (required)
        if not this.Read( c ) then
            -- expected at least one digit
            Raise_Parse_Error( "Invalid number", this.tokenLoc );
        elsif (c < '0' or '9' < c) and (c < 'a' or 'f' < c) and (c < 'A' or 'F' < c) then
            -- the digit must be hexadecimal
            Raise_Parse_Error( "Invalid number", this.tokenLoc );
        end if;

        -- read subsequent digits
        loop
            buffer := buffer & c;
            eof := not this.Read( c );
            exit when eof or ((c < '0' or '9' < c) and (c < 'a' or 'f' < c) and (c < 'A' or 'F' < c));
        end loop;

        -- put the look-ahead character back in the stream. the above loop
        -- should have ended just after performing a Read() operation without
        -- consuming the character into the buffer.
        if not eof then
            this.Unread( c );
            if not Tokens.Is_Delimiter( c ) then
                -- an extra non-delimiter postfixes the number
                Raise_Parse_Error( "Invalid number", this.tokenLoc );
            end if;
        end if;

        for i in 1..Length( buffer ) loop
            c := Element( buffer, i );
            id := Shift_Left( id, 4 );
            id := id or (Character'Pos( c ) - (if    c <= '9' then Character'Pos( '0' )
                                               elsif c <= 'F' then Character'Pos( 'A' ) - 10
                                               else                Character'Pos( 'a' ) - 10));
        end loop;

        return Create_Value( To_Id_Value( Create_Tagged_Id( id ) ), this.tokenLoc );
    end Parse_Id_Literal;

    ----------------------------------------------------------------------------

    function Parse_Number_Literal( this : not null access Tokenizer'Class ) return Token_Ptr is
        decimal    : Boolean := True;
        buffer     : Unbounded_String;
        c          : Character := ASCII.NUL;
        eof        : Boolean := False;   -- encountered EOF
        incomplete : Boolean := True;    -- missing some required characters
        hex        : Unsigned_32 := 0;
        val        : Value;
    begin
        --
        -- Number format:
        --
        -- Number                  ::= Hexadecimal | Decimal
        --     Hexadecimal         ::= '0' 'x' hexDigit {hexDigit}
        --     Decimal             ::= ((digit {digit} ['.' digit {digit}]) | ('.' digit {digit}))
        --         Integer_Part    ::= digit {digit}
        --         Fractional_Part ::= '.' digit {digit}
        --
        -- In plain speak, if a decimal number has a decimal point, then it must
        -- be followed by at least one digit. A number may begin with a decimal
        -- point instead of a 0.

        if not this.Read( c ) then
            -- expected digits in integer part, or decimal point of fractional part
            Raise_Parse_Error( "Invalid number", this.tokenLoc );
        end if;

        -- - - - - Hexadecimal - - - - --
        if c = '0' then
            if this.Read( c ) then
                if c = 'x' then
                    -- found a hexadecimal number
                    decimal := False;

                    -- at least 1 digit is required
                    if not this.Read( c ) then
                        -- expected a digit in the hex part
                        Raise_Parse_Error( "Invalid number", this.tokenLoc );
                    end if;
                    loop
                        buffer := buffer & c;
                        hex := Shift_Left( hex, 4 );
                        case c is
                            when '0'..'9' => hex := hex or Unsigned_32( 0 + Character'Pos( c ) - Character'Pos( '0' ));
                            when 'A'..'F' => hex := hex or Unsigned_32(10 + Character'Pos( c ) - Character'Pos( 'A' ));
                            when 'a'..'f' => hex := hex or Unsigned_32(10 + Character'Pos( c ) - Character'Pos( 'a' ));
                            when others => null;
                        end case;
                        eof := not this.Read( c );
                        exit when eof or ((c < '0' or '9' < c) and
                                          (c < 'a' or 'f' < c) and
                                          (c < 'A' or 'F' < c));
                    end loop;
                    val := Create( Integer(hex) );
                else
                    -- put the second character back and continue reading the
                    -- number as a decimal
                    this.Unread( c );
                    c := '0';
                end if;
            else
                -- the number is simply a zero; continue reading it as a decimal
                null;
            end if;
        end if;

        if decimal then
            -- - - - - Integer Part - - - - --
            if '0' <= c and c <= '9' then
                -- read the integer part of the number
                incomplete := False;
                loop
                    buffer := buffer & c;
                    eof := not this.Read( c );
                    exit when eof or (c < '0' or '9' < c);
                end loop;
            elsif c = '.' then
                -- special case: the number begins with a dot (implicit "0.")
                -- handle this by inserting the implicit '0' and continuing with c equals '.'
                buffer := buffer & '0';
                -- now a fractional part is required
            else
                -- error: no leading digit(s) or decimal point
                Raise_Parse_Error( "Invalid number", this.tokenLoc );
            end if;

            -- - - - - Fractional Part - - - - --
            if not eof and c = '.' then
                buffer := buffer & c;

                -- read the fractional part of the number
                if not this.Read( c ) then
                    Raise_Parse_Error( "Invalid number", this.tokenLoc );
                elsif '0' <= c and c <= '9' then
                    loop
                        buffer := buffer & c;
                        if not this.Read( c ) then
                            eof := True;      -- number terminated by EOF
                            exit;
                        end if;
                        exit when c < '0' or '9' < c;
                    end loop;
                else
                    -- error: decimal point without following digits (ex: "1.")
                    Raise_Parse_Error( "Invalid number", this.tokenLoc );
                end if;
            elsif incomplete then
                -- ex: number started with '.' but the fractional part did not follow
                Raise_Parse_Error( "Invalid number", this.tokenLoc );
            end if;

            begin
                val := Create( Long_Float'Value( To_String( buffer ) ) );
            exception
                when others =>
                    Raise_Parse_Error( "Invalid number", this.tokenLoc );
            end;
        end if;

        -- put the look-ahead character back in the stream. the above IFs should
        -- have ended just after performing a Read() operation without consuming
        -- the character into the buffer.
        if not eof then
            this.Unread( c );
            if not Tokens.Is_Delimiter( c ) then
                -- an extra non-delimiter postfixes the number
                Raise_Parse_Error( "Invalid number", this.tokenLoc );
            end if;
        end if;

        pragma Assert( val.Is_Number );
        return Create_Value( val, this.tokenLoc );
    end Parse_Number_Literal;

    ----------------------------------------------------------------------------

    function Parse_Simple_Token( this : not null access Tokenizer'Class ) return Token_Ptr is
        buffer  : Unbounded_String;
        c, next : Character := ASCII.NUL;
        tok     : Token_Enum;
        valid   : Boolean;
    begin
        loop
            exit when not this.Read( c );
            buffer := buffer & c;

            -- look ahead to detect double-delimiter tokens
            exit when not this.Read( next );

            -- check for a negative number token (e.g. "-1")
            --if c = '-' and then Is_Digit( next ) then
            --    -- put the negative and the digit back and tokenize the number
            --    this.Unread( next );
            --    this.Unread( c );
            --    return this.Parse_Number_Literal;
            --end if;

            -- check for a double-delimiter token (e.g. ":=", "<=", etc.)
            if Tokens.Is_Delimiter( c ) and then
               (Tokens.Is_Delimiter( next ) and then not Is_Whitespace( next ))
            then
                -- check if the double-delimiter is a recognized simple token
                To_Token_Enum( String'(c, next), tok, valid );
                if valid then
                    -- found a double-delimiter simple token; return it
                    return Create_Token( tok, this.tokenLoc );
                end if;
            end if;

            -- put the look-ahead character back
            this.Unread( next );

            -- stop parsing the current token if a delimiter character has been reached
            exit when Tokens.Is_Delimiter( c ) or else Tokens.Is_Delimiter( next );
        end loop;

        if Length( buffer ) = 0 then
            return Create_Token( TOKEN_EOF, this.tokenLoc );
        end if;

        -- valid will be False if 'buffer' isn't a recognized simple token
        To_Token_Enum( To_String( buffer ), tok, valid );
        if not valid then
            Raise_Parse_Error( "Unrecognized token", this.tokenLoc );
        end if;
        return Create_Token( tok, this.tokenLoc );
    end Parse_Simple_Token;

    ----------------------------------------------------------------------------

    function Parse_String_Literal( this : not null access Tokenizer'Class ) return Token_Ptr is
        buffer : Unbounded_String;
        c      : Character := ASCII.NUL;
        escape : Character := ASCII.NUL;
    begin
        if not this.Expect( '"' ) then
            -- if Tokenizer is bug free, this will never happen
            Assert( False, "Tokenizer bug: string token error" );
            Raise_Parse_Error( "Malformed string", this.tokenLoc );
        end if;

        loop
            if not this.Read( c ) then
                Raise_Parse_Error( "Unterminated string", this.tokenLoc );
            end if;

            if c = '\' then
                -- escape a character; look at the next character to see what is
                -- being escaped
                if not this.Read( escape ) or else Is_EOL( escape ) then
                    Raise_Parse_Error( "Unterminated string", this.tokenLoc );
                end if;

                case escape is
                    when '\' => c := '\';               -- backslash
                    when '"' => c := '"';               -- double quotes
                    when 'a' => c := ASCII.BEL;         -- bell
                    when 'b' => c := ASCII.BS;          -- backspace
                    when 'f' => c := ASCII.FF;          -- form feed / new page
                    when 'n' => c := ASCII.LF;          -- line feed / new line
                    when 'r' => c := ASCII.CR;          -- carriage return
                    when 't' => c := ASCII.HT;          -- horizontal tab
                    when 'v' => c := ASCII.VT;          -- vertical tab
                    when 'x' =>
                        declare
                            digs : String(1..3) := (others => '0');
                        begin
                            for i in digs'Range loop
                                if not this.Read( digs(i) ) then
                                    Raise_Parse_Error( "Unterminated string", this.tokenLoc );
                                elsif digs(i) < '0' or '9' < digs(i) then
                                    Raise_Parse_Error( "Invalid escape sequence", this.Mark_Location );
                                end if;
                            end loop;
                            if Integer'Value( digs ) > 255 then
                                Raise_Parse_Error( "Invalid escape sequence", this.Mark_Location );
                            end if;
                            c := Character'Val( Integer'Value( digs ) );
                        end;
                    when others =>
                        -- invalid escape sequence in the string; mark the bad character
                        Raise_Parse_Error( "Invalid escape sequence", this.Mark_Location );
                end case;

            elsif c = '"' then
                -- the string ended with a double quote
                exit;

            elsif Is_EOL( c ) then
                -- the string ended without seeing the end quote
                Raise_Parse_Error( "Unterminated string", this.tokenLoc );
            end if;

            -- store the character in the string
            buffer := buffer & c;
        end loop;

        return Create_Value( Create( To_String( buffer ) ), this.tokenLoc );
    end Parse_String_Literal;

    ----------------------------------------------------------------------------

    function Parse_Symbol( this : not null access Tokenizer'Class ) return Token_Ptr is
        keyword : Boolean := True;       -- symbol could be a keyword (only letters, lowercase)
        buffer  : Unbounded_String;
        c       : Character;
        tok     : Token_Enum;
        valid   : Boolean;
        result  : Token_Ptr;
    begin
        -- The symbol grammar is:
        --
        -- Identifier     ::= letter {letter | digit | '_'}

        loop
            exit when not this.Read( c );

            if 'a' <= c and c <= 'z' then
                -- could still be a keyword
                null;
            elsif ('A' <= c and c <= 'Z') or
                  ('0' <= c and c <= '9') or
                  (c = '_')
            then
                -- still an identifier but not a keyword
                keyword := False;
            elsif Tokens.Is_Delimiter( c ) then
                -- end of the token; put the terminating delimiter back
                this.Unread( c );
                exit;
            else
                -- invalid character in the symbol
                Raise_Parse_Error( "Invalid symbol", this.tokenLoc );
            end if;

            -- save the character in the token buffer
            buffer := buffer & c;
        end loop;

        -- Determine the type of token
        --
        -- 1. Is it a simple token?
        -- 2. Or is it the static image of a value? (null, true, false, etc.)
        -- 3. If it's neither, it's a symbol token

        -- if the symbol could still be a keyword, check it against the list of keywords.
        if keyword then
            -- check if the symbol matches one of the static symbol names
            To_Token_Enum( To_String( buffer ), tok, valid );
            if valid then
                result := Create_Token( tok, this.tokenLoc );
            else
                -- check if the symbol matches one of the static images of a value symbol
                if buffer = "null" then
                    result := Create_Value( Null_Value, this.tokenLoc );
                elsif buffer = "true" then
                    result := Create_Value( Create( True ), this.tokenLoc );
                elsif buffer = "false" then
                    result := Create_Value( Create( False ), this.tokenLoc );
                end if;
            end if;
        end if;

        if result.Is_Null then
            -- try to parse it as a standard Symbol_Token
            result := Create_Symbol( To_String( buffer ), this.tokenLoc );
            if result.Is_Null then
                -- malformed symbol token
                Raise_Parse_Error( "Invalid symbol", this.tokenLoc );
            end if;
        end if;

        return result;
    end Parse_Symbol;

    ----------------------------------------------------------------------------

    function Read( this : not null access Tokenizer'Class;
                   c    : out Character ) return Boolean is
        buf  : Stream_Element_Array(1..1);
        last : Stream_Element_Offset;
    begin
        if not this.pushedChars.Is_Empty then
            -- return the most recently unread character
            c := this.pushedChars.Last_Element;
            this.loc.absolute := this.loc.absolute + 1;
            this.pushedChars.Delete_Last;
            return True;
        end if;

        loop
            this.strm.Read( buf, last );
            if last < buf'First then
                -- reached end-of-file
                return False;
            end if;

            c := Character'Val( buf(buf'First) );
            this.loc.absolute := this.loc.absolute + 1;
            Append( this.source, c );

            -- ignore carriage returns for end-of-line compatability
            exit when c /= ASCII.CR;
        end loop;

        if Is_EOL( c ) then
            -- handle end-of-line
            this.loc.line := this.loc.line + 1;
            this.loc.column := 1;
        elsif c = ASCII.HT then
            -- handle tab characters
            this.loc.column := this.loc.column + 4;
        else
            this.loc.column := this.loc.column + 1;
        end if;

        return True;
    exception
        when others =>
            raise Stream_Error with "Input stream failed";
    end Read;

    ----------------------------------------------------------------------------

    procedure Set_Input( this     : not null access Tokenizer'Class;
                         input    : access Root_Stream_Type'Class;
                         filepath : String ) is
    begin
        this.strm := input;
        this.loc := Token_Location'(path => To_Unbounded_String( filepath ), others => <>);
        this.tokenLoc := this.loc;
        this.pushedChars.Clear;
        for i in this.tokenQueue'Range loop
            this.tokenQueue(i).Unset;
        end loop;
        this.source := To_Unbounded_String( "" );
    end Set_Input;

    ----------------------------------------------------------------------------

    procedure Step_Back( this : not null access Tokenizer'Class ) is
    begin
        -- push back ALL previously popped tokens. this will be either:
        --   one token of any type,
        -- OR
        --   two tokens, where the first is not an EOL and the second is an EOL
        pragma Assert( this.tokenQueue(0).Not_Null );
        pragma Assert( this.tokenQueue(1).Is_Null or else Is_EOL( this.tokenQueue(1) ) );

        -- there must be room to push back at least one token
        pragma Assert( this.tokenQueue(-2).Is_Null );

        while this.tokenQueue(0).Not_Null loop
            for i in -1..1 loop
                this.tokenQueue(i-1) := this.tokenQueue(i);
            end loop;
            this.tokenQueue(1).Unset;
        end loop;
    end Step_Back;

    ----------------------------------------------------------------------------

    procedure Unread( this : not null access Tokenizer'Class; c : Character ) is
    begin
        this.pushedChars.Append( c );
        this.loc.absolute := this.loc.absolute - 1;
    end Unread;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Tokenizer ) is
        procedure Free is new Ada.Unchecked_Deallocation( Tokenizer'Class, A_Tokenizer );
    begin
        Free( this );
    end Delete;

end Scribble.Tokenizers;
