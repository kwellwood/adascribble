--
-- Copyright (c) 2015-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with System.Address_To_Access_Conversions;

package body Values.Indirects is

    type Indirect_Data is
        record
            obj  : Object_Refs;
            impl : A_Indirection;
        end record;
    for Indirect_Data use
        record
            obj at 0 range 0..31;
        end record;

    package Accesses is new System.Address_To_Access_Conversions( Indirect_Data );
    use Accesses;

    --==========================================================================

    package body Generics is

        type Indirect_Implicit is new Indirection with
            record
                object : access Storage'Class;
                reader : A_Implicit_Reader := null;
                writer : A_Implicit_Writer := null;
            end record;

        overriding
        function Read( this : Indirect_Implicit ) return Value;

        overriding
        procedure Write( this : Indirect_Implicit; val : Value'Class );

        ------------------------------------------------------------------------

        function Create_Indirect( obj    : access Storage'Class;
                                  reader : not null A_Implicit_Reader;
                                  writer : A_Implicit_Writer ) return Value is
            data : constant Object_Pointer := new Indirect_Data'(impl   => new Indirect_Implicit,
                                                                 others => <>);
            this : Value := Value'(Controlled with v => TAG_INDIRECT);
        begin
            Indirect_Implicit(data.impl.all).object := obj;
            Indirect_Implicit(data.impl.all).reader := reader;
            Indirect_Implicit(data.impl.all).writer := writer;
            this.Set_Object( To_Address( data ) );
            return this;
        end Create_Indirect;

        ------------------------------------------------------------------------

        function Read( this : Indirect_Implicit ) return Value is (this.reader.all( this.object ));

        ------------------------------------------------------------------------

        procedure Write( this : Indirect_Implicit; val : Value'Class ) is
        begin
            if this.writer /= null then
                this.writer.all( this.object, val );
            end if;
        end Write;

        --======================================================================

        type Indirect_Explicit is new Indirection with
            record
                object : access Storage'Class;
                reader : A_Explicit_Reader := null;
                writer : A_Explicit_Writer := null;
                name   : Unbounded_String;
            end record;

        overriding
        function Read( this : Indirect_Explicit ) return Value;

        overriding
        procedure Write( this : Indirect_Explicit; val : Value'Class );

        ------------------------------------------------------------------------

        function Create_Indirect( obj    : access Storage'Class;
                                  reader : not null A_Explicit_Reader;
                                  writer : A_Explicit_Writer;
                                  name   : String ) return Value is
            data : constant Object_Pointer := new Indirect_Data'(impl   => new Indirect_Explicit,
                                                                 others => <>);
            this : Value := Value'(Controlled with v => TAG_INDIRECT);
        begin
            Indirect_Explicit(data.impl.all).object := obj;
            Indirect_Explicit(data.impl.all).reader := reader;
            Indirect_Explicit(data.impl.all).writer := writer;
            Indirect_Explicit(data.impl.all).name := To_Unbounded_String( name );
            this.Set_Object( To_Address( data ) );
            return this;
        end Create_Indirect;

        ------------------------------------------------------------------------

        function Read( this : Indirect_Explicit ) return Value
            is (this.reader.all( this.object, To_String( this.name ) ));

        ------------------------------------------------------------------------

        procedure Write( this : Indirect_Explicit; val : Value'Class ) is
        begin
            if this.writer /= null then
                this.writer.all( this.object, To_String( this.name ), val );
            end if;
        end Write;

    end Generics;

    --==========================================================================

    procedure Delete_Indirect( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( Indirection'Class, A_Indirection );
        procedure Free is new Ada.Unchecked_Deallocation( Indirect_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        if data /= null then
            Free( data.impl );
            Free( data );
        end if;
    end Delete_Indirect;

    ----------------------------------------------------------------------------

    function Image_Indirect( this : Value'Class ) return String is ("<indirect>");

    ----------------------------------------------------------------------------

    function Read_Indirect( this : Value'Class ) return Value is
    begin
        return To_Pointer( this.Get_Address ).impl.Read;
    end Read_Indirect;

    ----------------------------------------------------------------------------

    procedure Write_Indirect( this : Value'Class; val : Value'Class ) is
    begin
        To_Pointer( this.Get_Address ).impl.Write( val );
    end Write_Indirect;

end Values.Indirects;
