--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;

package body Scribble.Scanners is

    function Create_Scanner return A_Scanner is (new Scanner);

    ----------------------------------------------------------------------------

    function Accept_One( this       : not null access Scanner'Class;
                         acceptable : Token_Enums ) return Token_Ptr is
        token : Token_Ptr := this.tokenizer.Next;
        tt    : constant Token_Enum := token.Get.Get_Type;
    begin
        for i in acceptable'Range loop
            if tt = acceptable(i) then
                -- the next token is in the acceptable set
                return token;
            end if;
        end loop;

        -- does not match any in the set
        this.tokenizer.Step_Back;
        token.Unset;
        return token;
    end Accept_One;

    ----------------------------------------------------------------------------

    function Accept_Token( this : not null access Scanner'Class;
                           t    : Token_Enum ) return Token_Ptr is
        token : Token_Ptr := this.tokenizer.Next( skipEOL => t /= Tokenizers.Tokens.TOKEN_EOL );
    begin
        if token.Get.Get_Type /= t then
            this.tokenizer.Step_Back;
            token.Unset;
        end if;
        return token;
    end Accept_Token;

    ----------------------------------------------------------------------------

    function Expect_One( this     : not null access Scanner'Class;
                         expected : Token_Enums ) return Token_Ptr is
        token : constant Token_Ptr := this.tokenizer.Next;
        tt    : constant Token_Enum := token.Get.Get_Type;
    begin
        for i in expected'Range loop
            if tt = expected(i) then
                -- the next token is in the expected set
                return token;
            end if;
        end loop;

        this.tokenizer.Step_Back;
        Raise_Parse_Error( "Found unexpected '" & Name( token.Get.Get_Type ) & "'", token.Get.Location );

        -- won't happen
        return Null_Token;
    end Expect_One;

    ----------------------------------------------------------------------------

    function Expect_Token( this : not null access Scanner'Class;
                           t    : Token_Enum ) return Token_Ptr is
        token : constant Token_Ptr := this.tokenizer.Next( skipEOL => t /= Tokenizers.Tokens.TOKEN_EOL );
    begin
        if token.Get.Get_Type /= t then
            this.tokenizer.Step_Back;
            Raise_Parse_Error( "Expected '" & Name( t ) & "' but found '" & Name( token.Get.Get_Type ) & "'",
                               token.Get.Location );
        end if;
        return token;
    end Expect_Token;

    ----------------------------------------------------------------------------

    procedure Expect_Token( this : not null access Scanner'Class;
                            t    : Token_Enum ) is
        token : constant Token_Ptr := this.Expect_Token( t );
        pragma Unreferenced( token );
    begin
        null;
    end Expect_Token;

    ----------------------------------------------------------------------------

    function Get_Source( this : not null access Scanner'Class ) return Unbounded_String is (this.tokenizer.Get_Source);

    ----------------------------------------------------------------------------

    function Location( this : not null access Scanner'Class ) return Token_Location is (this.tokenizer.Location);

    ----------------------------------------------------------------------------

    procedure Set_Input( this     : not null access Scanner'Class;
                         strm     : access Root_Stream_Type'Class;
                         filepath : String := "" ) is
    begin
        this.tokenizer.Set_Input( strm, filepath );
    end Set_Input;

    ----------------------------------------------------------------------------

    procedure Step_Back( this  : not null access Scanner'Class ) is
    begin
        this.tokenizer.Step_Back;
    end Step_Back;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Scanner ) is
        procedure Free is new Ada.Unchecked_Deallocation( Scanner'Class, A_Scanner );
    begin
        if this /= null then
            Delete( this.tokenizer );
            Free( this );
        end if;
    end Delete;

end Scribble.Scanners;
