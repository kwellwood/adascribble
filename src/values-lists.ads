--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Lists is

    -- Creates a new empty List.
    function Create_List return Value;

    -- Creates a new List from a literal array of values. The values will be
    -- copied into the list by default, unless 'consume' is set to True. If
    -- 'consume' is True, the caller should discard these values after the call.
    function Create_List( elements : Value_Array; consume : Boolean := False ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type List_Value is new Value with private;

    -- Returns a List_Value pointing to the data of 'this'. The data is never
    -- copied.
    function As_List( this : Value'Class ) return List_Value;

    -- Appends a single value to the end of the list, increasing its length by
    -- one. The value will be copied unless 'consume' is set to True. If 'val'
    -- is consumed, the caller should discard the pointer after the call.
    procedure Append( this    : List_Value'Class;
                      val     : Value'Class;
                      consume : Boolean := False ) with Inline;
    pragma Precondition( this.Valid );

    -- Creates a new Value representing the given argument and appends it to the
    -- end of the List, increasing its size by one. These procedures are
    -- equivalent to calling .Append( Create( val ), consume => True )
    procedure Append( this : List_Value'Class; val : Boolean ) with Inline_Always;
    procedure Append( this : List_Value'Class; val : Integer ) with Inline_Always;
    procedure Append( this : List_Value'Class; val : Float ) with Inline_Always;
    procedure Append( this : List_Value'Class; val : Long_Float ) with Inline_Always;
    procedure Append( this : List_Value'Class; val : String ) with Inline;
    procedure Append( this : List_Value'Class; val : Unbounded_String ) with Inline;

    -- Appends the contents of List 'other' to the end of this List. All values
    -- in 'other' will be copied, unless 'consume' is set to True. If the
    -- elements of 'other' are consumed, the caller should discard the pointer
    -- to 'other' after the call.
    procedure Append_List( this    : List_Value'Class;
                           other   : Value'Class;
                           consume : Boolean := False );
    pragma Precondition( this.Valid );

    -- Removes all elements from the list, decreasing its length to zero. All
    -- outstanding reference values to elements in the list will become invalid.
    procedure Clear( this : List_Value'Class );
    pragma Precondition( this.Valid );

    -- Returns True if the List contains at least one element matching 'val'.
    function Contains( this : List_Value'Class; val : Value'Class ) return Boolean;
    pragma Precondition( this.Valid );

    -- Returns the index of the first instance of 'val' in the List, starting at
    -- index 'start', or zero if it isn't found. List indices start at 1.
    function Find( this  : List_Value'Class;
                   val   : Value'Class;
                   start : Integer := 1 ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Returns the value of the first element in the List. If the List has a
    -- length of zero, Null will be returned. The returned value is not a copy;
    -- the caller is reponsible for copying it as necessary.
    function First( this : List_Value'Class ) return Value with Inline;
    pragma Precondition( this.Valid );

    -- Returns the element at index 'index', or Null if the index does not
    -- exist. List indices start at 1. The returned value is not a copy; the
    -- caller is reponsible for copying it as necessary.
    function Get( this : List_Value'Class; index : Integer ) return Value;
    pragma Precondition( this.Valid );

    -- Returns the element at index 'index', cast to a specific type. List
    -- indices start at 1. 'default' will be returned if the index does not
    -- exist or its element does not match the requested type.
    function Get_Boolean( this : List_Value'Class; index : Integer; default : Boolean := False ) return Boolean with Inline;
    function Get_Int( this : List_Value'Class; index : Integer; default : Integer := 0 ) return Integer with Inline;
    function Get_Float( this : List_Value'Class; index : Integer; default : Float := 0.0 ) return Float with Inline;
    function Get_Long_Float( this : List_Value'Class; index : Integer; default : Long_Float := 0.0 ) return Long_Float with Inline;
    function Get_String( this : List_Value'Class; index : Integer; default : String := "" ) return String with Inline;
    function Get_Unbounded_String( this : List_Value'Class; index : Integer; default : String := "" ) return Unbounded_String with Inline;

    -- Returns a new List containing copies of up to the first 'count' elements
    -- in this List.
    function Head( this : List_Value'Class; count : Integer ) return Value;
    pragma Precondition( this.Valid );

    -- Inserts 'val' at index 'index', increasing the length of the list by one.
    -- If 'index' is off the end of the list, the size of the list will be
    -- increased to include 'index' as the last element. If 'index' is less than
    -- one, 'val' will be prepended. The given value will be copied, unless
    -- 'consume' is True. If 'val' is consumed, the caller should discard the
    -- pointer after the call. Because this operation shifts elements in the
    -- list, all outstanding reference values to elements from 'index' to the
    -- end of the list will become invalid.
    procedure Insert( this    : List_Value'Class;
                      val     : Value'Class;
                      index   : Integer;
                      consume : Boolean := False );
    pragma Precondition( this.Valid );

    -- Returns the value of the last element in the List. If the List has a
    -- length of zero, Null will be returned. The returned value is not a copy;
    -- the caller is reponsible for copying it as necessary.
    function Last( this : List_Value'Class ) return Value with Inline;
    pragma Precondition( this.Valid );

    -- Returns the number of elements in the List.
    function Length( this : List_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    -- Prepends a single value to the head of the list, increasing its length by
    -- one. The value will be copied unless 'consume' is set to True. If 'val'
    -- is consumed, the caller should discard the pointer after the call.
    -- Because this operation shifts elements in the list, ALL outstanding
    -- reference values to elements in the list will become invalid.
    procedure Prepend( this    : List_Value'Class;
                       val     : Value'Class;
                       consume : Boolean := False ) with Inline;
    pragma Precondition( this.Valid );

    -- Returns a reference in the element at index 'index'. List indices start
    -- at 1. If 'createMissing' is True and the index is off the end of the
    -- List, the length will be increased to contain the index and the newly
    -- added elements will be filled with Null values. Otherwise, Null will be
    -- returned to indicate the index is invalid.
    function Reference( this          : List_Value'Class;
                        index         : Positive;
                        createMissing : Boolean := True ) return Value;
    pragma Precondition( this.Valid );

    -- Removes up to 'count' elements, starting with 'index'. If 'index' is not
    -- within bounds, nothing will change. All outstanding reference values from
    -- 'index' to the end of the list will become invalid.
    procedure Remove( this : List_Value'Class; index : Integer; count : Integer := 1 );
    pragma Precondition( this.Valid );

    -- Sets the element at index 'index' to 'val'. List indices start at 1. If
    -- the index is past the end of the List, the List's length with be
    -- increased to contain the specified index and the newly added elements
    -- will be filled with Null values. The given value will be copied, unless
    -- 'consume' is True. If 'val' is consumed, the caller should discard the
    -- pointer after the call.
    procedure Set( this    : List_Value'Class;
                   index   : Positive;
                   val     : Value'Class;
                   consume : Boolean := False );
    pragma Precondition( this.Valid );

    -- Returns a new List containing copies of 'count' elements from this List,
    -- beginning at index 'from'. List indices start at 1. If 'from' is off the
    -- end of the List, an empty list will be returned. If 'count' goes past the
    -- end of the List, only up to (and including) the last element will be
    -- returned.
    function Slice( this : List_Value'Class; from : Integer; count : Integer ) return Value;
    pragma Precondition( this.Valid );

    -- Sorts the contents of the List using 'lt' for "less than" comparison. If
    -- 'lt' is null, the standard "<" operator will be used by default. All
    -- references to elements in the list will become invalid.
    procedure Sort( this : List_Value'Class;
                    lt   : access function( l, r : Value'Class ) return Boolean := null );

    -- Returns a new List containing copies of up to the last 'count' elements
    -- in this List.
    function Tail( this : List_Value'Class; count : Integer ) return Value;
    pragma Precondition( this.Valid );

    -- Fills a Value_Array 'output' with the elements of the list. If the array
    -- is shorter than the list, only the first output'Length items will be
    -- written. Like Get(), the values written to the array are not copies; the
    -- caller is responsible for copying them as necessary.
    procedure To_Array( this : List_Value'Class; output : in out Value_Array );
    pragma Precondition( this.Valid );

    function Valid( this : List_Value'Class ) return Boolean is (this.Is_List);

    -- Creates a new List from the concatenation of 'l' and 'r'.
    function "&"( l : List_Value'Class; r : List_Value'Class ) return Value with Inline;
    pragma Precondition( l.Valid );
    pragma Precondition( r.Valid );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Clone_List( this : Value'Class ) return Value;
    pragma Precondition( this.Is_List );

    function Compare_Lists( left, right : Value'Class ) return Integer;
    pragma Precondition( left.Is_List );
    pragma Precondition( right.Is_List );

    procedure Delete_List( obj : Address );

    function Image_List( this : Value'Class ) return String;
    pragma Precondition( this.Is_List );

    procedure Read_List( stream : access Root_Stream_Type'Class;
                         this   : in out Value'Class );
    pragma Precondition( this.Is_List );

    procedure Write_List( stream : access Root_Stream_Type'Class;
                          this   : Value'Class );
    pragma Precondition( this.Is_List );

private

    type List_Data;
    type A_List_Data is access all List_Data;
    pragma No_Strict_Aliasing( A_List_Data );

    type List_Value is new Value with
        record
            data : A_List_Data := null;
        end record;

end Values.Lists;

