--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Containers.Indefinite_Vectors;
with Ada.Text_IO;                       use Ada.Text_IO;
with Values;                            use Values;
with Values.Lists;                      use Values.Lists;

package Scribble.CLI is

    Exception_Fail : constant Exit_Status := Failure;
    Bad_Arguments  : constant Exit_Status := 2;
    Read_Fail      : constant Exit_Status := 3;
    Parse_Fail     : constant Exit_Status := 4;
    Write_Fail     : constant Exit_Status := 5;

    procedure Error_Line( str : String );

    procedure Output_Text( str : String ) renames Ada.Text_IO.Put;
    procedure Output_Line( str : String ) renames Ada.Text_IO.Put_Line;

    package String_Vectors is new Ada.Containers.Indefinite_Vectors( Positive, String, "=" );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Compile_Object( sourcePath   : String;
                             compiledPath : String;
                             namespaces   : String_Vectors.Vector;
                             enableDebug  : Boolean ) return Exit_Status;

    function Disassemble( compiledPath : String; indices : List_Value ) return Exit_Status;

    function Display_Source( compiledPath : String; indices : List_Value ) return Exit_Status;

    procedure Interpretor_Prompt( namespaces : String_Vectors.Vector );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Replace_Extension( path : String; extension : String ) return String;

private

    function Get_Extension( path : String ) return String;

    function Get_Filename( path : String ) return String;

end Scribble.CLI;
