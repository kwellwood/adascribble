--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Containers;                    use Ada.Containers;
with Ada.Unchecked_Deallocation;
with Scribble.Ast.Expressions.Operators;
 use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Values.Casting;                    use Values.Casting;
with Values.Construction;               use Values.Construction;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;

package body Scribble.Parsers is

    use Tokens;

    --==========================================================================

    function Create_Parser( runtime : not null A_Scribble_Runtime ) return A_Parser is
        this : constant A_Parser := new Parser;
    begin
        this.Construct( runtime );
        return this;
    end Create_Parser;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Parser; runtime : not null A_Scribble_Runtime ) is
    begin
        this.runtime := runtime;
        this.scanner := Create_Scanner;
    end Construct;

    ----------------------------------------------------------------------------

    not overriding
    procedure Delete( this : in out Parser ) is
    begin
        Delete( this.scanner );
    end Delete;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Parser ) is
        procedure Free is new Ada.Unchecked_Deallocation( Parser'Class, A_Parser );
    begin
        if this /= null then
            this.Delete;
            Free( this );
        end if;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Enable_Debug( this : not null access Parser'Class; enabled : Boolean ) is
    begin
        this.enableDebug := enabled;
    end Enable_Debug;

    ----------------------------------------------------------------------------

    procedure Expect_EOF( this : not null access Parser'Class ) is
        token : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_EOF );
        if token.Is_Null then
            -- didn't find a TK_EOF so there must be another token to read
            Raise_Parse_Error( "Expected End of File", this.scanner.Location );
        end if;
    end Expect_EOF;

    ----------------------------------------------------------------------------

    function Expect_Value( this : not null access Parser'Class ) return A_Ast_Expression is
        result : A_Ast_Expression;
    begin
        result := this.Scan_Value;
        if result = null then
            Raise_Parse_Error( "Expected literal or function definition", this.scanner.Location );
        end if;

        return result;
    end Expect_Value;

    ----------------------------------------------------------------------------

    function Get_Source( this : not null access Parser'Class ) return Unbounded_String is (this.scanner.Get_Source);

    ----------------------------------------------------------------------------

    procedure Set_Input( this : not null access Parser'Class;
                         strm : access Root_Stream_Type'Class;
                         path : String := "" ) is
    begin
        this.path := To_Unbounded_String( path );
        this.scanner.Set_Input( strm, path );
    end Set_Input;

    -- END PUBLIC DEFINITIONS --
    --==========================================================================

    function Expect_Expression( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is
        expr : A_Ast_Expression;
    begin
        expr := this.Scan_Expression( literal );
        if expr = null then
            Raise_Parse_Error( "Expected expression", this.scanner.Location );
        end if;
        return expr;
    end Expect_Expression;

    ----------------------------------------------------------------------------

    --
    -- Expression<literal> ::= Assign_Term<literal>
    --
    function Scan_Expression( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is (this.Scan_Assign_Term( literal ));

    ----------------------------------------------------------------------------

    --
    -- Expression_List<literal> ::= (Anon_Function_Definition | Expression<literal>) [(',' | end-of-line) Expression_List<literal>]
    --
    function Scan_Expression_List( this : not null access Parser'Class; literal : Boolean ) return Expression_Vectors.Vector is
        result : Expression_Vectors.Vector;
        expr   : A_Ast_Expression := null;
    begin
        loop
            -- scan the element as an anonymous function definition
            expr := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed ));
            if expr /= null then
                result.Append( expr );
                expr := null;

            -- scan the element as an expression
            else
                expr := this.Scan_Expression( literal );
                if expr /= null then
                    result.Append( expr );
                    expr := null;
                elsif result.Is_Empty then
                    -- list is empty, a closing token will be expected
                    exit;
                else
                    -- the list isn't empty, so the previous token was a
                    -- comma; another expression was expected here.
                    Raise_Parse_Error( "Expected expression", this.scanner.Location );
                end if;
            end if;

            -- check for a potentially closing token
            if this.scanner.Accept_One( (TK_RPAREN, TK_RBRACKET, TK_RBRACE) ).Not_Null then
                this.scanner.Step_Back;   -- put it back for the caller to see
                exit;
            end if;

            -- expect a newline or comma between elements
            if this.scanner.Accept_Token( TK_EOL ).Not_Null or this.scanner.Accept_Token( TK_COMMA ).Not_Null then
                -- if a newline is found first, a comma may still follow it.
                -- the check above uses 'or' instead of 'or else' so that both
                -- Accept_Token() calls occur. do not combine the token types
                -- and call Accept_One(); it does not handle the special TK_EOL.
                null;
            else
                this.scanner.Expect_Token( TK_COMMA );
            end if;
        end loop;

        return result;
    exception
        when others =>
            for e of result loop
                Delete( A_Ast_Node(e) );
            end loop;
            result.Clear;
            raise;
    end Scan_Expression_List;

    ----------------------------------------------------------------------------

    --
    -- Assign_Term<literal=False> ::= Ternary_Term<literal> [Assign_Op (Anon_Function_Definition | Assign_Term)]
    -- Assign_Term<literal=True>  ::= Ternary_Term<literal>
    --   Assign_Op ::= ':=' | '+=' | '-=' | '*=' | '/=' | '&='
    --
    function Scan_Assign_Term( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is

        ------------------------------------------------------------------------

        function Accept_Assign_Op return Token_Ptr is
        (
            this.scanner.Accept_One( (TK_COLON_EQUALS,
                                      TK_PLUS_EQUALS,
                                      TK_MINUS_EQUALS,
                                      TK_STAR_EQUALS,
                                      TK_SLASH_EQUALS,
                                      TK_AMP_EQUALS) )
        );

        ------------------------------------------------------------------------

        left,
        right  : A_Ast_Expression := null;
        aop    : Token_Ptr;
        assign : A_Ast_Expression;
    begin
        left := this.Scan_Ternary_Term( literal );
        if left /= null then
            -- check for an assignment operator that will assign into 'left'
            aop := Accept_Assign_Op;
            if aop.Not_Null then
                -- found an assignment operator
                if literal then
                    Raise_Parse_Error( "Expression is not literal", aop.Get.Location );
                end if;

                -- check for possible things to assign:
                -- 1. Anon_Function_Definition
                -- 2. Assign_Term
                right := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed ));
                if right = null then
                    -- if no Anon_Function_Definition is found, expect an Assign_Term
                    right := this.Scan_Assign_Term( literal => False );
                    if right = null then
                        Raise_Parse_Error( "Expected expression", this.scanner.Location );
                    end if;
                else
                    -- verify that the correct assignment operator was found
                    if aop.Get.Get_Type /= TK_COLON_EQUALS then
                        Raise_Parse_Error( "Expected '" & Name( TK_COLON_EQUALS ) &
                                           "' but found '" & Name( aop.Get.Get_Type ) & "'",
                                           aop.Get.Location );
                    end if;
                end if;

                assign := Create_Assign( (case aop.Get.Get_Type is
                                          when TK_COLON_EQUALS => ASSIGN_DIRECT,
                                          when TK_PLUS_EQUALS  => ASSIGN_ADD,
                                          when TK_MINUS_EQUALS => ASSIGN_SUB,
                                          when TK_STAR_EQUALS  => ASSIGN_MUL,
                                          when TK_SLASH_EQUALS => ASSIGN_DIV,
                                          when TK_AMP_EQUALS   => ASSIGN_CAT,
                                          when others          => ASSIGN_CAT),
                                         aop.Get.Location,
                                         left,
                                         right );
                if assign /= null then
                    left := assign;
                else
                    -- Create_Assign_Op did not recognize the assign token type
                    -- this will not happen unless there is a bug in the parser
                    Raise_Parse_Error( "Parser failure", aop.Get.Location );
                end if;
            else
                -- Assign_Term is just a Ternary_Term
                null;
            end if;
        else
            -- No Assign_Term found
            null;
        end if;

        return left;
    exception
        when others =>
            Delete( A_Ast_Node(left) );
            Delete( A_Ast_Node(right) );
            raise;
    end Scan_Assign_Term;

    ----------------------------------------------------------------------------

    --
    -- Ternary_Term<literal> ::= Binary_Term<literal> ['?' Ternary_Term<literal> ':' Ternary_Term<literal>]
    --
    function Scan_Ternary_Term( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is
        condition : A_Ast_Expression := null;
        question  : Token_Ptr;
        ifTrue,
        ifFalse   : A_Ast_Expression := null;
    begin
        condition := this.Scan_Binary_Term( literal );
        if condition /= null then
            -- found a binary_term, check for the conditional operator token
            question := this.scanner.Accept_Token( TK_QUESTION );
            if question.Not_Null then
                -- expect all the terms after the conditional operator token
                ifTrue := this.Scan_Ternary_Term( literal );
                if ifTrue /= null then
                    this.scanner.Expect_Token( TK_COLON );

                    ifFalse := this.Scan_Ternary_Term( literal );
                    if ifFalse /= null then
                        condition := Create_Conditional( question.Get.Location, condition, ifTrue, ifFalse );
                    else
                        Raise_Parse_Error( "Expected expression", this.scanner.Location );
                    end if;
                else
                    Raise_Parse_Error( "Expected expression", this.scanner.Location );
                end if;
            else
                -- Ternary_Term is just a Binary_Term
                null;
            end if;
        else
            -- no Ternary_Term found
            null;
        end if;

        return condition;
    exception
        when others =>
            Delete( A_Ast_Node(ifTrue) );
            Delete( A_Ast_Node(condition) );
            raise;
    end Scan_Ternary_Term;

    ----------------------------------------------------------------------------

    --
    -- Binary_Term<literal> ::= Lterm<literal> [Binary_Op Binary_Term<literal>]
    --   Binary_Op          ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' |
    --                          '<=' | '!=' | '+' | '-' | '&' | '*' | '/' | '%' | '^'
    --
    -- or, written differently,
    -- Binary_Term<literal> ::= Lterm<literal> {Binary_Op Lterm<literal>}
    --   Binary_Op          ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' |
    --                          '<=' | '!=' | '+' | '-' | '&' | '*' | '/' | '%' | '^'
    --
    function Scan_Binary_Term( this       : not null access Parser'Class;
                               literal    : Boolean;
                               precedence : Integer := 0 ) return A_Ast_Expression is

        ------------------------------------------------------------------------

        function Accept_Binary_Op return Token_Ptr is
        (
            this.scanner.Accept_One( (TK_AND,
                                      TK_OR,
                                      TK_XOR,
                                      TK_IN,
                                      TK_EQUALS,
                                      TK_GREATER,
                                      TK_GREATER_EQUALS,
                                      TK_LESS,
                                      TK_LESS_EQUALS,
                                      TK_BANG_EQUALS,
                                      TK_PLUS,
                                      TK_MINUS,
                                      TK_AMP,
                                      TK_STAR,
                                      TK_SLASH,
                                      TK_PERCENT,
                                      TK_CARAT) )
        );

        ------------------------------------------------------------------------

        function To_Binary_Type( token : Token_Ptr ) return Binary_Type is
        (
            case token.Get.Get_Type is
                when TK_AND            => BINARY_AND,
                when TK_OR             => BINARY_OR,
                when TK_XOR            => BINARY_XOR,
                when TK_IN             => BINARY_IN,
                when TK_EQUALS         => BINARY_EQ,
                when TK_GREATER        => BINARY_GT,
                when TK_GREATER_EQUALS => BINARY_GTE,
                when TK_LESS           => BINARY_LT,
                when TK_LESS_EQUALS    => BINARY_LTE,
                when TK_BANG_EQUALS    => BINARY_NEQ,
                when TK_PLUS           => BINARY_ADD,
                when TK_MINUS          => BINARY_SUB,
                when TK_AMP            => BINARY_CAT,
                when TK_STAR           => BINARY_MUL,
                when TK_SLASH          => BINARY_DIV,
                when TK_PERCENT        => BINARY_MOD,
                when TK_CARAT          => BINARY_POW,
                when others            => BINARY_POW    -- don't happen
        );

        ------------------------------------------------------------------------

        left,
        right,
        result        : A_Ast_Expression := null;
        bop           : Token_Ptr;
        bopPrecedence : Integer;
    begin
        left := this.Scan_Lterm( literal );
        if left /= null then
            -- found a left term, loop over all following Binary_Ops and Binary_Terms
            bop := Accept_Binary_Op;
            while bop.Not_Null loop
                -- found a binary operator; check the precedence

                bopPrecedence := Get_Precedence( To_Binary_Type( bop ) );
                if bopPrecedence > precedence then
                    -- the operator has a higher precedence than the current
                    -- scan level, so a right-hand term is required right now.
                    -- the result of this operator will replace the left hand
                    -- term in the binary term scanning.
                    right := this.Scan_Binary_Term( literal, bopPrecedence );
                    if right /= null then
                        result := Create_Binary_Op( To_Binary_Type( bop ),
                                                    bop.Get.Location,
                                                    left,
                                                    right );
                        if result = null then
                            -- Create_Binary_Op did not recognize the token type
                            -- this will not happen unless there is a bug in the
                            -- parser
                            Raise_Parse_Error( "Parser failure", bop.Get.Location );
                        end if;

                        left := result;
                        right := null;

                        -- continue scanning for binary terms until there are no more, or the next
                        -- operator isn't higher precedence.
                    else
                        -- expected a Binary_Term that wasn't found
                        Raise_Parse_Error( "Expected operand", this.scanner.Location );
                    end if;
                else
                    -- the operator's precedence is lower than, or the same as,
                    -- the current scan level. put it back in the stream so we
                    -- see it later. It will operate on the result of this
                    -- expression and whatever is to its right.
                    this.scanner.Step_Back;
                    exit;
                end if;

                bop := Accept_Binary_Op;
            end loop;
        else
            -- no Binary_Term found
            null;
        end if;

        return left;
    exception
        when others =>
            Delete( A_Ast_Node(right) );
            Delete( A_Ast_Node(left) );
            raise;
    end Scan_Binary_Term;

    ----------------------------------------------------------------------------

    --
    -- Lterm<literal> ::= Unary_Op<literal> Lterm<literal>
    -- Lterm<literal> ::= Rterm<literal>
    --   Unary_Op<literal=False> ::= '!' | '-' | '@'
    --   Unary_Op<literal=True>  ::= '!' | '-'
    --
    function Scan_Lterm( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is

        ------------------------------------------------------------------------

        function Accept_Unary_Op return Token_Ptr is
        (
            this.scanner.Accept_One( (TK_BANG,
                                      TK_MINUS,
                                      TK_AT) )
        );

        ------------------------------------------------------------------------

        function Create_Unary_Op( token   : Token_Ptr;
                                  right   : not null A_Ast_Expression;
                                  literal : Boolean ) return A_Ast_Expression is
            result : A_Ast_Expression := null;
        begin
            case token.Get.Get_Type is
                when TK_BANG  => result := Create_Unary_Op( UNARY_NOT, token.Get.Location, right );
                when TK_MINUS => result := Create_Unary_Op( UNARY_NEG, token.Get.Location, right );
                when TK_AT =>
                    if not literal then
                        result := Create_Unary_Op( UNARY_REF, token.Get.Location, right );
                    end if;
                when others => null;
            end case;
            return result;
        end Create_Unary_Op;

        ------------------------------------------------------------------------

        uop    : Token_Ptr;
        right  : A_Ast_Expression;
        result : A_Ast_Expression;
    begin
        -- accept a unary operator
        uop := Accept_Unary_Op;
        if uop.Not_Null then
            -- found: Unary_Op, required: Term
            right := this.Scan_Lterm( literal );
            if right /= null then
                result := Create_Unary_Op( uop, right, literal );
                if result = null then
                    -- the operator is not allowed in a literal expression
                    Raise_Parse_Error( "Expression is not literal", uop.Get.Location );
                end if;

                right := result;
            else
                -- expected a Term that wasn't found
                Raise_Parse_Error( "Expected operand", this.scanner.Location );
            end if;
        else
            -- accept an rterm
            right := this.Scan_Rterm( literal );
        end if;

        return right;
    exception
        when others =>
            Delete( A_Ast_Node(right) );
            raise;
    end Scan_Lterm;

    ----------------------------------------------------------------------------

    --
    -- Rterm<literal=False> ::= Operand<literal> {Index | Function_Call | Membership}
    -- Rterm<literal=True>  ::= Operand<literal>
    --
    function Scan_Rterm( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is
        left : A_Ast_Expression;
        rOp  : A_Ast_Expression;
    begin
        left := this.Scan_Operand( literal );

        if left /= null then
            loop
                -- a trick is used with Rterm to avoid ambiguity with end-of-line
                -- delimited lists and maps: if an EOL exists after the Operand,
                -- don't scan the rOp. this means that index, function call, or
                -- membership operators cannot be separated from the operand by
                -- a newline (which usually isn't done anyway, by convention).
                -- if they were scanned after an EOL, then EOL-delimited lists
                -- containing lists or parenthesized expressions would break.
                --
                -- Example:
                -- [ [1]        Parse this as: [ [1], [2], [3] ]
                --   [2]                  NOT: [ [1][2][3] ]
                --   [3] ]
                --
                if this.scanner.Accept_Token( TK_EOL ).Not_Null then
                    this.scanner.Step_Back;
                    exit;
                end if;

                rOp := this.Scan_Index( left );
                if rOp = null then
                    rOp := this.Scan_Function_Call( left );
                    if rOp = null then
                        rOp := this.Scan_Membership( left );
                        if rOp = null then
                            exit;
                        end if;
                    end if;
                end if;

                left := rOp;
                if literal then
                    Raise_Parse_Error( "Expression is not literal", left.Location );
                end if;
            end loop;
        end if;

        return left;
    exception
        when others =>
            Delete( A_Ast_Node(left) );
            raise;
    end Scan_Rterm;

    ----------------------------------------------------------------------------

    --
    -- Index ::= '[' Expression<literal=False> ']'
    --
    function Scan_Index( this          : not null access Parser'Class;
                         containerExpr : not null A_Ast_Expression ) return A_Ast_Expression is
        index : A_Ast_Expression;
        expr  : A_Ast_Expression;
        token : Token_Ptr;
    begin
        -- check for an opening '['
        token := this.scanner.Accept_Token( TK_LBRACKET );
        if token.Not_Null then
            -- the index expression and closing bracket are expected
            expr := this.Expect_Expression( literal => False );
            this.scanner.Expect_Token( TK_RBRACKET );

            index := Create_Index( token.Get.Location, containerExpr, expr );
            expr := null;
        else
            -- no Nth found
            null;
        end if;

        return index;
    exception
        when others =>
            Delete( A_Ast_Node(expr) );
            raise;
    end Scan_Index;

    ----------------------------------------------------------------------------

    --
    -- Function_Call ::= '(' [Expression_List<literal=False>] ')'
    --
    function Scan_Function_Call( this     : not null access Parser'Class;
                                 funcExpr : not null A_Ast_Expression ) return A_Ast_Expression is
        args  : Expression_Vectors.Vector;
        call  : A_Ast_Function_Call := null;
        token : Token_Ptr;
        ok    : Boolean;
    begin
        -- check for an opening '('
        token := this.scanner.Accept_Token( TK_LPAREN );
        if token.Not_Null then
            -- scan the list of arguments
            args := this.Scan_Expression_List( literal => False );             -- may raise exception
            if args.Length > 255 then
                Raise_Parse_Error( "Too many arguments", args.Element( 256 ).Location );
            end if;

            -- expect the closing ')'
            this.scanner.Expect_Token( TK_RPAREN );

            -- now it's safe to build the function
            if funcExpr.Get_Type = MEMBERSHIP then
                call := Create_Member_Call( token.Get.Location, funcExpr );    -- consumes funcExpr
            else
                call := Create_Function_Call( token.Get.Location, funcExpr );  -- consumes funcExpr
            end if;
            for a of args loop
                call.Add_Argument( a, ok );
                pragma Assert( ok, "Can't add argument to Ast_Function_Call" );
            end loop;
            args.Clear;

        else
            -- no Function_Call found
            null;
        end if;

        return A_Ast_Expression(call);
    exception
        when others =>
            for a of args loop
                Delete( A_Ast_Node(a) );
            end loop;
            raise;
    end Scan_Function_Call;

    ----------------------------------------------------------------------------

    --
    -- Membership ::= '.' Identifier
    --
    function Scan_Membership( this : not null access Parser'Class;
                              left : not null A_Ast_Expression ) return A_Ast_Expression is
        result : A_Ast_Expression := null;
        dot    : Token_Ptr;
        token  : Token_Ptr;
    begin
        dot := this.scanner.Accept_Token( TK_DOT );
        if dot.Not_Null then
            token := this.scanner.Expect_Token( TK_SYMBOL );
            result := Create_Membership( dot.Get.Location,
                                         left,
                                         Symbol_Token(token.Get.all).Get_Name );
        end if;
        return result;
    end Scan_Membership;

    ----------------------------------------------------------------------------

    --
    -- Operand<literal=False> ::= Literal<literal> | Identifier_Expression | ('(' Expression<literal> ')')
    -- Operand<literal=True>  ::= Literal<literal> | ('(' Expression<literal> ')')
    --
    function Scan_Operand( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is
        left : A_Ast_Expression;
    begin
        -- accept a literal value
        left := this.Scan_Literal( literal );
        if left = null then
            -- accept an identifier expression
            left := this.Scan_Identifier_Expression;
            if left = null then
                -- accept a parenthesized expression
                if this.scanner.Accept_Token( TK_LPAREN ).Not_Null then
                    -- found: '(', required: Expression ')'
                    left := this.Expect_Expression( literal );
                    this.scanner.Expect_Token( TK_RPAREN );
                else
                    -- no Operand found
                    null;
                end if;
            elsif literal then
                Raise_Parse_Error( "Expression is not literal", left.Location );
            end if;
        end if;

        return left;
    exception
        when others =>
            Delete( A_Ast_Node(left) );
            raise;
    end Scan_Operand;

    ----------------------------------------------------------------------------

    --
    -- Literal<literal=False>  ::= value-token | 'self' | 'this' | List_Literal<literal> | Map_Literal<literal>
    -- Literal<literal=True>   ::= value-token | List_Literal<literal> | Map_Literal<literal>
    --   List_Literal<literal> ::= '[' [Expression_List<literal>] ']'
    --   Map_Literal<literal>  ::= '{' [Map_Pair<literal> {(',' | end-of-line) Map_Pair<literal>}] '}'
    --     Map_Pair<literal>   ::= Named_Function_Definition
    --                             | Named_Member_Definition
    --                             | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal>))
    --       Map_Key           ::= string-value-token | symbol-token
    --
    function Scan_Literal( this : not null access Parser'Class; literal : Boolean ) return A_Ast_Expression is
        token       : Token_Ptr;
        val         : Value;
        literalExpr : A_Ast_Expression;
        key         : A_Ast_Expression;
        valExpr     : A_Ast_Expression;
        elements    : Expression_Vectors.Vector;
    begin
        -- - - - Atomic - - - --
        -- accept a boolean, number, string, or null
        token := this.scanner.Accept_Token( TK_VALUE );
        if token.Not_Null then
            return Create_Literal( token.Get.Location, Value_Token(token.Get.all).Get_Value );
        end if;

        -- - - - self - - - --
        token := this.scanner.Accept_Token( TK_SELF );
        if token.Not_Null then
            if literal then
                Raise_Parse_Error( "Expression is not literal", token.Get.Location );
            end if;
            return Create_Self( token.Get.Location );
        end if;

        -- - - - this - - - --
        token := this.scanner.Accept_Token( TK_THIS );
        if token.Not_Null then
            if literal then
                Raise_Parse_Error( "Expression is not literal", token.Get.Location );
            end if;
            return Create_This( token.Get.Location );
        end if;

        -- - - - List - - - --
        token := this.scanner.Accept_Token( TK_LBRACKET );
        if token.Not_Null then
            literalExpr := Create_List( token.Get.Location );

            -- scan the elements
            elements := this.Scan_Expression_List( literal );
            for e of elements loop
                A_Ast_List(literalExpr).Append( e );
            end loop;
            elements.Clear;

            -- expect closing bracket ']'
            this.scanner.Expect_Token( TK_RBRACKET );

            return literalExpr;
        end if;

        -- - - - Map - - - --
        token := this.scanner.Accept_Token( TK_LBRACE );
        if token.Not_Null then
            literalExpr := Create_Map( token.Get.Location );

            -- scan the elements
            loop
                -- scan the element as a named function definition
                valExpr := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Required, memberOption => Optional ));
                if valExpr /= null then
                    -- specify the key as a literal string value, located where
                    -- the function's name identifier is.
                    key := Create_Literal( A_Ast_Function_Definition(valExpr).Get_Name.Location,
                                           Create( A_Ast_Function_Definition(valExpr).Get_Name.Get_Name ) );

                -- scan the element a key:value pair
                else
                    token := this.scanner.Accept_Token( TK_VALUE );
                    if token.Not_Null then
                        val := Value_Token(token.Get.all).Get_Value;
                        if not val.Is_String then
                            Raise_Parse_Error( "Expected key", token.Get.Location );
                        end if;
                        key := Create_Literal( token.Get.Location, val );
                    else
                        token := this.scanner.Accept_Token( TK_SYMBOL );
                        if token.Not_Null then
                            key := Create_Literal( token.Get.Location, Create( Symbol_Token(token.Get.all).Get_Name ) );
                        elsif A_Ast_Map(literalExpr).Is_Empty then
                            -- map is empty, expect closing brace '}'
                            this.scanner.Expect_Token( TK_RBRACE );
                            exit;
                        else
                            -- the map isn't empty, so the previous token
                            -- was a comma; we expected another key here.
                            Raise_Parse_Error( "Expected key", this.scanner.Location );
                        end if;
                    end if;

                    this.scanner.Expect_Token( TK_COLON );
                    valExpr := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed,
                                                                               nameLoc => key.Location,
                                                                               nameString => Cast_String( A_Ast_Literal(key).Get_Value ),
                                                                               memberOption => Optional
                                                                             ));
                    if valExpr = null then
                        valExpr := this.Expect_Expression( literal );
                    end if;
                end if;

                A_Ast_Map(literalExpr).Insert( key, valExpr );   -- consumes 'key', 'value'

                -- check a for closing brace
                if this.scanner.Accept_Token( TK_RBRACE ).Not_Null then
                    exit;
                end if;

                -- expect a newline or comma between elements
                if this.scanner.Accept_Token( TK_EOL ).Not_Null then
                    token := this.scanner.Accept_Token( TK_COMMA );
                else
                    this.scanner.Expect_Token( TK_COMMA );
                end if;
            end loop;

            return literalExpr;
        end if;

        -- - - - No Literal Found - - - --
        return null;
    exception
        when others =>
            Delete( A_Ast_Node(valExpr) );
            Delete( A_Ast_Node(key) );
            Delete( A_Ast_Node(literalExpr) );
            raise;
    end Scan_Literal;

    ----------------------------------------------------------------------------

    --
    -- Identifier_Expression ::= Identifier ['::' (Identifier | Name_Expression)]
    --   Name_Expression     ::= '[' Expression<literal=False> ']'
    --
    function Scan_Identifier_Expression( this : not null access Parser'Class ) return A_Ast_Expression is

        function Is_Exported( fid : Function_Id ) return Boolean is
            proto : Prototype;
        begin
            this.runtime.Get_Prototype( fid, proto );
            return proto.exported;
        end Is_Exported;

        lToken : Token_Ptr;
        rToken : Token_Ptr;
        name   : A_Ast_Expression;
        result : A_Ast_Expression;
    begin
        lToken := this.scanner.Accept_Token( TK_SYMBOL );
        if lToken.Not_Null then
            -- found an identifier
            if this.scanner.Accept_Token( TK_DOUBLE_COLON ).Not_Null then
                -- found a namespace identifier; is it a static name reference
                -- or a dynamic name reference?
                rToken := this.scanner.Accept_Token( TK_LBRACKET );
                if rToken.Not_Null then
                    -- dynamic name resolution
                    name := this.Expect_Expression( literal => False );
                    this.scanner.Expect_Token( TK_RBRACKET );
                    result := Create_Name_Expr( lToken.Get.Location,
                                                namespace => Symbol_Token(lToken.Get.all).Get_Name,
                                                name      => name );
                    name := null;
                else
                    -- static name resolution
                    rToken := this.scanner.Expect_Token( TK_SYMBOL );
                    result := A_Ast_Expression(
                        Create_Identifier( lToken.Get.Location,
                                           name      => Symbol_Token(rToken.Get.all).Get_Name,
                                           namespace => Symbol_Token(lToken.Get.all).Get_Name ));
                end if;
            elsif Is_Exported( Fid_Hash( Symbol_Token(lToken.Get.all).Get_Name ) ) then
                -- reference to a built-in function exported by the runtime
                result := Create_Builtin( lToken.Get.Location,
                                          Symbol_Token(lToken.Get.all).Get_Name );
            else
                -- local variable/parameter or anonymous namespace reference
                result := A_Ast_Expression(
                    Create_Identifier( lToken.Get.Location,
                                       name => Symbol_Token(lToken.Get.all).Get_Name ));
            end if;
        else
            -- no identifier expression found
            null;
        end if;

        return result;
    exception
        when others =>
            Delete( A_Ast_Node(name) );
            raise;
    end Scan_Identifier_Expression;

    ----------------------------------------------------------------------------

    --
    -- Anon_Function_Definition  ::= 'function' Function_Definition
    -- Anon_Member_Definition    ::= 'member' ['function'] Function_Definition
    -- Named_Function_Definition ::= 'function' Identifier Function_Definition
    -- Named_Member_Definition   ::= 'member' ['function'] Identifier Function_Definition
    --   Function_Definition     ::= Parameters_Prototype Function_Implementation
    --   Parameters_Prototype    ::= '(' [Parameter {',' Parameter}] ')'
    --   Parameter               ::= Identifier [':' Value_Type] [':=' Expression<literal=True>]
    --   Value_Type              ::= 'map' | 'boolean' | 'id' | 'list' | 'number' | 'string'
    --   Function_Implementation ::= (':' Identifier) | (Statement_List 'end' 'function')
    --
    function Scan_Function_Definition( this         : not null access Parser'Class;
                                       nameOption   : Optional_Item;
                                       nameLoc      : Token_Location := (1, 1, 1, others => <>);
                                       nameString   : String := "";
                                       memberOption : Optional_Item := Disallowed ) return A_Ast_Function_Definition is
        isMember  : Boolean := False;
        func      : A_Ast_Function_Definition;
        funcLoc   : Token_Location;
        name      : Token_Ptr;
        token     : Token_Ptr;
        param     : Token_Ptr;
        nameIdent : A_Ast_Identifier;
        default   : A_Ast_Expression;
        expr      : A_Ast_Expression;
        statement : A_Ast_Statement;
        ident     : A_Ast_Identifier;
        block     : A_Ast_Block;
        ok        : Boolean;
    begin
        token := this.scanner.Accept_Token( TK_MEMBER );
        isMember := token.Not_Null;
        if isMember then
            -- 'member' is the location of a member function definition
            funcLoc := token.Get.Location;
            if memberOption = Disallowed then
                Raise_Parse_Error( "Member function definition not allowed", funcLoc );
            end if;
        elsif memberOption = Required then
            -- 'member' keyword not found and it would be required
            return null;
        end if;

        -- for member function definitions, the 'function' keyword is optional
        token := this.scanner.Accept_Token( TK_FUNCTION );

        if isMember or else token.Not_Null then
            if not isMember then
                -- 'function' is the location of a non-member function definition
                funcLoc := token.Get.Location;
            end if;

            name := this.scanner.Accept_Token( TK_SYMBOL );
            if name.Not_Null then
                -- this is a named function

                if nameOption = Disallowed then
                    -- as a function definition, this was required to be an
                    -- anonymous function definition. we detected that this is a
                    -- named function definition being used where one must be
                    -- anonymous. that's a syntax error.
                    Raise_Parse_Error( "Named function definition not allowed", name.Get.Location );
                end if;

                nameIdent := Create_Identifier( name.Get.Location, Symbol_Token(name.Get.all).Get_Name );
            else
                -- this is an anonymous function

                if nameOption = Required then
                    -- if we find a function definition, it was required to be a
                    -- named function definition (ex: as an map key). we
                    -- didn't find a name, so its a syntax error.
                    Raise_Parse_Error( "Expected function name", this.scanner.Location );
                end if;

                if nameString'Length > 0 then
                    nameIdent := Create_Identifier( (line     => nameLoc.line,
                                                     column   => nameLoc.column,
                                                     absolute => nameLoc.absolute,
                                                     path     => this.path),
                                                    nameString );
                end if;
            end if;

            -- already scanned the name (if any), expect to find a '(' now
            this.scanner.Expect_Token( TK_LPAREN );
            func := Create_Function_Definition( funcLoc, nameIdent, isMember );   -- consumes 'nameIdent'
            nameIdent := null;

            func.Set_Path( this.path );

            -- parse the function's parameters
            param := this.scanner.Accept_Token( TK_SYMBOL );
            while param.Not_Null loop
                default := null;

                -- accept the parameter's value type
                if this.scanner.Accept_Token( TK_COLON ).Not_Null then
                    token := this.scanner.Expect_Token( TK_SYMBOL );
                    if Symbol_Token(token.Get.all).Get_Name /= "boolean" and then
                       Symbol_Token(token.Get.all).Get_Name /= "id"      and then
                       Symbol_Token(token.Get.all).Get_Name /= "list"    and then
                       Symbol_Token(token.Get.all).Get_Name /= "map"     and then
                       Symbol_Token(token.Get.all).Get_Name /= "number"  and then
                       Symbol_Token(token.Get.all).Get_Name /= "string"
                    then
                        Raise_Parse_Error( "Invalid type", token.Get.Location );
                    end if;
                end if;

                -- accept the parameter's default value expression
                if this.scanner.Accept_Token( TK_COLON_EQUALS ).Not_Null then
                    default := this.Expect_Expression( literal => True );

                    -- prune the value expression to accept literal lists and
                    -- maps
                    expr := default.Prune;
                    if expr /= null then
                        Delete( A_Ast_Node(default) );
                        default := expr;
                        expr := null;
                    end if;
                end if;

                ident := Create_Identifier( param.Get.Location, Symbol_Token(param.Get.all).Get_Name );
                func.Add_Parameter( ident, default, ok );
                if not ok then
                    -- too many parameters defined in the function definition
                    Delete( A_Ast_Node(ident) );
                    Delete( A_Ast_Node(default) );
                    Raise_Parse_Error( "Too many parameters", param.Get.Location );
                end if;

                if this.scanner.Accept_Token( TK_COMMA ).Is_Null then
                    -- no more parameter names follow
                    exit;
                end if;

                param := this.scanner.Accept_Token( TK_SYMBOL );
            end loop;

            this.scanner.Expect_Token( TK_RPAREN );

            -- parse the implementation
            if this.scanner.Accept_Token( TK_COLON ).Not_Null then
                -- name of internal implementation method
                token := this.scanner.Expect_Token( TK_SYMBOL );
                func.Set_Internal_Name( Symbol_Token(token.Get.all).Get_Name );

                -- give it an empty body just to contain the parameter definitions needed by the
                -- semantic analyzer later.
                block := Create_Block( UNKNOWN_LOCATION, null );
                func.Set_Body( block );

                -- store the function's source code
                func.Set_Source( Unbounded_Slice( this.scanner.Get_Source,
                                                  func.Location.absolute,
                                                  token.Get.Location.absolute + func.Get_Internal_Name'Length - 1 ),
                                 (func.Location.line - 1, func.Location.column - 1, func.Location.absolute - 1, func.Location.path) );
            else
                -- Scribble body
                block := this.Scan_Statement_List( null );

                -- expect the end of the function
                this.scanner.Expect_Token( TK_END );
                token := this.scanner.Expect_Token( TK_FUNCTION );

                -- always return Null in case the author forgot a return statement
                expr := Create_Literal( UNKNOWN_LOCATION, Null_Value );
                statement := Create_Return( expr );
                statement.Set_Location( token.Get.Location );
                block.Append( statement );
                func.Set_Body( block );

                -- store the function's source code
                func.Set_Source( Unbounded_Slice( this.scanner.Get_Source,
                                                  func.Location.absolute,
                                                  token.Get.Location.absolute + Image( token.Get.Get_Type )'Length - 1 ),
                                 (func.Location.line - 1, func.Location.column - 1, func.Location.absolute - 1, func.Location.path) );
            end if;
        end if;

        return func;
    exception
        when others =>
            Delete( A_Ast_Node(func) );
            Delete( A_Ast_Node(block) );
            Delete( A_Ast_Node(nameIdent) );
            raise;
    end Scan_Function_Definition;

    ----------------------------------------------------------------------------

    --
    -- Statement ::= {';'} (
    --               Block
    --               | Exit
    --               | For
    --               | If
    --               | Loop
    --               | Return
    --               | Var
    --               | While
    --               | Yield
    --               | Expression_Statement
    --               ) {';'}
    --
    function Scan_Statement( this   : not null access Parser'Class;
                             parent : A_Ast_Block ) return A_Ast_Statement is
        statement : A_Ast_Statement;
    begin
        -- skip any empty statements preceeding the next real one
        while this.scanner.Accept_Token( TK_SEMICOLON ).Not_Null loop
            null;
        end loop;

        statement := this.Scan_Block( parent );
        if statement = null then
            statement := this.Scan_Exit( parent );
        end if;
        if statement = null then
            statement := this.Scan_For( parent );
        end if;
        if statement = null then
            statement := this.Scan_If( parent );
        end if;
        if statement = null then
            statement := A_Ast_Statement(this.Scan_Loop( parent ));
        end if;
        if statement = null then
            statement := this.Scan_Return;
        end if;
        if statement = null then
            statement := this.Scan_Var;
        end if;
        if statement = null then
            statement := this.Scan_While( parent );
        end if;
        if statement = null then
            statement := this.Scan_Yield;
        end if;
        if statement = null then
            statement := this.Scan_Expression_Statement;
        end if;

        if statement /= null then
            -- skip any empty statements following the one that was parsed
            while this.scanner.Accept_Token( TK_SEMICOLON ).Not_Null loop
                null;
            end loop;
        end if;

        return statement;
    end Scan_Statement;

    ----------------------------------------------------------------------------

    --
    -- Block ::= 'block' Statement_List 'end' 'block' ';'
    --
    function Scan_Block( this   : not null access Parser'Class;
                         parent : A_Ast_Block ) return A_Ast_Statement is
        block : A_Ast_Block;
        token : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_BLOCK );
        if token.Not_Null then
            block := this.Scan_Statement_List( parent );

            if this.scanner.Accept_Token( TK_END ).Is_Null then
                -- for clearer error reporting:
                -- we found something following 0 or more statements that should have been either
                -- another statement, or an 'end' token. we'll assume that 'end' wasn't what was
                -- meant, since that's not what was written, and report we expected it to have
                -- been another valid statement.
                Raise_Parse_Error( "Expected statement", this.scanner.Location );
            end if;
            this.scanner.Expect_Token( TK_BLOCK );
            this.scanner.Expect_Token( TK_SEMICOLON );
        else
            -- no Block found
            null;
        end if;

        return A_Ast_Statement(block);
    exception
        when others =>
            Delete( A_Ast_Node(block) );
            raise;
    end Scan_Block;

    ----------------------------------------------------------------------------

    --
    -- Exit ::= 'exit' ['when' Expression<literal=False>] ';'
    --
    function Scan_Exit( this   : not null access Parser'Class;
                        parent : A_Ast_Block ) return A_Ast_Statement is
        result      : A_Ast_Statement;
        token       : Token_Ptr;
        condition   : A_Ast_Expression;
        block       : A_Ast_Block;
        ifStatement : A_Ast_If;
    begin
        token := this.scanner.Accept_Token( TK_EXIT );
        if token.Not_Null then
            -- found an Exit statement
            result := Create_Exit( token.Get.Location );

            -- check for the 'when' clause
            token := this.scanner.Accept_Token( TK_WHEN );
            if token.Not_Null then
                condition := this.Expect_Expression( literal => False );

                -- create an Ast_Block to wrap the Ast_Exit
                block := Create_Block( result.Location, parent );
                block.Append( result );

                -- create an Ast_If to wrap the condition and Ast_Block
                ifStatement := Create_If( token.Get.Location );
                ifStatement.Add_Case( condition, block );

                result := A_Ast_Statement(ifStatement);
            end if;

            -- expect the ending semicolon
            this.scanner.Expect_Token( TK_SEMICOLON );
        else
            -- no Exit found
            null;
        end if;

        return result;
    exception
        when others =>
            Delete( A_Ast_Node(result) );
            raise;
    end Scan_Exit;

    ----------------------------------------------------------------------------

    --
    -- Expression_Statement ::= Expression<literal=False> ';'
    --
    function Scan_Expression_Statement( this : not null access Parser'Class ) return A_Ast_Statement is
        statement : A_Ast_Statement;
        expr      : A_Ast_Expression;
    begin
        expr := this.Scan_Expression( literal => False );
        if expr /= null then
            this.scanner.Expect_Token( TK_SEMICOLON );
            statement := Create_Expression_Statement( expr );
        else
            -- no Expression_Statement found
            null;
        end if;

        return statement;
    exception
        when others =>
            Delete( A_Ast_Node(expr) );
            Delete( A_Ast_Node(statement) );
            raise;
    end Scan_Expression_Statement;

    ----------------------------------------------------------------------------

    --
    -- For         ::= 'for' Identifier (For_Range | For_Each) Loop
    --   For_Range ::= 'is' Expression<literal=False> 'to' Expression<literal=False> ['step' Expression<literal=False>]
    --   For_Each  ::= 'of' Expression<literal=False>
    --
    -- The for statement does not have a direct representation in the AST class
    -- hierarchy. The translations below demonstrate how the parser builds the
    -- language constructs from simpler elements.
    function Scan_For( this   : not null access Parser'Class;
                       parent : A_Ast_Block ) return A_Ast_Statement is

        ------------------------------------------------------------------------

        -- for ELEMENT of CONTAINER loop
        --     STATEMENTS
        -- end loop;
        --
        -- ... is constructed as:
        --
        -- block
        --     local $c := CONTAINER;
        --     local $i := 1;
        --     local $keys := isMap($c) ? keys($c) : null;
        --     local $k;
        --     local ELEMENT;
        --     loop
        --         if isList($keys) then
        --             if $i > length($keys) then
        --                 exit;
        --             end if;
        --             $k := $keys[$i];
        --         else
        --             if !isList($c) or $i > length($c) then
        --                 exit;
        --             end if;
        --             $k := $i;
        --         end if;
        --         ELEMENT := $c[$k];
        --         block
        --             STATEMENTS
        --         end block;
        --         $i += 1;
        --     end loop;
        -- end block;
        function Expect_For_Each( loc     : Token_Location;
                                  element : Token_Ptr ) return A_Ast_Statement is
            container     : A_Ast_Expression;
            outer         : A_Ast_Block;
            looop         : A_Ast_Loop;

            statement     : A_Ast_Statement;
            typeTest      : A_Ast_If;
            expr, expr2   : A_Ast_Expression;
            typeTestBlock : A_Ast_Block;
            indexTest     : A_Ast_If;
            exitBlock     : A_Ast_Block;
        begin
            container := this.Expect_Expression( literal => False );    -- Expression

            -- create an outer scope block to contain the variable declarations and loop
            outer := Create_Block( loc, parent );

            looop := this.Scan_Loop( outer );                           -- Loop (nested in 'outer')
            if looop = null then
                -- this will throw a Parse_Error expecting 'loop' because if
                -- there actually was a TK_LOOP token in the stream, then
                -- Scan_Loop would not have returned null. this is here to
                -- create a more helpful error message.
                this.scanner.Expect_Token( TK_LOOP );
            end if;

            -- Construct the For statement block
            -- no exceptions are raised beyond this point

            --
            -- AST Structure                        Class                               C++ var
            -- -------------                        -----                               -------
            -- block                                Ast_Block                           outer
            --     local $c := @CONTAINER;          Ast_Var("$c, CONTAINER)
            --     local $i := 1;                   Ast_Var("$i", 1)
            --     local $keys := isMap($c)
            --                    ? keys($c)
            --                    : null;           Ast_Var("$keys", Ast_Conditional)
            --     local $k := null;                Ast_Var("$k", null)
            --     loop                             Ast_Loop                            looop
            --         block                                                            looop.Get_Body
            --             local ELEMENT := null;
            --             if                       Ast_If(...)                         typeTest
            --                 isList($keys)                                            expr
            --             then
            --                 block                Ast_Block                           typeMapBlock
            --                     if               Ast_If(...)                         indexTest
            --                         $i > length($keys)                               expr2
            --                     then
            --                         block        Ast_Block                           exitBlock
            --                             exit;    Ast_Exit
            --                         end block;
            --                     end if;
            --                     $k := $keys[$i]  Ast_Expression(Ast_Assign(:=, $k, ...))
            --                 end block;
            --             else
            --                 block                                                    typeMapBlock
            --                     if               Ast_If(...)                         indexTest
            --                         !isList($c) or $i > length($c)                   expr
            --                     then
            --                         block        Ast_Block                           exitBlock
            --                             exit;    Ast_Exit
            --                         end block;
            --                     end if;
            --                     $k := $i;        Ast_Expression(Ast_Assign(:=, $k, $i))
            --                 end block;
            --             end if;
            --             ELEMENT := @$c[$k];      Ast_Var(ELEMENT, Ast_Ref)
            --             block                    Ast_Block (result of double nesting)
            --                 STATEMENTS           (parsed from input)
            --             end block;
            --             $i += 1;                 Ast_Expression(Ast_Assign(+=, $i, 1))
            --         end block;
            --     end loop;
            -- end block;
            --

            -- constant $c := (@)CONTAINER;
            if container.all in Ast_Symbol'Class or else container.Get_Type = INDEX_OP then
                -- if the container expression is referencable (not a value
                -- evaluated on the stack) then take a reference to it instead,
                -- so its contents can be modified. if the container expression
                -- is a literal, then no reference will be taken and $c will be
                -- a copy that can be modified but then discarded.
                container := A_Ast_Expression(Create_Reference( container.Location, container, knownValid => True ));
            end if;
            statement := Create_Var(
                             looop.Location,
                             Create_Identifier( container.Location, "$c" ),
                             container,
                             const => True
                         );
            outer.Append( statement );

            -- local $i := 1;
            statement := Create_Var(
                             looop.Location,
                             Create_Identifier( looop.Location, "$i" ),
                             Create_Literal( looop.Location, Create( 1 ) )
                         );
            outer.Append( statement );

            -- constant $keys := isMap($c) ? keys($c) : null;
            statement := Create_Var(
                             looop.Location,
                             Create_Identifier( looop.Location, "$keys" ),
                             Create_Conditional( looop.Location,
                                 A_Ast_Expression(Create_Function_Call(
                                     looop.Location,
                                     Create_Builtin( looop.Location, FUNC_ISMAP ),
                                     A_Ast_Expression(Create_Identifier( looop.Location, "$c" ))
                                 )),
                                 A_Ast_Expression(Create_Function_Call(
                                     looop.Location,
                                     Create_Builtin( looop.Location, FUNC_KEYS ),
                                     A_Ast_Expression(Create_Identifier( looop.Location, "$c" ))
                                 )),
                                 Create_Literal( looop.Location, Create( 1 ) )
                             ),
                             const => True
                         );
            outer.Append( statement );

            -- local $k := null;
            statement := Create_Var(
                             looop.Location,
                             Create_Identifier( looop.Location, "$k" ),
                             Create_Literal( looop.Location, Null_Value )
                         );
            outer.Append( statement );

                -- doubly nest the statements in 'loop' inside another block. this
                -- allows an Ast_If to be inserted at the top of the loop without
                -- disallowing any subsequent Ast_Var statements that may have been
                -- defined at the top of the for loop. this also allows the loop
                -- counter to be properly incremented at the end of the loop,
                -- outside of the for's statement block.
                looop.Double_Nest;

                -- ELEMENT := @$c[$k];
                statement := Create_Expression_Statement(
                                 Create_Assign(
                                     ASSIGN_DIRECT,
                                     looop.Location,
                                     A_Ast_Expression(Create_Identifier( looop.Location, Symbol_Token(element.Get.all).Get_Name )),
                                     Create_Reference(
                                         looop.Location,
                                         Create_Index(
                                             looop.Location,
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$c" )),
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$k" ))
                                         ),
                                         knownValid => True
                                     )
                                 )
                             );
                -- prepend the container element assignment before the loop test
                -- first, so that it happens after the exit tests.
                looop.Get_Body.Prepend( statement );

                -- if ...
                typeTest := Create_If( looop.Location );

                    -- isList($keys)
                    expr := A_Ast_Expression(Create_Function_Call(
                                looop.Location,
                                Create_Builtin( looop.Location, FUNC_ISLIST ),
                                A_Ast_Expression(Create_Identifier( looop.Location, "$keys" ))
                            ));

                    -- then
                    typeTestBlock := Create_Block( looop.Location, looop.Get_Body );

                        -- if ...
                        indexTest := Create_If( looop.Location );

                            -- $i > length($keys)
                            expr2 := Create_Binary_Op(
                                         BINARY_GT,
                                         looop.Location,
                                         A_Ast_Expression(Create_Identifier( looop.Location, "$i" )),
                                         A_Ast_Expression(Create_Function_Call(
                                             looop.Location,
                                             Create_Builtin( looop.Location, FUNC_LENGTH ),
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$keys" )) ))
                                     );

                            -- then
                            exitBlock := Create_Block( looop.Location, typeTestBlock );
                            statement := Create_Exit( looop.Location );
                            exitBlock.Append( statement );

                        indexTest.Add_Case( expr2, exitBlock );
                        typeTestBlock.Append( A_Ast_Statement(indexTest) );

                        -- $k := $keys[$i];
                        statement := Create_Expression_Statement(
                                         Create_Assign(
                                             ASSIGN_DIRECT,
                                             looop.Location,
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$k" )),
                                             Create_Index(
                                                 looop.Location,
                                                 A_Ast_Expression(Create_Identifier( looop.Location, "$keys" )),
                                                 A_Ast_Expression(Create_Identifier( looop.Location, "$i" ))
                                             )
                                         )
                                     );
                        typeTestBlock.Append( statement );

                    typeTest.Add_Case( expr, typeTestBlock );

                    -- else
                    typeTestBlock := Create_Block( looop.Location, looop.Get_Body );

                        -- if ...
                        indexTest := Create_If( looop.Location );

                            -- !isList($c) or $i > length($c)
                            expr := Create_Binary_Op(
                                BINARY_OR,
                                looop.Location,
                                Create_Unary_Op(
                                    UNARY_NOT,
                                    looop.Location,
                                    A_Ast_Expression(Create_Function_Call(
                                        looop.Location,
                                        Create_Builtin( looop.Location, FUNC_ISLIST ),
                                        A_Ast_Expression(Create_Identifier( looop.Location, "$c" ))
                                    ))
                                ),
                                Create_Binary_Op(
                                    BINARY_GT,
                                    looop.Location,
                                    A_Ast_Expression(Create_Identifier( looop.Location, "$i" )),
                                    A_Ast_Expression(Create_Function_Call(
                                        looop.Location,
                                        Create_Builtin( looop.Location, FUNC_LENGTH ),
                                        A_Ast_Expression(Create_Identifier( looop.Location, "$c" ))
                                    ))
                                )
                            );

                            -- then
                            exitBlock := Create_Block( looop.Location, typeTestBlock );
                            statement := Create_Exit( looop.Location );
                            exitBlock.Append( statement );

                        indexTest.Add_Case( expr, exitBlock );
                        typeTestBlock.Append( A_Ast_Statement(indexTest) );

                        -- $k := $i;
                        statement := Create_Expression_Statement(
                                         Create_Assign(
                                             ASSIGN_DIRECT,
                                             looop.Location,
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$k" )),
                                             A_Ast_Expression(Create_Identifier( looop.Location, "$i" ))
                                         )
                                     );
                        typeTestBlock.Append( statement );

                    typeTest.Set_Else_Block( typeTestBlock );

                -- prepend the exit tests to the beginning of the loop
                looop.Get_Body.Prepend( A_Ast_Statement(typeTest) );

                -- local ELEMENT := null;
                statement := Create_Var(
                                 looop.Location,
                                 Create_Identifier( looop.Location, Symbol_Token(element.Get.all).Get_Name ),
                                 Create_Literal( looop.Location, Null_Value )
                             );
                -- prepend the ELEMENT variable declaration last, so that it
                -- happens ahead of the exit tests.
                looop.Get_Body.Prepend( statement );

                -- increment the index at the end of the loop
                -- $i += 1;
                statement := Create_Expression_Statement(
                                 Create_Assign( ASSIGN_ADD,
                                                looop.Location,
                                                A_Ast_Expression(Create_Identifier( looop.Location, "$i" )),
                                                Create_Literal( looop.Location, Create( 1 ) )
                                 )
                             );
                looop.Get_Body.Append( statement );

            -- nest the loop in the outer block after the variable declarations
            outer.Append( A_Ast_Statement(looop) );

            return A_Ast_Statement(outer);
        exception
            when others =>
                Delete( A_Ast_Node(looop) );
                Delete( A_Ast_Node(outer) );
                Delete( A_Ast_Node(container) );
                raise;
        end Expect_For_Each;

        ------------------------------------------------------------------------

        -- for COUNTER is FIRST to LAST step STEP loop
        --     STATEMENTS
        -- end loop;
        --
        -- ... is constructed as:
        --
        -- block
        --     local COUNTER := FIRST;
        --     local $last := LAST;
        --     local $step := STEP;
        --     loop
        --         if (COUNTER - $last) * sign($step) > 0 then
        --             exit;
        --         end if;
        --         block
        --             STATEMENTS
        --         end block;
        --         COUNTER += $step;
        --     end loop;
        -- end block;
        --
        function Expect_For_Range( loc     : Token_Location;
                                   counter : Token_Ptr ) return A_Ast_Statement is
            first     : A_Ast_Expression;
            last      : A_Ast_Expression;
            step      : A_Ast_Expression;
            outer     : A_Ast_Block;
            looop     : A_Ast_Loop;

            ident     : A_Ast_Identifier;
            statement : A_Ast_Statement;
            testBlock : A_Ast_Block;
            test      : A_Ast_If;
            expr      : A_Ast_Expression;
        begin
            first := this.Expect_Expression( literal => False );        -- Expression
            this.scanner.Expect_Token( TK_TO );                         -- 'to'
            last := this.Expect_Expression( literal => False );         -- Expression

            if this.scanner.Accept_Token( TK_STEP ).Not_Null then       -- 'step'
                step := this.Expect_Expression( literal => False );     -- Expression
            else
                -- default step increment is 1
                -- the lexical location of the default expression is declared
                -- as the 'loop' token that will be scanned in next.
                step := Create_Literal( this.scanner.Location, Create( 1 ) );
            end if;

            -- create an outer scope block to contain the counter declaration and loop
            outer := Create_Block( loc, parent );

            looop := this.Scan_Loop( outer );                           -- Loop (nested in 'outer')
            if looop = null then
                -- this will throw a Parse_Error expecting 'loop' because if
                -- there actually was a TK_LOOP token in the stream, then
                -- Scan_Loop would not have returned null. this is here to
                -- create a more helpful error message.
                this.scanner.Expect_Token( TK_LOOP );
            end if;

            -- Construct the For statement block
            -- no exceptions are raised beyond this point

            --
            -- AST Structure                        Class                               C++ var
            -- -------------                        -----                               -------
            -- block                                Ast_Block                           outer
            --     local COUNTER := FIRST;          Ast_Var(COUNTER, FIRST)
            --     local $last := LAST;             Ast_Var("$last", LAST)
            --     local $step := STEP;             Ast_Var("$step", STEP)
            --     loop                             Ast_Loop                            looop
            --         block                                                            looop.Get_Body
            --             if                       Ast_If(...)                         test
            --                 (COUNTER - $last) * sign($step) > 0
            --             then
            --                 block                Ast_Block                           testBlock
            --                     exit;            Ast_Exit
            --                 end block;
            --             end if;
            --             block                    Ast_Block (result of double nesting)
            --                 STATEMENTS           (parsed from input)
            --             end block;
            --             COUNTER += $step;        Ast_Expression(Ast_Assign(+=, COUNTER, $step))
            --         end block;
            --     end loop;
            -- end block;
            --

            -- create and add the counter variable declaration to the outer block
            ident := Create_Identifier( counter.Get.Location, Symbol_Token(counter.Get.all).Get_Name );
            statement := Create_Var( looop.Location, ident, first );                    -- consumes 'ident', 'first'
            outer.Append( statement );

            -- create and add the last value declaration to the outer block
            ident := Create_Identifier( last.Location, "$last" );
            statement := Create_Var( looop.Location, ident, last );                     -- consumes 'ident', 'last'
            outer.Append( statement );

            -- create and add the step value declaration to the outer block
            ident := Create_Identifier( step.Location, "$step" );
            statement := Create_Var( looop.Location, ident, step );                     -- consumes 'ident', 'step'
            outer.Append( statement );

                -- doubly nest the statements in 'loop' inside another block. this
                -- allows an Ast_If to be inserted at the top of the loop without
                -- disallowing any subsequent Ast_Var statements that may have been
                -- defined at the top of the for loop. this also allows the loop
                -- counter to be properly incremented at the end of the loop,
                -- outside of the for's statement block.
                looop.Double_Nest;

                -- create an Ast_If to test the counter at the beginning of the loop
                test := Create_If( looop.Location );

                    -- (COUNTER - $last) * sign($step) > 0
                    expr := Create_Binary_Op(
                            BINARY_GT, looop.Location,
                            Create_Binary_Op(
                                    BINARY_MUL, looop.Location,
                                    Create_Binary_Op(
                                            BINARY_SUB, looop.Location,
                                            A_Ast_Expression(Create_Identifier( looop.Location, Symbol_Token(counter.Get.all).Get_Name )),
                                            A_Ast_Expression(Create_Identifier( looop.Location, "$last" ))
                                    ),
                                    Create_Unary_Op( UNARY_SIGN, looop.Location, A_Ast_Expression(Create_Identifier( looop.Location, "$step" )) )
                            ),
                            Create_Literal( looop.Location, Create( 0 ) )
                    );

                    -- create a block that exits, to be nested in the test condition's True case
                    testBlock := Create_Block( looop.Location, looop.Get_Body );
                    statement := Create_Exit( looop.Location );
                    testBlock.Append( statement );                                                          -- consumes 'statement'

                test.Add_Case( expr, testBlock );                                                           -- consumes 'expr' and 'testBlock'

                -- prepend the counter test to the beginning of the loop
                looop.Get_Body.Prepend( A_Ast_Statement(test) );                                            -- consumes 'test'

                -- create and append a step increment operation to the end of the loop
                ident := Create_Identifier( looop.Location, Symbol_Token(counter.Get.all).Get_Name );
                step := A_Ast_Expression(Create_Identifier( looop.Location, "$step" ));
                expr := Create_Assign( ASSIGN_ADD, looop.Location, A_Ast_Expression(ident), step );         -- consumes 'ident', 'step'
                statement := Create_Expression_Statement( expr );                                           -- consumes 'expr'
                looop.Get_Body.Append( statement );                                                         -- consumes 'statement'

            -- nest the loop in the outer block after the counter declaration
            outer.Append( A_Ast_Statement(looop) );                                                         -- consumes 'looop'

            return A_Ast_Statement(outer);
        exception
            when others =>
                Delete( A_Ast_Node(looop) );
                Delete( A_Ast_Node(outer) );
                Delete( A_Ast_Node(step) );
                Delete( A_Ast_Node(last) );
                Delete( A_Ast_Node(first) );
                raise;
        end Expect_For_Range;

        ------------------------------------------------------------------------

        token   : Token_Ptr;
        element : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_FOR );
        if token.Not_Null then
            -- For loop found, determine the type
            element := this.scanner.Expect_Token( TK_SYMBOL );          -- Identifier
            if this.scanner.Accept_Token( TK_OF ).Not_Null then         -- 'of'
                return Expect_For_Each( token.Get.Location, element );
            else
                this.scanner.Expect_Token( TK_IS );                     -- 'is'
                return Expect_For_Range( token.Get.Location, element );
            end if;
        else
            -- no For found
            return null;
        end if;
    end Scan_For;

    ----------------------------------------------------------------------------

    --
    -- If ::= 'if' Expression<literal=False> 'then' Statement_List
    --        {'elsif' Expression<literal=False> 'then' Statement_List}
    --        ['else' Statement_List]
    --        'end' 'if' ';'
    --
    function Scan_If( this   : not null access Parser'Class;
                      parent : A_Ast_Block ) return A_Ast_Statement is
        statement : A_Ast_If;
        condition : A_Ast_Expression;
        block     : A_Ast_Block;
        token     : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_IF );
        if token.Not_Null then
            statement := Create_If( token.Get.Location );

            -- expect the condition and block to execute on True
            condition := this.Expect_Expression( literal => False );
            this.scanner.Expect_Token( TK_THEN );
            block := this.Scan_Statement_List( parent );
            block.Set_Location( token.Get.Location );

            -- add the first case to the if statement; the remaining cases are else-ifs
            statement.Add_Case( condition, block );

            -- add any optional else-if cases to the statement; max 255 + 1 (else)
            token := this.scanner.Accept_Token( TK_ELSIF );
            while token.Not_Null loop
                condition := this.Expect_Expression( literal => False );
                this.scanner.Expect_Token( TK_THEN );
                block := this.Scan_Statement_List( parent );
                block.Set_Location( token.Get.Location );
                statement.Add_Case( condition, block );
                token := this.scanner.Accept_Token( TK_ELSIF );
            end loop;

            -- add an optional else case to the statement
            token := this.scanner.Accept_Token( TK_ELSE );
            if token.Not_Null then
                block := this.Scan_Statement_List( parent );
                block.Set_Location( token.Get.Location );
                statement.Set_Else_Block( block );
            end if;

            this.scanner.Expect_Token( TK_END );
            this.scanner.Expect_Token( TK_IF );
            this.scanner.Expect_Token( TK_SEMICOLON );
        else
            -- no If statement found
            null;
        end if;

        return A_Ast_Statement(statement);
    exception
        when others =>
            Delete( A_Ast_Node(statement) );
            Delete( A_Ast_Node(condition) );
            Delete( A_Ast_Node(block) );
            raise;
    end Scan_If;

    ----------------------------------------------------------------------------

    --
    -- Loop ::= 'loop' Statement_List 'end' 'loop' ';'
    --
    function Scan_Loop( this   : not null access Parser'Class;
                        parent : A_Ast_Block ) return A_Ast_Loop is
        looop : A_Ast_Loop;
        block : A_Ast_Block;
        token : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_LOOP );
        if token.Not_Null then
            block := this.Scan_Statement_List( parent );
            looop := Create_Loop( token.Get.Location, block );

            if this.scanner.Accept_Token( TK_END ).Is_Null then
                -- for clearer error reporting:
                -- we found something following 0 or more statements that should
                -- have been either another statement, or an 'end' token. we'll
                -- assume that 'end' wasn't what was meant, since that's not
                -- what was written, and report we expected it to have been
                -- another valid statement.
                Raise_Parse_Error( "Expected statement", this.scanner.Location );
            end if;
            this.scanner.Expect_Token( TK_LOOP );
            this.scanner.Expect_Token( TK_SEMICOLON );
        else
            -- no Loop found
            null;
        end if;

        return looop;
    exception
        when others =>
            Delete( A_Ast_Node(looop) );
            raise;
    end Scan_Loop;

    ----------------------------------------------------------------------------

    --
    -- Return ::= 'return' [Expression<literal=False> | Anon_Function_Definition] ';'
    --
    function Scan_Return( this : not null access Parser'Class ) return A_Ast_Statement is
        token : Token_Ptr;
        ret   : A_Ast_Statement;
        expr  : A_Ast_Expression;
    begin
        token := this.scanner.Accept_Token( TK_RETURN );
        if token.Not_Null then
            expr := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed ));
            if expr = null then
                expr := this.Scan_Expression( literal => False );
                if expr = null then
                    expr := Create_Literal( token.Get.Location, Null_Value );
                end if;
            end if;
            this.scanner.Expect_Token( TK_SEMICOLON );

            ret := Create_Return( expr );
        else
           -- no Return found
           null;
        end if;

        return ret;
    exception
        when others =>
            Delete( A_Ast_Node(expr) );
            raise;
    end Scan_Return;

    ----------------------------------------------------------------------------

    --
    -- Var ::= 'local' Identifier [':=' (Expression<literal=False> | Anon_Function_Definition)] ';'
    -- Var ::= 'constant' Identifier ':=' (Expression<literal=False> | Anon_Function_Definition) ';'
    -- Var ::= Named_Function_Definition ';'
    --
    function Scan_Var( this : not null access Parser'Class ) return A_Ast_Statement is
        token        : Token_Ptr;
        varLoc       : Token_Location;
        name         : Token_Ptr;
        assign       : Token_Ptr;
        var          : A_Ast_Statement;
        expr         : A_Ast_Expression;
        ident        : A_Ast_Identifier;
        namedFuncDef : A_Ast_Function_Definition;
    begin
        token := this.scanner.Accept_Token( TK_LOCAL );
        if token.Is_Null then
            token := this.scanner.Accept_Token( TK_CONSTANT );
        end if;
        if token.Not_Null then
            varLoc := token.Get.Location;
            name := this.scanner.Expect_Token( TK_SYMBOL );
            if token.Get.Get_Type = TK_CONSTANT then
                -- constants require an initializer
                assign := this.scanner.Expect_Token( TK_COLON_EQUALS );
            else
                -- variables allow an initializer
                assign := this.scanner.Accept_Token( TK_COLON_EQUALS );
            end if;
            if assign.Not_Null then
                -- expect initializer: Anon_Function_Definition or Expression
                expr := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed,
                                                                        nameLoc => name.Get.Location,
                                                                        nameString => Symbol_Token(name.Get.all).Get_Name
                                                                      ));
                if expr = null then
                    expr := this.Expect_Expression( literal => False );
                end if;
            else
                -- initialize with Null if no expression is given
                expr := Create_Literal( name.Get.Location, Null_Value );
            end if;
            varLoc := this.scanner.Expect_Token( TK_SEMICOLON ).Get.Location;

            ident := Create_Identifier( name.Get.Location, Symbol_Token(name.Get.all).Get_Name );
            var := Create_Var( varLoc, ident, expr, const => token.Get.Get_Type = TK_CONSTANT );
        else
            namedFuncDef := this.Scan_Function_Definition( nameOption => Required );
            if namedFuncDef /= null then
                -- create a constant local variable declaration statement from
                -- a named function written as a statement.
                varLoc := this.scanner.Expect_Token( TK_SEMICOLON ).Get.Location;
                var := Create_Var( varLoc, namedFuncDef.Get_Name, A_Ast_Expression(namedFuncDef), const => True );
            else
                -- no Var statement
                null;
            end if;
        end if;

        return var;
    exception
        when others =>
            Delete( A_Ast_Node(expr) );
            Delete( A_Ast_Node(namedFuncDef) );
            raise;
    end Scan_Var;

    ----------------------------------------------------------------------------

    --
    -- While ::= 'while' Expression<literal=False> Loop
    --
    -- The while statement does not have a direct representation in the AST class
    -- hierarchy. The following translation demonstrates how the parser builds
    -- the language construct from simpler elements.
    --
    -- while CONDITION loop
    --     STATEMENTS
    -- end loop;
    --
    -- ... is constructed as:
    --
    -- loop
    --     if !(CONDITION) then
    --        exit;
    --     end if;
    --     block
    --         STATEMENTS
    --     end block;
    -- end loop;
    --
    function Scan_While( this   : not null access Parser'Class;
                         parent : A_Ast_Block ) return A_Ast_Statement is
        condition : A_Ast_Expression;
        looop     : A_Ast_Loop;

        testBlock : A_Ast_Block;
        test      : A_Ast_If;
        statement : A_Ast_Statement;
    begin
        declare
            token : Token_Ptr;
        begin
            token := this.scanner.Accept_Token( TK_WHILE );
            if token.Not_Null then
                -- Parse the rest of the While statement block
                condition := this.Expect_Expression( literal => False );
                looop := this.Scan_Loop( parent );
                if looop = null then
                    -- this will throw a Parse_Error expecting 'loop' because if
                    -- there actually was a TK_LOOP token in the stream,
                    -- Scan_Loop would not have returned null.
                    this.scanner.Expect_Token( TK_LOOP );
                end if;
            else
                -- no While found
                return null;
            end if;
        exception
            when others =>
                Delete( A_Ast_Node(looop) );
                Delete( A_Ast_Node(condition) );
                raise;
        end;

        -- Construct the While statement block
        -- no exceptions are thrown beyond this point

        --
        -- AST Structure                        Class                               C++ var
        -- -------------                        -----                               -------
        -- loop                                 Ast_Loop                            looop
        --     block                                                                looop.Get_Body
        --         if !(CONDITION) then         Ast_If(Ast_Not(condition))          test
        --             block                    Ast_Block                           testBlock
        --                 exit                 Ast_Exit
        --             end
        --         end
        --         block                        Ast_Block (result of double nesting)
        --             STATEMENTS               (parsed from input)
        --         end
        --     end
        -- end
        --

        -- doubly nest the statements in 'loop' inside another block. this
        -- allows an Ast_If to be inserted at the top of the loop without
        -- disallowing any subsequent Ast_Var statements that may have been
        -- defined at the top of the for loop.
        looop.Double_Nest;

        -- create a block that exits, to be nested in the condition test in the
        -- loop
        testBlock := Create_Block( UNKNOWN_LOCATION, looop.Get_Body );
        statement := Create_Exit( UNKNOWN_LOCATION );
        testBlock.Append( statement );

        -- create an Ast_If to test the condition at the beginning of the loop
        test := Create_If( UNKNOWN_LOCATION );
        condition := Create_Unary_Op( UNARY_NOT, UNKNOWN_LOCATION, condition );
        test.Add_Case( condition, testBlock );

        -- prepend the condition test to the beginning of the loop
        looop.Get_Body.Prepend( A_Ast_Statement(test) );

        return A_Ast_Statement(looop);
    end Scan_While;

    ----------------------------------------------------------------------------

    --
    -- Yield ::= 'yield' ';'
    --
    function Scan_Yield( this : not null access Parser'Class ) return A_Ast_Statement is
        result : A_Ast_Statement;
        token  : Token_Ptr;
    begin
        token := this.scanner.Accept_Token( TK_YIELD );
        if token.Not_Null then
            this.scanner.Expect_Token( TK_SEMICOLON );
            result := Create_Yield( token.Get.Location );
        end if;
        return result;
    end Scan_Yield;

    ----------------------------------------------------------------------------

    --
    -- Statement_List ::= {Statement}
    --
    function Scan_Statement_List( this   : not null access Parser'Class;
                                  parent : A_Ast_Block ) return A_Ast_Block is
        block     : A_Ast_Block;
        statement : A_Ast_Statement;
    begin
        block := Create_Block( this.scanner.Location, parent );

        statement := this.Scan_Statement( block );
        while statement /= null loop
            block.Append( statement );
            statement := this.Scan_Statement( block );
        end loop;

        return block;
    exception
        when others =>
            Delete( A_Ast_Node(block) );
            raise;
    end Scan_Statement_List;

    ----------------------------------------------------------------------------

    --
    -- Value         ::= value-token
    --                   | ('-' number-value-token)
    --                   | List_Value
    --                   | Map_Value
    --                   | Anon_Function_Definition
    --                   | Named_Function_Definition
    --   List_Value  ::= '[' [Expression_List<literal=True>] ']'
    --   Map_Value   ::= '{' [Map_Pair {(',' | end-of-line) Map_Pair}] '}'
    --     Map_Pair  ::= Named_Function_Definition
    --                   | Named_Member_Definition
    --                   | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal=True>))
    --       Map_Key ::= string-value-token | symbol-token
    --
    function Scan_Value( this : not null access Parser'Class ) return A_Ast_Expression is
        token    : Token_Ptr;
        token2   : Token_Ptr;
        elements : Expression_Vectors.Vector;
        func     : A_Ast_Function_Definition;
        key      : A_Ast_Expression;
        elem     : A_Ast_Expression;
        result   : A_Ast_Expression;
    begin
        -- - - - Boolean, Number, String, or Null - - - --
        token := this.scanner.Accept_Token( TK_VALUE );
        if token.Not_Null then
            return Create_Literal( token.Get.Location, Value_Token(token.Get.all).Get_Value );
        end if;

        -- - - - Negative Number - - - --
        token := this.scanner.Accept_Token( TK_MINUS );
        if token.Not_Null then
            token2 := this.scanner.Expect_Token( TK_VALUE );
            if not Value_Token(token2.Get.all).Get_Value.Is_Number then
                Raise_Parse_Error( "Unexpected " & Image( token.Get.Get_Type ), token.Get.Location );
            end if;
            return Create_Literal( token2.Get.Location, Create( -Value_Token(token2.Get.all).Get_Value.To_Long_Float ) );
        end if;

        -- - - - List - - - --
        token := this.scanner.Accept_Token( TK_LBRACKET );
        if token.Not_Null then
            -- found a list literal
            result := Create_List( token.Get.Location );

            elements := this.Scan_Expression_List( literal => True );
            for elem of elements loop
                A_Ast_List(result).Append( elem );
            end loop;
            elements.Clear;

            this.scanner.Expect_Token( TK_RBRACKET );

            return result;
        end if;

        -- - - - Map - - - --
        token := this.scanner.Accept_Token( TK_LBRACE );
        if token.Not_Null then
            -- found a map value
            result := Create_Map( token.Get.Location );

            -- scan the elements as a list of pairs
            loop
                -- scan a named function definition element
                func := this.Scan_Function_Definition( nameOption => Required, memberOption => Optional );
                if func /= null then
                    key := Create_Literal( func.Get_Name.Location, Create( func.Get_Name.Get_Name ) );
                    A_Ast_Map(result).Insert( key, A_Ast_Expression(func) );

                -- scan a key:value pair element
                else
                    token := this.scanner.Accept_Token( TK_VALUE );
                    if token.Not_Null then
                        if not Value_Token(token.Get.all).Get_Value.Is_String then
                            Raise_Parse_Error( "Expected key", token.Get.Location );
                        end if;
                        key := Create_Literal( token.Get.Location, Value_Token(token.Get.all).Get_Value );
                    else
                        token := this.scanner.Accept_Token( TK_SYMBOL );
                        if token.Not_Null then
                            key := Create_Literal( token.Get.Location, Create( Symbol_Token(token.Get.all).Get_Name ) );
                        elsif A_Ast_Map(result).Is_Empty then
                            -- map is empty, expect closing brace '}'
                            this.scanner.Expect_Token( TK_RBRACE );
                            exit;
                        else
                            -- the map isn't empty, so the previous token was a
                            -- comma; we expected another key here.
                            Raise_Parse_Error( "Expected key", this.scanner.Location );
                        end if;
                    end if;

                    this.scanner.Expect_Token( TK_COLON );

                    elem := A_Ast_Expression(this.Scan_Function_Definition( nameOption => Disallowed,
                                                                            nameLoc => key.Location,
                                                                            nameString => Cast_String( A_Ast_Literal(key).Get_Value ),
                                                                            memberOption => Optional
                                                                          ));
                    if elem = null then
                        elem := this.Expect_Expression( literal => True );
                    end if;
                    A_Ast_Map(result).Insert( key, elem );
                end if;

                -- check a for closing brace
                if this.scanner.Accept_Token( TK_RBRACE ).Not_Null then
                    exit;
                end if;

                -- expect a newline or comma between elements
                if this.scanner.Accept_Token( TK_EOL ).Not_Null then
                    token := this.scanner.Accept_Token( TK_COMMA );
                else
                    this.scanner.Expect_Token( TK_COMMA );
                end if;
            end loop;

            -- analyze the map for serialized (JSON encoded) binary value types
            -- todo: Create the Create_From_Serialized method in the
            --       Maps package that translates a specially
            --       formatted Map into a binary value type.
            --       This new method should also be called from Ast_Map.Prune
            --       in Scribble.Ast.Expressions package.
            --result := Maps.Create_From_Serialized( result.Map );

            return result;
        end if;

        -- - - - Function Definition - - - --
        func := this.Scan_Function_Definition( nameOption => Optional );
        if func /= null then
            -- the function is the top-level value in the input source code, so
            -- store the entire source in it.
            func.Set_Source( this.scanner.Get_Source );
            return A_Ast_Expression(func);
        end if;

        return null;
    exception
        when others =>
            Delete( A_Ast_Node(result) );
            Delete( A_Ast_Node(func) );
            Delete( A_Ast_Node(key) );
            Delete( A_Ast_Node(elem) );
            raise;
    end Scan_Value;

end Scribble.Parsers;
