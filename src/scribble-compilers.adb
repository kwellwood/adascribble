--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Exceptions;                    use Ada.Exceptions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;
--with Debugging;                         use Debugging;
with Scribble.Ast;                      use Scribble.Ast;
with Scribble.Ast.Expressions;          use Scribble.Ast.Expressions;
with Scribble.Ast.Expressions.Functions;use Scribble.Ast.Expressions.Functions;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Scribble.Ast.Statements;           use Scribble.Ast.Statements;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Scribble.String_Streams;           use Scribble.String_Streams;
with Scribble.Token_Locations;          use Scribble.Token_Locations;
with Values.Construction;               use Values.Construction;

package body Scribble.Compilers is

    function Create_Scribble_Compiler( runtime : not null A_Scribble_Runtime ) return A_Scribble_Compiler is
        this : constant A_Scribble_Compiler := new Scribble_Compiler;
    begin
        this.Construct( runtime );
        return this;
    end Create_Scribble_Compiler;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this    : access Scribble_Compiler;
                         runtime : not null A_Scribble_Runtime ) is
    begin
        this.runtime := runtime;
        this.parser := Create_Parser( runtime );
        this.analyzer := Create_Semantic_Analyzer( runtime );
        this.generator := Create_Value_Generator( runtime );
    end Construct;

    ----------------------------------------------------------------------------

    not overriding
    procedure Delete( this : in out Scribble_Compiler ) is
    begin
        Delete( this.generator );
        Delete( this.analyzer );
        Delete( this.parser );
    end Delete;

    ----------------------------------------------------------------------------

    function Bind_Implicit( this      : not null access Scribble_Compiler'Class;
                            name      : String;
                            implicits : Value_Array;
                            defaults  : Value_Array ) return Function_Value is
        fid      : constant Function_Id := Fid_Hash( name );
        proto    : Prototype;
        loc      : constant Token_Location := UNKNOWN_LOCATION;
        callArgs : Integer;
        func     : A_Ast_Function_Definition;
        ident    : A_Ast_Identifier;
        expr     : A_Ast_Expression;
        ok       : Boolean;
        result   : Value;
    begin
        this.runtime.Get_Prototype( fid, proto );
        callArgs := proto.maxArgs - implicits'Length;

        -- verify 'name', 'implicits' and 'defaults' are ok
        if not this.runtime.Is_Defined( fid ) then
            Raise_Parse_Error( "Function '" & name & "' is undefined", UNKNOWN_LOCATION );
        elsif callArgs < 0 then
            Raise_Parse_Error( "Too many implicit arguments to function '" & name & "'", UNKNOWN_LOCATION );
        elsif defaults'Length > callArgs then
            Raise_Parse_Error( "Too many default arguments to function '" & name & "'", UNKNOWN_LOCATION );
        end if;

        -- Build AST --
        func := Create_Function_Definition( loc, Create_Identifier( loc, name ), False );

        for i in 1..callArgs loop
            ident := Create_Identifier( loc, "a" & Trim( Integer'Image( i ), Left ) );
            if i > callArgs - defaults'Length then
                expr := Create_Literal( loc, Clone( defaults(defaults'First + i - (callArgs - defaults'Length + 1)) ) );
            end if;
            func.Add_Parameter( ident, expr, ok );   -- consumes 'ident', 'expr'
        end loop;

        declare
            block : A_Ast_Block := Create_Block( loc, null );
            ret   : A_Ast_Statement;
            call  : A_Ast_Function_Call;
        begin
            -- this generates the following:
            --
            --   return name(i1, i2, ..., iN, a1, a2, ..., aN);
            --
            -- where "i*" are literal implicit values and "a*" are argument references
            call := Create_Function_Call( loc, Create_Builtin( loc, name ) );

            for i in 1..implicits'Length loop
                expr := Create_Literal( loc, Clone( implicits(implicits'First + (i - 1)) ) );
                call.Add_Argument( expr, ok );                -- consumes 'expr'
            end loop;

            for i in 1..callArgs loop
                ident := Create_Identifier( loc, "a" & Trim( Integer'Image( i ), Left ) );
                call.Add_Argument( A_Ast_Expression(ident), ok );  -- consumes 'ident'
            end loop;

            ret := Create_Return( A_Ast_Expression(call) );   -- consumes 'call'
            block.Append( ret );                              -- consumes 'ret'
            func.Set_Body( block );                           -- consumes 'block'
        end;

        -- Semantic Analysis --
        this.analyzer.Analyze( A_Ast_Expression(func) );      -- may raise Parse_Error

        -- Value Generation --
        begin
            result := this.generator.Generate( A_Ast_Expression(func), enableDebug => False );
        exception
            when e : others =>
                Raise_Parse_Error( "Code generation failed: " & Exception_Message( e ), UNKNOWN_LOCATION );
        end;

        Delete( A_Ast_Node(func) );
        return result.Func;
    exception
        when others =>
            Delete( A_Ast_Node(func) );
            raise;
    end Bind_Implicit;

    ----------------------------------------------------------------------------

    function Bind_Implicit( this      : not null access Scribble_Compiler'Class;
                            name      : String;
                            implicits : Value_Array ) return Function_Value is
        defaults : Value_Array(1..0);
    begin
        return this.Bind_Implicit(name, implicits, defaults);
    end Bind_Implicit;

    ----------------------------------------------------------------------------

    function Bind( this     : not null access Scribble_Compiler'Class;
                   name     : String;
                   defaults : Value_Array ) return Function_Value is
        implicits : Value_Array(1..0);
    begin
        return this.Bind_Implicit(name, implicits, defaults);
    end Bind;

    ----------------------------------------------------------------------------

    function Bind( this : not null access Scribble_Compiler'Class;
                   name : String ) return Function_Value is
        implicits : Value_Array(1..0);
        defaults  : Value_Array(1..0);
    begin
        return this.Bind_Implicit(name, implicits, defaults);
    end Bind;

    ----------------------------------------------------------------------------

    function Compile( this        : not null access Scribble_Compiler'Class;
                      source      : access Root_Stream_Type'Class;
                      path        : String;
                      enableDebug : Boolean := False ) return Value is
        ast    : A_Ast_Expression;
        pruned : A_Ast_Expression;
        result : Value;
    begin
        -- Stage 1: Parsing --
        this.parser.Set_Input( source, path );
        this.parser.Enable_Debug( enableDebug );
        ast := this.parser.Expect_Value;             -- may raise Parse_Error
        this.parser.Expect_EOF;                      -- may raise Parse_Error

        -- if the top-level value is a function definition, put the entire source
        -- file into the function. this is so comments before and after the
        -- function declaration don't get left out.
        if ast.Get_Type = FUNCTION_DEFINITION then
            A_Ast_Function_Definition(ast).Set_Source( this.parser.Get_Source );
        end if;

        this.parser.Set_Input( null );

        -- Stage 2: Semantic Analysis --
        this.analyzer.Analyze( ast );                -- may raise Parse_Error

        -- Stage 3: Tree Pruning --
        pruned := ast.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(ast) );
            ast := pruned;
            pruned := null;
        end if;

        -- Stage 4: Value Generation --
        begin
            result := this.generator.Generate( ast, enableDebug );
        exception
            when e : others =>
                Raise_Parse_Error( "Code generation failed: " & Exception_Message( e ), UNKNOWN_LOCATION );
        end;

        Delete( A_Ast_Node(ast) );
        return result;
    exception
        when others =>
            this.parser.Set_Input( null );
            Delete( A_Ast_Node(ast) );
            raise;
    end Compile;

    ----------------------------------------------------------------------------

    function Compile( this        : not null access Scribble_Compiler'Class;
                      source      : Unbounded_String;
                      path        : String;
                      enableDebug : Boolean := False ) return Value is
        strm : A_String_Stream := Stream( source );
    begin
        return result : constant Value := this.Compile( strm, path, enableDebug ) do
            Close( strm );
        end return;
    exception
        when others =>
            Close( strm );
            raise;
    end Compile;

    ----------------------------------------------------------------------------

    function Get_Runtime( this : not null access Scribble_Compiler'Class ) return A_Scribble_Runtime is (this.runtime);

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Scribble_Compiler ) is
        procedure Free is new Ada.Unchecked_Deallocation( Scribble_Compiler'Class, A_Scribble_Compiler );
    begin
        if this /= null then
            this.Delete;
            Free( this );
        end if;
    end Delete;

    --==========================================================================

    function Compile( source : Unbounded_String; path : String := "" ) return Value is
        runtime  : A_Scribble_Runtime := Create_Scribble_Runtime;
        compiler : A_Scribble_Compiler := Create_Scribble_Compiler( runtime );
        result   : Value;
    begin
        result := compiler.Compile( source, path );
        Delete( compiler );
        Delete( runtime );
        return result;
    exception
        when Parse_Error =>
            Delete( compiler );
            Delete( runtime );
            raise;
    end Compile;

end Scribble.Compilers;
