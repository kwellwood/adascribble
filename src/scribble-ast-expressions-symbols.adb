--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Scribble.Ast.Processors;           use Scribble.Ast.Processors;

package body Scribble.Ast.Expressions.Symbols is

    function Is_Reference_Expression( this : not null access Ast_Symbol'Class ) return Boolean
    is (this.parent /= null and then this.parent.Get_Type = REFERENCE_OP);

    --==========================================================================

    function Create_Identifier( loc       : Token_Location;
                                name      : String;
                                namespace : String := "" ) return A_Ast_Identifier is
        this : constant A_Ast_Identifier := new Ast_Identifier;
    begin
        this.Construct( loc, name, namespace );
        return this;
    end Create_Identifier;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Ast_Identifier;
                         loc       : Token_Location;
                         name      : String;
                         namespace : String ) is
    begin
        Ast_Symbol(this.all).Construct( loc );
        this.name := To_Unbounded_String( name );
        this.namespace := To_Unbounded_String( namespace );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Identifier'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    function Get_Namespace( this : not null access Ast_Identifier'Class ) return String is (To_String( this.namespace ));

    ----------------------------------------------------------------------------

    function No_Namespace( this : not null access Ast_Identifier'Class ) return Boolean is (Length( this.namespace ) = 0);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Identifier;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Identifier( A_Ast_Identifier(this) );
    end Process;

    --==========================================================================

    function Create_This( loc : Token_Location ) return A_Ast_Expression is
        this : constant A_Ast_This := new Ast_This;
    begin
        this.Construct( loc );
        return A_Ast_Expression(this);
    end Create_This;

    ----------------------------------------------------------------------------

    procedure Process( this      : access Ast_This;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_This( A_Ast_This(this) );
    end Process;

    --==========================================================================

    function Create_Membership( loc  : Token_Location;
                                left : not null A_Ast_Expression;
                                name : String ) return A_Ast_Expression is
        this : constant A_Ast_Membership := new Ast_Membership;
    begin
        this.Construct( loc, left, name );
        return A_Ast_Expression(this);
    end Create_Membership;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Ast_Membership;
                         loc  : Token_Location;
                         left : not null A_Ast_Expression;
                         name : String ) is
    begin
        Ast_Symbol(this.all).Construct( loc );
        this.left := left;
        this.name := To_Unbounded_String( name );
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Membership ) is
    begin
        Delete( A_Ast_Node(this.left) );
        Ast_Symbol(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Expression( this : not null access Ast_Membership'Class ) return A_Ast_Expression is (this.left);

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Membership'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Membership;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Membership( A_Ast_Membership(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Membership ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        pruned := this.left.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.left) );
            this.left := pruned;
        end if;
        return null;
    end Prune;

    ----------------------------------------------------------------------------

    procedure Take_Expression( this : not null access Ast_Membership'Class;
                               expr : in out A_Ast_Expression ) is
    begin
        expr := this.left;
        this.left := null;
    end Take_Expression;

    --==========================================================================

    function Create_Name_Expr( loc       : Token_Location;
                               namespace : String;
                               name      : not null A_Ast_Expression ) return A_Ast_Expression is
        this : constant A_Ast_Name_Expr := new Ast_Name_Expr;
    begin
        this.Construct( loc, namespace, name );
        return A_Ast_Expression(this);
    end Create_Name_Expr;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this      : access Ast_Name_Expr;
                         loc       : Token_Location;
                         namespace : String;
                         name      : not null A_Ast_Expression ) is
    begin
        Ast_Symbol(this.all).Construct( loc );
        this.namespace := To_Unbounded_String( namespace );
        this.name := name;
    end Construct;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out Ast_Name_Expr ) is
    begin
        Delete( A_Ast_Node(this.name) );
        Ast_Symbol(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Name_Expr'Class ) return A_Ast_Expression is (this.name);

    ----------------------------------------------------------------------------

    function Get_Namespace( this : not null access Ast_Name_Expr'Class ) return String is (To_String( this.namespace ));

    ----------------------------------------------------------------------------

    procedure Process( this      : access Ast_Name_Expr;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Name_Expr( A_Ast_Name_Expr(this) );
    end Process;

    ----------------------------------------------------------------------------

    function Prune( this : access Ast_Name_Expr ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        pruned := this.name.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.name) );
            this.name := pruned;
        end if;

        return null;
    end Prune;

end Scribble.Ast.Expressions.Symbols;
