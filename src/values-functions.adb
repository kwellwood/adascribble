--
-- Copyright (c) 2015-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with System.Address_To_Access_Conversions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Values;                            use Values;
with Values.Streaming;                  use Values.Streaming;

package body Values.Functions is

    type Function_Data(defArgs, progLen, dataLen : Integer) is limited
        record
            obj          : Object_Refs;
            argCount     : Natural := 0;
            defaultArgs  : Value_Array(1..defArgs);
            instructions : aliased Opcode_Array(1..progLen);
            staticData   : aliased Value_Array(1..dataLen);
            isMember     : Boolean := False;
            name         : Unbounded_String;
            debugInfo    : A_Debug_Info := null;
        end record;
    for Function_Data use
        record
            obj at 0 range 0..31;
        end record;

    ----------------------------------------------------------------------------

    package Accesses is new System.Address_To_Access_Conversions( Function_Data );
    use Accesses;

    --==========================================================================

    function Create_Function( argCount    : Natural;
                              defaultArgs : Value_Array;
                              program     : Opcode_Array;
                              staticData  : Value_Array;
                              isMember    : Boolean;
                              name        : String;
                              debugInfo   : A_Debug_Info ) return Value is
        this : Value := Value'(Controlled with v => TAG_FUNCTION);
        data : constant Object_Pointer := new Function_Data'(defArgs => defaultArgs'Length,
                                                             progLen => program'Length,
                                                             dataLen => staticData'Length,
                                                             others  => <>);
    begin
        data.argCount := argCount;
        data.defaultArgs := defaultArgs;
        data.instructions := program;
        data.staticData := staticData;
        data.isMember := isMember;
        data.name := To_Unbounded_String( name );
        data.debugInfo := debugInfo;

        this.Set_Object( To_Address( data ) );
        return this;
    end Create_Function;

    --==========================================================================

    function As_Function( this : Value'Class ) return Function_Value is
        (Function_Value'(Value'(Value(this)) with data => A_Function_Data(To_Pointer( this.Get_Address ))));

    ----------------------------------------------------------------------------

    function Arg_Count( this : Function_Value'Class ) return Natural is (this.data.argCount);

    ----------------------------------------------------------------------------

    function Arg_Default( this : Function_Value'Class; arg : Positive ) return Value is
        reqArgs : constant Integer := this.data.argCount - this.data.defArgs;
    begin
        if reqArgs < arg and arg <= this.data.argCount then
            return this.data.defaultArgs(arg - reqArgs);
        end if;
        return Null_Value;
    end Arg_Default;

    ----------------------------------------------------------------------------

    function Data_Length( this : Function_Value'Class ) return Natural is (this.data.dataLen);

    ----------------------------------------------------------------------------

    function Get_Data( this : Function_Value'Class; index : Integer ) return Value
        is (if index in 1..this.data.dataLen then this.data.staticData(index) else Null_Value);

    ----------------------------------------------------------------------------

    function Get_Debug_Info( this : Function_Value'Class ) return A_Debug_Info is (this.data.debugInfo);

    ----------------------------------------------------------------------------

    function Get_Name( this : Function_Value'Class ) return String is (To_String( this.data.name ));

    ----------------------------------------------------------------------------

    function Get_Opcode( this : Function_Value'Class; index : Positive ) return Opcode
        is (if index <= this.data.progLen then this.data.instructions(index) else To_Opcode( NOOP ));

    ----------------------------------------------------------------------------

    function Get_Path( this : Function_Value'Class ) return String
        is (if this.data.debugInfo /= null then To_String( this.data.debugInfo.path ) else "");

    ----------------------------------------------------------------------------

    function Is_Member( this : Function_Value'Class ) return Boolean is (this.data.isMember);

    ----------------------------------------------------------------------------

    function Min_Args( this : Function_Value'Class ) return Natural
        is (this.data.argCount - this.data.defArgs);

    ----------------------------------------------------------------------------

    function Program( this : Function_Value'Class ) return Address
        is (this.data.instructions(this.data.instructions'First)'Address);

    ----------------------------------------------------------------------------

    function Program_Length( this : Function_Value'Class ) return Natural is (this.data.progLen);

    ----------------------------------------------------------------------------

    function Prototype_Image( this : Function_Value'Class ) return String is
        result : Unbounded_String := To_Unbounded_String( "function" );
        param  : Integer;
    begin
        if this.data.isMember then
            result := "member " & result;
        end if;

        if Length( this.data.name ) > 0 then
            Append( result, ' ' & this.data.name );
        end if;
        Append( result, '(' );

        for p in (if not this.data.isMember then 1 else 2)..this.data.argCount loop
            if this.data.debugInfo /= null then
                Append( result, this.data.debugInfo.paramNames(p) );
            else
                -- a, b, ..., aa, bb, ..., aaa, etc.
                param := p;
                if this.data.isMember then
                    param := param - 1;
                end if;
                Append( result, String'(((param + 25) / 26) * Character'Val( Character'Pos( 'a' ) + ((param - 1) mod 26) )) );
            end if;
            if p > this.Min_Args then
                Append( result, ":=" & this.Arg_Default( p ).Image );
            end if;
            if p < this.data.argCount then
                Append( result, ", " );
            end if;
        end loop;
        Append( result, ')' );
        return To_String( result );
    end Prototype_Image;

    ----------------------------------------------------------------------------

    function Source_Code( this : Function_Value'Class ) return String
        is (if this.data.debugInfo /= null then To_String(this.data.debugInfo.source) else "");

    ----------------------------------------------------------------------------

    function Source_Line( this : Function_Value'Class; line : Natural ) return String is

        function Get_Line( text : Unbounded_String; line : Positive ) return String is
            first : Integer := 1;
            last  : Integer := Index( text, "" & ASCII.LF, first );
        begin
            for i in 2..line loop
                first := last + 1;
                if first > Length( text ) then
                    return "";
                end if;
                last := Index( text, "" & ASCII.LF, first );
            end loop;

            if last < first then
                last := Length( text );
            else
                last := last - 1;      -- discard the LF
                if Element( text, last ) = ASCII.CR then
                    last := last - 1;
                end if;                -- discard the CR
            end if;

            return Slice( text, first, last );
        end Get_Line;

    begin
        if line > 0 and then this.data.debugInfo /= null then
            return Get_Line( this.data.debugInfo.source, line );
        end if;
        return "";
    end Source_Line;

    --==========================================================================

    procedure Delete_Function( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( Function_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        Delete( data.debugInfo );
        Free( data );
    end Delete_Function;

    ----------------------------------------------------------------------------

    function Image_Function( this : Value'Class ) return String is

        ------------------------------------------------------------------------

        -- Removes extra whitespace and comments from source code. All runs of
        -- whitespace will be crunched down to a single space. Line endings will
        -- be treated as a space. Whitespace within literal strings will remain.
        function Remove_Whitespace( str : Unbounded_String ) return String is
            result : Unbounded_String;
            ws     : Integer := 0;

            type Scan_State is (None, InString, InComment);
            state : Scan_State := None;
        begin
            for i in 1..Length( str ) loop
                case state is

                    -- whitespace is removed until a string or comment is encountered
                    when None =>
                        if Element( str, i ) = '"' then
                            -- enter a string
                            state := InString;
                            if ws > 0 then
                                Append( result, ' ' );  -- whitespace preceeding string
                                ws := 0;
                            end if;
                            Append( result, '"' );
                        elsif Element( str, i ) = ' ' or else Element( str, i ) = ASCII.HT or else
                              Element( str, i ) = ASCII.LF or else Element( str, i ) = ASCII.CR
                        then
                            ws := ws + 1;
                        elsif Element( str, i ) = '/' and then i < Length( str ) and then Element( str, i + 1 ) = '/' then
                            state := InComment;
                        else
                            if ws > 0 then
                                Append( result, ' ' );
                                ws := 0;
                            end if;
                            Append( result, Element( str, i ) );
                        end if;

                    -- whitespace is not removed within a string
                    when InString =>
                        if Element( str, i ) = '"' and then Element( str, i - 1 ) /= '\' then
                            -- not an escaped quote; exit the string
                            state := None;
                        elsif Element( str, i ) = ASCII.LF or else Element( str, i ) = ASCII.CR then
                            -- unterminated string, but exit the string
                            state := None;
                        end if;
                        Append( result, Element( str, i ) );

                    -- all text in comments is stripped
                    when InComment =>
                        if Element( str, i ) = ASCII.LF or else Element( str, i ) = ASCII.CR then
                            -- exit the comment
                            state := None;
                            ws := ws + 1;
                        end if;

                end case;
            end loop;
            return To_String( result );
        end Remove_Whitespace;

        ------------------------------------------------------------------------

        data : constant Object_Pointer := To_Pointer( this.Get_Address );
    begin
        if data.debugInfo /= null and then Length( data.debugInfo.source ) > 0 then
            return Remove_Whitespace( data.debugInfo.source );
        end if;
        return As_Function( this ).Prototype_Image;
    end Image_Function;

    ----------------------------------------------------------------------------

    procedure Read_Function( stream : access Root_Stream_Type'Class;
                             this   : in out Value'Class ) is
        data     : Object_Pointer;
        argCount : Integer;
        isMember : Boolean;
        defArgs  : Integer;
        progLen  : Integer;
        dataLen  : Integer;
        len      : Integer;
    begin
        argCount := Integer(Unsigned_8'Input( stream ));

        isMember := Boolean'Input( stream );

        defArgs := Integer(Unsigned_8'Input( stream ));
        if (defArgs > argCount) or (isMember and defArgs = argCount) then
            raise Constraint_Error with "Illegal Function in stream";
        end if;

        progLen := Integer'Input( stream );
        if progLen < 1 or else progLen >= 2**25 then    -- signed 25 bits in jump addresses
            raise Constraint_Error with "Illegal Function in stream";
        end if;

        dataLen := Integer'Input( stream );
        if dataLen < 0 or else dataLen >= 2**26 then    -- 26 bits in data addresses
            raise Constraint_Error with "Illegal Function in stream";
        end if;

        data := new Function_Data'(argCount => argCount,
                                   defArgs  => defArgs,
                                   progLen  => progLen,
                                   dataLen  => dataLen,
                                   isMember => isMember,
                                   others   => <>);
        this.Set_Object( To_Address( data ) );

        for i in data.defaultArgs'Range loop
            data.defaultArgs(i) := Value_Input( stream );
        end loop;

        for i in data.instructions'Range loop
            data.instructions(i) := Opcode'Input( stream );
        end loop;

        for i in data.staticData'Range loop
            data.staticData(i) := Value_Input( stream );
        end loop;

        data.name := Read_String( stream );

        if Boolean'Input( stream ) then
            data.debugInfo := new Debug_Info;

            -- instruction lines
            data.debugInfo.lines := new Integer_Array(data.instructions'Range);
            for i in data.debugInfo.lines'Range loop
                data.debugInfo.lines(i) := Integer(Unsigned_16'Input( stream ));
            end loop;

            -- parameter names
            data.debugInfo.paramNames := new String_Array(1..data.argCount);
            for i in data.debugInfo.paramNames'Range loop
                data.debugInfo.paramNames(i) := Read_String( stream );
            end loop;

            -- variable names
            len := Integer(Unsigned_8'Input( stream ));
            data.debugInfo.varNames := new Var_Name_Array(1..len);
            for i in data.debugInfo.varNames'Range loop
                len := Integer(Unsigned_16'Input( stream ));
                data.debugInfo.varNames(i) := new Var_Use_Array(1..len);
                for j in data.debugInfo.varNames(i)'Range loop
                    data.debugInfo.varNames(i).all(j).line := Integer(Unsigned_16'Input( stream ));
                    data.debugInfo.varNames(i).all(j).name := Read_String( stream );
                end loop;
            end loop;

            -- function names
            len := Integer(Unsigned_16'Input( stream ));
            data.debugInfo.funcNames := new Func_Name_Array(1..len);
            for i in data.debugInfo.funcNames'Range loop
                data.debugInfo.funcNames(i).fid := Unsigned_32'Input( stream );
                data.debugInfo.funcNames(i).name := Read_String( stream );
            end loop;

            -- source code
            data.debugInfo.source := Read_String( stream );
            data.debugInfo.path := Read_String( stream );
            data.debugInfo.firstLine := Integer'Input( stream );
        end if;

    end Read_Function;

    ----------------------------------------------------------------------------

    procedure Write_Function( stream : access Root_Stream_Type'Class;
                              this   : Value'Class ) is
        data : constant Object_Pointer := To_Pointer( this.Get_Address );
    begin
        Unsigned_8'Output( stream, Unsigned_8(data.argCount) );
        Boolean'Output( stream, data.isMember );
        Unsigned_8'Output( stream, Unsigned_8(data.defaultArgs'Length) );
        Integer'Output( stream, data.instructions'Length );
        Integer'Output( stream, data.staticData'Length );

        for i in data.defaultArgs'Range loop
            Value_Output( stream, data.defaultArgs(i) );
        end loop;

        for i in data.instructions'Range loop
            Opcode'Output( stream, data.instructions(i) );
        end loop;

        for i in data.staticData'Range loop
            Value_Output( stream, data.staticData(i) );
        end loop;

        Write_String( stream, data.name );

        Boolean'Output( stream, data.debugInfo /= null );
        if data.debugInfo /= null then
             -- instruction line numbers
            for i in data.debugInfo.lines'Range loop
                Unsigned_16'Output( stream, Unsigned_16(data.debugInfo.lines(i)) );
            end loop;

            -- parameter names
            for i in data.debugInfo.paramNames'Range loop
                Write_String( stream, data.debugInfo.paramNames(i) );
            end loop;

            -- variable names
            Unsigned_8'Output( stream, data.debugInfo.varNames'Length );
            for i in data.debugInfo.varNames'Range loop
                Unsigned_16'Output( stream, data.debugInfo.varNames(i)'Length );
                for j in data.debugInfo.varNames(i)'Range loop
                    Unsigned_16'Output( stream, Unsigned_16(data.debugInfo.varNames(i).all(j).line) );
                    Write_String( stream, data.debugInfo.varNames(i).all(j).name );
                end loop;
            end loop;

            -- function names
            Unsigned_16'Output( stream, Unsigned_16(data.debugInfo.funcNames'Length) );
            for i in data.debugInfo.funcNames'Range loop
                Unsigned_32'Output( stream, data.debugInfo.funcNames(i).fid );
                Write_String( stream, data.debugInfo.funcNames(i).name );
            end loop;

            -- source code
            Write_String( stream, data.debugInfo.source );
            Write_String( stream, data.debugInfo.path );
            Integer'Output( stream, data.debugInfo.firstLine );
        end if;
    end Write_Function;

end Values.Functions;
