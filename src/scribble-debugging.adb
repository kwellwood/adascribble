--
-- Copyright (c) 2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;

package body Scribble.Debugging is

    procedure Delete( info : in out A_Debug_Info ) is
        procedure Delete is new Ada.Unchecked_Deallocation( Integer_Array, A_Integer_Array );
        procedure Delete is new Ada.Unchecked_Deallocation( String_Array, A_String_Array );
        procedure Delete is new Ada.Unchecked_Deallocation( Func_Name_Array, A_Func_Name_Array );

        procedure Delete( vna : in out A_Var_Name_Array ) is
            procedure Delete is new Ada.Unchecked_Deallocation( Var_Use_Array, A_Var_Use_Array );
            procedure Free is new Ada.Unchecked_Deallocation( Var_Name_Array, A_Var_Name_Array );
        begin
            if vna /= null then
                for i in vna'Range loop
                    Delete( vna(i) );
                end loop;
                Free( vna );
            end if;
        end Delete;

        procedure Free is new Ada.Unchecked_Deallocation( Debug_Info, A_Debug_Info );
    begin
        if info /= null then
            Delete( info.lines );
            Delete( info.paramNames );
            Delete( info.varNames );
            Delete( info.funcNames );
            Free( info );
        end if;
    end Delete;

end Scribble.Debugging;
