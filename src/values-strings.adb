--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with System.Address_To_Access_Conversions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;

package body Values.Strings is

    type String_Data(length : Integer) is limited
        record
            obj : Object_Refs;
            str : String(1..length);
        end record;
    for String_Data use
        record
            obj at 0 range 0..31;
        end record;

    ----------------------------------------------------------------------------

    package Accesses is new System.Address_To_Access_Conversions( String_Data );
    use Accesses;

    Empty_String : constant Object_Pointer := new String_Data'(obj => Object_Refs'(refs => 1),   -- not deletable
                                                               length => 0,
                                                               others => <>);

    --==========================================================================

    function Create( str : String ) return Value is
        this : Value := Value'(Controlled with v => TAG_STRING);
        data : Object_Pointer;
    begin
        if str'Length > 0 then
            data := new String_Data(str'Length);
            data.str := str;
        else
            data := Empty_String;
        end if;
        this.Set_Object( To_Address( data ) );
        return this;
    end Create;

    ----------------------------------------------------------------------------

    function Create( str : Unbounded_String ) return Value is
        this : Value := Value'(Controlled with v => TAG_STRING);
        data : Object_Pointer;
    begin
        if Length( str ) > 0 then
            data := new String_Data(Length( str ));
            data.str := To_String( str );
        else
            data := Empty_String;
        end if;
        this.Set_Object( To_Address( data ) );
        return this;
    end Create;

    --==========================================================================

    function As_String( this : Value'Class ) return String_Value is
        (String_Value'(Value'(Value(this)) with data => A_String_Data(To_Pointer( this.Get_Address ))));

    ----------------------------------------------------------------------------

    function Append( this : String_Value'Class; text : String_Value'Class ) return Value is (this.Insert( text, this.data.length + 1 ));

    ----------------------------------------------------------------------------

    function Begins_With( this : String_Value'Class; text : String_Value'Class ) return Boolean is
    begin
        return text.data.length > 0 and then
               this.data.length >= text.data.length and then
               this.data.str(1..text.data.length) = text.data.str;
    end Begins_With;

    ----------------------------------------------------------------------------

    function Ends_With( this : String_Value'Class; text : String_Value'Class ) return Boolean is
    begin
        return text.data.length > 0 and then
               this.data.length >= text.data.length and then
               this.data.str((this.data.length - text.data.length + 1)..this.data.length) = text.data.str;
    end Ends_With;

    ----------------------------------------------------------------------------

    function Get( this : String_Value'Class; index : Integer ) return Character is (if index in 1..this.data.length then this.data.str(index) else ASCII.NUL);

    ----------------------------------------------------------------------------

    function Head( this : String_Value'Class; count : Natural ) return Value is (this.Slice( 1, count ));

    ----------------------------------------------------------------------------

    function IndexOf( this : String_Value'Class; pattern : String; from : Positive := 1 ) return Natural is
    begin
        if pattern'Length = 0 then
            return 0;
        end if;
        return Index( this.data.str, pattern, from );
    end IndexOf;

    ----------------------------------------------------------------------------

    function Insert( this  : String_Value'Class;
                     text  : String_Value'Class;
                     start : Positive ) return Value is
        combined : Value := Value'(Controlled with v => TAG_STRING);
        data     : Object_Pointer;
        preSize  : constant Integer := Integer'Min( start - 1, this.data.length );   -- 0 <= preSize <= length
        postSize : constant Integer := this.data.length - preSize;                   -- 0 <= postSize <= length
    begin
        if this.data.length + text.data.length > 0 then
            data := new String_Data(this.data.length + text.data.length);
            data.str(1..preSize) := this.data.str(1..preSize);
            data.str(preSize+1..preSize+text.data.length) := text.data.str;
            data.str(preSize+text.data.length+1..data.str'Last) := this.data.str(this.data.str'Last-(postSize-1)..this.data.str'Last);
        else
            data := Empty_String;
        end if;
        combined.Set_Object( To_Address( data ) );
        return combined;
    end Insert;

    ----------------------------------------------------------------------------

    function Length( this : String_Value'Class ) return Natural is (this.data.length);

    ----------------------------------------------------------------------------

    function Ltrim( this : String_Value'Class ) return Value is (Create( Trim( this.data.str, Left ) ));

    ----------------------------------------------------------------------------

    function Prepend( this : String_Value'Class; text : String_Value'Class ) return Value is (this.Insert( text, 1 ));

    ----------------------------------------------------------------------------

    function Remove( this : String_Value'Class; start : Positive; count : Natural ) return Value is
        removed : Value := Value'(Controlled with v => TAG_STRING);
        data     : Object_Pointer;
        preSize  : constant Integer := Integer'Min( start - 1, this.data.length );             -- 0 <= preSize <= length
        postSize : constant Integer := Integer'Max( this.data.length - preSize - count, 0 );   -- 0 <= postSize <= length
    begin
        if preSize + postSize > 0 then
            data := new String_Data(preSize + postSize);
            data.str(1..preSize) := this.data.str(1..preSize);
            data.str(preSize+1..data.str'Last) := this.data.str(this.data.str'Last-(postSize-1)..this.data.str'Last);
        else
            data := Empty_String;
        end if;
        removed.Set_Object( To_Address( data ) );
        return removed;
    end Remove;

    ----------------------------------------------------------------------------

    function Rtrim( this : String_Value'Class ) return Value is (Create( Trim( this.data.str, Right ) ));

    ----------------------------------------------------------------------------

    function Slice( this  : String_Value'Class;
                    from  : Positive;
                    count : Natural ) return Value is
    begin
        if from > this.data.length then
            return Create( "" );
        end if;
        return Create( Unbounded_Slice( To_Unbounded_String( this.data.str ),
                                        from,
                                        Integer'Min( from + count - 1, this.data.length )
                                      ) );
    end Slice;

    ----------------------------------------------------------------------------

    function Tail( this : String_Value'Class; count : Natural ) return Value
        is (this.Slice( Integer'Max( this.data.length - count + 1, 1 ), count ));

    ----------------------------------------------------------------------------

    function To_String( this : String_Value'Class ) return String is (this.data.str);

    ----------------------------------------------------------------------------

    function To_Unbounded_String( this : String_Value'Class ) return Unbounded_String is (To_Unbounded_String( this.data.str ));

    ----------------------------------------------------------------------------

    function Trim( this : String_Value'Class ) return Value is (Create( Trim( this.data.str, Both ) ));

    ----------------------------------------------------------------------------

    function "&"( l : String_Value'Class; r : String_Value'Class ) return Value is (Create( l.data.str & r.data.str ));
    function "&"( l : String_Value'Class; r : String             ) return Value is (Create( l.data.str & r ));
    function "&"( l : String_Value'Class; r : Unbounded_String   ) return Value is (Create( l.data.str & r ));
    function "&"( l : String;             r : String_Value'Class ) return Value is (Create( l & r.data.str ));
    function "&"( l : Unbounded_String;   r : String_Value'Class ) return Value is (Create( l & r.data.str ));

    ----------------------------------------------------------------------------

    function  "="( l : String_Value'Class; r : String             ) return Boolean is (l.data.str = r);
    function  "="( l : String_Value'Class; r : Unbounded_String   ) return Boolean is (l.data.str = r);
    function  "="( l : String;             r : String_Value'Class ) return Boolean is (l = r.data.str);
    function  "="( l : Unbounded_String;   r : String_Value'Class ) return Boolean is (l = r.data.str);

    ----------------------------------------------------------------------------

    function  "<"( l : String_Value'Class; r : String             ) return Boolean is (l.data.str < r);
    function  "<"( l : String_Value'Class; r : Unbounded_String   ) return Boolean is (l.data.str < r);
    function  "<"( l : String;             r : String_Value'Class ) return Boolean is (l < r.data.str);
    function  "<"( l : Unbounded_String;   r : String_Value'Class ) return Boolean is (l < r.data.str);

    ----------------------------------------------------------------------------

    function  ">"( l : String_Value'Class; r : String             ) return Boolean is (l.data.str > r);
    function  ">"( l : String_Value'Class; r : Unbounded_String   ) return Boolean is (l.data.str > r);
    function  ">"( l : String;             r : String_Value'Class ) return Boolean is (l > r.data.str);
    function  ">"( l : Unbounded_String;   r : String_Value'Class ) return Boolean is (l > r.data.str);

    ----------------------------------------------------------------------------

    function "<="( l : String_Value'Class; r : String             ) return Boolean is (l.data.str <= r);
    function "<="( l : String_Value'Class; r : Unbounded_String   ) return Boolean is (l.data.str <= r);
    function "<="( l : String;             r : String_Value'Class ) return Boolean is (l <= r.data.str);
    function "<="( l : Unbounded_String;   r : String_Value'Class ) return Boolean is (l <= r.data.str);

    ----------------------------------------------------------------------------

    function ">="( l : String_Value'Class; r : String             ) return Boolean is (l.data.str >= r);
    function ">="( l : String_Value'Class; r : Unbounded_String   ) return Boolean is (l.data.str >= r);
    function ">="( l : String;             r : String_Value'Class ) return Boolean is (l >= r.data.str);
    function ">="( l : Unbounded_String;   r : String_Value'Class ) return Boolean is (l >= r.data.str);

    --==========================================================================

    function Compare_Strings( left, right : Value'Class ) return Integer is
        leftPtr  : constant Object_Pointer := To_Pointer( left.Get_Address );
        rightPtr : constant Object_Pointer := To_Pointer( right.Get_Address );
    begin
        if leftPtr.str = rightPtr.str then
            return 0;
        elsif leftPtr.str > rightPtr.str then
            return 1;
        end if;
        return -1;
    end Compare_Strings;

    ----------------------------------------------------------------------------

    procedure Delete_String( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( String_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        Free( data );
    end Delete_String;

    ----------------------------------------------------------------------------

    function Image_String( this : Value'Class ) return String is
    begin
        return ("""" & Escape( To_Pointer( this.Get_Address ).str ) & """");
    end Image_String;

    ----------------------------------------------------------------------------

    procedure Read_String( stream : access Root_Stream_Type'Class;
                           this   : in out Value'Class ) is
        data : Object_Pointer := Empty_String;
        str  : Unbounded_String;
    begin
        str := Read_String( stream );
        if Length( str ) > 0 then
            data := new String_Data(Length( str ));
            data.str := To_String( str );
        end if;
        this.Set_Object( To_Address( data ) );
    end Read_String;

    ----------------------------------------------------------------------------

    procedure Write_String( stream : access Root_Stream_Type'Class;
                            this   : Value'Class ) is
    begin
        Write_String( stream, To_Pointer( this.Get_Address ).str );
    end Write_String;

    --==========================================================================

    -- Escapes special characters in a string with backslash notation.
    -- Tab => \t
    -- LF  => \n
    -- CR  => \r
    -- \   => \\
    -- "   => \"
    function Escape( str : String ) return String is
        result : Unbounded_String;
    begin
        for i in str'Range loop
            case str(i) is
                when ASCII.HT => Append( result, "\t" );
                when ASCII.LF => Append( result, "\n" );
                when ASCII.CR => Append( result, "\f" );
                when '\'      => Append( result, "\\" );
                 when '"'      => Append( result, "\""" );
                when others   => Append( result, str(i) );
            end case;
        end loop;
        return To_String( result );
    end Escape;

end Values.Strings;
