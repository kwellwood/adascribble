--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;

package Scribble.Token_Locations is

    type Token_Location is
        record
            line     : Integer := 1;
            column   : Integer := 1;
            absolute : Integer := 1;
            path     : Unbounded_String;
        end record;

    UNKNOWN_LOCATION : constant Token_Location := (0, 0, 0, others => <>);

    function "<="( l, r : Token_Location ) return Boolean is (l.absolute <= r.absolute);

    -- Returns an image of 'loc' following the format "<path>:<row>:<col>".
    --
    -- If the path is an empty string or it matches 'implicitPath', then the
    -- path and subsequent ':' will also be omitted.
    --
    -- The purpose of 'implicitPath' is to generate simpler error messages when
    -- referring to a location in the same file as the error that is being
    -- reported. To the user, it is implicitly understood that <row>:<col> is in
    -- the same file as the error being reported.
    function Image( loc          : Token_Location;
                    implicitPath : Unbounded_String := Null_Unbounded_String ) return String;

end Scribble.Token_Locations;
