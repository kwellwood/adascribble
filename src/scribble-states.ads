--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Values;                            use Values;

limited with Scribble.VMs;

package Scribble.States is

    type Scribble_State(argCount : Natural) is
        record
            vm     : access Scribble.VMs.Scribble_VM'Class;
            result : Value := Null_Value;
            args   : Value_Array(1..argCount);
        end record;

end Scribble.States;
