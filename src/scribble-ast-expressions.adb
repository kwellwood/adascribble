--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Containers;                    use Ada.Containers;
with Scribble.Ast.Processors;           use Scribble.Ast.Processors;
with Scribble.Ast.Expressions.Operators;use Scribble.Ast.Expressions.Operators;
with Scribble.Ast.Expressions.Symbols;  use Scribble.Ast.Expressions.Symbols;
with Values.Lists;                      use Values.Lists;
with Values.Maps;                       use Values.Maps;
with Values.Strings;                    use Values.Strings;

package body Scribble.Ast.Expressions is

    function Get_Parent( this : not null access Ast_Expression'Class ) return A_Ast_Expression is (this.parent);

    ----------------------------------------------------------------------------

    procedure Set_Parent( this   : not null access Ast_Expression'Class;
                          parent : A_Ast_Expression ) is
    begin
        this.parent := parent;
    end Set_Parent;

    --==========================================================================

    function Create_Builtin( loc  : Token_Location;
                             name : String ) return A_Ast_Expression is
        this : constant A_Ast_Builtin := new Ast_Builtin;
    begin
        this.Construct( loc, name );
        return A_Ast_Expression(this);
    end Create_Builtin;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Ast_Builtin;
                         loc  : Token_Location;
                         name : String ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.name := To_Unbounded_String( name );
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Fid( this : not null access Ast_Builtin'Class ) return Function_Id is (Fid_Hash( this.Get_Name ));

    ----------------------------------------------------------------------------

    function Get_Name( this : not null access Ast_Builtin'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    procedure Process( this      : access Ast_Builtin;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Builtin( A_Ast_Builtin(this) );
    end Process;

    --==========================================================================

    function Create_Function_Call( loc      : Token_Location;
                                   callExpr : not null A_Ast_Expression ) return A_Ast_Function_Call is
        this : constant A_Ast_Function_Call := new Ast_Function_Call;
    begin
        this.Construct( loc, callExpr );
        return this;
    end Create_Function_Call;

    ----------------------------------------------------------------------------

    function Create_Function_Call( loc      : Token_Location;
                                   callExpr : not null A_Ast_Expression;
                                   arg1     : not null A_Ast_Expression ) return A_Ast_Function_Call is
        this : constant A_Ast_Function_Call := Create_Function_Call( loc, callExpr );
    begin
        this.args.Append( arg1 );
        return this;
    end Create_Function_Call;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this     : access Ast_Function_Call;
                         loc      : Token_Location;
                         callExpr : A_Ast_Expression ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.callExpr := callExpr;
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Function_Call ) is
    begin
        for a of this.args loop
            Delete( A_Ast_Node(a) );
        end loop;
        Delete( A_Ast_Node(this.callExpr) );
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Add_Argument( this  : not null access Ast_Function_Call'Class;
                            arg   : in out A_Ast_Expression;
                            added : out Boolean ) is
    begin
        if this.args.Length < 255 then
            this.args.Append( arg );
            arg := null;
            added := True;
        else
            added := False;
        end if;
    end Add_Argument;

    ----------------------------------------------------------------------------

    function Arguments_Count( this : not null access Ast_Function_Call'Class ) return Natural is (Natural(this.args.Length));

    ----------------------------------------------------------------------------

    function Get_Argument( this : not null access Ast_Function_Call'Class;
                           n    : Positive ) return A_Ast_Expression is
    begin
        if n <= Integer(this.args.Length) then
            return this.args.Element( n );
        end if;
        return null;
    end Get_Argument;

    ----------------------------------------------------------------------------

    function Get_Call_Expression( this : not null access Ast_Function_Call'Class ) return A_Ast_Expression is (this.callExpr);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Function_Call;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Function_Call( A_Ast_Function_Call(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Function_Call ) return A_Ast_Expression is
        pruned : A_Ast_Expression;
    begin
        -- prune the calling expression
        pruned := this.callExpr.Prune;
        if pruned /= null then
            Delete( A_Ast_Node(this.callExpr) );
            this.callExpr := pruned;
        end if;

        -- prune all the arguments
        for a of this.args loop
            pruned := a.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(a) );
                a := pruned;
            end if;
        end loop;

        return null;
    end Prune;

    --==========================================================================

    function Create_Member_Call( loc      : Token_Location;
                                 callExpr : not null A_Ast_Expression ) return A_Ast_Function_Call is
        this : A_Ast_Member_Call;
    begin
        Assert( callExpr.Get_Type = MEMBERSHIP );

        this := new Ast_Member_Call;
        this.Construct( loc, callExpr );            -- consumes callExpr
        return A_Ast_Function_Call(this);
    end Create_Member_Call;

    ----------------------------------------------------------------------------

    overriding
    procedure Construct( this     : access Ast_Member_Call;
                         loc      : Token_Location;
                         callExpr : A_Ast_Expression ) is
        membershipOp : A_Ast_Membership := A_Ast_Membership(callExpr);
    begin
        Ast_Function_Call(this.all).Construct( loc, null );

        -- extract the member name and the expression from the Membership
        -- operator and enclose it in a Reference operator as the call
        -- call expression for the function. The Membership operator needs to be
        -- deleted after.
        --
        -- "obj.member(arg1)" => Member_Call( Reference( obj ), "member", arg1 )

        this.name := To_Unbounded_String( membershipOp.Get_Name );
        membershipOp.Take_Expression( this.callExpr );
        Delete( A_Ast_Node(membershipOp) );
        this.callExpr := A_Ast_Expression(Create_Reference( loc, this.callExpr ));
    end Construct;

    ----------------------------------------------------------------------------

    overriding
    function Get_Left( this : access Ast_Member_Call ) return A_Ast_Expression is (this.callExpr);

    ----------------------------------------------------------------------------

    function Get_Member_Name( this : not null access Ast_Member_Call'Class ) return String is (To_String( this.name ));

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Member_Call;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Member_Call( A_Ast_Member_Call(this) );
    end Process;

    --==========================================================================

    function Create_List( loc : Token_Location ) return A_Ast_Expression is
        this : constant A_Ast_List := new Ast_List;
    begin
        this.Construct( loc );
        return A_Ast_Expression(this);
    end Create_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_List ) is
    begin
        for e of this.elements loop
            Delete( A_Ast_Node(e) );
        end loop;
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    procedure Append( this    : not null access Ast_List'Class;
                      element : in out A_Ast_Expression ) is
    begin
        this.elements.Append( element );
        element := null;
    end Append;

    ----------------------------------------------------------------------------

    function Get_Element( this  : not null access Ast_List'Class;
                          index : Positive ) return A_Ast_Expression is
    begin
        if index <= Integer(this.elements.Length) then
            return this.elements.Element( index );
        end if;
        return null;
    end Get_Element;

    ----------------------------------------------------------------------------

    function Length( this : not null access Ast_List'Class ) return Natural is (Natural(this.elements.Length));

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_List;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_List( A_Ast_List(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_List ) return A_Ast_Expression is
        pruned     : A_Ast_Expression;
        allLiteral : Boolean := True;
    begin
        -- prune all the elements
        for e of this.elements loop
            pruned := e.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(e) );
                e := pruned;
            end if;
            allLiteral := allLiteral and then e.Get_Type = LITERAL;
        end loop;

        if not allLiteral then
            return null;
        end if;

        -- all elements are literals; create a literal list
        declare
            elems : Value_Array(1..Integer(this.elements.Length));
        begin
            for e in elems'Range loop
                elems(e) := A_Ast_Literal(this.elements.Element( e )).Get_Value;
            end loop;
            return Create_Literal( this.Location, Create_List( elems, consume => True ).Lst );
        end;
    end Prune;

    --==========================================================================

    function Create_Literal( loc : Token_Location; val : Value'Class ) return A_Ast_Expression is
        this : constant A_Ast_Literal := new Ast_Literal;
    begin
        this.Construct( loc, val );
        return A_Ast_Expression(this);
    end Create_Literal;

    ----------------------------------------------------------------------------

    not overriding
    procedure Construct( this : access Ast_Literal;
                         loc  : Token_Location;
                         val  : Value'Class ) is
    begin
        Ast_Expression(this.all).Construct( loc );
        this.val := Value(val);
    end Construct;

    ----------------------------------------------------------------------------

    function Get_Value( this : not null access Ast_Literal'Class ) return Value is (this.val);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Literal;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Literal( A_Ast_Literal(this) );
    end Process;

    --==========================================================================

    function Create_Map( loc : Token_Location ) return A_Ast_Expression is
        this : constant A_Ast_Map := new Ast_Map;
    begin
        this.Construct( loc );
        return A_Ast_Expression(this);
    end Create_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Delete( this : in out Ast_Map ) is
    begin
        for p of this.pairs loop
            Delete( A_Ast_Node(p.key) );
            Delete( A_Ast_Node(p.val) );
        end loop;
        Ast_Expression(this).Delete;
    end Delete;

    ----------------------------------------------------------------------------

    function Get_Key( this  : not null access Ast_Map'Class;
                      index : Positive ) return A_Ast_Expression is
    begin
        if index <= Integer(this.pairs.Length) then
            return this.pairs.Element( index ).key;
        end if;
        return null;
    end Get_Key;

    ----------------------------------------------------------------------------

    function Get_Value( this  : not null access Ast_Map'Class;
                        index : Positive ) return A_Ast_Expression is
    begin
        if index <= Integer(this.pairs.Length) then
            return this.pairs.Element( index ).val;
        end if;
        return null;
    end Get_Value;

    ----------------------------------------------------------------------------

    procedure Insert( this : not null access Ast_Map'Class;
                      key  : in out A_Ast_Expression;
                      val  : in out A_Ast_Expression ) is
    begin
        this.pairs.Append( Expr_Pair'(key, val) );
        key := null;
        val := null;
    end Insert;

    ----------------------------------------------------------------------------

    function Is_Empty( this : not null access Ast_Map'Class ) return Boolean is (this.pairs.Length = 0);

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Map;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Map( A_Ast_Map(this) );
    end Process;

    ----------------------------------------------------------------------------

    overriding
    function Prune( this : access Ast_Map ) return A_Ast_Expression is
        allLiteral : Boolean := True;
        pruned     : A_Ast_Expression;
        val        : Value;
        valueData  : Value;
        pragma Warnings( Off, valueData );  -- assigned but not read (yet)
        map        : Map_Value;
    begin
        -- prune all the keys and values
        for p of this.pairs loop
            pruned := p.key.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(p.key) );
                p.key := pruned;
            end if;
            pruned := p.val.Prune;
            if pruned /= null then
                Delete( A_Ast_Node(p.val) );
                p.val := pruned;
            end if;

            allLiteral := allLiteral and then
                          p.key.Get_Type = LITERAL and then
                          p.val.Get_Type = LITERAL;
        end loop;

        if not allLiteral then
            return null;    -- the map will need to be evaluated
        end if;

        -- NOTE: the following comments and code about text serialized values
        -- are not implemented. they are left in here for future use if Values
        -- need to be written in a text format even though some value types do
        -- not have a direct Scribble syntax.

        -- check to see if the map is actually a serialized value. several value
        -- classes contain binary data (e.g. functions, etc.) that must be
        -- serialized using other "text-friendly" values in order to represent
        -- their contents when serialized into a text-based format.
        --
        -- these special maps are created by the Write_Serialized methods of the
        -- Value classes. they contain only two keys: "#type" and "#value". the
        -- "#type" key identifies the type of the serialized value (its
        -- serialized id, or SID), and the "#value" key contains the serialized
        -- value itself.
        --
        -- {"#type":"<SerializedIdOfValueType>", "#value":<ValueContent>}

--      declare
--          typeIndex  : Integer := 0;
--          valueIndex : Integer := 0;
--          sid        : Value;
--      begin
--          if this.pairs.Length = 2 then   -- serialized values should have only the two keys
--              -- search for the "#type" and "#value" keys ('pairs' is unordered)
--              for i in 1..Integer(this.pairs.Length) loop
--                  if typeIndex < 1 and then Cast_String( A_Ast_Literal(this.pairs.Element( i ).key).Get_Value ) = "#type" then
--                      typeIndex := i;
--                      exit when valueIndex > 0;      -- found both keys
--                  elsif valueIndex < 1 and then Cast_String( A_Ast_Literal(this.pairs.Element( i ).key).Get_Value ) = "#value" then
--                      valueIndex := i;
--                      exit when typeIndex > 0;       -- found both keys
--                  end if;
--              end loop;
--
--              -- check if both the "#type" and "#value" keys were found
--              if typeIndex > 0 and then valueIndex > 0 then
--                  sid := A_Ast_Literal(this.pairs.Element( typeIndex ).val).Get_Value;
--                  valueData := A_Ast_Literal(this.pairs.Element( valueIndex ).val).Get_Value;
--                  if sid.Is_String then
--                      if sid.Str.To_String = "num" then
--                          val := Create_Number_From_Serialized( valueData );
--                      elsif sid.Str.To_String = "func" then
--                          val := Create_Function_From_Serialized( valueData );
--                      elsif sid.Str.To_String = "error" then
--                          val := Create_Error_From_Serialized( valueData );
--                      end if;
--                  end if;
--              end if;
--          end if;
--      end;

--      if val.Is_Null then
            -- replace the Ast_Map with a literal map
            map := Create_Map.Map;
            for i in 1..Integer(this.pairs.Length) loop
                map.Set( A_Ast_Literal(this.pairs.Element( i ).key).Get_Value.Str.To_Unbounded_String,
                         A_Ast_Literal(this.pairs.Element( i ).val).Get_Value,
                         consume => True );
            end loop;
            val := Value(map);
--      end if;

        return Create_Literal( this.Location, val );
    end Prune;

    ----------------------------------------------------------------------------

    function Length( this : not null access Ast_Map'Class ) return Natural is (Natural(this.pairs.Length));

    --==========================================================================

    function Create_Self( loc : Token_Location ) return A_Ast_Expression is
        this : constant A_Ast_Self := new Ast_Self;
    begin
        this.Construct( loc );
        return A_Ast_Expression(this);
    end Create_Self;

    ----------------------------------------------------------------------------

    overriding
    procedure Process( this      : access Ast_Self;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class ) is
    begin
        processor.Process_Self( A_Ast_Self(this) );
    end Process;

end Scribble.Ast.Expressions;
