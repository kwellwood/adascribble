--
-- Copyright (c) 2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Scribble.Token_Locations;          use Scribble.Token_Locations;

package Scribble.Parse_Errors is

    -- Displays a Parse_Error exception's message into a nicely formatted
    -- multi-line string showing the error cross-referenced with its location
    -- in source code. The returned string follows the format:
    --
    --   <filepath>:<row>:<col>: <message>
    --     <source-line>                                  (Indented 2 spaces)
    --   ........^
    --
    -- Where <source-code> is the line of source referenced by the error
    -- location, and the the carat points to the offending column.
    --
    -- If the parsed source code was not read from a file, then it can be
    -- provided, in its entirety, as 'source'. 'omitLines' and 'omitCols' will
    -- be subtracted from the reported error location. This allows additional
    -- lines of source to prefixed for compilation but exluded from reported
    -- error locations, for convenience. For example, the user may provide a
    -- Scribble expression that is silently wrapped into a Scribble function by
    -- prefixing and postfixing additional lines of source code. The silently
    -- prefixed line(s) of source can be removed with 'omitLines' so that errors
    -- in the first line of source provided by the user are displayed as if they
    -- occured on line 1.
    --
    -- If the source was parsed from a file or is not available, leave 'source'
    --- empty; 'omitLines' and 'omitCols' will also be ignored.
    function Format_Parse_Error( emsg      : String;
                                 source    : Unbounded_String := Null_Unbounded_String;
                                 omitLines : Natural := 0;
                                 omitCols  : Natural := 0
                               ) return String;

    -- Raises a Parse_Error exception with a specially formatted message. The
    -- string follows the format (where [LF] is a linefeed character):
    --
    --   <filepath>:<row>:<col>[LF]<msg>
    --
    -- If the error occurred in a source file included by another source, then
    -- Raise_Parse_Error() can be called again directly on the Parse_Error's
    -- exception message to re-raise and add the location of the reference:
    --
    --   <filepath>:<row>:<col>[LF]<filepath>:<row>:<col>[LF]<msg>
    --
    procedure Raise_Parse_Error( msg : String; loc : Token_Location );

end Scribble.Parse_Errors;
