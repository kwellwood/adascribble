--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Maps is

    type Pair_Type is
        record
            key : Unbounded_String;
            val : Value;
        end record;

    function Pair( key : String; val : Value'Class ) return Pair_Type
        is (Pair_Type'(To_Unbounded_String( key ), Value(val)));

    type Pair_Array is array (Integer range <>) of Pair_Type;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Creates a new, empty Map.
    function Create_Map return Value;

    -- Creates a new Map from a literal array of key/value pairs. The values
    -- will be copied into the map by default, unless 'consume' is set to True.
    -- If 'consume' is True, the caller should discard these values after the
    -- call. 'pairs' must not contain duplicate keys, otherwise an exception
    -- will be raised.
    function Create( pairs : Pair_Array; consume : Boolean := False ) return Value;

    function Create_Map( pairs : Pair_Array; consume : Boolean := False ) return Value renames Create;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type Map_Value is new Value with private;

    -- Returns a Map_Value pointing to the data of 'this'. The data is never
    -- copied.
    function As_Map( this : Value'Class ) return Map_Value;

    -- Returns True if 'field' is a key defined in the map.
    function Contains( this : Map_Value'Class; field : Unbounded_String ) return Boolean with Inline;
    pragma Precondition( this.Valid );

    function Contains( this : Map_Value'Class; field : String ) return Boolean
        is (this.Contains( To_Unbounded_String( field ) )) with Inline_Always;

    -- Returns the value associated with the key named 'field', or Null if the
    -- key is not defined. The returned value is not a copy; the caller is
    -- reponsible for copying it as necessary.
    function Get( this : Map_Value'Class; field : Unbounded_String ) return Value;
    pragma Precondition( this.Valid );

    function Get( this : Map_Value'Class; field : String ) return Value is (this.Get( To_Unbounded_String( field ) ));

    -- Returns the value associated with the key named 'field', cast to a
    -- specific type. If the key is undefined or its value does not match the
    -- requested type, 'default' will be returned.
    function Get_Boolean( this : Map_Value'Class; field : Unbounded_String; default : Boolean := False ) return Boolean with Inline;
    function Get_Boolean( this : Map_Value'Class; field : String;           default : Boolean := False ) return Boolean with Inline;
    function Get_Int( this : Map_Value'Class; field : Unbounded_String; default : Integer := 0 ) return Integer with Inline;
    function Get_Int( this : Map_Value'Class; field : String;           default : Integer := 0 ) return Integer with Inline;
    function Get_Float( this : Map_Value'Class; field : Unbounded_String; default : Float := 0.0 ) return Float with Inline;
    function Get_Float( this : Map_Value'Class; field : String;           default : Float := 0.0 ) return Float with Inline;
    function Get_Long_Float( this : Map_Value'Class; field : Unbounded_String; default : Long_Float := 0.0 ) return Long_Float with Inline;
    function Get_Long_Float( this : Map_Value'Class; field : String;           default : Long_Float := 0.0 ) return Long_Float with Inline;
    function Get_String( this : Map_Value'Class; field : Unbounded_String; default : String := "" ) return String;
    function Get_String( this : Map_Value'Class; field : String;           default : String := "" ) return String;
    function Get_Unbounded_String( this : Map_Value'Class; field : Unbounded_String; default : String := "" ) return Unbounded_String with Inline;
    function Get_Unbounded_String( this : Map_Value'Class; field : String;           default : String := "" ) return Unbounded_String with Inline;
    function Get_Unbounded_String( this : Map_Value'Class; field : Unbounded_String; default : Unbounded_String ) return Unbounded_String with Inline;
    function Get_Unbounded_String( this : Map_Value'Class; field : String;           default : Unbounded_String ) return Unbounded_String with Inline;

    -- Returns a list of the keys in the map. The order of the keys is undefined.
    function Get_Keys( this : Map_Value'Class ) return Value;
    pragma Precondition( this.Valid );

    -- Returns True if any value in the map matches 'val'.
    function Has_Value( this : Map_Value'Class; val : Value'Class ) return Boolean;
    pragma Precondition( this.Valid );

    -- Returns True if there are no keys defined in the map.
    function Is_Empty( this : Map_Value'Class ) return Boolean with Inline;
    pragma Precondition( this.Valid );

    -- Iterates across each key/value pair in the map in an undefined order,
    -- calling 'examine' once for each pair. The value passed to 'examine' is
    -- not a copy; 'examine' is responsible for copying 'val' as necessary.
    procedure Iterate( this    : Map_Value'Class;
                       examine : not null access procedure( key : String;
                                                            val : Value ) );
    pragma Precondition( this.Valid );

    -- Merges the keys from map 'from' into this one, by copy. On key conflicts,
    -- the value of 'from' will override.
    procedure Merge( this : Map_Value'Class; from : Value'Class );
    pragma Precondition( this.Valid );
    pragma Precondition( from.Is_Map or else from.Is_Null );

    -- Creates a new map value by recursively merging maps 'base' and
    -- 'overrides'. The algorithm works the same as the Merge procedure, where
    -- values in 'overrides' override matching values in 'base', but a new map
    -- Value is produced instead.
    function Merge( base : Map_Value'Class; overrides : Value'Class ) return Value;
    pragma Precondition( base.Valid );
    pragma Precondition( overrides.Is_Map or else overrides.Is_Null );

    -- Recursively merges the key/value pairs from map 'from' into this one, by
    -- copy. This procedure is similar to Merge, but the values of matching keys
    -- within this and 'from' will be recursively merged (into this) if they are
    -- both map values, instead of 'from' simply overriding. On key conflicts
    -- where not both values are maps, the value of 'from' will still override.
    --
    -- Example:
    -- this := {"a":1, "b":{"c":2, "d":3}, "e":4}
    -- this.Recursive_Merge( from => {"a":{}, "b":{"d":true, "f":false}} );
    -- this = {"a":{}, "b":{"c":2, "d":true, "f":false}, "e":4}
    procedure Recursive_Merge( this : Map_Value'Class; from : Value'Class );
    pragma Precondition( this.Valid );
    pragma Precondition( from.Is_Map or else from.Is_Null );

    -- Creates a new map value by recursively merging maps 'base' and
    -- 'overrides'. The algorithm works the same as the Recursive_Merge
    -- procedure, where values in 'overrides' override matching values in
    -- 'base', but a new map Value is produced instead.
    function Recursive_Merge( base : Map_Value'Class; overrides : Value'Class ) return Value;
    pragma Precondition( base.Valid );
    pragma Precondition( overrides.Is_Map or else overrides.Is_Null );

    -- Returns a reference to the value associated with key 'field'. If
    -- 'createMissing' is True, the key will be defined with a value of Null if
    -- it does not already exist. Otherwise, Null will be returned to indicate
    -- 'field' is undefined.
    --
    -- This function should be used only to set values in the map. If it is used
    -- to query a non-existant key, the key may become defined and associated
    -- with the Null value (which is not allowed) and the pair will stay in the
    -- map until it is overwritten or explicitly removed.
    function Reference( this          : Map_Value'Class;
                        field         : Unbounded_String;
                        createMissing : Boolean := True ) return Value;
    pragma Precondition( this.Valid );

    function Reference( this          : Map_Value'Class;
                        field         : String;
                        createMissing : Boolean := True ) return Value
        is (this.Reference( To_Unbounded_String( field ), createMissing ));

    -- Removes the key named 'field', if it is defined.
    procedure Remove( this : Map_Value'Class; field : Unbounded_String );
    pragma Precondition( this.Valid );

    procedure Remove( this : Map_Value'Class; field : String );
    pragma Precondition( this.Is_Map );

    -- Sets or updates the value associated with the key named 'field' in the
    -- map. If 'val' is Null, then the key will be removed. The given value will
    -- be copied, unless 'consume' is set True. If 'val' is consumed, the caller
    -- should discard it after the call.
    procedure Set( this    : Map_Value'Class;
                   field   : Unbounded_String;
                   val     : Value'Class;
                   consume : Boolean := False );
    pragma Precondition( this.Valid );

    procedure Set( this    : Map_Value'Class;
                   field   : String;
                   val     : Value'Class;
                   consume : Boolean := False ) with Inline;

    -- Returns the number of keys in the map.
    function Size( this : Map_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Valid );

    function Valid( this : Map_Value'Class ) return Boolean is (this.Is_Map);

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Clone_Map( this : Value'Class ) return Value;
    pragma Precondition( this.Is_Map );

    function Compare_Maps( left, right : Value'Class ) return Integer;
    pragma Precondition( left.Is_Map );
    pragma Precondition( right.Is_Map );

    procedure Delete_Map( obj : Address );

    function Image_Map( this : Value'Class ) return String;
    pragma Precondition( this.Is_Map );

    procedure Read_Map( stream : access Root_Stream_Type'Class;
                        this   : in out Value'Class );
    pragma Precondition( this.Is_Map );

    procedure Write_Map( stream : access Root_Stream_Type'Class;
                         this   : Value'Class );
    pragma Precondition( this.Is_Map );

private

    type Map_Data;
    type A_Map_Data is access all Map_Data;
    pragma No_Strict_Aliasing( A_Map_Data );

    type Map_Value is new Value with
        record
            data : A_Map_Data := null;
        end record;

end Values.Maps;
