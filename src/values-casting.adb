--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Equal_Case_Insensitive;
with Values.Strings;                    use Values.Strings;

package body Values.Casting is

    function Cast_Boolean( val : Value'Class; default : Boolean := False ) return Boolean
        is (if val.Is_Boolean then val.To_Boolean else default);

    ----------------------------------------------------------------------------

    function Cast_Enum( val : Value'Class; default : Enumeration ) return Enumeration is
        result : Enumeration;
    begin
        if val.Is_String then
            begin
                result := Enumeration'Value( val.Str.To_String );
                if result'Valid then
                    return result;
                end if;
            exception
                when others =>
                    -- Enumeration'Value threw exception on invalid enumeration value
                    null;
            end;
        elsif val.Is_Number then
            result := Enumeration'Val( val.To_Int );
            if result'Valid then
                return result;
            end if;
        end if;
        return default;
    end Cast_Enum;

    ----------------------------------------------------------------------------

    function Cast_Float( val : Value'Class; default : Float := 0.0 ) return Float
        is (if val.Is_Number then val.To_Float else default);

    ----------------------------------------------------------------------------

    function Cast_Long_Float( val : Value'Class; default : Long_Float := 0.0 ) return Long_Float
        is (if val.Is_Number then val.To_Long_Float else default);

    ----------------------------------------------------------------------------

    function Cast_Id( val : Value'Class; default : Id_Type := 0 ) return Id_Type
        is (if val.Is_Id then Id_Type(val.To_Id) else default);

    ----------------------------------------------------------------------------

    function Cast_Int( val : Value'Class; default : Integer := 0 ) return Integer
        is (if val.Is_Number then val.To_Int else default);

    ----------------------------------------------------------------------------

    function Cast_String( val : Value'Class; default : String := "" ) return String
        is (if val.Is_String then val.Str.To_String else default);

    ----------------------------------------------------------------------------

    function Coerce_Boolean( val : Value'Class; default : Boolean := False ) return Boolean is
    begin
        if val.Is_Boolean then
            return val.To_Boolean;
        elsif val.Is_Number then
            return val.To_Float /= 0.0;
        elsif val.Is_String then
            if Ada.Strings.Equal_Case_Insensitive( val.Str.To_String, "true" ) then
                return True;
            elsif Ada.Strings.Equal_Case_Insensitive( val.Str.To_String, "false" ) then
                return False;
            end if;
        end if;
        return default;
    end Coerce_Boolean;

    ----------------------------------------------------------------------------

    function Coerce_Int( val : Value'Class; default : Integer := 0 ) return Integer is
    begin
        if val.Is_Number then
            return val.To_Int;
        elsif val.Is_Boolean then
            return (if val.To_Boolean then 1 else 0);
        elsif val.Is_String then
            begin
                return Integer'Value( val.Str.To_String );
            exception
                when others => null;
            end;
        end if;
        return default;
    end Coerce_Int;

    ----------------------------------------------------------------------------

    function Coerce_String( val : Value'Class; default : String := "" ) return String is
    begin
        if val.Is_String then
            return val.Str.To_String;
        elsif val.Is_Number  or else
              val.Is_Boolean or else
              val.Is_Null
        then
            return val.Image;
        end if;
        return default;
    end Coerce_String;

end Values.Casting;
