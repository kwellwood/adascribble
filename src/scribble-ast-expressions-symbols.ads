--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Scribble.Ast.Expressions.Symbols is

    -- Ast_Symbol is an expression node that represents a variable reference,
    -- for read or write. Its value can only be determined at runtime.
    type Ast_Symbol is abstract new Ast_Expression with private;
    type A_Ast_Symbol is access all Ast_Symbol'Class;

    -- Returns True because symbols are allowed in a reference expression. This
    -- does not mean he
    overriding
    function Can_Produce_Reference( this : access Ast_Symbol ) return Boolean is (True);

    overriding
    function Get_Type( this : access Ast_Symbol ) return Ast_Type is abstract;

    -- Returns True if the Symbol is at the root of a reference expression.
    function Is_Reference_Expression( this : not null access Ast_Symbol'Class ) return Boolean;

    ----------------------------------------------------------------------------

    -- Ast_Identifier is a symbol that references a parameter/local variable or
    -- a static namespace value. Depending on scope and available namespaces,
    -- an identifier may reference either a local variable or anonymous
    -- namespace, since the cases are syntactically identifical.
    type Ast_Identifier is new Ast_Symbol with private;
    type A_Ast_Identifier is access all Ast_Identifier'Class;

    -- Creates a new Ast_Identifier referencing variable 'name', found in
    -- Namespace 'namespace'. If 'namespace' is an empty string, the identifier
    -- may reference a parameter or local variable, or the anonymous namespace.
    function Create_Identifier( loc       : Token_Location;
                                name      : String;
                                namespace : String := "" ) return A_Ast_Identifier;
    pragma Precondition( name'Length > 0 );
    pragma Postcondition( Create_Identifier'Result /= null );

    -- Returns the name of the identifier.
    function Get_Name( this : not null access Ast_Identifier'Class ) return String;

    -- Returns the name of the identifier's namespace.
    function Get_Namespace( this : not null access Ast_Identifier'Class ) return String;

    overriding
    function Get_Type( this : access Ast_Identifier ) return Ast_Type is (IDENTIFIER);

    -- Returns True if the variable does not have a namespace specified. It may
    -- be a local variable/parameter, or implicitly a name in the anonymous
    -- namespace, depending on the identifier's scope.
    function No_Namespace( this : not null access Ast_Identifier'Class ) return Boolean;

    procedure Process( this      : access Ast_Identifier;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_This is a symbol that represents the implicit 'this' parameter of a
    -- member function. Its value value is that of the first argument to a
    -- member function. The 'this' keyword can only be used in a member function.
    type Ast_This is new Ast_Symbol with private;
    type A_Ast_This is access all Ast_This'Class;

    -- Creates a new Ast_Symbol referencing the current member function's
    -- implicit 'this' argument.
    function Create_This( loc : Token_Location ) return A_Ast_Expression;
    pragma Postcondition( Create_This'Result /= null );

    overriding
    function Get_Type( this : access Ast_This ) return Ast_Type is (THIS_ARG);

    procedure Process( this      : access Ast_This;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    ----------------------------------------------------------------------------

    -- Ast_Membership is an expression node that represents a member access
    -- operation, accessing named member value with a static name
    -- (e.g.: value.name).
    type Ast_Membership is new Ast_Symbol with private;
    type A_Ast_Membership is access all Ast_Membership'Class;

    -- Creates a new Ast_Membership operation on an expression with a name.
    -- 'left' must be evaluated to produce the value containing the member to be
    -- accessed. 'left' will be consumed even though it is not set to null.
    function Create_Membership( loc  : Token_Location;
                                left : not null A_Ast_Expression;
                                name : String ) return A_Ast_Expression;
    pragma Postcondition( Create_Membership'Result /= null );

    -- Returns a reference to the left-hand expression. Ownership is maintained
    -- by the Ast_Membership.
    function Get_Expression( this : not null access Ast_Membership'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_Expression'Result /= null );

    -- Returns the static member name.
    function Get_Name( this : not null access Ast_Membership'Class ) return String;

    overriding
    function Get_Type( this : access Ast_Membership ) return Ast_Type is (MEMBERSHIP);

    procedure Process( this      : access Ast_Membership;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Membership ) return A_Ast_Expression;

    -- Transfers ownership of the membership operator's left hand expression to
    -- the caller. This membership object will discard its reference to it. This
    -- is used when creating a new Ast_Member_Call from a Function_Call and its
    -- Membership call expression.
    procedure Take_Expression( this : not null access Ast_Membership'Class;
                               expr : in out A_Ast_Expression );
    pragma Precondition( expr = null );

    ----------------------------------------------------------------------------

    -- Ast_Name_Expr is a symbol that represents a dynamic namespace name
    -- reference, where the name being referenced is resolved at runtime.
    type Ast_Name_Expr is new Ast_Symbol with private;
    type A_Ast_Name_Expr is access all Ast_Name_Expr'Class;

    -- Creates a new Ast_Name_Expr operation to reference a yet-to-be-determined
    -- name in namespace 'namespace'. The namespace may not be anonymous. The
    -- name expression 'name' will be consumed even though it is not set to null.
    function Create_Name_Expr( loc       : Token_Location;
                               namespace : String;
                               name      : not null A_Ast_Expression ) return A_Ast_Expression;
    pragma Precondition( namespace'Length > 0 );
    pragma Postcondition( Create_Name_Expr'Result /= null );

    -- Returns a reference to the name expression. Ownership is maintained by
    -- the Ast_Name_Expr.
    function Get_Name( this : not null access Ast_Name_Expr'Class ) return A_Ast_Expression;
    pragma Postcondition( Get_Name'Result /= null );

    -- Returns the name of the namespace being referenced.
    function Get_Namespace( this : not null access Ast_Name_Expr'Class ) return String;
    pragma Postcondition( Get_Namespace'Result'Length > 0 );

    overriding
    function Get_Type( this : access Ast_Name_Expr ) return Ast_Type is (NAME_EXPRESSION);

    procedure Process( this      : access Ast_Name_Expr;
                       processor : access Scribble.Ast.Processors.Ast_Processor'Class );

    function Prune( this : access Ast_Name_Expr ) return A_Ast_Expression;

private

    type Ast_Symbol is abstract new Ast_Expression with null record;

    ----------------------------------------------------------------------------

    type Ast_Identifier is new Ast_Symbol with
        record
            name      : Unbounded_String;
            namespace : Unbounded_String;
        end record;

    procedure Construct( this      : access Ast_Identifier;
                         loc       : Token_Location;
                         name      : String;
                         namespace : String );

    ----------------------------------------------------------------------------

    type Ast_This is new Ast_Symbol with null record;

    ----------------------------------------------------------------------------

    type Ast_Membership is new Ast_Symbol with
        record
            left : A_Ast_Expression := null;
            name : Unbounded_String;
        end record;

    procedure Construct( this : access Ast_Membership;
                         loc  : Token_Location;
                         left : not null A_Ast_Expression;
                         name : String );

    procedure Delete( this : in out Ast_Membership );

    ----------------------------------------------------------------------------

    type Ast_Name_Expr is new Ast_Symbol with
        record
            namespace : Unbounded_String;
            name      : A_Ast_Expression;
        end record;

    procedure Construct( this      : access Ast_Name_Expr;
                         loc       : Token_Location;
                         namespace : String;
                         name      : not null A_Ast_Expression );

    procedure Delete( this : in out Ast_Name_Expr );

end Scribble.Ast.Expressions.Symbols;
