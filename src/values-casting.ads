--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Casting is

    pragma Elaborate_Body;

    function Cast_Boolean( val : Value'Class; default : Boolean := False ) return Boolean with Inline;

    generic
        type Enumeration is (<>);
    function Cast_Enum( val : Value'Class; default : Enumeration ) return Enumeration;

    function Cast_Float( val : Value'Class; default : Float := 0.0 ) return Float with Inline;

    function Cast_Long_Float( val : Value'Class; default : Long_Float := 0.0 ) return Long_Float with Inline;

    generic
        type Id_Type is new Unsigned_64;
    function Cast_Id( val : Value'Class; default : Id_Type := 0 ) return Id_Type;

    function Cast_Int( val : Value'Class; default : Integer := 0 ) return Integer with Inline;

    function Cast_String( val : Value'Class; default : String := "" ) return String with Inline;

    function Cast_Unbounded_String( val     : Value'Class;
                                    default : String := "" ) return Unbounded_String
    is (To_Unbounded_String( Cast_String( val, default ) ));

    function Cast_Unbounded_String( val     : Value'Class;
                                    default : Unbounded_String ) return Unbounded_String
    is (To_Unbounded_String( Cast_String( val, To_String( default ) ) ));

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Attempts to coerce 'val' into a boolean. Supported value types are
    -- numbers (0 = false, non-zero = true), booleans, and strings ("false" is
    -- false, and "true" is true, case insensitive). 'default' will be returned
    -- if 'val' could not be coerced.
    function Coerce_Boolean( val : Value'Class; default : Boolean := False ) return Boolean;

    -- Attempts to coerce 'val' into an integer. Supported value types are
    -- numbers, booleans, and strings. 'default' will be returned if 'val' could
    -- not be coerced.
    function Coerce_Int( val : Value'Class; default : Integer := 0 ) return Integer;

    -- Attempts to coerce 'val' into a string. Supported value types are numbers,
    -- booleans, nulls and strings.
    function Coerce_String( val : Value'Class; default : String := "" ) return String;

end Values.Casting;
