--
-- Copyright (c) 2014-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Values.Construction;               use Values.Construction;

package body Scribble.Namespaces.Simple is

    function Create_Simple_Namespace( contents : Map_Value := Create_Map.Map;
                                      consume  : Boolean := True ) return A_Simple_Namespace is
        this : constant A_Simple_Namespace := new Simple_Namespace;
    begin
        if contents.Valid then
            this.namespace := (if consume then contents else Clone( contents ).Map);
        else
            this.namespace := Create_Map.Map;
        end if;
        return this;
    end Create_Simple_Namespace;

    ----------------------------------------------------------------------------

    procedure Add_Contents( this  : not null access Simple_Namespace'Class;
                            names : Map_Value ) is

        procedure Add_Name( name : String; val : Value ) is
        begin
            this.Set_Name( name, val );
        end Add_Name;

    begin
        if not this.readonly and then names.Valid then
            names.Iterate( Add_Name'Access );
        end if;
    end Add_Contents;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Name( this : access Simple_Namespace;
                                 name : String ) return Value is
        result : Value;
    begin
        result := this.namespace.Get( name );
        if not this.readonly or else not result.Is_Null then
            return result;
        end if;

        -- name is undefined and can't be defined now
        return Null_Value;
    end Get_Namespace_Name;

    ----------------------------------------------------------------------------

    overriding
    function Get_Namespace_Ref( this : access Simple_Namespace;
                                name : String ) return Value
    is (this.namespace.Reference( name, createMissing => not this.readonly ));

    ----------------------------------------------------------------------------

    function Is_Readonly( this  : not null access Simple_Namespace'Class ) return Boolean is (this.readonly);

    ----------------------------------------------------------------------------

    procedure Set_Contents( this     : not null access Simple_Namespace'Class;
                            contents : Map_Value ) is
    begin
        if not this.readonly then
            this.namespace := Clone( contents ).Map;
            if not this.namespace.Valid then
                this.namespace := Create_Map.Map;
            end if;
        end if;
    end Set_Contents;

    ----------------------------------------------------------------------------

    procedure Set_Name( this    : not null access Simple_Namespace'Class;
                        name    : String;
                        val     : Value'Class;
                        consume : Boolean := False ) is
    begin
        if not this.readonly then
            this.namespace.Set( name, val, consume );
        end if;
    end Set_Name;

    ----------------------------------------------------------------------------

    procedure Set_Readonly( this     : not null access Simple_Namespace'Class;
                            readonly : Boolean ) is
    begin
        this.readonly := readonly;
    end Set_Readonly;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Simple_Namespace ) is
        procedure Free is new Ada.Unchecked_Deallocation( Simple_Namespace'Class,
                                                          A_Simple_Namespace );
    begin
        Free( this );
    end Delete;

end  Scribble.Namespaces.Simple;
