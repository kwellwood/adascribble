--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Finalization;                  use Ada.Finalization;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Address_To_Access_Conversions;

limited with Values.Errors;
limited with Values.Functions;
limited with Values.Lists;
limited with Values.Maps;
limited with Values.Strings;

package Values is

    -- Defines the first class value types
    type Value_Type is
    (
        V_NULL,
        V_BOOLEAN,
        V_NUMBER,
        V_STRING,
        V_COLOR,
        V_ID,
        V_LIST,
        V_MAP,
        V_FUNCTION,
        V_REFERENCE,
        V_ERROR
    );

    type Type_Array is array (Integer range <>) of Value_Type;

    function Image( vt : Value_Type ) return String;

    ----------------------------------------------------------------------------

    -- A Value represents a generic value of one of the primitive types. The
    -- value can be converted to other types in some cases. Instances of the
    -- Value class can be copied and written to a stream.
    type Value is tagged private;
    type A_Value is access all Value;
    pragma No_Strict_Aliasing( A_Value );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Misc Operations

    -- Compares 'this' to 'other', returning -1 if this is less than 'other',
    -- zero if this equals 'other', and 1 if this is greater than 'other'. If
    -- 'this' and 'other' are of different value types, then the comparison is
    -- based solely on the values' types.
    function Compare( this : Value'Class; other : Value'Class ) return Integer;

    -- Returns the value's type as described in the Value_Type enumeration.
    function Get_Type( this : Value'Class ) return Value_Type with Inline;

    -- Returns a depiction of the value as a string.
    function Image( this : Value'Class ) return String;

    -- Tests the type of a value.
    function Is_Null    ( this : Value'Class ) return Boolean with Inline;
    function Is_Boolean ( this : Value'Class ) return Boolean with Inline;
    function Is_Number  ( this : Value'Class ) return Boolean with Inline;
    function Is_String  ( this : Value'Class ) return Boolean with Inline;
    function Is_Color   ( this : Value'Class ) return Boolean with Inline;
    function Is_Id      ( this : Value'Class ) return Boolean with Inline;
    function Is_List    ( this : Value'Class ) return Boolean with Inline;
    function Is_Map     ( this : Value'Class ) return Boolean with Inline;
    function Is_Function( this : Value'Class ) return Boolean with Inline;
    function Is_Ref     ( this : Value'Class ) return Boolean with Inline;
    function Is_Error   ( this : Value'Class ) return Boolean with Inline;

    -- Relational operators based on Compare()
    overriding
    function "="( this : Value; other : Value ) return Boolean;
    function "<"( this : Value'Class; other : Value'Class ) return Boolean;
    function ">"( this : Value'Class; other : Value'Class ) return Boolean;
    function "<="( this : Value'Class; other : Value'Class ) return Boolean;
    function ">="( this : Value'Class; other : Value'Class ) return Boolean;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Nulls

    -- The global singleton Null value.
    Null_Value : constant Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Booleans

    -- Returns True if the value is a boolean True, otherwise false.
    function To_Boolean( this : Value'Class ) return Boolean with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Numbers

    -- Returns True if the value is a whole integer.
    function Is_Int( this : Value'Class ) return Boolean with Inline;
    pragma Precondition( this.Is_Number );

    -- Returns the value as a 32-bit float.
    function To_Float( this : Value'Class ) return Float with Inline;
    pragma Precondition( this.Is_Number );

    -- Returns the value as a 32-bit integer.
    function To_Int( this : Value'Class ) return Integer with Inline;
    pragma Precondition( this.Is_Number );

    -- Returns the value as a 64-bit long float.
    function To_Long_Float( this : Value'Class ) return Long_Float with Inline;
    pragma Precondition( this.Is_Number );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Identifiers

    -- Returns the value as a 48-bit identifier.
    function To_Id( this : Value'Class ) return Unsigned_64 with Inline;
    pragma Precondition( this.Is_Id );
    pragma Postcondition( To_Id'Result < 2 ** 48 );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- References

    -- Returns the target value being referenced.
    function Deref( this : Value'Class ) return Value'Class;
    pragma Precondition( this.Is_Ref );

    -- Overwrites the target value, in-place, with 'val'.
    procedure Replace( this : Value'Class; val : Value'Class );
    pragma Precondition( this.Is_Ref );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Color

    -- Returns the red, green and blue channels of the color value.
    procedure To_RGB( this : Value'Class; r, g, b : out Float ) with Inline;
    pragma Precondition( this.Is_Color );

    -- Returns the red, green, blue and alpha channels of the color value.
    procedure To_RGBA( this : Value'Class; r, g, b, a : out Float ) with Inline;
    pragma Precondition( this.Is_Color );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    -- Object Subclasses

    function Err( this : Value'Class ) return Values.Errors.Error_Value;

    function Func( this : Value'Class ) return Values.Functions.Function_Value;

    function Lst( this : Value'Class ) return Values.Lists.List_Value;

    function Map( this : Value'Class ) return Values.Maps.Map_Value;

    function Str( this : Value'Class ) return Values.Strings.String_Value;

    ----------------------------------------------------------------------------

    type Value_Array is array (Positive range <>) of Value;
    type A_Value_Array is access all Value_Array;

    Empty_Array : constant Value_Array;

    procedure Delete( va : in out A_Value_Array );

    -- The stream format for Value types. This is updated each time a binary
    -- representation change is made in the streaming functions.
    STREAM_VERSION_READ  : constant := 8;
    STREAM_VERSION_WRITE : constant := 8;

private

    --                                             quiet
    --                                 NaN         |     payload(value)
    QNAN : constant Unsigned_64 := 2#0_11111111111_1_000_000000000000000000000000000000000000000000000000#;
    --                               |               |
    --                               object          type

    PAYLOAD_OFFSET : constant := 2**0;
    PAYLOAD_SIZE   : constant := 48;
    PAYLOAD_MASK   : constant Unsigned_64 := ((2**PAYLOAD_SIZE) - 1) * PAYLOAD_OFFSET;

    TYPE_OFFSET  : constant := 2**PAYLOAD_SIZE;
    TYPE_SIZE    : constant := 3;
    TYPE_MASK    : constant Unsigned_64 := ((2**TYPE_SIZE) - 1) * TYPE_OFFSET;

    OBJECT_OFFSET : constant := 2**63;
    OBJECT_SIZE   : constant := 1;
    OBJECT_MASK   : constant Unsigned_64 := ((2**OBJECT_SIZE) - 1) * OBJECT_OFFSET;

    BOXED_TYPE     : constant Unsigned_64 := (2#0# * OBJECT_OFFSET) or QNAN;
    OBJECT_TYPE    : constant Unsigned_64 := (2#1# * OBJECT_OFFSET) or QNAN;

    -- Value types
    -- the upper bit is the object flag in a value. the lower 3 bits are the
    -- type field in a value. combined, the bits uniquely identify a value type.
    SINGLETON_TYPE : constant := 2#0_000#;
    ID_TYPE        : constant := 2#0_001#;
    REF_TYPE       : constant := 2#0_010#;
    COLOR_TYPE     : constant := 2#0_011#;
    STRING_TYPE    : constant := 2#1_000#;
    LIST_TYPE      : constant := 2#1_001#;
    MAP_TYPE       : constant := 2#1_010#;
    FUNCTION_TYPE  : constant := 2#1_011#;
    ERROR_TYPE     : constant := 2#1_100#;
    INDIRECT_TYPE  : constant := 2#1_101#;

    -- Singleton value types, of which there is only one value
    SINGLETON : constant Unsigned_64 := BOXED_TYPE or (TYPE_MASK and (SINGLETON_TYPE * TYPE_OFFSET));
    TAG_NULL  : constant Unsigned_64 := SINGLETON or 0;
    TAG_FALSE : constant Unsigned_64 := SINGLETON or 1;
    TAG_TRUE  : constant Unsigned_64 := SINGLETON or 2;

    -- Boxed value types
    TAG_ID    : constant Unsigned_64 := BOXED_TYPE or (TYPE_MASK and (ID_TYPE  * TYPE_OFFSET));
    TAG_REF   : constant Unsigned_64 := BOXED_TYPE or (TYPE_MASK and (REF_TYPE * TYPE_OFFSET));
    TAG_COLOR : constant Unsigned_64 := BOXED_TYPE or (TYPE_MASK and (COLOR_TYPE * TYPE_OFFSET));

    -- Object value types (can't be boxed into 48 bits)
    TAG_STRING   : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (STRING_TYPE   * TYPE_OFFSET));
    TAG_LIST     : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (LIST_TYPE     * TYPE_OFFSET));
    TAG_MAP      : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (MAP_TYPE      * TYPE_OFFSET));
    TAG_FUNCTION : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (FUNCTION_TYPE * TYPE_OFFSET));
    TAG_ERROR    : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (ERROR_TYPE    * TYPE_OFFSET));
    TAG_INDIRECT : constant Unsigned_64 := OBJECT_TYPE or (TYPE_MASK and (INDIRECT_TYPE * TYPE_OFFSET));

    FULL_TYPE_MASK : constant Unsigned_64 := OBJECT_MASK or QNAN or TYPE_MASK;

    ----------------------------------------------------------------------------

    function U64_To_F64 is new Ada.Unchecked_Conversion( Unsigned_64, Long_Float );
    function F64_To_U64 is new Ada.Unchecked_Conversion( Long_Float, Unsigned_64 );

    ----------------------------------------------------------------------------

    type Value is new Ada.Finalization.Controlled with
        record
            v : Unsigned_64 := TAG_NULL;
        end record;

    overriding
    procedure Adjust( this : in out Value );

    overriding
    procedure Finalize( this : in out Value );

    -- Returns the payload of the value as an address.
    function Get_Address( this : Value'Class ) return Address with Inline;

    -- Returns the type of the value, from 0 to 15. Types 8..15 are objects.
    function Get_Type( this : Value'Class ) return Integer
        is (Integer(Shift_Right( this.v and TYPE_MASK, PAYLOAD_SIZE ) or
                    Shift_Right( this.v and OBJECT_MASK, 63 - TYPE_SIZE )
           ))
        with Inline_Always;
    --pragma Postcondition( Get_Type'Result >= 0 );
    --pragma Postcondition( Get_Type'Result < 2**(OBJECT_SIZE + TYPE_SIZE) );

    function Is_Object( this : Value'Class ) return Boolean
        is ((this.v and OBJECT_TYPE) = OBJECT_TYPE)
        with Inline_Always;

    -- Sets the payload of the value to an address 'addr'. This only does bit
    -- twiddling, the type of object at the given address is irrelevant.
    procedure Set_Address( this : in out Value'Class; addr : Address ) with Inline;

    -- Sets the Object_Refs pointed to by this value. This may only be used if
    -- the object type is not changing. The value's tag must be an object type
    -- (e.g.: list, map, string); it will not be changed. Reference counting for
    -- the previous object and the new one will be handled.
    procedure Set_Object( this : in out Value'Class; obj : Address );

    procedure Free is new Ada.Unchecked_Deallocation( Value, A_Value );

    package Value_Accesses is new System.Address_To_Access_Conversions( Value'Class );

    Null_Value : constant Value := Value'(Controlled with v => TAG_NULL);

    Empty_Array : constant Value_Array(1..0) := (others => <>);

    ----------------------------------------------------------------------------

    type Counter is new Integer_32 with Volatile;

    -- This record contains the reference count for each type of value that is
    -- an object. It must be the first field
    type Object_Refs is
        record
            refs : aliased Counter := 0;
        end record;

    ----------------------------------------------------------------------------

    -- Reads a string written with Write_String without overflowing the stack
    -- for large strings.
    function Read_String( stream : access Root_Stream_Type'Class ) return Unbounded_String;

    -- Writes a string to a stream with only the length preceeding it, instead
    -- of the full bounds (save 4 bytes).
    procedure Write_String( stream : access Root_Stream_Type'Class; str : String );
    procedure Write_String( stream : access Root_Stream_Type'Class; str : Unbounded_String );

end Values;
