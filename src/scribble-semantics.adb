--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Assertions;                    use Ada.Assertions;
with Ada.Unchecked_Deallocation;
with Scribble.Parse_Errors;             use Scribble.Parse_Errors;
with Values;                            use Values;

package body Scribble.Semantics is

    function Create_Semantic_Analyzer( runtime : not null A_Scribble_Runtime ) return A_Semantic_Analyzer is
        this : constant A_Semantic_Analyzer := new Semantic_Analyzer;
    begin
        this.runtime := runtime;
        return this;
    end Create_Semantic_Analyzer;

    ----------------------------------------------------------------------------

    procedure Analyze( this  : not null access Semantic_Analyzer'Class;
                       value : not null A_Ast_Expression ) is
    begin
        -- re-initialize, in case of a previous exception
        this.func := null;
        this.exitFound.Clear;
        this.varAllowed := False;
        this.scope := null;

        value.Process( this );
    end Analyze;

    ----------------------------------------------------------------------------

    function Is_Exported_Function( this : not null access Semantic_Analyzer'Class;
                                   fid  : Function_Id ) return Boolean is
        proto : Prototype;
    begin
        this.runtime.Get_Prototype( fid, proto );
        return proto.exported;
    end Is_Exported_Function;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Map( this : access Semantic_Analyzer; node : A_Ast_Map ) is
    begin
        for i in 1..node.Length loop
            node.Get_Key( i ).Process( A_Ast_Processor(this) );
            if node.Get_Key( i ).Get_Type in Ast_Container_Type or else
               (node.Get_Key( i ).Get_Type = LITERAL and then not A_Ast_Literal(node.Get_Key( i )).Get_Value.Is_String)
            then
                Raise_Parse_Error( "Expected string", node.Get_Key( i ).Location );
            end if;
            node.Get_Value( i ).Process( A_Ast_Processor(this) );
        end loop;
    end Process_Map;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_List( this : access Semantic_Analyzer; node : A_Ast_List ) is
    begin
        for i in 1..node.Length loop
            node.Get_Element( i ).Process( A_Ast_Processor(this) );
        end loop;
    end Process_List;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Call( this : access Semantic_Analyzer; node : A_Ast_Function_Call ) is
    begin
        if node.Get_Call_Expression.Get_Type /= BUILTIN then
            node.Get_Call_Expression.Process( A_Ast_Processor(this) );

            -- Prune the function's expression. then we can test if it's an
            -- invalid value.
            if node.Prune /= null then
                -- this won't happen
                Assert( False, "Compiler bug: Function_Call can't be pruned" );
            end if;
            -- the following types cannot possibly produce a Function value.
            if node.Get_Call_Expression.Get_Type = LITERAL or else
               node.Get_Call_Expression.Get_Type in Ast_Container_Type or else
               node.Get_Call_Expression.Get_Type = BINARY_OP or else
               node.Get_Call_Expression.Get_Type = UNARY_OP
            then
                Raise_Parse_Error( "Expected function", node.Get_Call_Expression.Location );
            end if;
        end if;

        for i in 1..node.Arguments_Count loop
            if node.Get_Argument( i ).Get_Type = REFERENCE_OP then
                -- if the top level of the argument expression is a reference
                -- operator, then its use is valid. otherwise, when we process
                -- a reference we'll assume it's being used incorrectly.
                A_Ast_Reference(node.Get_Argument( i )).Mark_Valid;
            end if;
            node.Get_Argument( i ).Process( A_Ast_Processor(this) );
        end loop;
    end Process_Function_Call;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Member_Call( this : access Semantic_Analyzer; node : A_Ast_Member_Call ) is
    begin
        Assert( node.Get_Call_Expression.Get_Type = REFERENCE_OP );

        -- the call expression of a member call is always a Reference operator.
        -- mark that this is a valid use of the Reference operator.
        A_Ast_Reference(node.Get_Call_Expression).Mark_Valid;

        this.Process_Function_Call( A_Ast_Function_Call(node) );
    end Process_Member_Call;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_This( this : access Semantic_Analyzer; node : A_Ast_This ) is
    begin
        if this.func = null or else not this.func.Is_Member then
            Raise_Parse_Error( "Illegal use of 'this'", node.Location );
        end if;
    end Process_This;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Builtin( this : access Semantic_Analyzer; node : A_Ast_Builtin ) is
        pragma Unreferenced( this );
    begin
        Raise_Parse_Error( "Symbol '" & node.Get_Name & "' matches built-in function name", node.Location );
    end Process_Builtin;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Identifier( this : access Semantic_Analyzer; node : A_Ast_Identifier ) is
    begin
        if not this.runtime.Is_Namespace( node.Get_Namespace ) then
            if node.No_Namespace then
                if this.scope.Resolve_Identifier_Index( node ) = 0 then
                    -- this is either a reference/assignment to a member of the
                    -- anonymous namespace without it being registered, or its a
                    -- reference/assignment to an undefined local variable.
                    -- assume the anonymous namespace was not intended to be
                    -- registered, and report this as a failed local variable
                    -- reference.
                    Raise_Parse_Error( "Undefined variable '" & node.Get_Name & "'", node.Location );
                end if;
            else
                -- this is a reference/assignment to a member of a namespace
                -- that has not been registered.
                Raise_Parse_Error( "Member of undefined namespace '" & node.Get_Namespace & "'", node.Location );
            end if;
        end if;

        -- a reference cannot be created to a local constant because it's value
        -- could potentially be changed! e.g.: assignment, pass-by-reference
        if node.Is_Reference_Expression then
            if node.No_Namespace then
                if this.scope.Is_Identifier_Constant( node ) then
                    if not this.scope.Is_Defining_Identifier( node ) then
                        Raise_Parse_Error( "Assignment to local constant '" & node.Get_Name & "'", node.Location );
                    end if;
                end if;
            end if;
        end if;
    end Process_Identifier;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Membership( this : access Semantic_Analyzer; node : A_Ast_Membership ) is
    begin
        node.Get_Expression.Process( A_Ast_Processor(this) );

        -- Prune the membership's container expression. then we can test if it's
        -- an invalid type.
        if node.Prune /= null then
            -- this won't happen
            Assert( False, "Compiler bug: Membership can't be pruned" );
        end if;
        if node.Get_Expression.Get_Type = LIST then
            Raise_Parse_Error( "Illegal member access", node.Location );
        elsif node.Get_Expression.Get_Type = LITERAL and then
              not A_Ast_Literal(node.Get_Expression).Get_Value.Is_Map and then
              not A_Ast_Literal(node.Get_Expression).Get_Value.Is_Id
        then
            -- don't allow member access of any other value types; it makes no sense
            Raise_Parse_Error( "Illegal member access", node.Location );
        end if;
    end Process_Membership;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Name_Expr( this : access Semantic_Analyzer; node : A_Ast_Name_Expr ) is
    begin
        if not this.runtime.Is_Namespace( node.Get_Namespace ) then
            Raise_Parse_Error( "Member of undefined namespace '" & node.Get_Namespace & "'", node.Location );
        end if;

        if node.Get_Name.Get_Type in Ast_Container_Type or else
           (node.Get_Name.Get_Type = LITERAL and then not A_Ast_Literal(node.Get_Name).Get_Value.Is_String)
        then
            Raise_Parse_Error( "Expected string", node.Get_Name.Location );
        end if;
    end Process_Name_Expr;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Unary_Op( this : access Semantic_Analyzer; node : A_Ast_Unary_Op ) is
    begin
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Unary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Reference( this : access Semantic_Analyzer; node : A_Ast_Reference ) is
        n : A_Ast_Expression;
    begin
        if not node.Is_Valid then
            -- if this usage of the reference value wasn't previously marked as
            -- valid (e.g.: parsed at the top level of a function call argument
            -- expression, or auto-generated by the parser), then assume it's
            -- being used incorrectly.
            Raise_Parse_Error( "Invalid use of reference operator", node.Location );
        end if;

        -- verify the right hand expression follows this format:
        --
        -- Ex: @sym[expr1][expr2]
        --
        -- Ast_Symbol <-left- { Ast_Index } <-right Ast_Reference
        --
        --                          Ast_Reference
        --                                 \
        --                                Ast_Index -> expr2
        --                                   /
        --                              Ast_Index -> expr1
        --                                 /
        --                          Ast_Symbol

        n := node.Get_Right;                              -- won't be null
        loop
            if n.Can_Produce_Reference then
                -- the expression is still a valid reference expression.
                -- did we find the symbol at the end of it yet?
                exit when n.Get_Left = null;              -- yes, we did
                n := n.Get_Left;                          -- no, go deerer
            else
                -- error: not an index operation or symbol
                -- this doesn't mean the 'n' element is incorrect. it means the
                -- expression can't create a reference, so the use of the
                -- reference operator with it is wrong.
                Raise_Parse_Error( "Invalid use of reference operator", node.Location );
            end if;
        end loop;

        n.Process( A_Ast_Processor(this) );
    end Process_Reference;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Binary_Op( this : access Semantic_Analyzer; node : A_Ast_Binary_Op ) is
    begin
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Binary_Op;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Assign( this : access Semantic_Analyzer; node : A_Ast_Assign ) is
        n : A_Ast_Expression;
    begin
        node.Get_Right.Process( A_Ast_Processor(this) );
        node.Get_Left.Process( A_Ast_Processor(this) );

        -- if execution gets here, then the left-hand side of the assignment is
        -- valid, so we know where the symbol is:
        --
        -- Ast_Symbol { <-left- Ast_Index } <-right- Ast_Reference <-left- Ast_Assign
        --

        n := node.Get_Left.Get_Right;        -- left of Ast_Assign, then right of Ast_Reference
        loop
            pragma Assert( n.Can_Produce_Reference );
            exit when n.Get_Left = null;     -- found Ast_Symbol?
            n := n.Get_Left;                 -- left of Ast_Index
        end loop;

        pragma Assert( n.Can_Produce_Reference );
        node.Set_Target( A_Ast_Symbol(n) );
    end Process_Assign;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Index( this : access Semantic_Analyzer; node : A_Ast_Index ) is
    begin
        node.Get_Left.Process( A_Ast_Processor(this) );
        node.Get_Right.Process( A_Ast_Processor(this) );
    end Process_Index;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Conditional( this : access Semantic_Analyzer; node : A_Ast_Conditional ) is
    begin
        node.Get_Condition.Process( A_Ast_Processor(this) );
        node.Get_True_Case.Process( A_Ast_Processor(this) );
        node.Get_False_Case.Process( A_Ast_Processor(this) );
    end Process_Conditional;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Function_Definition( this : access Semantic_Analyzer; node : A_Ast_Function_Definition ) is
        argDefaultRequired : Boolean := False;
    begin
        if this.func /= null then
            -- 'node' is a nested function definition. use the child analyzer to
            -- process it.
            if this.child = null then
                this.child := Create_Semantic_Analyzer( this.runtime );
            end if;
            this.child.Analyze( A_Ast_Expression(node) );   -- may raise Parse_Error
            return;
        end if;

        -- verify parameters (names and default values)
        for i in 1..node.Parameters_Count loop

            -- verify parameter name doesn't match any exported functions
            if this.Is_Exported_Function( Fid_Hash( node.Get_Parameter( i ).Get_Name ) ) then
                Raise_Parse_Error( "Parameter '" & node.Get_Parameter( i ).Get_Name &
                                   "' matches built-in function name",
                                   node.Get_Parameter( i ).Location );
            end if;

            -- verify no duplicate parameter names
            if not node.Get_Body.Define_Parameter( node.Get_Parameter( i ) ) then
                Raise_Parse_Error( "Duplicate parameter name '" &
                                   node.Get_Parameter( i ).Get_Name & "'",
                                   node.Get_Parameter( i ).Location );
            end if;

            -- verify proper default value
            if node.Get_Parameter_Default( i ) /= null then
                -- all remaining parameters are required to have default values
                argDefaultRequired := True;

                if node.Get_Parameter_Default( i ).Get_Type /= LITERAL then
                    Raise_Parse_Error( "Default for '" & node.Get_Parameter( i ).Get_Name &
                                       "' must be literal value",
                                       node.Get_Parameter( i ).Location );
                end if;
            elsif argDefaultRequired then
                -- a default value was specified for a previous parameter but not for this one
                Raise_Parse_Error( "Default value required for parameter '" &
                                   node.Get_Parameter( i ).Get_Name & "'",
                                   node.Get_Parameter( i ).Location );
            end if;
        end loop;

        if node.Get_Internal_Name'Length = 0 then
            -- process the Scribble implementation body
            this.func := node;
            this.func.Get_Body.Process( A_Ast_Processor(this) );
            this.func := null;

            -- verify that everything was cleaned up
            pragma Assert( this.exitFound.Is_Empty );
            pragma Assert( this.varAllowed = False );
            pragma Assert( this.scope = null );
        end if;
    end Process_Function_Definition;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Block( this : access Semantic_Analyzer; node : A_Ast_Block ) is
        parent    : constant A_Ast_Block := this.scope;
        statement : A_Ast_Statement;
    begin
        -- node MUST be a child scope of the current scope
        -- if it's not, the AST is inconsistent and there is a bug in the compiler
        pragma Assert( node.Get_Parent = parent );

        this.scope := node;                         -- push scope
        this.varAllowed := True;                    -- allow variable definitions

        statement := node.Get_First;
        while statement /= null loop
            this.varAllowed := this.varAllowed and (statement.Get_Type = VAR_STATEMENT);
            statement.Process( A_Ast_Processor(this) );
            statement := statement.Get_Next;
        end loop;

        this.varAllowed := False;
        this.scope := parent;                       -- pop scope
    end Process_Block;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Exit( this : access Semantic_Analyzer; node : A_Ast_Exit ) is
    begin
        if this.exitFound.Is_Empty then
            Raise_Parse_Error( "Found exit without loop", node.Location );
        end if;
        this.exitFound.Replace_Element( this.exitFound.Last, True );
    end Process_Exit;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Expression_Statement( this : access Semantic_Analyzer; node : A_Ast_Expression_Statement ) is
    begin
        node.Get_Expression.Process( A_Ast_Processor(this) );
    end Process_Expression_Statement;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_If( this : access Semantic_Analyzer; node : A_Ast_If ) is
    begin
        for i in 1..node.Case_Count loop
            node.Get_Case_Condition( i ).Process( A_Ast_Processor(this) );
            node.Get_Case_Block( i ).Process( A_Ast_Processor(this) );
        end loop;
        if node.Get_Else_Block /= null then
             node.Get_Else_Block.Process( A_Ast_Processor(this) );
        end if;
    end Process_If;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Loop( this : access Semantic_Analyzer; node : A_Ast_Loop ) is
    begin
        this.exitFound.Append( False );
        node.Get_Body.Process( A_Ast_Processor(this) );
        if this.exitFound.Last_Element = False then
            -- no exit or return statement was found
            Raise_Parse_Error( "Infinite loop detected", node.Location );
        end if;
        this.exitFound.Delete_Last;
    end Process_Loop;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Return( this : access Semantic_Analyzer; node : A_Ast_Return ) is
    begin
        node.Get_Expression.Process( A_Ast_Processor(this) );
        for i of this.exitFound loop
            i := True;
        end loop;
    end Process_Return;

    ----------------------------------------------------------------------------

    overriding
    procedure Process_Var( this : access Semantic_Analyzer; node : A_Ast_Var ) is
        s : A_Ast_Block;
    begin
        if not this.varAllowed then
            Raise_Parse_Error( "Late variable declaration", node.Get_Variable_Location );
        end if;

        if this.Is_Exported_Function( Fid_Hash( node.Get_Variable.Get_Name ) ) then
            Raise_Parse_Error( "Variable name '" & node.Get_Variable.Get_Name &
                               "' collides with built-in function name",
                               node.Get_Variable_Location );
        end if;

        if not this.scope.Define_Variable( node.Get_Variable, node.Is_Constant, node.Location ) then
            Raise_Parse_Error( "Duplicate variable definition", node.Get_Variable_Location );
        end if;

        -- check that the definition of this variable hasn't maxed-out the local
        -- variable space in this scope or any of its ancestors.
        s := this.scope;
        while s /= null loop
            if s.Reserved_Variables > 256 then
                Raise_Parse_Error( "Too many local variables", node.Get_Variable_Location );
            end if;
            s := s.Get_Parent;
        end loop;

        node.Get_Expression.Process( A_Ast_Processor(this) );
    end Process_Var;

    ----------------------------------------------------------------------------

    procedure Delete( this : in out A_Semantic_Analyzer ) is
        procedure Free is new Ada.Unchecked_Deallocation( Semantic_Analyzer'Class, A_Semantic_Analyzer );
    begin
        if this /= null then
            Delete( this.child );
            Free( this );
        end if;
    end Delete;

end Scribble.Semantics;
