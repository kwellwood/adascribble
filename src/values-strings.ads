--
-- Copyright (c) 2015-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package Values.Strings is

    -- Creates and returns a new string Value.
    function Create( str : String ) return Value;

    -- Creates and returns a new string Value.
    function Create( str : Unbounded_String ) return Value;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    type String_Value is new Value with private;

    -- Returns a String_Value pointing to the data of 'this'. The data is never
    -- copied.
    function As_String( this : Value'Class ) return String_Value;

    -- Returns a new string with 'text' appended to this string.
    function Append( this : String_Value'Class; text : String_Value'Class ) return Value with Inline;
    pragma Precondition( this.Is_String );
    pragma Precondition( text.Is_String );
    pragma Postcondition( Append'Result.Is_String );

    -- Returns true if the string begins with 'text'. Returns False if 'text'
    -- is an empty string.
    function Begins_With( this : String_Value'Class; text : String_Value'Class ) return Boolean;
    pragma Precondition( this.Is_String );
    pragma Precondition( text.Is_String );

    -- Returns true if the string ends with 'text'. Returns False if 'text' is
    -- an empty string.
    function Ends_With( this : String_Value'Class; text : String_Value'Class ) return Boolean;
    pragma Precondition( this.Is_String );
    pragma Precondition( text.Is_String );

    -- Returns the character at index 'index' or ASCII.NUL if 'index' is invalid.
    function Get( this : String_Value'Class; index : Integer ) return Character with Inline;

    -- Returns up 'count' characters from the head of the string.
    function Head( this : String_Value'Class; count : Natural ) return Value with Inline;
    pragma Precondition( this.Is_String );

    -- Searches for 'pattern' in the string, starting at 'from', and returns the
    -- index of the first character, or 0 if the substring is not found.
    function IndexOf( this    : String_Value'Class;
                      pattern : String;
                      from    : Positive := 1 ) return Natural;
    pragma Precondition( this.Is_String );

    -- Returns a new string created by inserting 'text' into this string at
    -- index 'start'. A 'start' value of 1 will prepend 'text' and values
    -- greater than length will append 'text'.
    function Insert( this  : String_Value'Class;
                     text  : String_Value'Class;
                     start : Positive ) return Value;
    pragma Precondition( this.Is_String );
    pragma Precondition( text.Is_String );
    pragma Postcondition( Insert'Result.Is_String );

    -- Returns the length of the string in characters.
    function Length( this : String_Value'Class ) return Natural with Inline;
    pragma Precondition( this.Is_String );

    -- Returns a new string with leading whitespace trimmed off.
    function Ltrim( this : String_Value'Class ) return Value with Inline;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Ltrim'Result.Is_String );

    -- Returns a new string with 'text' prepended to this string.
    function Prepend( this : String_Value'Class; text : String_Value'Class ) return Value with Inline;
    pragma Precondition( this.Is_String );
    pragma Precondition( text.Is_String );
    pragma Postcondition( Prepend'Result.Is_String );

    -- Creates a new string from this with 'count' characters removed, starting
    -- at index 'start'.
    function Remove( this : String_Value'Class; start : Positive; count : Natural ) return Value;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Remove'Result.Is_String );

    -- Returns a new string with trailing whitespace trimmed off.
    function Rtrim( this : String_Value'Class ) return Value with Inline;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Rtrim'Result.Is_String );

    -- Returns a substring containing 'count' characters starting at index
    -- 'count'. If the substring would go off the end of the string, the result
    -- will not be padded.
    function Slice( this  : String_Value'Class;
                    from  : Positive;
                    count : Natural ) return Value;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Slice'Result.Is_String );

    -- Returns up 'count' characters from the tail of the string.
    function Tail( this : String_Value'Class; count : Natural ) return Value with Inline;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Tail'Result.Is_String );

    -- Returns this as an Ada String.
    function To_String( this : String_Value'Class ) return String;
    pragma Precondition( this.Is_String );

     -- Returns this as an Ada Unbounded_String.
    function To_Unbounded_String( this : String_Value'Class ) return Unbounded_String;
    pragma Precondition( this.Is_String );

    -- Returns a new string with leading and trailing whitespace trimmed off.
    function Trim( this : String_Value'Class ) return Value;
    pragma Precondition( this.Is_String );
    pragma Postcondition( Trim'Result.Is_String );

    function Valid( this : String_Value'Class ) return Boolean is (this.Is_String);

    function "&"( l : String_Value'Class; r : String_Value'Class ) return Value with Inline;
    function "&"( l : String_Value'Class; r : String             ) return Value with Inline;
    function "&"( l : String_Value'Class; r : Unbounded_String   ) return Value with Inline;
    function "&"( l : String;             r : String_Value'Class ) return Value with Inline;
    function "&"( l : Unbounded_String;   r : String_Value'Class ) return Value with Inline;

    function  "="( l : String_Value'Class; r : String             ) return Boolean with Inline;
    function  "="( l : String_Value'Class; r : Unbounded_String   ) return Boolean with Inline;
    function  "="( l : String;             r : String_Value'Class ) return Boolean with Inline;
    function  "="( l : Unbounded_String;   r : String_Value'Class ) return Boolean with Inline;

    function  "<"( l : String_Value'Class; r : String             ) return Boolean with Inline;
    function  "<"( l : String_Value'Class; r : Unbounded_String   ) return Boolean with Inline;
    function  "<"( l : String;             r : String_Value'Class ) return Boolean with Inline;
    function  "<"( l : Unbounded_String;   r : String_Value'Class ) return Boolean with Inline;

    function  ">"( l : String_Value'Class; r : String             ) return Boolean with Inline;
    function  ">"( l : String_Value'Class; r : Unbounded_String   ) return Boolean with Inline;
    function  ">"( l : String;             r : String_Value'Class ) return Boolean with Inline;
    function  ">"( l : Unbounded_String;   r : String_Value'Class ) return Boolean with Inline;

    function "<="( l : String_Value'Class; r : String             ) return Boolean with Inline;
    function "<="( l : String_Value'Class; r : Unbounded_String   ) return Boolean with Inline;
    function "<="( l : String;             r : String_Value'Class ) return Boolean with Inline;
    function "<="( l : Unbounded_String;   r : String_Value'Class ) return Boolean with Inline;

    function ">="( l : String_Value'Class; r : String             ) return Boolean with Inline;
    function ">="( l : String_Value'Class; r : Unbounded_String   ) return Boolean with Inline;
    function ">="( l : String;             r : String_Value'Class ) return Boolean with Inline;
    function ">="( l : Unbounded_String;   r : String_Value'Class ) return Boolean with Inline;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Compare_Strings( left, right : Value'Class ) return Integer;
    pragma Precondition( left.Is_String );
    pragma Precondition( right.Is_String );

    procedure Delete_String( obj : Address );

    function Image_String( this : Value'Class ) return String;
    pragma Precondition( this.Is_String );

    procedure Read_String( stream : access Root_Stream_Type'Class;
                           this   : in out Value'Class );
    pragma Precondition( this.Is_String );

    procedure Write_String( stream : access Root_Stream_Type'Class;
                            this   : Value'Class );
    pragma Precondition( this.Is_String );

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    -- Escapes special characters in a string with backslash notation.
    -- Tab => \t
    -- LF  => \n
    -- CR  => \r
    -- \   => \\
    -- "   => \"
    function Escape( str : String ) return String;

private

    type String_Data;
    type A_String_Data is access all String_Data;
    pragma No_Strict_Aliasing( A_String_Data );

    type String_Value is new Value with
        record
            data : A_String_Data := null;
        end record;

end Values.Strings;
