--
-- Copyright (c) 2015 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with System.Address_To_Access_Conversions;

package body Values.Errors is

    type Error_Data(msgLength : Integer) is limited
        record
            obj  : Object_Refs;
            code : Error_Code;
            msg  : String(1..msgLength);
        end record;
    for Error_Data use
        record
            obj at 0 range 0..31;
        end record;

    ----------------------------------------------------------------------------

    package Accesses is new System.Address_To_Access_Conversions( Error_Data );
    use Accesses;

    --==========================================================================

    function Create( code : Error_Code; msg : String ) return Value is
        this : constant Object_Pointer := new Error_Data(msg'Length);
        val  : Value := Value'(Controlled with v => TAG_ERROR);
    begin
        this.code := code;
        this.msg := msg;
        val.Set_Object( To_Address( this ) );
        return val;
    end Create;

    --==========================================================================

    function As_Error( this : Value'Class ) return Error_Value is
        (Error_Value'(Value'(Value(this)) with data => A_Error_Data(To_Pointer( this.Get_Address ))));

    ----------------------------------------------------------------------------

    function Get_Code( this : Error_Value'Class ) return Error_Code is (this.data.code);

    ----------------------------------------------------------------------------

    function Get_Message( this : Error_Value'Class ) return String is (this.data.msg);

    --==========================================================================

    function Compare_Errors( left, right : Value'Class ) return Integer is
        leftData  : constant Object_Pointer := To_Pointer( left.Get_Address );
        rightData : constant Object_Pointer := To_Pointer( right.Get_Address );
    begin
        if leftData.code = rightData.code then
            return 0;
        elsif leftData.code > rightData.code then
            return 1;
        end if;
        return -1;
    end Compare_Errors;

    ----------------------------------------------------------------------------

    procedure Delete_Error( obj : Address ) is
        procedure Free is new Ada.Unchecked_Deallocation( Error_Data, Object_Pointer );
        data : Object_Pointer := To_Pointer( obj );
    begin
        Free( data );
    end Delete_Error;

    ----------------------------------------------------------------------------

    function Image_Error( this : Value'Class ) return String is
        data : constant Object_Pointer := To_Pointer( this.Get_Address );
    begin
        return "[Error " & Error_Code'Image( data.code ) & ": " & data.msg & ']';
    end Image_Error;

    ----------------------------------------------------------------------------

    procedure Read_Error( stream : access Root_Stream_Type'Class;
                          this   : in out Value'Class ) is
        code : Error_Code;
        msg  : Unbounded_String;
        data : Object_Pointer;
    begin
        code := Error_Code'Input( stream );
        msg := Read_String( stream );
        data := new Error_Data(Length( msg ));
        data.code := code;
        data.msg := To_String( msg );
        this.Set_Object( To_Address( data ) );
    end Read_Error;

    ----------------------------------------------------------------------------

    procedure Write_Error( stream : access Root_Stream_Type'Class;
                           this   : Value'Class ) is
        data : constant Object_Pointer := To_Pointer( this.Get_Address );
    begin
        Error_Code'Output( stream, data.code );
        Write_String( stream, data.msg );
    end Write_Error;

end Values.Errors;
