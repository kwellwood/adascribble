Scribble Scripting
==================
Scribble is a lightweight scripting language with a compiler and runtime written in Ada. It provides
a simple dynamically typed scripting language with first-class functions. Scribble source code is
compiled into an intermediate byte-code format and then executed very quickly by a stack-based virtual
machine. 

Below is an example program written in Scribble, and a definition of the language's grammar.
Updated February 11, 2019.

EXAMPLE
=======

    //
    // Solution to the Eight Queens Problem
    //
    // Adapted from Edsger Dijkstra's algorithm
    //
    function generate(x    := [],
                      n    := 1,
                      col  := [true, true, true, true, true, true, true, true],
                      up   := [true, true, true, true, true, true, true, true,
                               true, true, true, true, true, true, true, true],
                      down := [true, true, true, true, true, true, true, true,
                               true, true, true, true, true, true, true, true])
        for h is 1 to 8 loop
            // check if square N,H is free
            if col[h] and up[8+n-h] and down[n+h] then
                // place the queen at N,H
                x[n] := h;
                col[h] := false;
                up[8+n-h] := false;
                down[n+h] := false;
    
                if n = 8 then
                    print(image(x));            // print this solution
                else
                    self(x, n+1, col, up, down);  // add another queen
                end if;
    
                // remove the queen at N,H
                down[n+h] := true;
                up[8+n-h] := true;
                col[h] := true;
            end if;
        end loop;
    end function;
    
    generate();

GRAMMAR
=======

VALUES
------

    Value         ::= value-token
                      | ('-' number-value-token)
                      | List_Value
                      | Map_Value
                      | Anon_Function_Definition
                      | Named_Function_Definition
      List_Value  ::= '[' [Expression_List<literal=True>] ']'
      Map_Value   ::= '{' [Map_Pair {(',' | end-of-line) Map_Pair}] '}'
        Map_Pair  ::= Named_Function_Definition
                      | Named_Member_Definition
                      | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal=True>))
          Map_Key ::= string-value-token | symbol-token

EXPRESSIONS
-----------

    Expression_List<literal> ::= (Anon_Function_Definition | Expression<literal>) [(',' | end-of-line) Expression_List<literal>]

    Expression<literal> ::= Assign_Term<literal>

    Assign_Term<literal=False> ::= Ternary_Term<literal> [Assign_Op (Anon_Function_Definition | Assign_Term)]
    Assign_Term<literal=True>  ::= Ternary_Term<literal>
      Assign_Op ::= ':=' | '+=' | '-=' | '*=' | '/=' | '&='

    Ternary_Term<literal> ::= Binary_Term<literal> ['?' Ternary_Term<literal> ':' Ternary_Term<literal>]

    Binary_Term<literal> ::= Lterm<literal> [Binary_Op Binary_Term<literal>]
      Binary_Op ::= 'and' | 'or' | 'xor' | 'in' | '=' | '>' | '>=' | '<' |
                    '<=' | '!=' | '+' | '-' | '&' | '*' | '/' | '%' | '^'

    Lterm<literal> ::= Unary_Op<literal> Lterm<literal>
    Lterm<literal> ::= Rterm<literal>
      Unary_Op<literal=False> ::= '!' | '-' | '@'
      Unary_Op<literal=True>  ::= '!' | '-'

    Rterm<literal=False> ::= Operand<literal> {Index | Function_Call | Membership}
    Rterm<literal=True>  ::= Operand<literal>

    Index ::= '[' Expression<literal=False> ']'

    Function_Call ::= '(' [Expression_List<literal=False>] ')'

    Membership ::= '.' Identifier

    Operand<literal=False> ::= Literal<literal> | Identifier_Expression | ('(' Expression<literal> ')')
    Operand<literal=True>  ::= Literal<literal> | ('(' Expression<literal> ')')

    Literal<literal=False>  ::= value-token | 'self' | 'this' | List_Literal<literal> | Map_Literal<literal>
    Literal<literal=True>   ::= value-token | List_Literal<literal> | Map_Literal<literal>
      List_Literal<literal> ::= '[' [Expression_List<literal>] ']'
      Map_Literal<literal>  ::= '{' [Map_Pair<literal> {(',' | end-of-line) Map_Pair<literal>}] '}'
        Map_Pair<literal>   ::= Named_Function_Definition
                                | Named_Member_Definition
                                | (Map_Key ':' (Anon_Function_Definition | Anon_Member_Definition | Expression<literal>))
          Map_Key           ::= string-value-token | symbol-token

    Identifier_Expression ::= Identifier ['::' (Identifier | Name_Expression)]
      Name_Expression     ::= '[' Expression<literal=False> ']'

    Identifier     ::= symbol-token
      symbol-token ::= letter {letter | digit | '_'}

OPERATOR PRECEDENCE
--------------------

    Membership/Index  . []             (greatest)
    Unary             ! - @
    Exponentiation    ^
    Multiplication    * / %
    Addition          + - &
    Comparison        = > >= < <= !=
    Logical           and or xor in    (least)

FUNCTION DEFINITIONS
--------------------

    Anon_Function_Definition  ::= 'function' Function_Definition
    Anon_Member_Definition    ::= 'member' ['function'] Function_Definition
    Named_Function_Definition ::= 'function' Identifier Function_Definition
    Named_Member_Definition   ::= 'member' ['function'] Identifier Function_Definition
      Function_Definition     ::= Parameters_Prototype Function_Implementation
      Parameters_Prototype    ::= '(' [Parameter {',' Parameter}] ')'
      Parameter               ::= Identifier [':' Value_Type] [':=' Expression<literal=True>]
      Value_Type              ::= 'map' | 'boolean' | 'id' | 'list' | 'number' | 'string'
      Function_Implementation ::= (':' Identifier) | (Statement_List 'end' 'function')

STATEMENTS
----------

    Statement_List ::= {Statement}

    Statement ::= {';'} (
                  Block
                  | Exit
                  | For
                  | If
                  | Loop
                  | Return
                  | Var
                  | While
                  | Yield
                  | Expression_Statement
                  ) {';'}

    Block ::= 'block' Statement_List 'end' 'block' ';'

    Exit ::= 'exit' ['when' Expression<literal=False>] ';'

    For         ::= 'for' Identifier (For_Range | For_Each) Loop
      For_Range ::= 'is' Expression<literal=False> 'to' Expression<literal=False> ['step' Expression<literal=False>]
      For_Each  ::= 'of' Expression<literal=False>

    If ::= 'if' Expression<literal=False> 'then' Statement_List
           {'elsif' Expression<literal=False> 'then' Statement_List}
           ['else' Statement_List]
           'end' 'if' ';'

    Loop ::= 'loop' Statement_List 'end' 'loop' ';'

    Return ::= 'return' [Expression<literal=False> | Anon_Function_Definition] ';'

    Var ::= 'local' Identifier [':=' (Expression<literal=False> | Anon_Function_Definition)] ';'
    Var ::= 'constant' Identifier ':=' (Expression<literal=False> | Anon_Function_Definition) ';'
    Var ::= Named_Function_Definition ';'

    While ::= 'while' Expression<literal=False> Loop

    Yield ::= 'yield' ';'

    Expression_Statement ::= Expression<literal=False> ';'
